﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class Orden_CompraController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/Orden_Compra
        public IQueryable<Orden_Compra> GetOrden_Compra()
        {
            return db.Orden_Compra.Include(b => b.Proveedor).Include(b=> b.Empleado);
        }

        // GET: api/Orden_Compra/5
        [ResponseType(typeof(Orden_Compra))]
        public async Task<IHttpActionResult> GetOrden_Compra(int id)
        {
            Orden_Compra orden_Compra = await db.Orden_Compra.FindAsync(id);
            if (orden_Compra == null)
            {
                return NotFound();
            }

            return Ok(orden_Compra);
        }

        // PUT: api/Orden_Compra/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutOrden_Compra(int id, Orden_Compra orden_Compra)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != orden_Compra.Id)
            {
                return BadRequest();
            }

            db.Entry(orden_Compra).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Orden_CompraExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Orden_Compra
        [ResponseType(typeof(Orden_Compra))]
        public async Task<IHttpActionResult> PostOrden_Compra(Orden_Compra orden_Compra)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Orden_Compra.Add(orden_Compra);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = orden_Compra.Id }, orden_Compra);
        }

        // DELETE: api/Orden_Compra/5
        [ResponseType(typeof(Orden_Compra))]
        public async Task<IHttpActionResult> DeleteOrden_Compra(int id)
        {
            Orden_Compra orden_Compra = await db.Orden_Compra.FindAsync(id);
            if (orden_Compra == null)
            {
                return NotFound();
            }

            db.Orden_Compra.Remove(orden_Compra);
            await db.SaveChangesAsync();

            return Ok(orden_Compra);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Orden_CompraExists(int id)
        {
            return db.Orden_Compra.Count(e => e.Id == id) > 0;
        }
    }
}