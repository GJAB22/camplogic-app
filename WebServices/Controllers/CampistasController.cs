﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class CampistasController : ApiController
    {
        private CampLContext db = new CampLContext();// ejemplo de 1)
        

        //ejemplo
        public IQueryable<Activohistorial> Get()
        {

            return db.Activohistorial;

        }


        // GET: api/Campistas
        public IQueryable<Campistas> GetCampistas()
        {
            return db.Campistas;
        }

        // GET: api/Campistas/5
        [ResponseType(typeof(Campistas))]
        public async Task<IHttpActionResult> GetCampistas(int id)
        {
            Campistas campistas = await db.Campistas.FindAsync(id);
            if (campistas == null)
            {
                return NotFound();
            }

            return Ok(campistas);
        }

        // PUT: api/Campistas/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCampistas(int id, Campistas campistas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != campistas.Id)
            {
                return BadRequest();
            }

            db.Entry(campistas).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CampistasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Campistas
        [ResponseType(typeof(Campistas))]
        public async Task<IHttpActionResult> PostCampistas(Campistas campistas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Campistas.Add(campistas);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = campistas.Id }, campistas);
        }

        // DELETE: api/Campistas/5
        [ResponseType(typeof(Campistas))]
        public async Task<IHttpActionResult> DeleteCampistas(int id)
        {
            Campistas campistas = await db.Campistas.FindAsync(id);
            if (campistas == null)
            {
                return NotFound();
            }

            db.Campistas.Remove(campistas);
            await db.SaveChangesAsync();

            return Ok(campistas);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CampistasExists(int id)
        {
            return db.Campistas.Count(e => e.Id == id) > 0;
        }
    }
}