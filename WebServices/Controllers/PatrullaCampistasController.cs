﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class PatrullaCampistasController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/PatrullaCampistas
        public IQueryable<PatrullaCampista> GetPatrullaCampistas()
        {
            return db.PatrullaCampistas.Include(b=>b.Campista).Include(b=>b.Patrulla);
        }

        // GET: api/PatrullaCampistas/5
        [ResponseType(typeof(PatrullaCampista))]
        public async Task<IHttpActionResult> GetPatrullaCampista(int id)
        {
            PatrullaCampista patrullaCampista = await db.PatrullaCampistas.FindAsync(id);
            if (patrullaCampista == null)
            {
                return NotFound();
            }

            return Ok(patrullaCampista);
        }

        // PUT: api/PatrullaCampistas/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPatrullaCampista(int id, PatrullaCampista patrullaCampista)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != patrullaCampista.Id)
            {
                return BadRequest();
            }

            db.Entry(patrullaCampista).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PatrullaCampistaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PatrullaCampistas
        [ResponseType(typeof(PatrullaCampista))]
        public async Task<IHttpActionResult> PostPatrullaCampista(PatrullaCampista patrullaCampista)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PatrullaCampistas.Add(patrullaCampista);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = patrullaCampista.Id }, patrullaCampista);
        }

        // DELETE: api/PatrullaCampistas/5
        [ResponseType(typeof(PatrullaCampista))]
        public async Task<IHttpActionResult> DeletePatrullaCampista(int id)
        {
            PatrullaCampista patrullaCampista = await db.PatrullaCampistas.FindAsync(id);
            if (patrullaCampista == null)
            {
                return NotFound();
            }

            db.PatrullaCampistas.Remove(patrullaCampista);
            await db.SaveChangesAsync();

            return Ok(patrullaCampista);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PatrullaCampistaExists(int id)
        {
            return db.PatrullaCampistas.Count(e => e.Id == id) > 0;
        }
    }
}