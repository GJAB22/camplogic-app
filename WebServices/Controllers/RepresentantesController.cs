﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class RepresentantesController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/Representantes
        public IQueryable<Representante> GetRepresentantes()
        {
            return db.Representantes;
        }

        // GET: api/Representantes/5
        [ResponseType(typeof(Representante))]
        public async Task<IHttpActionResult> GetRepresentante(int id)
        {
            Representante representante = await db.Representantes.FindAsync(id);
            if (representante == null)
            {
                return NotFound();
            }

            return Ok(representante);
        }

        [Route("api/Representantes/{cedula}")]
        [ResponseType(typeof(Representante))]
        public async Task<IHttpActionResult> GetRepresentanteCorreo(string cedula)
        {
            Representante representante = await db.Representantes.FirstOrDefaultAsync(e => e.Cedula == cedula);
            if (representante == null)
            {
                return NotFound();
            }

            return Ok(representante);
        }

        // PUT: api/Representantes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutRepresentante(int id, Representante representante)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != representante.Id)
            {
                return BadRequest();
            }

            db.Entry(representante).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RepresentanteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Representantes
        [ResponseType(typeof(Representante))]
        public async Task<IHttpActionResult> PostRepresentante(Representante representante)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Representantes.Add(representante);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = representante.Id }, representante);
        }

        // DELETE: api/Representantes/5
        [ResponseType(typeof(Representante))]
        public async Task<IHttpActionResult> DeleteRepresentante(int id)
        {
            Representante representante = await db.Representantes.FindAsync(id);
            if (representante == null)
            {
                return NotFound();
            }

            db.Representantes.Remove(representante);
            await db.SaveChangesAsync();

            return Ok(representante);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RepresentanteExists(int id)
        {
            return db.Representantes.Count(e => e.Id == id) > 0;
        }
    }
}