﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class NoticiasController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/Noticias
        public IQueryable<Noticias> GetNoticias()
        {
            return db.Noticias.Include(b => b.Empleado);
        }

        // GET: api/Noticias/5
        [ResponseType(typeof(Noticias))]
        public async Task<IHttpActionResult> GetNoticias(int id)
        {
            Noticias noticias = await db.Noticias.FindAsync(id);
            if (noticias == null)
            {
                return NotFound();
            }

            return Ok(noticias);
        }

        // PUT: api/Noticias/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutNoticias(int id, Noticias noticias)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != noticias.Id)
            {
                return BadRequest();
            }

            db.Entry(noticias).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NoticiasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Noticias
        [ResponseType(typeof(Noticias))]
        public async Task<IHttpActionResult> PostNoticias(Noticias noticias)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Noticias.Add(noticias);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = noticias.Id }, noticias);
        }

        // DELETE: api/Noticias/5
        [ResponseType(typeof(Noticias))]
        public async Task<IHttpActionResult> DeleteNoticias(int id)
        {
            Noticias noticias = await db.Noticias.FindAsync(id);
            if (noticias == null)
            {
                return NotFound();
            }

            db.Noticias.Remove(noticias);
            await db.SaveChangesAsync();

            return Ok(noticias);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NoticiasExists(int id)
        {
            return db.Noticias.Count(e => e.Id == id) > 0;
        }
    }
}