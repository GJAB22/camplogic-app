﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class CotizacionesController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/Cotizaciones
        public IQueryable<Cotizacion> GetCotizacions()
        {
            return db.Cotizacions;
        }

        // GET: api/Cotizaciones/5
        [ResponseType(typeof(Cotizacion))]
        public async Task<IHttpActionResult> GetCotizacion(int id)
        {
            Cotizacion cotizacion = await db.Cotizacions.FindAsync(id);
            if (cotizacion == null)
            {
                return NotFound();
            }

            return Ok(cotizacion);
        }

        // PUT: api/Cotizaciones/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCotizacion(int id, Cotizacion cotizacion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cotizacion.Id)
            {
                return BadRequest();
            }

            db.Entry(cotizacion).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CotizacionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Cotizaciones
        [ResponseType(typeof(Cotizacion))]
        public async Task<IHttpActionResult> PostCotizacion(Cotizacion cotizacion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Cotizacions.Add(cotizacion);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = cotizacion.Id }, cotizacion);
        }

        // DELETE: api/Cotizaciones/5
        [ResponseType(typeof(Cotizacion))]
        public async Task<IHttpActionResult> DeleteCotizacion(int id)
        {
            Cotizacion cotizacion = await db.Cotizacions.FindAsync(id);
            if (cotizacion == null)
            {
                return NotFound();
            }

            db.Cotizacions.Remove(cotizacion);
            await db.SaveChangesAsync();

            return Ok(cotizacion);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CotizacionExists(int id)
        {
            return db.Cotizacions.Count(e => e.Id == id) > 0;
        }
    }
}