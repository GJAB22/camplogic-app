﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class ActividadProductoesController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/ActividadProductoes
        public IQueryable<ActividadProducto> GetActividadProductoes()
        {
            return db.ActividadProductoes.Include(b=>b.Actividad).Include(b=>b.Producto).Include(b => b.Producto.Ubicacion);
        }

        // GET: api/ActividadProductoes/5
        [ResponseType(typeof(ActividadProducto))]
        public async Task<IHttpActionResult> GetActividadProducto(int id)
        {
            ActividadProducto actividadProducto = await db.ActividadProductoes.FindAsync(id);
            if (actividadProducto == null)
            {
                return NotFound();
            }

            return Ok(actividadProducto);
        }

        // PUT: api/ActividadProductoes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutActividadProducto(int id, ActividadProducto actividadProducto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != actividadProducto.Id)
            {
                return BadRequest();
            }

            db.Entry(actividadProducto).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActividadProductoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ActividadProductoes
        [ResponseType(typeof(ActividadProducto))]
        public async Task<IHttpActionResult> PostActividadProducto(ActividadProducto actividadProducto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ActividadProductoes.Add(actividadProducto);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = actividadProducto.Id }, actividadProducto);
        }

        // DELETE: api/ActividadProductoes/5
        [ResponseType(typeof(ActividadProducto))]
        public async Task<IHttpActionResult> DeleteActividadProducto(int id)
        {
            ActividadProducto actividadProducto = await db.ActividadProductoes.FindAsync(id);
            if (actividadProducto == null)
            {
                return NotFound();
            }

            db.ActividadProductoes.Remove(actividadProducto);
            await db.SaveChangesAsync();

            return Ok(actividadProducto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ActividadProductoExists(int id)
        {
            return db.ActividadProductoes.Count(e => e.Id == id) > 0;
        }
    }
}