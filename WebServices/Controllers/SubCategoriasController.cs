﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class SubCategoriasController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/SubCategorias
        public IQueryable<SubCategoria> GetSubCategorias()
        {
            return db.SubCategorias.Include(b => b.Categoria);
        }

        // GET: api/SubCategorias/5
        [ResponseType(typeof(SubCategoria))]
        public async Task<IHttpActionResult> GetSubCategoria(int id)
        {
            SubCategoria subCategoria = await db.SubCategorias.FindAsync(id);
            if (subCategoria == null)
            {
                return NotFound();
            }

            return Ok(subCategoria);
        }

        // PUT: api/SubCategorias/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSubCategoria(int id, SubCategoria subCategoria)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != subCategoria.Id)
            {
                return BadRequest();
            }

            db.Entry(subCategoria).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubCategoriaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SubCategorias
        [ResponseType(typeof(SubCategoria))]
        public async Task<IHttpActionResult> PostSubCategoria(SubCategoria subCategoria)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SubCategorias.Add(subCategoria);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = subCategoria.Id }, subCategoria);
        }

        // DELETE: api/SubCategorias/5
        [ResponseType(typeof(SubCategoria))]
        public async Task<IHttpActionResult> DeleteSubCategoria(int id)
        {
            SubCategoria subCategoria = await db.SubCategorias.FindAsync(id);
            if (subCategoria == null)
            {
                return NotFound();
            }

            db.SubCategorias.Remove(subCategoria);
            await db.SaveChangesAsync();

            return Ok(subCategoria);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SubCategoriaExists(int id)
        {
            return db.SubCategorias.Count(e => e.Id == id) > 0;
        }
    }
}