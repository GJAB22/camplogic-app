﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class Cotizacion_ProductoController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/Cotizacion_Producto
        public IQueryable<Cotizacion_Producto> GetCotizacion_Producto()
        {
            return db.Cotizacion_Producto.Include(b => b.Cotizacion);
        }

        // GET: api/Cotizacion_Producto/5
        [ResponseType(typeof(Cotizacion_Producto))]
        public async Task<IHttpActionResult> GetCotizacion_Producto(int id)
        {
            Cotizacion_Producto cotizacion_Producto = await db.Cotizacion_Producto.FindAsync(id);
            if (cotizacion_Producto == null)
            {
                return NotFound();
            }

            return Ok(cotizacion_Producto);
        }

        

        // PUT: api/Cotizacion_Producto/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCotizacion_Producto(int id, Cotizacion_Producto cotizacion_Producto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cotizacion_Producto.Id)
            {
                return BadRequest();
            }

            db.Entry(cotizacion_Producto).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Cotizacion_ProductoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Cotizacion_Producto
        [ResponseType(typeof(Cotizacion_Producto))]
        public async Task<IHttpActionResult> PostCotizacion_Producto(Cotizacion_Producto cotizacion_Producto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Cotizacion_Producto.Add(cotizacion_Producto);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = cotizacion_Producto.Id }, cotizacion_Producto);
        }

        // DELETE: api/Cotizacion_Producto/5
        [ResponseType(typeof(Cotizacion_Producto))]
        public async Task<IHttpActionResult> DeleteCotizacion_Producto(int id)
        {
            Cotizacion_Producto cotizacion_Producto = await db.Cotizacion_Producto.FindAsync(id);
            if (cotizacion_Producto == null)
            {
                return NotFound();
            }

            db.Cotizacion_Producto.Remove(cotizacion_Producto);
            await db.SaveChangesAsync();

            return Ok(cotizacion_Producto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Cotizacion_ProductoExists(int id)
        {
            return db.Cotizacion_Producto.Count(e => e.Id == id) > 0;
        }
    }
}