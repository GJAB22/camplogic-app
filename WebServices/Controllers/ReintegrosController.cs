﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class ReintegrosController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/Reintegros
        public IQueryable<Reintegros> GetReintegros()
        {
            return db.Reintegros;
        }

        // GET: api/Reintegros/5
        [ResponseType(typeof(Reintegros))]
        public async Task<IHttpActionResult> GetReintegros(int id)
        {
            Reintegros reintegros = await db.Reintegros.FindAsync(id);
            if (reintegros == null)
            {
                return NotFound();
            }

            return Ok(reintegros);
        }

        // PUT: api/Reintegros/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutReintegros(int id, Reintegros reintegros)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != reintegros.Id)
            {
                return BadRequest();
            }

            db.Entry(reintegros).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReintegrosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Reintegros
        [ResponseType(typeof(Reintegros))]
        public async Task<IHttpActionResult> PostReintegros(Reintegros reintegros)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Reintegros.Add(reintegros);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = reintegros.Id }, reintegros);
        }

        // DELETE: api/Reintegros/5
        [ResponseType(typeof(Reintegros))]
        public async Task<IHttpActionResult> DeleteReintegros(int id)
        {
            Reintegros reintegros = await db.Reintegros.FindAsync(id);
            if (reintegros == null)
            {
                return NotFound();
            }

            db.Reintegros.Remove(reintegros);
            await db.SaveChangesAsync();

            return Ok(reintegros);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReintegrosExists(int id)
        {
            return db.Reintegros.Count(e => e.Id == id) > 0;
        }
    }
}