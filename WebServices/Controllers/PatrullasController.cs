﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class PatrullasController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/Patrullas
        public IQueryable<Patrulla> GetPatrullas()
        {
            return db.Patrullas;
        }

        // GET: api/Patrullas/5
        [ResponseType(typeof(Patrulla))]
        public async Task<IHttpActionResult> GetPatrulla(int id)
        {
            Patrulla patrulla = await db.Patrullas.FindAsync(id);
            if (patrulla == null)
            {
                return NotFound();
            }

            return Ok(patrulla);
        }

        [Route("api/Patrullas/{nombre}")]
        [ResponseType(typeof(Patrulla))]
        public async Task<IHttpActionResult> GetPatrullaNombre(string nombre)
        {
            Patrulla patrulla = await db.Patrullas.FirstOrDefaultAsync(e => e.Nombre == nombre);
            if (patrulla == null)
            {
                return NotFound();
            }

            return Ok(patrulla);
        }

        // PUT: api/Patrullas/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPatrulla(int id, Patrulla patrulla)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != patrulla.Id)
            {
                return BadRequest();
            }

            db.Entry(patrulla).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PatrullaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Patrullas
        [ResponseType(typeof(Patrulla))]
        public async Task<IHttpActionResult> PostPatrulla(Patrulla patrulla)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Patrullas.Add(patrulla);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = patrulla.Id }, patrulla);
        }

        // DELETE: api/Patrullas/5
        [ResponseType(typeof(Patrulla))]
        public async Task<IHttpActionResult> DeletePatrulla(int id)
        {
            Patrulla patrulla = await db.Patrullas.FindAsync(id);
            if (patrulla == null)
            {
                return NotFound();
            }

            db.Patrullas.Remove(patrulla);
            await db.SaveChangesAsync();

            return Ok(patrulla);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PatrullaExists(int id)
        {
            return db.Patrullas.Count(e => e.Id == id) > 0;
        }
    }
}