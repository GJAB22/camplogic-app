﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class ActividadesController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/Actividades
        public IQueryable<Actividades> GetActividades()
        {
            return db.Actividades;
        }

        // GET: api/Actividades/5
        [ResponseType(typeof(Actividades))]
        public async Task<IHttpActionResult> GetActividades(int id)
        {
            Actividades actividades = await db.Actividades.FindAsync(id);
            if (actividades == null)
            {
                return NotFound();
            }

            return Ok(actividades);
        }

        [Route("api/Actividades/{nombre}")]
        [ResponseType(typeof(Actividades))]
        public async Task<IHttpActionResult> GetActividadesNombre(string nombre)
        {
            Actividades actividades = await db.Actividades.FirstOrDefaultAsync(e => e.Nombre == nombre);
            if (actividades == null)
            {
                return NotFound();
            }

            return Ok(actividades);
        }

        // PUT: api/Actividades/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutActividades(int id, Actividades actividades)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != actividades.Id)
            {
                return BadRequest();
            }

            db.Entry(actividades).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActividadesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Actividades
        [ResponseType(typeof(Actividades))]
        public async Task<IHttpActionResult> PostActividades(Actividades actividades)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Actividades.Add(actividades);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = actividades.Id }, actividades);
        }

        // DELETE: api/Actividades/5
        [ResponseType(typeof(Actividades))]
        public async Task<IHttpActionResult> DeleteActividades(int id)
        {
            Actividades actividades = await db.Actividades.FindAsync(id);
            if (actividades == null)
            {
                return NotFound();
            }

            db.Actividades.Remove(actividades);
            await db.SaveChangesAsync();

            return Ok(actividades);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ActividadesExists(int id)
        {
            return db.Actividades.Count(e => e.Id == id) > 0;
        }
    }
}