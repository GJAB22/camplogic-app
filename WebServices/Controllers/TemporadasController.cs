﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class TemporadasController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/Temporadas
        public IQueryable<Temporadas> GetTemporadas()
        {
            return db.Temporadas;
        }

        // GET: api/Temporadas/5
        [ResponseType(typeof(Temporadas))]
        public async Task<IHttpActionResult> GetTemporadas(int id)
        {
            Temporadas temporadas = await db.Temporadas.FindAsync(id);
            if (temporadas == null)
            {
                return NotFound();
            }

            return Ok(temporadas);
        }

        // PUT: api/Temporadas/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTemporadas(int id, Temporadas temporadas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != temporadas.Id)
            {
                return BadRequest();
            }

            db.Entry(temporadas).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TemporadasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Temporadas
        [ResponseType(typeof(Temporadas))]
        public async Task<IHttpActionResult> PostTemporadas(Temporadas temporadas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Temporadas.Add(temporadas);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = temporadas.Id }, temporadas);
        }

        // DELETE: api/Temporadas/5
        [ResponseType(typeof(Temporadas))]
        public async Task<IHttpActionResult> DeleteTemporadas(int id)
        {
            Temporadas temporadas = await db.Temporadas.FindAsync(id);
            if (temporadas == null)
            {
                return NotFound();
            }

            db.Temporadas.Remove(temporadas);
            await db.SaveChangesAsync();

            return Ok(temporadas);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TemporadasExists(int id)
        {
            return db.Temporadas.Count(e => e.Id == id) > 0;
        }
    }
}