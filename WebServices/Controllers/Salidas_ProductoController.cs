﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Migrations
{
    public class Salidas_ProductoController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/Salidas_Producto
        public IQueryable<Salidas_Producto> GetSalidas_Producto()
        {
            return db.Salidas_Producto.Include(x=>x.Producto).Include(x => x.Salida).Include(x => x.Salida.Empleado);
        }

        // GET: api/Salidas_Producto/5
        [ResponseType(typeof(Salidas_Producto))]
        public async Task<IHttpActionResult> GetSalidas_Producto(int id)
        {
            Salidas_Producto salidas_Producto = await db.Salidas_Producto.FindAsync(id);
            if (salidas_Producto == null)
            {
                return NotFound();
            }

            return Ok(salidas_Producto);
        }

        // PUT: api/Salidas_Producto/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSalidas_Producto(int id, Salidas_Producto salidas_Producto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != salidas_Producto.Id)
            {
                return BadRequest();
            }

            db.Entry(salidas_Producto).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Salidas_ProductoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Salidas_Producto
        [ResponseType(typeof(Salidas_Producto))]
        public async Task<IHttpActionResult> PostSalidas_Producto(Salidas_Producto salidas_Producto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Salidas_Producto.Add(salidas_Producto);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = salidas_Producto.Id }, salidas_Producto);
        }

        // DELETE: api/Salidas_Producto/5
        [ResponseType(typeof(Salidas_Producto))]
        public async Task<IHttpActionResult> DeleteSalidas_Producto(int id)
        {
            Salidas_Producto salidas_Producto = await db.Salidas_Producto.FindAsync(id);
            if (salidas_Producto == null)
            {
                return NotFound();
            }

            db.Salidas_Producto.Remove(salidas_Producto);
            await db.SaveChangesAsync();

            return Ok(salidas_Producto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Salidas_ProductoExists(int id)
        {
            return db.Salidas_Producto.Count(e => e.Id == id) > 0;
        }
    }
}