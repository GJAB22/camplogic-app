﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class CampistaRepresentantesController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/CampistaRepresentantes
        public IQueryable<CampistaRepresentante> GetCampistaRepresentantes()
        {
            return db.CampistaRepresentantes.Include(b=>b.Campista).Include(b=>b.Representante);
        }

        // GET: api/CampistaRepresentantes/5
        [ResponseType(typeof(CampistaRepresentante))]
        public async Task<IHttpActionResult> GetCampistaRepresentante(int id)
        {
            CampistaRepresentante campistaRepresentante = await db.CampistaRepresentantes.FindAsync(id);
            if (campistaRepresentante == null)
            {
                return NotFound();
            }

            return Ok(campistaRepresentante);
        }

        // PUT: api/CampistaRepresentantes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCampistaRepresentante(int id, CampistaRepresentante campistaRepresentante)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != campistaRepresentante.Id)
            {
                return BadRequest();
            }

            db.Entry(campistaRepresentante).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CampistaRepresentanteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CampistaRepresentantes
        [ResponseType(typeof(CampistaRepresentante))]
        public async Task<IHttpActionResult> PostCampistaRepresentante(CampistaRepresentante campistaRepresentante)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CampistaRepresentantes.Add(campistaRepresentante);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = campistaRepresentante.Id }, campistaRepresentante);
        }

        // DELETE: api/CampistaRepresentantes/5
        [ResponseType(typeof(CampistaRepresentante))]
        public async Task<IHttpActionResult> DeleteCampistaRepresentante(int id)
        {
            CampistaRepresentante campistaRepresentante = await db.CampistaRepresentantes.FindAsync(id);
            if (campistaRepresentante == null)
            {
                return NotFound();
            }

            db.CampistaRepresentantes.Remove(campistaRepresentante);
            await db.SaveChangesAsync();

            return Ok(campistaRepresentante);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CampistaRepresentanteExists(int id)
        {
            return db.CampistaRepresentantes.Count(e => e.Id == id) > 0;
        }
    }
}