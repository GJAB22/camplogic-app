﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class CestasController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/Cestas
        public IQueryable<Cesta> GetCestas()
        {
            return db.Cestas.Include(b => b.Empleado).Include(b => b.Producto).Include(b => b.Producto.Proveedor).Include(b => b.Producto.SubCategoria.Categoria);
        }

        // GET: api/Cestas/5
        [ResponseType(typeof(Cesta))]
        public async Task<IHttpActionResult> GetCesta(int id)
        {
            Cesta cesta = await db.Cestas.FindAsync(id);
            if (cesta == null)
            {
                return NotFound();
            }

            return Ok(cesta);
        }

        // PUT: api/Cestas/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCesta(int id, Cesta cesta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cesta.Id)
            {
                return BadRequest();
            }

            db.Entry(cesta).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CestaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Cestas
        [ResponseType(typeof(Cesta))]
        public async Task<IHttpActionResult> PostCesta(Cesta cesta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Cestas.Add(cesta);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = cesta.Id }, cesta);
        }

        // DELETE: api/Cestas/5
        [ResponseType(typeof(Cesta))]
        public async Task<IHttpActionResult> DeleteCesta(int id)
        {
            Cesta cesta = await db.Cestas.FindAsync(id);
            if (cesta == null)
            {
                return NotFound();
            }

            db.Cestas.Remove(cesta);
            await db.SaveChangesAsync();

            return Ok(cesta);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CestaExists(int id)
        {
            return db.Cestas.Count(e => e.Id == id) > 0;
        }
    }
}