﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class UbicacionsController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/Ubicacions
        public IQueryable<Ubicacion> GetUbicacions()
        {
            return db.Ubicacions;
        }

        // GET: api/Ubicacions/5
        [ResponseType(typeof(Ubicacion))]
        public async Task<IHttpActionResult> GetUbicacion(int id)
        {
            Ubicacion ubicacion = await db.Ubicacions.FindAsync(id);
            if (ubicacion == null)
            {
                return NotFound();
            }

            return Ok(ubicacion);
        }

        // PUT: api/Ubicacions/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUbicacion(int id, Ubicacion ubicacion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ubicacion.Id)
            {
                return BadRequest();
            }

            db.Entry(ubicacion).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UbicacionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Ubicacions
        [ResponseType(typeof(Ubicacion))]
        public async Task<IHttpActionResult> PostUbicacion(Ubicacion ubicacion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Ubicacions.Add(ubicacion);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = ubicacion.Id }, ubicacion);
        }

        // DELETE: api/Ubicacions/5
        [ResponseType(typeof(Ubicacion))]
        public async Task<IHttpActionResult> DeleteUbicacion(int id)
        {
            Ubicacion ubicacion = await db.Ubicacions.FindAsync(id);
            if (ubicacion == null)
            {
                return NotFound();
            }

            db.Ubicacions.Remove(ubicacion);
            await db.SaveChangesAsync();

            return Ok(ubicacion);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UbicacionExists(int id)
        {
            return db.Ubicacions.Count(e => e.Id == id) > 0;
        }
    }
}