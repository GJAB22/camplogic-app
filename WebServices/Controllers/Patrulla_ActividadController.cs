﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class Patrulla_ActividadController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/Patrulla_Actividad
        public IQueryable<Patrulla_Actividad> GetPatrulla_Actividad()
        {
            return db.Patrulla_Actividad.Include(b => b.Actividad).Include(b => b.Patrulla);
        }

        // GET: api/Patrulla_Actividad/5
        [ResponseType(typeof(Patrulla_Actividad))]
        public async Task<IHttpActionResult> GetPatrulla_Actividad(int id)
        {
            Patrulla_Actividad patrulla_Actividad = await db.Patrulla_Actividad.FindAsync(id);
            if (patrulla_Actividad == null)
            {
                return NotFound();
            }

            return Ok(patrulla_Actividad);
        }

        // PUT: api/Patrulla_Actividad/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPatrulla_Actividad(int id, Patrulla_Actividad patrulla_Actividad)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != patrulla_Actividad.Id)
            {
                return BadRequest();
            }

            db.Entry(patrulla_Actividad).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Patrulla_ActividadExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Patrulla_Actividad
        [ResponseType(typeof(Patrulla_Actividad))]
        public async Task<IHttpActionResult> PostPatrulla_Actividad(Patrulla_Actividad patrulla_Actividad)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Patrulla_Actividad.Add(patrulla_Actividad);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = patrulla_Actividad.Id }, patrulla_Actividad);
        }

        // DELETE: api/Patrulla_Actividad/5
        [ResponseType(typeof(Patrulla_Actividad))]
        public async Task<IHttpActionResult> DeletePatrulla_Actividad(int id)
        {
            Patrulla_Actividad patrulla_Actividad = await db.Patrulla_Actividad.FindAsync(id);
            if (patrulla_Actividad == null)
            {
                return NotFound();
            }

            db.Patrulla_Actividad.Remove(patrulla_Actividad);
            await db.SaveChangesAsync();

            return Ok(patrulla_Actividad);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Patrulla_ActividadExists(int id)
        {
            return db.Patrulla_Actividad.Count(e => e.Id == id) > 0;
        }
    }
}