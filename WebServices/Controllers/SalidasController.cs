﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class SalidasController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/Salidas
        public IQueryable<Salida> GetSalidas()
        {
            return db.Salidas.Include(b => b.Empleado);
        }

        // GET: api/Salidas/5
        [ResponseType(typeof(Salida))]
        public async Task<IHttpActionResult> GetSalida(int id)
        {
            Salida salida = await db.Salidas.FindAsync(id);
            if (salida == null)
            {
                return NotFound();
            }

            return Ok(salida);
        }

        // PUT: api/Salidas/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSalida(int id, Salida salida)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != salida.Id)
            {
                return BadRequest();
            }

            db.Entry(salida).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SalidaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Salidas
        [ResponseType(typeof(Salida))]
        public async Task<IHttpActionResult> PostSalida(Salida salida)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Salidas.Add(salida);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = salida.Id }, salida);
        }

        // DELETE: api/Salidas/5
        [ResponseType(typeof(Salida))]
        public async Task<IHttpActionResult> DeleteSalida(int id)
        {
            Salida salida = await db.Salidas.FindAsync(id);
            if (salida == null)
            {
                return NotFound();
            }

            db.Salidas.Remove(salida);
            await db.SaveChangesAsync();

            return Ok(salida);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SalidaExists(int id)
        {
            return db.Salidas.Count(e => e.Id == id) > 0;
        }
    }
}