﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class PatrullaEmpleadoesController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/PatrullaEmpleadoes
        public IQueryable<PatrullaEmpleado> GetPatrullaEmpleadoes()
        {
            return db.PatrullaEmpleadoes.Include(b=>b.Empleado).Include(b=>b.Patrulla);
        }

        // GET: api/PatrullaEmpleadoes/5
        [ResponseType(typeof(PatrullaEmpleado))]
        public async Task<IHttpActionResult> GetPatrullaEmpleado(int id)
        {
            PatrullaEmpleado patrullaEmpleado = await db.PatrullaEmpleadoes.FindAsync(id);
            if (patrullaEmpleado == null)
            {
                return NotFound();
            }

            return Ok(patrullaEmpleado);
        }

        // PUT: api/PatrullaEmpleadoes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPatrullaEmpleado(int id, PatrullaEmpleado patrullaEmpleado)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != patrullaEmpleado.Id)
            {
                return BadRequest();
            }

            db.Entry(patrullaEmpleado).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PatrullaEmpleadoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PatrullaEmpleadoes
        [ResponseType(typeof(PatrullaEmpleado))]
        public async Task<IHttpActionResult> PostPatrullaEmpleado(PatrullaEmpleado patrullaEmpleado)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PatrullaEmpleadoes.Add(patrullaEmpleado);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = patrullaEmpleado.Id }, patrullaEmpleado);
        }

        // DELETE: api/PatrullaEmpleadoes/5
        [ResponseType(typeof(PatrullaEmpleado))]
        public async Task<IHttpActionResult> DeletePatrullaEmpleado(int id)
        {
            PatrullaEmpleado patrullaEmpleado = await db.PatrullaEmpleadoes.FindAsync(id);
            if (patrullaEmpleado == null)
            {
                return NotFound();
            }

            db.PatrullaEmpleadoes.Remove(patrullaEmpleado);
            await db.SaveChangesAsync();

            return Ok(patrullaEmpleado);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PatrullaEmpleadoExists(int id)
        {
            return db.PatrullaEmpleadoes.Count(e => e.Id == id) > 0;
        }
    }
}