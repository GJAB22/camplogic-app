﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class ReintegroProductoesController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/ReintegroProductoes
        public IQueryable<ReintegroProducto> GetReintegroProductoes()
        {
            return db.ReintegroProductoes.Include(r => r.Reintegro).Include(r=>r.Producto);
        }

        // GET: api/ReintegroProductoes/5
        [ResponseType(typeof(ReintegroProducto))]
        public async Task<IHttpActionResult> GetReintegroProducto(int id)
        {
            ReintegroProducto reintegroProducto = await db.ReintegroProductoes.FindAsync(id);
            if (reintegroProducto == null)
            {
                return NotFound();
            }

            return Ok(reintegroProducto);
        }

        // PUT: api/ReintegroProductoes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutReintegroProducto(int id, ReintegroProducto reintegroProducto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != reintegroProducto.Id)
            {
                return BadRequest();
            }

            db.Entry(reintegroProducto).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReintegroProductoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ReintegroProductoes
        [ResponseType(typeof(ReintegroProducto))]
        public async Task<IHttpActionResult> PostReintegroProducto(ReintegroProducto reintegroProducto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ReintegroProductoes.Add(reintegroProducto);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = reintegroProducto.Id }, reintegroProducto);
        }

        // DELETE: api/ReintegroProductoes/5
        [ResponseType(typeof(ReintegroProducto))]
        public async Task<IHttpActionResult> DeleteReintegroProducto(int id)
        {
            ReintegroProducto reintegroProducto = await db.ReintegroProductoes.FindAsync(id);
            if (reintegroProducto == null)
            {
                return NotFound();
            }

            db.ReintegroProductoes.Remove(reintegroProducto);
            await db.SaveChangesAsync();

            return Ok(reintegroProducto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReintegroProductoExists(int id)
        {
            return db.ReintegroProductoes.Count(e => e.Id == id) > 0;
        }
    }
}