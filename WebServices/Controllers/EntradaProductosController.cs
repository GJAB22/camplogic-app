﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class EntradaProductosController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/EntradaProductos
        public IQueryable<EntradaProducto> GetEntradaProductoes()
        {
            return db.EntradaProductoes.Include(t => t.Entrada).Include(t => t.Producto);
        }

        // GET: api/EntradaProductos/5
        [ResponseType(typeof(EntradaProducto))]
        public async Task<IHttpActionResult> GetEntradaProducto(int id)
        {
            EntradaProducto entradaProducto = await db.EntradaProductoes.FindAsync(id);
            if (entradaProducto == null)
            {
                return NotFound();
            }

            return Ok(entradaProducto);
        }

        // PUT: api/EntradaProductos/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutEntradaProducto(int id, EntradaProducto entradaProducto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != entradaProducto.Id)
            {
                return BadRequest();
            }

            db.Entry(entradaProducto).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EntradaProductoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/EntradaProductos
        [ResponseType(typeof(EntradaProducto))]
        public async Task<IHttpActionResult> PostEntradaProducto(EntradaProducto entradaProducto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.EntradaProductoes.Add(entradaProducto);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = entradaProducto.Id }, entradaProducto);
        }

        // DELETE: api/EntradaProductos/5
        [ResponseType(typeof(EntradaProducto))]
        public async Task<IHttpActionResult> DeleteEntradaProducto(int id)
        {
            EntradaProducto entradaProducto = await db.EntradaProductoes.FindAsync(id);
            if (entradaProducto == null)
            {
                return NotFound();
            }

            db.EntradaProductoes.Remove(entradaProducto);
            await db.SaveChangesAsync();

            return Ok(entradaProducto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EntradaProductoExists(int id)
        {
            return db.EntradaProductoes.Count(e => e.Id == id) > 0;
        }
    }
}