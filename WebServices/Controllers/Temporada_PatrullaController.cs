﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class Temporada_PatrullaController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/Temporada_Patrulla
        public IQueryable<Temporada_Patrulla> GetTemporada_Patrulla()
        {
            return db.Temporada_Patrulla.Include(b=>b.Patrulla).Include(b=>b.Temporada);
        }

        // GET: api/Temporada_Patrulla/5
        [ResponseType(typeof(Temporada_Patrulla))]
        public async Task<IHttpActionResult> GetTemporada_Patrulla(int id)
        {
            Temporada_Patrulla temporada_Patrulla = await db.Temporada_Patrulla.FindAsync(id);
            if (temporada_Patrulla == null)
            {
                return NotFound();
            }

            return Ok(temporada_Patrulla);
        }

        // PUT: api/Temporada_Patrulla/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTemporada_Patrulla(int id, Temporada_Patrulla temporada_Patrulla)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != temporada_Patrulla.Id)
            {
                return BadRequest();
            }

            db.Entry(temporada_Patrulla).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Temporada_PatrullaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Temporada_Patrulla
        [ResponseType(typeof(Temporada_Patrulla))]
        public async Task<IHttpActionResult> PostTemporada_Patrulla(Temporada_Patrulla temporada_Patrulla)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Temporada_Patrulla.Add(temporada_Patrulla);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = temporada_Patrulla.Id }, temporada_Patrulla);
        }

        // DELETE: api/Temporada_Patrulla/5
        [ResponseType(typeof(Temporada_Patrulla))]
        public async Task<IHttpActionResult> DeleteTemporada_Patrulla(int id)
        {
            Temporada_Patrulla temporada_Patrulla = await db.Temporada_Patrulla.FindAsync(id);
            if (temporada_Patrulla == null)
            {
                return NotFound();
            }

            db.Temporada_Patrulla.Remove(temporada_Patrulla);
            await db.SaveChangesAsync();

            return Ok(temporada_Patrulla);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Temporada_PatrullaExists(int id)
        {
            return db.Temporada_Patrulla.Count(e => e.Id == id) > 0;
        }
    }
}