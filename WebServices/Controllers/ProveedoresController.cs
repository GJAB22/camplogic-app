﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CampLogic.Model;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class ProveedoresController : ApiController
    {
        private CampLContext db = new CampLContext();

        // GET: api/Proveedores
        public IQueryable<Proveedor> GetProveedors()
        {
            return db.Proveedors;
        }

        // GET: api/Proveedores/5
        [ResponseType(typeof(Proveedor))]
        public async Task<IHttpActionResult> GetProveedor(int id)
        {
            Proveedor proveedor = await db.Proveedors.FindAsync(id);
            if (proveedor == null)
            {
                return NotFound();
            }

            return Ok(proveedor);
        }

        // PUT: api/Proveedores/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutProveedor(int id, Proveedor proveedor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != proveedor.Id)
            {
                return BadRequest();
            }

            db.Entry(proveedor).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProveedorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Proveedores
        [ResponseType(typeof(Proveedor))]
        public async Task<IHttpActionResult> PostProveedor(Proveedor proveedor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Proveedors.Add(proveedor);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = proveedor.Id }, proveedor);
        }

        // DELETE: api/Proveedores/5
        [ResponseType(typeof(Proveedor))]
        public async Task<IHttpActionResult> DeleteProveedor(int id)
        {
            Proveedor proveedor = await db.Proveedors.FindAsync(id);
            if (proveedor == null)
            {
                return NotFound();
            }

            db.Proveedors.Remove(proveedor);
            await db.SaveChangesAsync();

            return Ok(proveedor);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProveedorExists(int id)
        {
            return db.Proveedors.Count(e => e.Id == id) > 0;
        }
    }
}