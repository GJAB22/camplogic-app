﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class Entrada
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public int Cantidad { get; set; }
        public float Valor_Total { get; set; }
        public int BackOrderTotal { get; set; }


        //Orden Compra
        //Foreign Key
        public int OrdenCompraId { get; set; }
        //Navigation Property
        public Orden_Compra OrdenCompra { get; set; }

    }
}
