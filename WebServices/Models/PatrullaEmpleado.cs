﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class PatrullaEmpleado
    {
        public int Id { get; set; }
        public DateTime FechaAsignacion { get; set; }

        //Patrulla
        public int PatrullaId { get; set; }
        public Patrulla Patrulla { get; set; }

        //Empleado
        public int EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }
    }
}
