﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class Cesta
    {
        public int Id { get; set; }
        public int Cantidad { get; set; }
        public float Precio { get; set; }
        public float ValorTotal { get; set; }
        public string Prioridad { get; set; }

        //Empleado
        public int EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }

        //Producto
        public int ProductoId { get; set; }
        public Producto Producto { get; set; }
    }
}
