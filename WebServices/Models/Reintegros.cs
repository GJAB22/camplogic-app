﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class Reintegros
    {
        public int Id { get; set; }
        public int CantidadTotal { get; set; }
        public DateTime FechaEntrada { get; set; }
    }
}
