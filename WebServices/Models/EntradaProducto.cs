﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class EntradaProducto
    {
        public int Id { get; set; }
        public int CantidadEntregada { get; set; }
        public int BackOrder { get; set; }
        public float CostoUnitario { get; set; }
        public float ValorTotal { get; set; }

        //Foreign Key Producto
        public int ProductoId { get; set; }
        public Producto Producto { get; set; }

        //Foreign Key Entrada
        public int EntradaId { get; set; }
        public Entrada Entrada { get; set; }

    }
}
