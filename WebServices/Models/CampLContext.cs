﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebServices.Models
{
    public class CampLContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public CampLContext() : base("name=CampLContext")
        {
        }

        public System.Data.Entity.DbSet<CampLogic.Model.Patrulla> Patrullas { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Empleado> Empleadoes { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Actividades> Actividades { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Patrulla_Actividad> Patrulla_Actividad { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Representante> Representantes { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Campistas> Campistas { get; set; }
        
        public System.Data.Entity.DbSet<CampLogic.Model.Producto> Productoes { get; set; }
        
        public System.Data.Entity.DbSet<CampLogic.Model.Cotizacion> Cotizacions { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Cotizacion_Producto> Cotizacion_Producto { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Proveedor> Proveedors { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Orden_Compra> Orden_Compra { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Entrada> Entradas { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Salida> Salidas { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Noticias> Noticias { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Cesta> Cestas { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Pedidos> Pedidos { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Categoria> Categorias { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.SubCategoria> SubCategorias { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Salidas_Producto> Salidas_Producto { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.EntradaProducto> EntradaProductoes { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.ReintegroProducto> ReintegroProductoes { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Reintegros> Reintegros { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Ubicacion> Ubicacions { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Temporadas> Temporadas { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.Temporada_Patrulla> Temporada_Patrulla { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.PatrullaEmpleado> PatrullaEmpleadoes { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.PatrullaCampista> PatrullaCampistas { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.CampistaRepresentante> CampistaRepresentantes { get; set; }

        public System.Data.Entity.DbSet<CampLogic.Model.ActividadProducto> ActividadProductoes { get; set; }
    }
}
