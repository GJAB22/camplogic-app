﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class Patrulla_Actividad
    {
        public int Id { get; set; }
        public DateTime Horario { get; set; }
        public DateTime FechaAsignacion { get; set; }

        //Patrulla
        //Foreign Key
        public int PatrullaId { get; set; }
        //Navigation Property
        public Patrulla Patrulla { get; set; }

        //Actividad
        //Foreign Key
        public int ActividadId { get; set; }
        //Navigation Property
        public Actividades Actividad { get; set; }
    }
}
