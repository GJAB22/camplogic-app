﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class Empleado
    {
        public int Id { get; set; }
        public string FotoPerfil { get; set; } 
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Cedula { get; set; }
        public string Cargo { get; set; }
        public DateTime Fecha_Nacimiento { get; set; } 
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public Boolean Disponibilidad { get; set; }
        public string PreferenciaCampistas { get; set; }
        public string UserId { get; set; }
       
        public string NombrePatrulla { get; set; }

    }
}
