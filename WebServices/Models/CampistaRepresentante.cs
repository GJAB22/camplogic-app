﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class CampistaRepresentante
    {
        public int Id { get; set; }

        //Campista
        public int CampistaId { get; set; }
        public Campistas Campista { get; set; }

        //Representante
        public int RepresentanteId { get; set; }
        public Representante Representante { get; set; }
    }
}
