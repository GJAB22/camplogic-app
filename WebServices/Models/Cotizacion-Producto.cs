﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class Cotizacion_Producto
    {
        public int Id { get; set; }
        public int Cantidad { get; set; }
        public float Precio { get; set; }

        //Foreign Key Cotizacion
        public int CotizacionId { get; set; }
        //Navigation Property Cotizacion
        public Cotizacion Cotizacion { get; set; }

        //Foreign Key producto
        public int ProductoId { get; set; }
        //Navigation Property Producto
        public Producto Producto { get; set; }
    }
}
