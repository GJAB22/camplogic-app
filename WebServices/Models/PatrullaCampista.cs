﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class PatrullaCampista
    {
        public int Id { get; set; }
        public DateTime FechaAsignada { get; set; }

        //Campista
        public int CampistaId { get; set; }
        public Campistas Campista { get; set; }

        //Patrulla
        public int PatrullaId { get; set; }
        public Patrulla Patrulla { get; set; }
    }
}
