﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class Producto
    {
        public int Id { get; set; }
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        public int Disponible { get; set; }
        public int CantidadIdeal { get; set; }
        public int PuntoReorden { get; set; }
        public int Existencia_Inicial { get; set; }
        public int Valor_Inicial { get; set; }


        //Proveedor
        public int ProveedorId { get; set; }
        public Proveedor Proveedor { get; set; }

        //SubCategoria
        public int SubCategoriaId { get; set; }
        public SubCategoria SubCategoria { get; set; }

        //Ubicacion
        public int UbicacionId { get; set; }
        public Ubicacion Ubicacion { get; set; }
    }
}
