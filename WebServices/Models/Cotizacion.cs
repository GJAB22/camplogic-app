﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class Cotizacion
    {
        public int Id { get; set; }
        public DateTime Fecha_Cotizacion { get; set; }

        //Foreign Key Empleado
        public int EmpleadoId { get; set; }
        //Navigation Property Empleado
        public Empleado Empleado { get; set; }

    }
}
