namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EntraNuevo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Entradas", "CantidadR", c => c.Int(nullable: false));
            AddColumn("dbo.Entradas", "BackOrder", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Entradas", "BackOrder");
            DropColumn("dbo.Entradas", "CantidadR");
        }
    }
}
