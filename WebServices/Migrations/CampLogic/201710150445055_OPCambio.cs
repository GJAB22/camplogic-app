namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OPCambio : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrdenProds", "OrdenCompraId", c => c.Int(nullable: false));
            CreateIndex("dbo.OrdenProds", "OrdenCompraId");
            AddForeignKey("dbo.OrdenProds", "OrdenCompraId", "dbo.Orden_Compra", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrdenProds", "OrdenCompraId", "dbo.Orden_Compra");
            DropIndex("dbo.OrdenProds", new[] { "OrdenCompraId" });
            DropColumn("dbo.OrdenProds", "OrdenCompraId");
        }
    }
}
