// <auto-generated />
namespace WebServices.Migrations.CampLogic
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class EntradaOrdenId : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(EntradaOrdenId));
        
        string IMigrationMetadata.Id
        {
            get { return "201709261716457_EntradaOrdenId"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
