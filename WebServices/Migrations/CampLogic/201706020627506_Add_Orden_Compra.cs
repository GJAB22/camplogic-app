namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Orden_Compra : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Orden_Compra",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fecha_Orden = c.DateTime(nullable: false),
                        Fecha_Entrega = c.DateTime(nullable: false),
                        Costo_Total = c.Single(nullable: false),
                        Aprobado = c.Boolean(nullable: false),
                        Cantidad_Orden = c.Int(nullable: false),
                        ProveedorId = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                        EmpleadoId = c.Int(nullable: false),
                        CotizacionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cotizacions", t => t.CotizacionId, cascadeDelete: true)
                .ForeignKey("dbo.Empleadoes", t => t.EmpleadoId, cascadeDelete: false)
                .ForeignKey("dbo.Productoes", t => t.ProductoId, cascadeDelete: true)
                .ForeignKey("dbo.Proveedors", t => t.ProveedorId, cascadeDelete: true)
                .Index(t => t.ProveedorId)
                .Index(t => t.ProductoId)
                .Index(t => t.EmpleadoId)
                .Index(t => t.CotizacionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orden_Compra", "ProveedorId", "dbo.Proveedors");
            DropForeignKey("dbo.Orden_Compra", "ProductoId", "dbo.Productoes");
            DropForeignKey("dbo.Orden_Compra", "EmpleadoId", "dbo.Empleadoes");
            DropForeignKey("dbo.Orden_Compra", "CotizacionId", "dbo.Cotizacions");
            DropIndex("dbo.Orden_Compra", new[] { "CotizacionId" });
            DropIndex("dbo.Orden_Compra", new[] { "EmpleadoId" });
            DropIndex("dbo.Orden_Compra", new[] { "ProductoId" });
            DropIndex("dbo.Orden_Compra", new[] { "ProveedorId" });
            DropTable("dbo.Orden_Compra");
        }
    }
}
