namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambioEmpleadoTable2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Empleadoes", "PreferenciaCampistas", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Empleadoes", "PreferenciaCampistas");
        }
    }
}
