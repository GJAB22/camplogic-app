namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OCCambio : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Orden_Compra", "ProductoId", "dbo.Productoes");
            DropIndex("dbo.Orden_Compra", new[] { "ProductoId" });
            DropColumn("dbo.Orden_Compra", "ProductoId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orden_Compra", "ProductoId", c => c.Int(nullable: false));
            CreateIndex("dbo.Orden_Compra", "ProductoId");
            AddForeignKey("dbo.Orden_Compra", "ProductoId", "dbo.Productoes", "Id", cascadeDelete: true);
        }
    }
}
