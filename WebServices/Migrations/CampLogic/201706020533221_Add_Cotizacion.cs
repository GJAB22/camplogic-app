namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Cotizacion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cotizacions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fecha_Cotizacion = c.DateTime(nullable: false),
                        EmpleadoId = c.Int(nullable: false),
                        SolicitudId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Empleadoes", t => t.EmpleadoId, cascadeDelete: true)
                .Index(t => t.EmpleadoId)
                .Index(t => t.SolicitudId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cotizacions", "SolicitudId", "dbo.Solicituds");
            DropForeignKey("dbo.Cotizacions", "EmpleadoId", "dbo.Empleadoes");
            DropIndex("dbo.Cotizacions", new[] { "SolicitudId" });
            DropIndex("dbo.Cotizacions", new[] { "EmpleadoId" });
            DropTable("dbo.Cotizacions");
        }
    }
}
