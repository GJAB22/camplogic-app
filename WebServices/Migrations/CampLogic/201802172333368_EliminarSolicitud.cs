namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EliminarSolicitud : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Solicituds", "EmpleadoId", "dbo.Empleadoes");
            DropIndex("dbo.Solicituds", new[] { "EmpleadoId" });
            DropTable("dbo.Solicituds");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Solicituds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fecha_Solicitud = c.DateTime(nullable: false),
                        Num = c.Int(nullable: false),
                        EmpleadoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Solicituds", "EmpleadoId");
            AddForeignKey("dbo.Solicituds", "EmpleadoId", "dbo.Empleadoes", "Id", cascadeDelete: true);
        }
    }
}
