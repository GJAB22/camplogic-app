namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambioCampistas : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Campistas", "PatrullaId", "dbo.Patrullas");
            DropForeignKey("dbo.Campistas", "RepresentanteId", "dbo.Representantes");
            DropIndex("dbo.Campistas", new[] { "RepresentanteId" });
            DropIndex("dbo.Campistas", new[] { "PatrullaId" });
            AddColumn("dbo.Campistas", "Descripcion", c => c.String());
            AddColumn("dbo.Campistas", "Inscrito", c => c.Boolean(nullable: false));
            AddColumn("dbo.Campistas", "Asistencia", c => c.Boolean(nullable: false));
            AddColumn("dbo.Campistas", "Transporte", c => c.Boolean(nullable: false));
            DropColumn("dbo.Campistas", "RepresentanteId");
            DropColumn("dbo.Campistas", "PatrullaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Campistas", "PatrullaId", c => c.Int(nullable: false));
            AddColumn("dbo.Campistas", "RepresentanteId", c => c.Int(nullable: false));
            DropColumn("dbo.Campistas", "Transporte");
            DropColumn("dbo.Campistas", "Asistencia");
            DropColumn("dbo.Campistas", "Inscrito");
            DropColumn("dbo.Campistas", "Descripcion");
            CreateIndex("dbo.Campistas", "PatrullaId");
            CreateIndex("dbo.Campistas", "RepresentanteId");
            AddForeignKey("dbo.Campistas", "RepresentanteId", "dbo.Representantes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Campistas", "PatrullaId", "dbo.Patrullas", "Id", cascadeDelete: true);
        }
    }
}
