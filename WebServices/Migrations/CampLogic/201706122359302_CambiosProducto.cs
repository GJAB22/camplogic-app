namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambiosProducto : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Productoes", "Disponible", c => c.Int(nullable: false));
            AddColumn("dbo.Productoes", "PuntoReorden", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Productoes", "PuntoReorden");
            DropColumn("dbo.Productoes", "Disponible");
        }
    }
}
