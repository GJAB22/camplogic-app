namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPatrullaCampistaTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PatrullaCampistas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FechaAsignada = c.DateTime(nullable: false),
                        CampistaId = c.Int(nullable: false),
                        PatrullaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campistas", t => t.CampistaId, cascadeDelete: true)
                .ForeignKey("dbo.Patrullas", t => t.PatrullaId, cascadeDelete: true)
                .Index(t => t.CampistaId)
                .Index(t => t.PatrullaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PatrullaCampistas", "PatrullaId", "dbo.Patrullas");
            DropForeignKey("dbo.PatrullaCampistas", "CampistaId", "dbo.Campistas");
            DropIndex("dbo.PatrullaCampistas", new[] { "PatrullaId" });
            DropIndex("dbo.PatrullaCampistas", new[] { "CampistaId" });
            DropTable("dbo.PatrullaCampistas");
        }
    }
}
