namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambioProdsySalidas : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Salidas", "ProductoId", "dbo.Productoes");
            DropIndex("dbo.Salidas", new[] { "ProductoId" });
            AddColumn("dbo.Productoes", "Ubicacion", c => c.String());
            DropColumn("dbo.Salidas", "ProductoId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Salidas", "ProductoId", c => c.Int(nullable: false));
            DropColumn("dbo.Productoes", "Ubicacion");
            CreateIndex("dbo.Salidas", "ProductoId");
            AddForeignKey("dbo.Salidas", "ProductoId", "dbo.Productoes", "Id", cascadeDelete: true);
        }
    }
}
