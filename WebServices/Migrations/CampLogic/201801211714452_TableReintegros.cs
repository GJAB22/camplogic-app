namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TableReintegros : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReintegroProductoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cantidad = c.Int(nullable: false),
                        Faltante = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                        ReintegroId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Productoes", t => t.ProductoId, cascadeDelete: true)
                .ForeignKey("dbo.Reintegros", t => t.ReintegroId, cascadeDelete: true)
                .Index(t => t.ProductoId)
                .Index(t => t.ReintegroId);
            
            CreateTable(
                "dbo.Reintegros",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CantidadTotal = c.Int(nullable: false),
                        FechaEntrada = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReintegroProductoes", "ReintegroId", "dbo.Reintegros");
            DropForeignKey("dbo.ReintegroProductoes", "ProductoId", "dbo.Productoes");
            DropIndex("dbo.ReintegroProductoes", new[] { "ReintegroId" });
            DropIndex("dbo.ReintegroProductoes", new[] { "ProductoId" });
            DropTable("dbo.Reintegros");
            DropTable("dbo.ReintegroProductoes");
        }
    }
}
