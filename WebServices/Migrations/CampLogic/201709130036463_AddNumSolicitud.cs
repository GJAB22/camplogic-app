namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNumSolicitud : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Solicituds", "Num", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Solicituds", "Num");
        }
    }
}
