namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OCProveedor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orden_Compra", "ProveedorId", c => c.Int(nullable: false));
            CreateIndex("dbo.Orden_Compra", "ProveedorId");
            AddForeignKey("dbo.Orden_Compra", "ProveedorId", "dbo.Proveedors", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orden_Compra", "ProveedorId", "dbo.Proveedors");
            DropIndex("dbo.Orden_Compra", new[] { "ProveedorId" });
            DropColumn("dbo.Orden_Compra", "ProveedorId");
        }
    }
}
