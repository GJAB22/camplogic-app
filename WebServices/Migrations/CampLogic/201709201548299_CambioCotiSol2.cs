namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambioCotiSol2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Cotizacions", "Number");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Cotizacions", "Number", c => c.Int(nullable: false));
        }
    }
}
