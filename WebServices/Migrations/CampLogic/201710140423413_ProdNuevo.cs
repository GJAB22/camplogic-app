namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProdNuevo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Productoes", "Existencia_Inicial", c => c.Int(nullable: false));
            AddColumn("dbo.Productoes", "Valor_Inicial", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Productoes", "Valor_Inicial");
            DropColumn("dbo.Productoes", "Existencia_Inicial");
        }
    }
}
