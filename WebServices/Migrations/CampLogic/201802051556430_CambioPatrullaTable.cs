namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambioPatrullaTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Patrullas", "EdadMinima", c => c.Int(nullable: false));
            AddColumn("dbo.Patrullas", "EdadMaxima", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Patrullas", "EdadMaxima");
            DropColumn("dbo.Patrullas", "EdadMinima");
        }
    }
}
