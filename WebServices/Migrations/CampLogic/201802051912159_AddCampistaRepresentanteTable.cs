namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCampistaRepresentanteTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CampistaRepresentantes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CampistaId = c.Int(nullable: false),
                        RepresentanteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campistas", t => t.CampistaId, cascadeDelete: true)
                .ForeignKey("dbo.Representantes", t => t.RepresentanteId, cascadeDelete: true)
                .Index(t => t.CampistaId)
                .Index(t => t.RepresentanteId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CampistaRepresentantes", "RepresentanteId", "dbo.Representantes");
            DropForeignKey("dbo.CampistaRepresentantes", "CampistaId", "dbo.Campistas");
            DropIndex("dbo.CampistaRepresentantes", new[] { "RepresentanteId" });
            DropIndex("dbo.CampistaRepresentantes", new[] { "CampistaId" });
            DropTable("dbo.CampistaRepresentantes");
        }
    }
}
