namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Patrulla_Actividad : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Patrulla_Actividad",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Horario = c.DateTime(nullable: false),
                        PatrullaId = c.Int(nullable: false),
                        ActividadId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Actividades", t => t.ActividadId, cascadeDelete: true)
                .ForeignKey("dbo.Patrullas", t => t.PatrullaId, cascadeDelete: true)
                .Index(t => t.PatrullaId)
                .Index(t => t.ActividadId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Patrulla_Actividad", "PatrullaId", "dbo.Patrullas");
            DropForeignKey("dbo.Patrulla_Actividad", "ActividadId", "dbo.Actividades");
            DropIndex("dbo.Patrulla_Actividad", new[] { "ActividadId" });
            DropIndex("dbo.Patrulla_Actividad", new[] { "PatrullaId" });
            DropTable("dbo.Patrulla_Actividad");
        }
    }
}
