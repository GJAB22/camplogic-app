namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmpleadoes : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Empleadoes", "UserId", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Empleadoes", "UserId", c => c.Int(nullable: false));
        }
    }
}
