namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProdProv : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Productoes", "ProveedorId", c => c.Int(nullable: true));
            CreateIndex("dbo.Productoes", "ProveedorId");
            AddForeignKey("dbo.Productoes", "ProveedorId", "dbo.Proveedors", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Productoes", "ProveedorId", "dbo.Proveedors");
            DropIndex("dbo.Productoes", new[] { "ProveedorId" });
            DropColumn("dbo.Productoes", "ProveedorId");
        }
    }
}
