namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEntradaProductoTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EntradaProductoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CantidadEntregada = c.Int(nullable: false),
                        BackOrder = c.Int(nullable: false),
                        CostoUnitario = c.Single(nullable: false),
                        ValorTotal = c.Single(nullable: false),
                        ProductoId = c.Int(nullable: false),
                        EntradaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Entradas", t => t.EntradaId, cascadeDelete: true)
                .ForeignKey("dbo.Productoes", t => t.ProductoId, cascadeDelete: true)
                .Index(t => t.ProductoId)
                .Index(t => t.EntradaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EntradaProductoes", "ProductoId", "dbo.Productoes");
            DropForeignKey("dbo.EntradaProductoes", "EntradaId", "dbo.Entradas");
            DropIndex("dbo.EntradaProductoes", new[] { "EntradaId" });
            DropIndex("dbo.EntradaProductoes", new[] { "ProductoId" });
            DropTable("dbo.EntradaProductoes");
        }
    }
}
