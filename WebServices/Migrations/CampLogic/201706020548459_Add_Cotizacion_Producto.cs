namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Cotizacion_Producto : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cotizacion_Producto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cantidad = c.Int(nullable: false),
                        Precio = c.Single(nullable: false),
                        CotizacionId = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cotizacions", t => t.CotizacionId, cascadeDelete: true)
                .ForeignKey("dbo.Productoes", t => t.ProductoId, cascadeDelete: true)
                .Index(t => t.CotizacionId)
                .Index(t => t.ProductoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cotizacion_Producto", "ProductoId", "dbo.Productoes");
            DropForeignKey("dbo.Cotizacion_Producto", "CotizacionId", "dbo.Cotizacions");
            DropIndex("dbo.Cotizacion_Producto", new[] { "ProductoId" });
            DropIndex("dbo.Cotizacion_Producto", new[] { "CotizacionId" });
            DropTable("dbo.Cotizacion_Producto");
        }
    }
}
