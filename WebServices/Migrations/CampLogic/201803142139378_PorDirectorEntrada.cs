namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PorDirectorEntrada : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReintegroProductoes", "PorDirector", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ReintegroProductoes", "PorDirector");
        }
    }
}
