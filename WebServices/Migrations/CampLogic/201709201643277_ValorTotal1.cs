namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ValorTotal1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Entradas", "Valor_Total", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Entradas", "Valor_Total", c => c.Double(nullable: false));
        }
    }
}
