namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Solicitudes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Solicituds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fecha_Solicitud = c.DateTime(nullable: false),
                        EmpleadoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Empleadoes", t => t.EmpleadoId, cascadeDelete: true)
                .Index(t => t.EmpleadoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Solicituds", "EmpleadoId", "dbo.Empleadoes");
            DropIndex("dbo.Solicituds", new[] { "EmpleadoId" });
            DropTable("dbo.Solicituds");
        }
    }
}
