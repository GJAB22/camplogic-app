namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Solicitud_Producto : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Solicitud_Producto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cantidad = c.Int(nullable: false),
                        Resolucion = c.Boolean(nullable: false),
                        SolicitudId = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Productoes", t => t.ProductoId, cascadeDelete: true)
                .ForeignKey("dbo.Solicituds", t => t.SolicitudId, cascadeDelete: true)
                .Index(t => t.SolicitudId)
                .Index(t => t.ProductoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Solicitud_Producto", "SolicitudId", "dbo.Solicituds");
            DropForeignKey("dbo.Solicitud_Producto", "ProductoId", "dbo.Productoes");
            DropIndex("dbo.Solicitud_Producto", new[] { "ProductoId" });
            DropIndex("dbo.Solicitud_Producto", new[] { "SolicitudId" });
            DropTable("dbo.Solicitud_Producto");
        }
    }
}
