namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EntradaCambio : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Entradas", "CantidadR");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Entradas", "CantidadR", c => c.Int(nullable: false));
        }
    }
}
