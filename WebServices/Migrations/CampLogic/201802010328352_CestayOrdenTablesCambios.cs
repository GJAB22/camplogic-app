namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CestayOrdenTablesCambios : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cestas", "Prioridad", c => c.String());
            DropColumn("dbo.Cestas", "FechaEntrega");
            DropColumn("dbo.Orden_Compra", "FechaEntrega");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orden_Compra", "FechaEntrega", c => c.DateTime(nullable: false));
            AddColumn("dbo.Cestas", "FechaEntrega", c => c.DateTime(nullable: false));
            DropColumn("dbo.Cestas", "Prioridad");
        }
    }
}
