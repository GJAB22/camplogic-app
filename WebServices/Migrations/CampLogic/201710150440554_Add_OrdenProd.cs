namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_OrdenProd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrdenProds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CantidadProd = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Productoes", t => t.ProductoId, cascadeDelete: true)
                .Index(t => t.ProductoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrdenProds", "ProductoId", "dbo.Productoes");
            DropIndex("dbo.OrdenProds", new[] { "ProductoId" });
            DropTable("dbo.OrdenProds");
        }
    }
}
