namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SubCategoriaUpdate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SubCategorias", "Nombre", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SubCategorias", "Nombre", c => c.Int(nullable: false));
        }
    }
}
