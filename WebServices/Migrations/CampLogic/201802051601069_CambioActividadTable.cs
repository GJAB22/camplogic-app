namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambioActividadTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Actividades", "Clasificacion", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Actividades", "Clasificacion");
        }
    }
}
