namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Salidas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Salidas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fecha_Salida = c.DateTime(nullable: false),
                        Cantidad_Entregada = c.Int(nullable: false),
                        EmpleadoId = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                        EntradaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Empleadoes", t => t.EmpleadoId, cascadeDelete: true)
                .ForeignKey("dbo.Entradas", t => t.EntradaId, cascadeDelete: true)
                .ForeignKey("dbo.Productoes", t => t.ProductoId, cascadeDelete: false)
                .Index(t => t.EmpleadoId)
                .Index(t => t.ProductoId)
                .Index(t => t.EntradaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Salidas", "ProductoId", "dbo.Productoes");
            DropForeignKey("dbo.Salidas", "EntradaId", "dbo.Entradas");
            DropForeignKey("dbo.Salidas", "EmpleadoId", "dbo.Empleadoes");
            DropIndex("dbo.Salidas", new[] { "EntradaId" });
            DropIndex("dbo.Salidas", new[] { "ProductoId" });
            DropIndex("dbo.Salidas", new[] { "EmpleadoId" });
            DropTable("dbo.Salidas");
        }
    }
}
