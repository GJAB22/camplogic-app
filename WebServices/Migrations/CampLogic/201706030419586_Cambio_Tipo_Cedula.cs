namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Cambio_Tipo_Cedula : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Empleadoes", "Cedula", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Empleadoes", "Cedula", c => c.Int(nullable: false));
        }
    }
}
