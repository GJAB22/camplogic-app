namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambioEntradasOrden : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Entradas", "OrdenCompraId", "dbo.Orden_Compra");
            DropIndex("dbo.Entradas", new[] { "OrdenCompraId" });
            AlterColumn("dbo.Entradas", "OrdenCompraId", c => c.Int());
            CreateIndex("dbo.Entradas", "OrdenCompraId");
            AddForeignKey("dbo.Entradas", "OrdenCompraId", "dbo.Orden_Compra", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Entradas", "OrdenCompraId", "dbo.Orden_Compra");
            DropIndex("dbo.Entradas", new[] { "OrdenCompraId" });
            AlterColumn("dbo.Entradas", "OrdenCompraId", c => c.Int(nullable: false));
            CreateIndex("dbo.Entradas", "OrdenCompraId");
            AddForeignKey("dbo.Entradas", "OrdenCompraId", "dbo.Orden_Compra", "Id", cascadeDelete: true);
        }
    }
}
