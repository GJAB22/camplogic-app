namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FotoPerfil : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Empleadoes", "FotoPerfil", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Empleadoes", "FotoPerfil", c => c.Binary());
        }
    }
}
