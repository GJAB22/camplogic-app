namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPatrullaEmpleadoTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PatrullaEmpleadoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FechaAsignacion = c.DateTime(nullable: false),
                        PatrullaId = c.Int(nullable: false),
                        EmpleadoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Empleadoes", t => t.EmpleadoId, cascadeDelete: true)
                .ForeignKey("dbo.Patrullas", t => t.PatrullaId, cascadeDelete: true)
                .Index(t => t.PatrullaId)
                .Index(t => t.EmpleadoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PatrullaEmpleadoes", "PatrullaId", "dbo.Patrullas");
            DropForeignKey("dbo.PatrullaEmpleadoes", "EmpleadoId", "dbo.Empleadoes");
            DropIndex("dbo.PatrullaEmpleadoes", new[] { "EmpleadoId" });
            DropIndex("dbo.PatrullaEmpleadoes", new[] { "PatrullaId" });
            DropTable("dbo.PatrullaEmpleadoes");
        }
    }
}
