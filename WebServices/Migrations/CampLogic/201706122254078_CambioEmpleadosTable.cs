namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambioEmpleadosTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Empleadoes", "PatrullaId", "dbo.Patrullas");
            DropIndex("dbo.Empleadoes", new[] { "PatrullaId" });
            AddColumn("dbo.Empleadoes", "NombrePatrulla", c => c.String());
            DropColumn("dbo.Empleadoes", "PatrullaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Empleadoes", "PatrullaId", c => c.Int(nullable: false));
            DropColumn("dbo.Empleadoes", "NombrePatrulla");
            CreateIndex("dbo.Empleadoes", "PatrullaId");
            AddForeignKey("dbo.Empleadoes", "PatrullaId", "dbo.Patrullas", "Id", cascadeDelete: true);
        }
    }
}
