namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTemporadasTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Temporadas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FechaInicial = c.DateTime(nullable: false),
                        FechaFinal = c.DateTime(nullable: false),
                        CantidadCampistas = c.Int(nullable: false),
                        Epoca = c.String(),
                        NumeroTemporada = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Temporadas");
        }
    }
}
