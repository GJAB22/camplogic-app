namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class COrden_Compra : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Orden_Compra", "CotizacionId", "dbo.Cotizacion_Producto");
            DropForeignKey("dbo.Orden_Compra", "ProveedorId", "dbo.Proveedors");
            DropForeignKey("dbo.OrdenProds", "OrdenCompraId", "dbo.Orden_Compra");
            DropForeignKey("dbo.OrdenProds", "ProductoId", "dbo.Productoes");
            DropIndex("dbo.Orden_Compra", new[] { "ProveedorId" });
            DropIndex("dbo.Orden_Compra", new[] { "CotizacionId" });
            DropIndex("dbo.OrdenProds", new[] { "ProductoId" });
            DropIndex("dbo.OrdenProds", new[] { "OrdenCompraId" });
            DropColumn("dbo.Orden_Compra", "ProveedorId");
            DropColumn("dbo.Orden_Compra", "CotizacionId");
            DropTable("dbo.OrdenProds");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.OrdenProds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CantidadProd = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                        OrdenCompraId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Orden_Compra", "CotizacionId", c => c.Int(nullable: false));
            AddColumn("dbo.Orden_Compra", "ProveedorId", c => c.Int(nullable: false));
            CreateIndex("dbo.OrdenProds", "OrdenCompraId");
            CreateIndex("dbo.OrdenProds", "ProductoId");
            CreateIndex("dbo.Orden_Compra", "CotizacionId");
            CreateIndex("dbo.Orden_Compra", "ProveedorId");
            AddForeignKey("dbo.OrdenProds", "ProductoId", "dbo.Productoes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.OrdenProds", "OrdenCompraId", "dbo.Orden_Compra", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Orden_Compra", "ProveedorId", "dbo.Proveedors", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Orden_Compra", "CotizacionId", "dbo.Cotizacion_Producto", "Id", cascadeDelete: true);
        }
    }
}
