// <auto-generated />
namespace WebServices.Migrations.CampLogic
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Add_Cotizacion_Producto : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Add_Cotizacion_Producto));
        
        string IMigrationMetadata.Id
        {
            get { return "201706020548459_Add_Cotizacion_Producto"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
