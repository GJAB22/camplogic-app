namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSubCategoriaTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SubCategorias",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.Int(nullable: false),
                        CategoriaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categorias", t => t.CategoriaId, cascadeDelete: true)
                .Index(t => t.CategoriaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SubCategorias", "CategoriaId", "dbo.Categorias");
            DropIndex("dbo.SubCategorias", new[] { "CategoriaId" });
            DropTable("dbo.SubCategorias");
        }
    }
}
