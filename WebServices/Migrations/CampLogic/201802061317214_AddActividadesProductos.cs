namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddActividadesProductos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActividadProductoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cantidad = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                        ActividadId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Actividades", t => t.ActividadId, cascadeDelete: true)
                .ForeignKey("dbo.Productoes", t => t.ProductoId, cascadeDelete: true)
                .Index(t => t.ProductoId)
                .Index(t => t.ActividadId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ActividadProductoes", "ProductoId", "dbo.Productoes");
            DropForeignKey("dbo.ActividadProductoes", "ActividadId", "dbo.Actividades");
            DropIndex("dbo.ActividadProductoes", new[] { "ActividadId" });
            DropIndex("dbo.ActividadProductoes", new[] { "ProductoId" });
            DropTable("dbo.ActividadProductoes");
        }
    }
}
