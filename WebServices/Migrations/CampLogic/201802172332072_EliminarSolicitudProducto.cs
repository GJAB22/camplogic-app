namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EliminarSolicitudProducto : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Solicitud_Producto", "ProductoId", "dbo.Productoes");
            DropForeignKey("dbo.Solicitud_Producto", "SolicitudId", "dbo.Solicituds");
            DropIndex("dbo.Solicitud_Producto", new[] { "SolicitudId" });
            DropIndex("dbo.Solicitud_Producto", new[] { "ProductoId" });
            DropTable("dbo.Solicitud_Producto");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Solicitud_Producto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cantidad = c.Int(nullable: false),
                        Resolucion = c.Boolean(nullable: false),
                        SolicitudId = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Solicitud_Producto", "ProductoId");
            CreateIndex("dbo.Solicitud_Producto", "SolicitudId");
            AddForeignKey("dbo.Solicitud_Producto", "SolicitudId", "dbo.Solicituds", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Solicitud_Producto", "ProductoId", "dbo.Productoes", "Id", cascadeDelete: true);
        }
    }
}
