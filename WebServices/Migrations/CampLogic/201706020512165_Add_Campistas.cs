namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Campistas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Campistas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cedula = c.Int(nullable: false),
                        Nombre = c.String(),
                        Apellido = c.String(),
                        Fecha_Nacimiento = c.DateTime(nullable: false),
                        RepresentanteId = c.Int(nullable: false),
                        PatrullaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Patrullas", t => t.PatrullaId, cascadeDelete: true)
                .ForeignKey("dbo.Representantes", t => t.RepresentanteId, cascadeDelete: true)
                .Index(t => t.RepresentanteId)
                .Index(t => t.PatrullaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Campistas", "RepresentanteId", "dbo.Representantes");
            DropForeignKey("dbo.Campistas", "PatrullaId", "dbo.Patrullas");
            DropIndex("dbo.Campistas", new[] { "PatrullaId" });
            DropIndex("dbo.Campistas", new[] { "RepresentanteId" });
            DropTable("dbo.Campistas");
        }
    }
}
