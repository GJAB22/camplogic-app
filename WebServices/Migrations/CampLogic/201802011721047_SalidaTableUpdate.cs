namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SalidaTableUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Salidas_Producto", "SalidaPuntual", c => c.Boolean(nullable: false));
            AddColumn("dbo.Salidas_Producto", "EstaPendiente", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Salidas_Producto", "EstaPendiente");
            DropColumn("dbo.Salidas_Producto", "SalidaPuntual");
        }
    }
}
