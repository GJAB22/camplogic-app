namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Entradas", "Fecha", c => c.DateTime(nullable: false));
            AddColumn("dbo.Salidas", "Fecha", c => c.DateTime(nullable: false));
            AddColumn("dbo.Salidas", "Cantidad", c => c.Int(nullable: false));
            DropColumn("dbo.Entradas", "Fecha_Entrada");
            DropColumn("dbo.Salidas", "Fecha_Salida");
            DropColumn("dbo.Salidas", "Cantidad_Entregada");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Salidas", "Cantidad_Entregada", c => c.Int(nullable: false));
            AddColumn("dbo.Salidas", "Fecha_Salida", c => c.DateTime(nullable: false));
            AddColumn("dbo.Entradas", "Fecha_Entrada", c => c.DateTime(nullable: false));
            DropColumn("dbo.Salidas", "Cantidad");
            DropColumn("dbo.Salidas", "Fecha");
            DropColumn("dbo.Entradas", "Fecha");
        }
    }
}
