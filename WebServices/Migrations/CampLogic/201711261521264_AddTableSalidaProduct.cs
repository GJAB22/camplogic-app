namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTableSalidaProduct : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Salidas_Producto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cantidad = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                        SalidaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Productoes", t => t.ProductoId, cascadeDelete: true)
                .ForeignKey("dbo.Salidas", t => t.SalidaId, cascadeDelete: true)
                .Index(t => t.ProductoId)
                .Index(t => t.SalidaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Salidas_Producto", "SalidaId", "dbo.Salidas");
            DropForeignKey("dbo.Salidas_Producto", "ProductoId", "dbo.Productoes");
            DropIndex("dbo.Salidas_Producto", new[] { "SalidaId" });
            DropIndex("dbo.Salidas_Producto", new[] { "ProductoId" });
            DropTable("dbo.Salidas_Producto");
        }
    }
}
