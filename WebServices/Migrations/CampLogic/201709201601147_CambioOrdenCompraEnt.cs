namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambioOrdenCompraEnt : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Orden_Compra", "Fecha_Entrega");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orden_Compra", "Fecha_Entrega", c => c.DateTime(nullable: false));
        }
    }
}
