namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductoTableUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Productoes", "SubCategoriaId", c => c.Int(nullable: true));
            CreateIndex("dbo.Productoes", "SubCategoriaId");
            AddForeignKey("dbo.Productoes", "SubCategoriaId", "dbo.SubCategorias", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Productoes", "SubCategoriaId", "dbo.SubCategorias");
            DropIndex("dbo.Productoes", new[] { "SubCategoriaId" });
            DropColumn("dbo.Productoes", "SubCategoriaId");
        }
    }
}
