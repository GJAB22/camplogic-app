namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTemporadasPatrullaTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Temporada_Patrulla",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CantidadCampistas = c.Int(nullable: false),
                        TemporadaId = c.Int(nullable: false),
                        PatrullaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Patrullas", t => t.PatrullaId, cascadeDelete: true)
                .ForeignKey("dbo.Temporadas", t => t.TemporadaId, cascadeDelete: true)
                .Index(t => t.TemporadaId)
                .Index(t => t.PatrullaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Temporada_Patrulla", "TemporadaId", "dbo.Temporadas");
            DropForeignKey("dbo.Temporada_Patrulla", "PatrullaId", "dbo.Patrullas");
            DropIndex("dbo.Temporada_Patrulla", new[] { "PatrullaId" });
            DropIndex("dbo.Temporada_Patrulla", new[] { "TemporadaId" });
            DropTable("dbo.Temporada_Patrulla");
        }
    }
}
