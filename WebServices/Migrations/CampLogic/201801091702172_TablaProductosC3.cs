namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TablaProductosC3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Productoes", "Articulo", c => c.String());
            AddColumn("dbo.Productoes", "CantidadIdeal", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Productoes", "CantidadIdeal");
            DropColumn("dbo.Productoes", "Articulo");
        }
    }
}
