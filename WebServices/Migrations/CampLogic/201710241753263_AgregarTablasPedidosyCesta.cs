namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregarTablasPedidosyCesta : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cestas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cantidad = c.Int(nullable: false),
                        Precio = c.Single(nullable: false),
                        EmpleadoId = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Empleadoes", t => t.EmpleadoId, cascadeDelete: true)
                .ForeignKey("dbo.Productoes", t => t.ProductoId, cascadeDelete: true)
                .Index(t => t.EmpleadoId)
                .Index(t => t.ProductoId);
            
            CreateTable(
                "dbo.Pedidos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cantidad = c.Int(nullable: false),
                        Precio = c.Single(nullable: false),
                        EmpleadoId = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                        OrdenCompraId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Empleadoes", t => t.EmpleadoId, cascadeDelete: true)
                .ForeignKey("dbo.Orden_Compra", t => t.OrdenCompraId, cascadeDelete: true)
                .ForeignKey("dbo.Productoes", t => t.ProductoId, cascadeDelete: true)
                .Index(t => t.EmpleadoId)
                .Index(t => t.ProductoId)
                .Index(t => t.OrdenCompraId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pedidos", "ProductoId", "dbo.Productoes");
            DropForeignKey("dbo.Pedidos", "OrdenCompraId", "dbo.Orden_Compra");
            DropForeignKey("dbo.Pedidos", "EmpleadoId", "dbo.Empleadoes");
            DropForeignKey("dbo.Cestas", "ProductoId", "dbo.Productoes");
            DropForeignKey("dbo.Cestas", "EmpleadoId", "dbo.Empleadoes");
            DropIndex("dbo.Pedidos", new[] { "OrdenCompraId" });
            DropIndex("dbo.Pedidos", new[] { "ProductoId" });
            DropIndex("dbo.Pedidos", new[] { "EmpleadoId" });
            DropIndex("dbo.Cestas", new[] { "ProductoId" });
            DropIndex("dbo.Cestas", new[] { "EmpleadoId" });
            DropTable("dbo.Pedidos");
            DropTable("dbo.Cestas");
        }
    }
}
