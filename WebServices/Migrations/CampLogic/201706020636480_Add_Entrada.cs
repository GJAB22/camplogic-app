namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Entrada : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Entradas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fecha_Entrada = c.DateTime(nullable: false),
                        Cantidad = c.Int(nullable: false),
                        Valor_Total = c.Single(nullable: false),
                        ProductoId = c.Int(nullable: false),
                        ProveedorId = c.Int(nullable: false),
                        OrdenCompra_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orden_Compra", t => t.OrdenCompra_Id)
                .ForeignKey("dbo.Productoes", t => t.ProductoId, cascadeDelete: true)
                .ForeignKey("dbo.Proveedors", t => t.ProveedorId, cascadeDelete: true)
                .Index(t => t.ProductoId)
                .Index(t => t.ProveedorId)
                .Index(t => t.OrdenCompra_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Entradas", "ProveedorId", "dbo.Proveedors");
            DropForeignKey("dbo.Entradas", "ProductoId", "dbo.Productoes");
            DropForeignKey("dbo.Entradas", "OrdenCompra_Id", "dbo.Orden_Compra");
            DropIndex("dbo.Entradas", new[] { "OrdenCompra_Id" });
            DropIndex("dbo.Entradas", new[] { "ProveedorId" });
            DropIndex("dbo.Entradas", new[] { "ProductoId" });
            DropTable("dbo.Entradas");
        }
    }
}
