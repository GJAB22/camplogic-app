namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregarCedulaProveedor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Proveedors", "Cedula", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Proveedors", "Cedula");
        }
    }
}
