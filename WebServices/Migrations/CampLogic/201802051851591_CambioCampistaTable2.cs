namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambioCampistaTable2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Campistas", "NombrePatrulla", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Campistas", "NombrePatrulla");
        }
    }
}
