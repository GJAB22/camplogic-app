namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambioCestayPedido : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cestas", "ValorTotal", c => c.Single(nullable: false));
            AddColumn("dbo.Pedidos", "ValorTotal", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pedidos", "ValorTotal");
            DropColumn("dbo.Cestas", "ValorTotal");
        }
    }
}
