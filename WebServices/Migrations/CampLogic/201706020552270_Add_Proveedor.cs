namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Proveedor : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Proveedors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RIF = c.String(),
                        Nombre = c.String(),
                        Apellido = c.String(),
                        Descripcion = c.String(),
                        Telefono_1 = c.String(),
                        Telefono_2 = c.String(),
                        Correo = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Proveedors");
        }
    }
}
