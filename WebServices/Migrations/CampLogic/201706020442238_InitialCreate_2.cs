namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate_2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Empleadoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FotoPerfil = c.Binary(),
                        Nombre = c.String(),
                        Apellido = c.String(),
                        Cedula = c.Int(nullable: false),
                        Cargo = c.String(),
                        Fecha_Nacimiento = c.DateTime(nullable: false),
                        Correo = c.String(),
                        Telefono = c.String(),
                        Disponibilidad = c.Boolean(nullable: false),
                        UserId = c.Int(nullable: false),
                        PatrullaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Patrullas", t => t.PatrullaId, cascadeDelete: true)
                .Index(t => t.PatrullaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Empleadoes", "PatrullaId", "dbo.Patrullas");
            DropIndex("dbo.Empleadoes", new[] { "PatrullaId" });
            DropTable("dbo.Empleadoes");
        }
    }
}
