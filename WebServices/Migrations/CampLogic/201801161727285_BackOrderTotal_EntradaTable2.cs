namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BackOrderTotal_EntradaTable2 : DbMigration
    {
        public override void Up()
        {
            
            AddColumn("dbo.Entradas", "BackOrderTotal", c => c.Int(nullable: false));
            
        }
        
        public override void Down()
        {
            AddColumn("dbo.Entradas", "ProductoId", c => c.Int(nullable: false));
            AddColumn("dbo.Entradas", "BackOrder", c => c.Int(nullable: false));
            DropColumn("dbo.Entradas", "BackOrderTotal");
            CreateIndex("dbo.Entradas", "ProductoId");
            AddForeignKey("dbo.Entradas", "ProductoId", "dbo.Productoes", "Id", cascadeDelete: true);
        }
    }
}
