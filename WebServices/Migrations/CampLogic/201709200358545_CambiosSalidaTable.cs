namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambiosSalidaTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Salidas", "EntradaId", "dbo.Entradas");
            DropIndex("dbo.Salidas", new[] { "EntradaId" });
            DropColumn("dbo.Salidas", "EntradaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Salidas", "EntradaId", c => c.Int(nullable: false));
            CreateIndex("dbo.Salidas", "EntradaId");
            AddForeignKey("dbo.Salidas", "EntradaId", "dbo.Entradas", "Id", cascadeDelete: true);
        }
    }
}
