namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambiosCestayOrdenFE : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cestas", "FechaEntrega", c => c.DateTime(nullable: false));
            AddColumn("dbo.Orden_Compra", "FechaEntrega", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orden_Compra", "FechaEntrega");
            DropColumn("dbo.Cestas", "FechaEntrega");
        }
    }
}
