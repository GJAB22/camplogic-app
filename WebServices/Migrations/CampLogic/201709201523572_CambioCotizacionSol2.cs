namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambioCotizacionSol2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Cotizacions", "SolicitudId", "dbo.Solicituds");
            DropIndex("dbo.Cotizacions", new[] { "SolicitudId" });
            DropColumn("dbo.Cotizacions", "SolicitudId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Cotizacions", "SolicitudId", c => c.Int(nullable: false));
            CreateIndex("dbo.Cotizacions", "SolicitudId");
            AddForeignKey("dbo.Cotizacions", "SolicitudId", "dbo.Solicituds", "Id", cascadeDelete: true);
        }
    }
}
