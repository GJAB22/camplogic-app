namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambioActividadProducto : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ActividadProductoes", "EsIndividual", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ActividadProductoes", "EsIndividual");
        }
    }
}
