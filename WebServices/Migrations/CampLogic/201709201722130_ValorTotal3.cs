namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ValorTotal3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Entradas", "Valor_Total", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Entradas", "Valor_Total", c => c.Double(nullable: false));
        }
    }
}
