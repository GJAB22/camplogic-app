namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Entrada_OrdenCompra_2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Entradas", "ProductoId", "dbo.Productoes");
            DropForeignKey("dbo.Entradas", "ProveedorId", "dbo.Proveedors");
            DropForeignKey("dbo.Entradas", "OrdenCompra_Id", "dbo.Orden_Compra");
            DropIndex("dbo.Entradas", new[] { "ProductoId" });
            DropIndex("dbo.Entradas", new[] { "ProveedorId" });
            DropIndex("dbo.Entradas", new[] { "OrdenCompra_Id" });
            RenameColumn(table: "dbo.Entradas", name: "OrdenCompra_Id", newName: "OrdenCompraId");
            AlterColumn("dbo.Entradas", "OrdenCompraId", c => c.Int(nullable: false));
            CreateIndex("dbo.Entradas", "OrdenCompraId");
            AddForeignKey("dbo.Entradas", "OrdenCompraId", "dbo.Orden_Compra", "Id", cascadeDelete: true);
            DropColumn("dbo.Entradas", "ProductoId");
            DropColumn("dbo.Entradas", "ProveedorId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Entradas", "ProveedorId", c => c.Int(nullable: false));
            AddColumn("dbo.Entradas", "ProductoId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Entradas", "OrdenCompraId", "dbo.Orden_Compra");
            DropIndex("dbo.Entradas", new[] { "OrdenCompraId" });
            AlterColumn("dbo.Entradas", "OrdenCompraId", c => c.Int());
            RenameColumn(table: "dbo.Entradas", name: "OrdenCompraId", newName: "OrdenCompra_Id");
            CreateIndex("dbo.Entradas", "OrdenCompra_Id");
            CreateIndex("dbo.Entradas", "ProveedorId");
            CreateIndex("dbo.Entradas", "ProductoId");
            AddForeignKey("dbo.Entradas", "OrdenCompra_Id", "dbo.Orden_Compra", "Id");
            AddForeignKey("dbo.Entradas", "ProveedorId", "dbo.Proveedors", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Entradas", "ProductoId", "dbo.Productoes", "Id", cascadeDelete: true);
        }
    }
}
