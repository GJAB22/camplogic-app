namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductoTableCambios : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Productoes", "UbicacionId", c => c.Int(nullable: true));
            CreateIndex("dbo.Productoes", "UbicacionId");
            AddForeignKey("dbo.Productoes", "UbicacionId", "dbo.Ubicacions", "Id", cascadeDelete: true);
            DropColumn("dbo.Productoes", "Ubicacion");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Productoes", "Ubicacion", c => c.String());
            DropForeignKey("dbo.Productoes", "UbicacionId", "dbo.Ubicacions");
            DropIndex("dbo.Productoes", new[] { "UbicacionId" });
            DropColumn("dbo.Productoes", "UbicacionId");
        }
    }
}
