namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambioCotiSol : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cotizacions", "Number", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cotizacions", "Number");
        }
    }
}
