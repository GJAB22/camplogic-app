namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Patrulla_Actividad1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Representantes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Apellido = c.String(),
                        Telefono_1 = c.String(),
                        Telefono_2 = c.String(),
                        Correo = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Representantes");
        }
    }
}
