namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambioPatrullaActTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Patrulla_Actividad", "FechaAsignacion", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Patrulla_Actividad", "FechaAsignacion");
        }
    }
}
