namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CedRep : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Representantes", "Cedula", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Representantes", "Cedula");
        }
    }
}
