namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambioRepresentante : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Representantes", "Telefono_2");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Representantes", "Telefono_2", c => c.String());
        }
    }
}
