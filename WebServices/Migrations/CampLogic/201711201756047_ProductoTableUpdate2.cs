namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductoTableUpdate2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Productoes", "Tipo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Productoes", "Tipo", c => c.String());
        }
    }
}
