namespace WebServices.Migrations.CampLogic
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ENCambio : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Entradas", "ProductoId", c => c.Int(nullable: false));
            CreateIndex("dbo.Entradas", "ProductoId");
            AddForeignKey("dbo.Entradas", "ProductoId", "dbo.Productoes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Entradas", "ProductoId", "dbo.Productoes");
            DropIndex("dbo.Entradas", new[] { "ProductoId" });
            DropColumn("dbo.Entradas", "ProductoId");
        }
    }
}
