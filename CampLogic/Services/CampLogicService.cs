﻿using CampLogic.Model;
using CampLogic.WSModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CampLogic.Services
{
    public class CampLogicService
    {
        
        //LocalHost

        //Registro y Login
        public async Task<bool> RegisterAsync(string correo, string password, string confirmPassword)
       {
           //Registrar Cuenta
           var client = new HttpClient();

           var model = new RegisterBindingModel
           {
               Email = correo,
               Password = password,
               ConfirmPassword = confirmPassword
           };

           var json = JsonConvert.SerializeObject(model);

           HttpContent content = new StringContent(json);

        
           content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

           var responseR = await client.PostAsync
               ("http://localhost:62431/api/Account/Register", content);

            
            return responseR.IsSuccessStatusCode;

            
        }

        public async Task<string> LogInAsync(string userName, string password)
        {
            var keyValues = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("username",userName),
                new KeyValuePair<string, string>("password",password),
                new KeyValuePair<string, string>("grant_type","password")
            };

            var request = new HttpRequestMessage(
                HttpMethod.Post, "http://localhost:62431/Token");

            request.Content = new FormUrlEncodedContent(keyValues);

            var client = new HttpClient();
            var response = await client.SendAsync(request);

            var jwt = await response.Content.ReadAsStringAsync();

            JObject jwtDynamic = JsonConvert.DeserializeObject<dynamic>(jwt);

            var accessToken = jwtDynamic.Value<string>("access_token");

            return accessToken;
        }

        //Noticias
        public async Task<bool> PostNoticiaAsync(Noticias noticia)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(noticia);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Noticias", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<ObservableCollection<Noticias>> GetNoticiaAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Noticias");
            var noticias = JsonConvert.DeserializeObject<ObservableCollection<Noticias>>(json);

            return new ObservableCollection<Noticias>(noticias.OrderByDescending(e => e.Fecha_Noticia));
        }
   
        //Empleados
        public async Task<List<Empleado>> GetEmpleadosAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Empleados");

            var empleados = JsonConvert.DeserializeObject<List<Empleado>>(json);

            return empleados;

        }

        public async Task<Empleado> GetEmpleadoAsync(Empleado emp)
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("" + emp.Id);
            var empleado = JsonConvert.DeserializeObject<Empleado>(json);

            return empleado;

        }

        public async Task<Empleado> GetEmpleadoCedulaAsync(string cedula)
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Empleados/" + cedula);
            var empleado = JsonConvert.DeserializeObject<Empleado>(json);

            return empleado;
        }

        public async Task PutEmpleadoAsync(Empleado emp)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(emp);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PutAsync("http://localhost:62431/api/Empleados/" + emp.Cedula, content);
        }

        public async Task<bool> PostEmpleadoAsync(Empleado emp, string accessToken)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accessToken);

            var json = JsonConvert.SerializeObject(emp);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Empleados", content);

            return response.IsSuccessStatusCode;
        }





        //Logistica

        //Campistas
        public async Task<ObservableCollection<Campistas>> GetCampistasAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Campistas");

            var campistas = JsonConvert.DeserializeObject<ObservableCollection<Campistas>>(json);

            return campistas;
        }

        public async Task<bool> PostCampistaAsync(Campistas campista)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(campista);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Campistas", content);

            return response.IsSuccessStatusCode;

        }

        public async Task PutCampistaAsync (Campistas campista)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(campista);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PutAsync("http://localhost:62431/api/Campistas/" + campista.Id, content);
        }

        //Campista-Patrulla
        public async Task<ObservableCollection<PatrullaCampista>> GetPatrullasCampistasAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/PatrullaCampistas");

            var patrullas = JsonConvert.DeserializeObject<ObservableCollection<PatrullaCampista>>(json);

            return patrullas;
        }

        public async Task<bool> PostPatrullaCampistaAsync(PatrullaCampista patrulla)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(patrulla);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/PatrullaCampistas", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> DeletePatrullaCampistaAsync(int id, PatrullaCampista prod)
        {
            var client = new HttpClient();

            var response = await client.DeleteAsync("http://localhost:62431/api/PatrullaCampistas/" + id);

            return response.IsSuccessStatusCode;
        }

        //Campista-Representante
        public async Task<ObservableCollection<CampistaRepresentante>> GetCampistasRepresentanteAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/CampistaRepresentantes");

            var patrullas = JsonConvert.DeserializeObject<ObservableCollection<CampistaRepresentante>>(json);

            return patrullas;
        }

        public async Task<bool> PostRepresentanteCampistaAsync(CampistaRepresentante camprep)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(camprep);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/CampistaRepresentantes", content);

            return response.IsSuccessStatusCode;
        }

        //Representante
        public async Task<bool> PostRepresentanteAsync(Representante representante)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(representante);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Representantes", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<Representante> GetRepresentanteAsync(string cedula)
        {
            var client = new HttpClient();
            var response = await client.GetStringAsync("http://localhost:62431/api/Representantes/" + cedula);
            if ( response == "null")
            {
                return null; 
            }
            else
            {
                var json = await client.GetStringAsync("http://localhost:62431/api/Representantes/" + cedula);
                var representante = JsonConvert.DeserializeObject<Representante>(json);

                return representante;
            }
            
        } 

        //Patrulla
        public async Task<ObservableCollection<Patrulla>> GetPatrullasAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Patrullas");

            var patrullas = JsonConvert.DeserializeObject<ObservableCollection<Patrulla>>(json);

            return patrullas;
        }

        public async Task<bool> PostPatrullaAsync(Patrulla patrulla)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(patrulla);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Patrullas", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<Patrulla> GetPatrullaNombreAsync(string nombre)
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Patrullas/" + nombre);
            var patrulla = JsonConvert.DeserializeObject<Patrulla>(json);

            return patrulla;
        }

        //Patrulla-Empleado
        public async Task<List<PatrullaEmpleado>> GetPatrullasEmpleadoAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/PatrullaEmpleadoes");

            var patrullas = JsonConvert.DeserializeObject<List<PatrullaEmpleado>>(json);

            return patrullas;
        }

        public async Task<bool> PostPatrullaEmpleadoAsync(PatrullaEmpleado patrulla)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(patrulla);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/PatrullaEmpleadoes", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> DeletePatrullaEmpleadoAsync(int id, PatrullaEmpleado prod)
        {
            var client = new HttpClient();

            var response = await client.DeleteAsync("http://localhost:62431/api/PatrullaEmpleadoes/" + id);

            return response.IsSuccessStatusCode;
        }

        //Actividades
        public async Task<bool> PostActividadAsync(Actividades actividad)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(actividad);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Actividades", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<Actividades> GetActividadAsync(string nombre)
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Actividades/" + nombre);
            var actividad = JsonConvert.DeserializeObject<Actividades>(json);

            return actividad;
        }

        public async Task<ObservableCollection<Actividades>> GetActividadesAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Actividades");

            var actividades = JsonConvert.DeserializeObject<ObservableCollection<Actividades>>(json);

            return actividades;
        }

        //Actividades-Patrulla
        public async Task<List<Patrulla_Actividad>> GetActividadesPatrullaAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Patrulla_Actividad");

            var actividades = JsonConvert.DeserializeObject<List<Patrulla_Actividad>>(json);

            return actividades;
        }

        public async Task<bool> PostPatrullaActividadAsync(Patrulla_Actividad PActividad)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(PActividad);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Patrulla_Actividad", content);

            return response.IsSuccessStatusCode;
        }

        //Actividades-Productos
        public async Task<List<ActividadProducto>> GetActividadesProductosAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/ActividadProductoes");

            var actividades = JsonConvert.DeserializeObject<List<ActividadProducto>>(json);

            return actividades;
        }

        public async Task<bool> PostActividadProductosAsync(ActividadProducto PActividad)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(PActividad);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/ActividadProductoes", content);

            return response.IsSuccessStatusCode;
        }

        //Temporadas
        public async Task<ObservableCollection<Temporadas>> GetTemporadasAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Temporadas");

            var temporadas = JsonConvert.DeserializeObject<ObservableCollection<Temporadas>>(json);

            return temporadas;
        }

        public async Task<bool> PostTemporadaAsync(Temporadas temporada)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(temporada);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Temporadas", content);

            return response.IsSuccessStatusCode;
        }

        //Temporadas-Patrullas
        public async Task<ObservableCollection<Temporada_Patrulla>> GetTemporadasPatrullasAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Temporada_Patrulla");

            var temporadas = JsonConvert.DeserializeObject<ObservableCollection<Temporada_Patrulla>>(json);

            return temporadas;
        }

        public async Task<bool> PostTemporadaPatrullaAsync(Temporada_Patrulla temporada)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(temporada);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Temporada_Patrulla", content);

            return response.IsSuccessStatusCode;
        }










        //Inventario

        //Producto
        public async Task<List<Producto>> GetProductosAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Productos");

            var productos = JsonConvert.DeserializeObject<List<Producto>>(json);

            return productos;
        }

        public async Task<Producto> GetProductoAsync(int Id)
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Productos/" + Id);
            var producto = JsonConvert.DeserializeObject<Producto>(json);

            return producto;
        }

        public async Task<bool> PostProductoAsync(Producto prod)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(prod);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Productos", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> PutProductAsync(Producto prod)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(prod);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PutAsync("http://localhost:62431/api/Productos/" + prod.Id, content);

            return response.IsSuccessStatusCode;
        }

        //Categoria
        public async Task<List<Categoria>> GetCategoriasAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Categorias");

            var categorias = JsonConvert.DeserializeObject<List<Categoria>>(json);

            return categorias;
        }

        public async Task<bool> PostCategoriaAsync(Categoria categoria)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(categoria);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Categorias", content);

            return response.IsSuccessStatusCode;
        }
        
        //Sub-Categoria
        public async Task<List<SubCategoria>> GetSubCategoriasAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/SubCategorias");

            var subcategorias = JsonConvert.DeserializeObject<List<SubCategoria>>(json);

            return subcategorias;
        }

        public async Task<bool> PostSubCategoriaAsync(SubCategoria subcategoria)
        {
            var client = new HttpClient();
            var json = JsonConvert.SerializeObject(subcategoria);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/SubCategorias", content);

            return response.IsSuccessStatusCode;
        }

        //Ubicacion
        public async Task<List<Ubicacion>> GetUbicacionAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Ubicacions");

            var ubicaciones = JsonConvert.DeserializeObject<List<Ubicacion>>(json);

            return ubicaciones;
        }

        public async Task<bool> PostUbicacionAsync(Ubicacion ubicacion)
        {
            var client = new HttpClient();
            var json = JsonConvert.SerializeObject(ubicacion);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Ubicacions", content);

            return response.IsSuccessStatusCode;
        }

        
        //Cotizacion
        public async Task<bool> PostCotizacionAsync(Cotizacion cot)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(cot);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            var response = await client.PostAsync("http://localhost:62431/api/Cotizaciones", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> PostCotizacionProdAsync(Cotizacion_Producto cot)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(cot);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Cotizacion_Producto", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<List<Cotizacion>> GetCotizacionListAsync()
        {
            var client = new HttpClient();
            var json = await client.GetStringAsync("http://localhost:62431/api/Cotizaciones");
            var cotizaciones = JsonConvert.DeserializeObject<List<Cotizacion>>(json);
            return cotizaciones;
        }

        public async Task<List<Cotizacion_Producto>> GetCotizacionProdAsync()
        {
            var client = new HttpClient();
            var json = await client.GetStringAsync("http://localhost:62431/api/Cotizacion_Producto");
            var cotizaciones = JsonConvert.DeserializeObject<List<Cotizacion_Producto>>(json);
            return cotizaciones;
        }

        //Cesta
        public async Task<bool> PostProdCestaAsync(Cesta prod)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(prod);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Cestas", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<ObservableCollection<Cesta>> GetCestaAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Cestas");

            var cesta = JsonConvert.DeserializeObject<ObservableCollection<Cesta>>(json);

            return cesta;
        }

        public async Task<bool> DeleteCestaAsync(int id, Cesta prod)
        {
            var client = new HttpClient();

            var response = await client.DeleteAsync("http://localhost:62431/api/Cestas/"+id);

            return response.IsSuccessStatusCode;
        }


        //Pedidos
        public async Task<bool> PostPedidoAsync(Pedidos prod)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(prod);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Pedidos", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<List<Pedidos>> GetPedidosAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Pedidos");

            var pedidos = JsonConvert.DeserializeObject<List<Pedidos>>(json);

            return pedidos;
        }


        //Orden de Compra
        public async Task<bool> PostOrdenCompraAsync(Orden_Compra ordenCompra)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(ordenCompra);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Orden_Compra", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<List<Orden_Compra>> GetOrdenesCompraAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Orden_Compra");

            var ordenesCompra = JsonConvert.DeserializeObject<List<Orden_Compra>>(json);

            return ordenesCompra;
        }

        public async Task<Orden_Compra> GetOrdenCompraAsync(int id)
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Orden_Compra/" + id);
            var OrdenFecha = JsonConvert.DeserializeObject<Orden_Compra>(json);

            return OrdenFecha;
        }

        public async Task<bool> PutOrdenCompraAsync(Orden_Compra orden)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(orden);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PutAsync("http://localhost:62431/api/Orden_Compra/" + orden.Id, content);

            return response.IsSuccessStatusCode;
        }
        
        
        //Entradas
        public async Task<bool> PostEntradaAsync(Entrada entrada)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(entrada);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Entradas", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<List<Entrada>> GetEntradasAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Entradas");

            var entradas = JsonConvert.DeserializeObject<List<Entrada>>(json);

            return entradas;
        }

        //Entradas-Productos
        public async Task<List<EntradaProducto>> GetEntradasProdsAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/EntradaProductos");

            var entradas = JsonConvert.DeserializeObject<List<EntradaProducto>>(json);

            return entradas;
        }

        public async Task<bool> PostEntradaProdAsync(EntradaProducto entrada)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(entrada);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/EntradaProductos", content);

            return response.IsSuccessStatusCode;
        }

        //Reintegros
        public async Task<bool> PostReintegroAsync(Reintegros entrada)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(entrada);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Reintegros", content);

            return response.IsSuccessStatusCode;
        }
        public async Task<List<Reintegros>> GetReintegrosAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Reintegros");

            var entradas = JsonConvert.DeserializeObject<List<Reintegros>>(json);

            return entradas;
        }

        //Reintegros-Productos
        public async Task<bool> PostReintegroProdAsync(ReintegroProducto entrada)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(entrada);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/ReintegroProductoes", content);

            return response.IsSuccessStatusCode;
        }
        public async Task<List<ReintegroProducto>> GetReintegrosProdAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/ReintegroProductoes");

            var entradas = JsonConvert.DeserializeObject<List<ReintegroProducto>>(json);

            return entradas;
        }

        //Salidas
        public async Task<bool> PostSalidasAsync(Salida salida)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(salida);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Salidas", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<List<Salida>> GetSalidasAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Salidas");

            var salidas = JsonConvert.DeserializeObject<List<Salida>>(json);

            return salidas;
        }
        

        //Salidas Productos
        public async Task<List<Salidas_Producto>> GetSalidasProdsAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Salidas_Producto");

            var salidas = JsonConvert.DeserializeObject<List<Salidas_Producto>>(json);

            return salidas;
        }

        public async Task<bool> PostSalidasProdAsync(Salidas_Producto salida)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(salida);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Salidas_Producto", content);

            return response.IsSuccessStatusCode;
        }

        public async Task PutSalidaProdsAsync(Salidas_Producto salidap)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(salidap);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PutAsync("http://localhost:62431/api/Salidas_Producto/" + salidap.Id, content);
        }

        //Proveedor
        public async Task<ObservableCollection<Proveedor>> GetProveedoresAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://localhost:62431/api/Proveedores");

            var proveedor = JsonConvert.DeserializeObject<ObservableCollection<Proveedor>>(json);

            return proveedor;

        }

        public async Task<bool> PostProveedorAsync(Proveedor prov)
        {
            var client = new HttpClient();
            var json = JsonConvert.SerializeObject(prov);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://localhost:62431/api/Proveedores", content);

            return response.IsSuccessStatusCode;
        }
        
        


        /*
        //Azure

        //Registro y Login
        public async Task<bool> RegisterAsync(string correo, string password, string confirmPassword)
        {
            //Registrar Cuenta
            var client = new HttpClient();

            var model = new RegisterBindingModel
            {
                Email = correo,
                Password = password,
                ConfirmPassword = confirmPassword
            };

            var json = JsonConvert.SerializeObject(model);

            HttpContent content = new StringContent(json);


            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var responseR = await client.PostAsync
                ("http://camplogic.azurewebsites.net/api/Account/Register", content);

            var r = responseR.IsSuccessStatusCode;

            return r;


        }

        public async Task<string> LogInAsync(string userName, string password)
        {
            var keyValues = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("username",userName),
                new KeyValuePair<string, string>("password",password),
                new KeyValuePair<string, string>("grant_type","password")
            };

            var request = new HttpRequestMessage(
                HttpMethod.Post, "http://camplogic.azurewebsites.net/Token");

            request.Content = new FormUrlEncodedContent(keyValues);

            var client = new HttpClient();
            var response = await client.SendAsync(request);

            var jwt = await response.Content.ReadAsStringAsync();

            JObject jwtDynamic = JsonConvert.DeserializeObject<dynamic>(jwt);

            var accessToken = jwtDynamic.Value<string>("access_token");

            return accessToken;
        }

        //Noticias
        public async Task<bool> PostNoticiaAsync(Noticias noticia)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(noticia);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Noticias", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<ObservableCollection<Noticias>> GetNoticiaAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Noticias");
            var noticias = JsonConvert.DeserializeObject<ObservableCollection<Noticias>>(json);

            return new ObservableCollection<Noticias>(noticias.OrderByDescending(e => e.Fecha_Noticia));
        }

        //Empleados
        public async Task<List<Empleado>> GetEmpleadosAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Empleados");

            var empleados = JsonConvert.DeserializeObject<List<Empleado>>(json);

            return empleados;

        }

        public async Task<Empleado> GetEmpleadoAsync(Empleado emp)
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("" + emp.Id);
            var empleado = JsonConvert.DeserializeObject<Empleado>(json);

            return empleado;

        }

        public async Task<Empleado> GetEmpleadoCedulaAsync(string cedula)
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Empleados/" + cedula);
            var empleado = JsonConvert.DeserializeObject<Empleado>(json);

            return empleado;
        }

        public async Task PutEmpleadoAsync(Empleado emp)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(emp);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PutAsync("http://camplogic.azurewebsites.net/api/Empleados/" + emp.Cedula, content);
        }

        public async Task<bool> PostEmpleadoAsync(Empleado emp, string accessToken)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accessToken);

            var json = JsonConvert.SerializeObject(emp);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Empleados", content);

            return response.IsSuccessStatusCode;
        }





        //Logistica

        //Campistas
        public async Task<ObservableCollection<Campistas>> GetCampistasAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Campistas");

            var campistas = JsonConvert.DeserializeObject<ObservableCollection<Campistas>>(json);

            return campistas;
        }

        public async Task<bool> PostCampistaAsync(Campistas campista)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(campista);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Campistas", content);

            return response.IsSuccessStatusCode;

        }

        public async Task PutCampistaAsync(Campistas campista)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(campista);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PutAsync("http://camplogic.azurewebsites.net/api/Campistas/" + campista.Id, content);
        }

        //Campista-Patrulla
        public async Task<ObservableCollection<PatrullaCampista>> GetPatrullasCampistasAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/PatrullaCampistas");

            var patrullas = JsonConvert.DeserializeObject<ObservableCollection<PatrullaCampista>>(json);

            return patrullas;
        }

        public async Task<bool> PostPatrullaCampistaAsync(PatrullaCampista patrulla)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(patrulla);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/PatrullaCampistas", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> DeletePatrullaCampistaAsync(int id, PatrullaCampista prod)
        {
            var client = new HttpClient();

            var response = await client.DeleteAsync("http://camplogic.azurewebsites.net/api/PatrullaCampistas/" + id);

            return response.IsSuccessStatusCode;
        }

        //Campista-Representante
        public async Task<ObservableCollection<CampistaRepresentante>> GetCampistasRepresentanteAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/CampistaRepresentantes");

            var patrullas = JsonConvert.DeserializeObject<ObservableCollection<CampistaRepresentante>>(json);

            return patrullas;
        }

        public async Task<bool> PostRepresentanteCampistaAsync(CampistaRepresentante camprep)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(camprep);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/CampistaRepresentantes", content);

            return response.IsSuccessStatusCode;
        }

        //Representante
        public async Task<bool> PostRepresentanteAsync(Representante representante)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(representante);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Representantes", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<Representante> GetRepresentanteAsync(string cedula)
        {
            var client = new HttpClient();
            var response = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Representantes/" + cedula);
            if (response == "null")
            {
                return null;
            }
            else
            {
                var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Representantes/" + cedula);
                var representante = JsonConvert.DeserializeObject<Representante>(json);

                return representante;
            }

        }

        //Patrulla
        public async Task<ObservableCollection<Patrulla>> GetPatrullasAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Patrullas");

            var patrullas = JsonConvert.DeserializeObject<ObservableCollection<Patrulla>>(json);

            return patrullas;
        }

        public async Task<bool> PostPatrullaAsync(Patrulla patrulla)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(patrulla);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Patrullas", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<Patrulla> GetPatrullaNombreAsync(string nombre)
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Patrullas/" + nombre);
            var patrulla = JsonConvert.DeserializeObject<Patrulla>(json);

            return patrulla;
        }

        //Patrulla-Empleado
        public async Task<List<PatrullaEmpleado>> GetPatrullasEmpleadoAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/PatrullaEmpleadoes");

            var patrullas = JsonConvert.DeserializeObject<List<PatrullaEmpleado>>(json);

            return patrullas;
        }

        public async Task<bool> PostPatrullaEmpleadoAsync(PatrullaEmpleado patrulla)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(patrulla);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/PatrullaEmpleadoes", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> DeletePatrullaEmpleadoAsync(int id, PatrullaEmpleado prod)
        {
            var client = new HttpClient();

            var response = await client.DeleteAsync("http://camplogic.azurewebsites.net/api/PatrullaEmpleadoes/" + id);

            return response.IsSuccessStatusCode;
        }

        //Actividades
        public async Task<bool> PostActividadAsync(Actividades actividad)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(actividad);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Actividades", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<Actividades> GetActividadAsync(string nombre)
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Actividades/" + nombre);
            var actividad = JsonConvert.DeserializeObject<Actividades>(json);

            return actividad;
        }

        public async Task<ObservableCollection<Actividades>> GetActividadesAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Actividades");

            var actividades = JsonConvert.DeserializeObject<ObservableCollection<Actividades>>(json);

            return actividades;
        }

        //Actividades-Patrulla
        public async Task<List<Patrulla_Actividad>> GetActividadesPatrullaAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Patrulla_Actividad");

            var actividades = JsonConvert.DeserializeObject<List<Patrulla_Actividad>>(json);

            return actividades;
        }

        public async Task<bool> PostPatrullaActividadAsync(Patrulla_Actividad PActividad)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(PActividad);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Patrulla_Actividad", content);

            return response.IsSuccessStatusCode;
        }

        //Actividades-Productos
        public async Task<List<ActividadProducto>> GetActividadesProductosAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/ActividadProductoes");

            var actividades = JsonConvert.DeserializeObject<List<ActividadProducto>>(json);

            return actividades;
        }

        public async Task<bool> PostActividadProductosAsync(ActividadProducto PActividad)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(PActividad);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/ActividadProductoes", content);

            return response.IsSuccessStatusCode;
        }

        //Temporadas
        public async Task<ObservableCollection<Temporadas>> GetTemporadasAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Temporadas");

            var temporadas = JsonConvert.DeserializeObject<ObservableCollection<Temporadas>>(json);

            return temporadas;
        }

        public async Task<bool> PostTemporadaAsync(Temporadas temporada)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(temporada);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Temporadas", content);

            return response.IsSuccessStatusCode;
        }

        //Temporadas-Patrullas
        public async Task<ObservableCollection<Temporada_Patrulla>> GetTemporadasPatrullasAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Temporada_Patrulla");

            var temporadas = JsonConvert.DeserializeObject<ObservableCollection<Temporada_Patrulla>>(json);

            return temporadas;
        }

        public async Task<bool> PostTemporadaPatrullaAsync(Temporada_Patrulla temporada)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(temporada);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Temporada_Patrulla", content);

            return response.IsSuccessStatusCode;
        }










        //Inventario

        //Producto
        public async Task<List<Producto>> GetProductosAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Productos");

            var productos = JsonConvert.DeserializeObject<List<Producto>>(json);

            return productos;
        }

        public async Task<Producto> GetProductoAsync(int Id)
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Productos/" + Id);
            var producto = JsonConvert.DeserializeObject<Producto>(json);

            return producto;
        }

        public async Task<bool> PostProductoAsync(Producto prod)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(prod);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Productos", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> PutProductAsync(Producto prod)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(prod);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PutAsync("http://camplogic.azurewebsites.net/api/Productos/" + prod.Id, content);

            return response.IsSuccessStatusCode;
        }

        //Categoria
        public async Task<List<Categoria>> GetCategoriasAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Categorias");

            var categorias = JsonConvert.DeserializeObject<List<Categoria>>(json);

            return categorias;
        }

        public async Task<bool> PostCategoriaAsync(Categoria categoria)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(categoria);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Categorias", content);

            return response.IsSuccessStatusCode;
        }

        //Sub-Categoria
        public async Task<List<SubCategoria>> GetSubCategoriasAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/SubCategorias");

            var subcategorias = JsonConvert.DeserializeObject<List<SubCategoria>>(json);

            return subcategorias;
        }

        public async Task<bool> PostSubCategoriaAsync(SubCategoria subcategoria)
        {
            var client = new HttpClient();
            var json = JsonConvert.SerializeObject(subcategoria);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/SubCategorias", content);

            return response.IsSuccessStatusCode;
        }

        //Ubicacion
        public async Task<List<Ubicacion>> GetUbicacionAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Ubicacions");

            var ubicaciones = JsonConvert.DeserializeObject<List<Ubicacion>>(json);

            return ubicaciones;
        }

        public async Task<bool> PostUbicacionAsync(Ubicacion ubicacion)
        {
            var client = new HttpClient();
            var json = JsonConvert.SerializeObject(ubicacion);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Ubicacions", content);

            return response.IsSuccessStatusCode;
        }


        //Cotizacion
        public async Task<bool> PostCotizacionAsync(Cotizacion cot)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(cot);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Cotizaciones", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> PostCotizacionProdAsync(Cotizacion_Producto cot)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(cot);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Cotizacion_Producto", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<List<Cotizacion>> GetCotizacionListAsync()
        {
            var client = new HttpClient();
            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Cotizaciones");
            var cotizaciones = JsonConvert.DeserializeObject<List<Cotizacion>>(json);
            return cotizaciones;
        }

        public async Task<List<Cotizacion_Producto>> GetCotizacionProdAsync()
        {
            var client = new HttpClient();
            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Cotizacion_Producto");
            var cotizaciones = JsonConvert.DeserializeObject<List<Cotizacion_Producto>>(json);
            return cotizaciones;
        }

        //Cesta
        public async Task<bool> PostProdCestaAsync(Cesta prod)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(prod);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Cestas", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<ObservableCollection<Cesta>> GetCestaAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Cestas");

            var cesta = JsonConvert.DeserializeObject<ObservableCollection<Cesta>>(json);

            return cesta;
        }

        public async Task<bool> DeleteCestaAsync(int id, Cesta prod)
        {
            var client = new HttpClient();

            var response = await client.DeleteAsync("http://camplogic.azurewebsites.net/api/Cestas/" + id);

            return response.IsSuccessStatusCode;
        }


        //Pedidos
        public async Task<bool> PostPedidoAsync(Pedidos prod)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(prod);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Pedidos", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<List<Pedidos>> GetPedidosAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Pedidos");

            var pedidos = JsonConvert.DeserializeObject<List<Pedidos>>(json);

            return pedidos;
        }


        //Orden de Compra
        public async Task<bool> PostOrdenCompraAsync(Orden_Compra ordenCompra)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(ordenCompra);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Orden_Compra", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<List<Orden_Compra>> GetOrdenesCompraAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Orden_Compra");

            var ordenesCompra = JsonConvert.DeserializeObject<List<Orden_Compra>>(json);

            return ordenesCompra;
        }

        public async Task<Orden_Compra> GetOrdenCompraAsync(int id)
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Orden_Compra/" + id);
            var OrdenFecha = JsonConvert.DeserializeObject<Orden_Compra>(json);

            return OrdenFecha;
        }

        public async Task<bool> PutOrdenCompraAsync(Orden_Compra orden)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(orden);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PutAsync("http://camplogic.azurewebsites.net/api/Orden_Compra/" + orden.Id, content);

            return response.IsSuccessStatusCode;
        }


        //Entradas
        public async Task<bool> PostEntradaAsync(Entrada entrada)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(entrada);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Entradas", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<List<Entrada>> GetEntradasAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Entradas");

            var entradas = JsonConvert.DeserializeObject<List<Entrada>>(json);

            return entradas;
        }

        //Entradas-Productos
        public async Task<List<EntradaProducto>> GetEntradasProdsAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/EntradaProductos");

            var entradas = JsonConvert.DeserializeObject<List<EntradaProducto>>(json);

            return entradas;
        }

        public async Task<bool> PostEntradaProdAsync(EntradaProducto entrada)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(entrada);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/EntradaProductos", content);

            return response.IsSuccessStatusCode;
        }

        //Reintegros
        public async Task<bool> PostReintegroAsync(Reintegros entrada)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(entrada);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Reintegros", content);

            return response.IsSuccessStatusCode;
        }
        public async Task<List<Reintegros>> GetReintegrosAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Reintegros");

            var entradas = JsonConvert.DeserializeObject<List<Reintegros>>(json);

            return entradas;
        }

        //Reintegros-Productos
        public async Task<bool> PostReintegroProdAsync(ReintegroProducto entrada)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(entrada);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/ReintegroProductoes", content);

            return response.IsSuccessStatusCode;
        }
        public async Task<List<ReintegroProducto>> GetReintegrosProdAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/ReintegroProductoes");

            var entradas = JsonConvert.DeserializeObject<List<ReintegroProducto>>(json);

            return entradas;
        }

        //Salidas
        public async Task<bool> PostSalidasAsync(Salida salida)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(salida);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Salidas", content);

            return response.IsSuccessStatusCode;
        }

        public async Task<List<Salida>> GetSalidasAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Salidas");

            var salidas = JsonConvert.DeserializeObject<List<Salida>>(json);

            return salidas;
        }


        //Salidas Productos
        public async Task<List<Salidas_Producto>> GetSalidasProdsAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Salidas_Producto");

            var salidas = JsonConvert.DeserializeObject<List<Salidas_Producto>>(json);

            return salidas;
        }

        public async Task<bool> PostSalidasProdAsync(Salidas_Producto salida)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(salida);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Salidas_Producto", content);

            return response.IsSuccessStatusCode;
        }

        public async Task PutSalidaProdsAsync(Salidas_Producto salidap)
        {
            var client = new HttpClient();

            var json = JsonConvert.SerializeObject(salidap);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PutAsync("http://camplogic.azurewebsites.net/api/Salidas_Producto/" + salidap.Id, content);
        }

        //Proveedor
        public async Task<ObservableCollection<Proveedor>> GetProveedoresAsync()
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://camplogic.azurewebsites.net/api/Proveedores");

            var proveedor = JsonConvert.DeserializeObject<ObservableCollection<Proveedor>>(json);

            return proveedor;

        }

        public async Task<bool> PostProveedorAsync(Proveedor prov)
        {
            var client = new HttpClient();
            var json = JsonConvert.SerializeObject(prov);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://camplogic.azurewebsites.net/api/Proveedores", content);

            return response.IsSuccessStatusCode;
        }
        */



    }
}
