﻿using CampLogic.Model;
using CampLogic.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.ViewModel
{
    public class PersonalInvViewModel : INotifyPropertyChanged
    {
        CampLogicService empleadoServices = new CampLogicService();

        private List<Empleado> _personalInv { get; set; }
        public List<Empleado> PersonalInv
        {
            get { return _personalInv; }
            set
            {
                _personalInv = value;
                OnPropertyChanged();
            }
        }

        public PersonalInvViewModel()
        {

            InitializeDataAsync();
            
        }

        public async void InitializeDataAsync()
        {
            var personal = await empleadoServices.GetEmpleadosAsync();
            PersonalInv = personal.Where(empleado => empleado.Cargo == "Inventario").ToList();
        }
        

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
