﻿using CampLogic.HelpClasses;
using CampLogic.Model;
using CampLogic.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.ViewModel
{
    public class FaltanteViewModel : INotifyPropertyChanged
    {
        CampLogicService camplogicService = new CampLogicService();

        private List<Faltante> _faltanteList { get; set; }
        public List<Faltante> FaltanteList
        {
            get { return _faltanteList; }
            set
            {
                _faltanteList = value;
                OnPropertyChanged();
            }
        }

        public FaltanteViewModel(DateTime FI, DateTime FF, string categoria, string subcategoria, string ubicacion, string articulo)
        {
            InitializeDataAsync(FI,FF,categoria,subcategoria,ubicacion,articulo);
        }

        public async void InitializeDataAsync(DateTime FI, DateTime FF, string categoria, string subcategoria, string ubcacion, string articulo)
        {
            var salidasS = await camplogicService.GetSalidasProdsAsync();
            var entradasS = await camplogicService.GetEntradasProdsAsync();
            var entradasN = await camplogicService.GetReintegrosProdAsync();
            var productos = await camplogicService.GetProductosAsync();
            List<Producto> prods = new List<Producto>();
            var faltanteList = new List<Faltante>();
            if (categoria != "NT" && subcategoria == "NT" && ubcacion == "NT" && articulo == "NT")
            {
                prods = PorCategoria(categoria, productos);
                foreach (var prod in prods)
                {
                    var CD = CantidadDisponible(prod, FI, FF, salidasS, entradasS,entradasN);
                    var CF = CantidadFaltante(CD, prod);
                    var faltante = new Faltante()
                    {
                        Producto = prod,
                        CantidadDisponible = CD,
                        CantidadFaltante = CF,
                        PF = PorcentajeFaltante(CF, prod)
                    };
                    faltanteList.Add(faltante);
                }

                FaltanteList = faltanteList.OrderBy(x => x.Producto.Articulo).ToList();
            }
            else if (categoria != "NT" && subcategoria == "NT" && ubcacion == "NT" && articulo != "NT")
            {
                prods = PorArticulo(articulo, productos);
                foreach (var prod in prods)
                {
                    var CD = CantidadDisponible(prod, FI, FF, salidasS, entradasS, entradasN);
                    var CF = CantidadFaltante(CD, prod);
                    var faltante = new Faltante()
                    {
                        Producto = prod,
                        CantidadDisponible = CD,
                        CantidadFaltante = CF,
                        PF = PorcentajeFaltante(CF, prod)
                    };
                    faltanteList.Add(faltante);
                }

                FaltanteList = faltanteList.OrderBy(x => x.Producto.Articulo).ToList();
            }
            else if (categoria != "NT" && subcategoria != "NT" && ubcacion == "NT" && articulo == "NT")
            {
                prods = PorSubCategoria(categoria, subcategoria, productos);
                foreach (var prod in prods)
                {
                    var CD = CantidadDisponible(prod, FI, FF, salidasS, entradasS, entradasN);
                    var CF = CantidadFaltante(CD, prod);
                    var faltante = new Faltante()
                    {
                        Producto = prod,
                        CantidadDisponible = CD,
                        CantidadFaltante = CF,
                        PF = PorcentajeFaltante(CF, prod)
                    };
                    faltanteList.Add(faltante);
                }

                FaltanteList = faltanteList.OrderBy(x => x.Producto.Articulo).ToList();
            }
            else if (categoria != "NT" && subcategoria != "NT" && ubcacion == "NT" && articulo != "NT")
            {
                prods = PorArticulo(articulo, productos);
                foreach (var prod in prods)
                {
                    var CD = CantidadDisponible(prod, FI, FF, salidasS, entradasS,entradasN);
                    var CF = CantidadFaltante(CD, prod);
                    var faltante = new Faltante()
                    {
                        Producto = prod,
                        CantidadDisponible = CD,
                        CantidadFaltante = CF,
                        PF = PorcentajeFaltante(CF, prod)
                    };
                    faltanteList.Add(faltante);
                }

                FaltanteList = faltanteList.OrderBy(x => x.Producto.Articulo).ToList();
            }
            else if (categoria == "NT" && subcategoria == "NT" && ubcacion == "NT" && articulo != "NT")
            {
                prods = PorArticulo(articulo, productos);
                foreach (var prod in prods)
                {
                    var CD = CantidadDisponible(prod, FI, FF, salidasS, entradasS,entradasN);
                    var CF = CantidadFaltante(CD, prod);
                    var faltante = new Faltante()
                    {
                        Producto = prod,
                        CantidadDisponible = CD,
                        CantidadFaltante = CF,
                        PF = PorcentajeFaltante(CF, prod)
                    };
                    faltanteList.Add(faltante);
                }

                FaltanteList = faltanteList.OrderBy(x => x.Producto.Articulo).ToList();
            }
            else if (categoria == "NT" && subcategoria == "NT" && ubcacion != "NT" && articulo == "NT")
            {
                prods = PorUbicacion(ubcacion, productos);
                foreach (var prod in prods)
                {
                    var CD = CantidadDisponible(prod, FI, FF, salidasS, entradasS, entradasN);
                    var CF = CantidadFaltante(CD, prod);
                    var faltante = new Faltante()
                    {
                        Producto = prod,
                        CantidadDisponible = CD,
                        CantidadFaltante = CF,
                        PF = PorcentajeFaltante(CF, prod)
                    };
                    faltanteList.Add(faltante);
                }

                FaltanteList = faltanteList.OrderBy(x => x.Producto.Articulo).ToList();
            }
            else if (categoria != "NT" && subcategoria == "NT" && ubcacion != "NT" && articulo == "NT")
            {
                prods = PorUbicacionCat(ubcacion, categoria, productos);
                foreach (var prod in prods)
                {
                    var CD = CantidadDisponible(prod, FI, FF, salidasS, entradasS, entradasN);
                    var CF = CantidadFaltante(CD, prod);
                    var faltante = new Faltante()
                    {
                        Producto = prod,
                        CantidadDisponible = CD,
                        CantidadFaltante = CF,
                        PF = PorcentajeFaltante(CF, prod)
                    };
                    faltanteList.Add(faltante);
                }

                FaltanteList = faltanteList.OrderBy(x => x.Producto.Articulo).ToList();
            }
            else if (categoria != "NT" && subcategoria != "NT" && ubcacion != "NT" && articulo == "NT")
            {
                prods = PorUbicacionSub(ubcacion, subcategoria, productos);
                foreach (var prod in prods)
                {
                    var CD = CantidadDisponible(prod, FI, FF, salidasS, entradasS, entradasN);
                    var CF = CantidadFaltante(CD, prod);
                    var faltante = new Faltante()
                    {
                        Producto = prod,
                        CantidadDisponible = CD,
                        CantidadFaltante = CF,
                        PF = PorcentajeFaltante(CF, prod)
                    };
                    faltanteList.Add(faltante);
                }

                FaltanteList = faltanteList.OrderBy(x => x.Producto.Articulo).ToList();
            }
            else if (categoria != "NT" && subcategoria != "NT" && ubcacion != "NT" && articulo != "NT")
            {
                prods = PorArticulo(articulo, productos);
                foreach (var prod in prods)
                {
                    var CD = CantidadDisponible(prod, FI, FF, salidasS, entradasS, entradasN);
                    var CF = CantidadFaltante(CD, prod);
                    var faltante = new Faltante()
                    {
                        Producto = prod,
                        CantidadDisponible = CD,
                        CantidadFaltante = CF,
                        PF = PorcentajeFaltante(CF, prod)
                    };
                    faltanteList.Add(faltante);
                }

                FaltanteList = faltanteList.OrderBy(x => x.Producto.Articulo).ToList();
            }
            else
            {
                prods = PorArticulo(articulo, productos);
                foreach (var prod in prods)
                {
                    var CD = CantidadDisponible(prod, FI, FF, salidasS, entradasS, entradasN);
                    var CF = CantidadFaltante(CD, prod);
                    var faltante = new Faltante()
                    {
                        Producto = prod,
                        CantidadDisponible = CD,
                        CantidadFaltante = CF,
                        PF = PorcentajeFaltante(CF, prod)
                    };
                    faltanteList.Add(faltante);
                }

                FaltanteList = faltanteList.OrderBy(x => x.Producto.Articulo).ToList();
            }
        }

        public List<Producto> PorCategoria(string categoria, List<Producto> prodWS)
        {
            List<Producto> prods = new List<Producto>();
            if(categoria == "Todas")
            {
                var productos = prodWS;
                prods = productos.OrderBy(x=>x.SubCategoria.Categoria.Nombre).ToList();
            }
            else
            {
                prods = prodWS.Where(x => x.SubCategoria.Categoria.Nombre == categoria).ToList();
            }

            return prods;

        }

        public List<Producto> PorSubCategoria(string categoria, string subcategoria, List<Producto> prodWS)
        {
            var productos = prodWS;
            var prods = productos.Where(x => x.SubCategoria.Categoria.Nombre == categoria && x.SubCategoria.Nombre == subcategoria).ToList();
            return prods;
        }

        public List<Producto> PorUbicacion(string ubicacion, List<Producto> prodWS)
        {
            List<Producto> prods = new List<Producto>();
            prods = prodWS.Where(x => x.Ubicacion.Nombre == ubicacion).ToList();
            return prods;
        }

        public List<Producto> PorUbicacionCat(string ubicacion, string categoria, List<Producto> prodWS)
        {
            List<Producto> prods = new List<Producto>();
            if (categoria == "Todas" && ubicacion == "Todas")
            {
                prods = prodWS.OrderBy(x => x.Articulo).ToList();
                
            }
            else if(categoria == "Todas")
            {
                prods = prodWS.Where(x => x.Ubicacion.Nombre == ubicacion).ToList();
            }
            else if (ubicacion == "Todas")
            {
                prods = prodWS.Where(x => x.SubCategoria.Categoria.Nombre == categoria).ToList();
            }
            else
            {
                prods = prodWS.Where(x => x.Ubicacion.Nombre == ubicacion && x.SubCategoria.Categoria.Nombre == categoria).ToList();
            }
            return prods;
        }

        public List<Producto> PorUbicacionSub(string ubicacion, string subcategoria, List<Producto> prodWS)
        {
            List<Producto> prods = new List<Producto>();
            if (subcategoria == "Todas")
            {
                prods = prodWS.Where(x => x.Ubicacion.Nombre == ubicacion).ToList();
            }
            else
            {
                prods = prodWS.Where(x => x.Ubicacion.Nombre == ubicacion && x.SubCategoria.Nombre == subcategoria).ToList();
            }
            return prods;
        }

        public List<Producto> PorArticulo(string articulo, List<Producto> prodWS)
        {
            List<Producto> prods = new List<Producto>();
            if (articulo == "Todos")
            {
                var productos = prodWS;
                prods = productos.OrderBy(x => x.Articulo == articulo).ToList();
            }
            else
            {
                var productos = prodWS;
                prods = productos.Where(x => x.Articulo == articulo).ToList();
            }
            
            return prods;
        }

        public List<Producto> Products(List<Producto> prodWS)
        {
            var productos = prodWS;
            var prods = productos.OrderBy(x => x.Articulo).ToList();
            return prods;
        }

        //CALCULOS DE INFORME

        //Cantidad Disponible
        public int CantidadDisponible(Producto prod, DateTime FI, DateTime FF, List<Salidas_Producto> salidasWS, List<EntradaProducto> entradasWS, List<ReintegroProducto> entradasN)
        {
            int CantDisp;
            //Existencial Inicial
            var EX = ExistenciaInicial(prod, FI, salidasWS, entradasWS, entradasN);
            var Entradas = SEntradas(prod, FI, FF, entradasWS, entradasN);
            var Salidas = SSalidas(prod, FI, FF, salidasWS);

            CantDisp = EX + Entradas - Salidas;

            if (CantDisp < 0)
            {
                return CantDisp = 0;
            }
            else
            {
                return CantDisp;
            }
        }

        //Existencia Inicial
        public int ExistenciaInicial(Producto prod, DateTime FI, List<Salidas_Producto> salidasWS, List<EntradaProducto> entradasWS, List<ReintegroProducto> entradasN)
        {
            int EX = 0;
            int entradasRealizadas = 0;
            int salidasRealizadas = 0;
            //Salidas
            var salidas = salidasWS.Where(x => x.Salida.Fecha.Date < FI.Date && x.ProductoId == prod.Id);
            //Entradas
            var entradas = entradasWS.Where(x => x.Entrada.Fecha.Date < FI && x.ProductoId == prod.Id);
            var entradas2 = entradasN.Where(x => x.Reintegro.FechaEntrada.Date < FI && x.ProductoId == prod.Id);

            //Contar Salidas
            if (salidas.Count()==0)
            {
                salidasRealizadas = 0;
            }
            else
            {
                foreach (var item in salidas)
                {
                    salidasRealizadas = salidasRealizadas + item.Cantidad;
                }
            }

            //Contar Entradas
            if (entradas.Count() == 0 && entradas2.Count() == 0)
            {
                entradasRealizadas = 0;
            }
            else if (entradas.Count() == 0 && entradas2.Count() != 0)
            {
                foreach (var item in entradas2)
                {
                    entradasRealizadas = entradasRealizadas + item.Cantidad;
                }
            }
            else if (entradas.Count() != 0 && entradas2.Count() == 0)
            {
                foreach (var item in entradas)
                {
                    entradasRealizadas = entradasRealizadas + item.CantidadEntregada;
                }
            }
            else
            {
                foreach (var item in entradas)
                {
                    entradasRealizadas = entradasRealizadas + item.CantidadEntregada;
                }
                foreach (var item in entradas2)
                {
                    entradasRealizadas = entradasRealizadas + item.Cantidad;
                }
            }

            EX = prod.Existencia_Inicial + entradasRealizadas - salidasRealizadas;
            
            return EX;

        }

        //Entradas
        public int SEntradas(Producto prod, DateTime FI, DateTime FF, List<EntradaProducto> entradasWS, List<ReintegroProducto> entradasN)
        {
            int SE = 0;

            var entradas = entradasWS.Where(b => b.Entrada.Fecha.Date <= FF.Date && b.Entrada.Fecha.Date >= FI.Date && b.ProductoId == prod.Id).ToList();
            var entradas2 = entradasN.Where(b => b.Reintegro.FechaEntrada.Date <= FF.Date && b.Reintegro.FechaEntrada.Date >= FI.Date && b.ProductoId == prod.Id).ToList();
            if (entradas.Count() == 0 && entradas2.Count() == 0)
            {
                SE = 0;
            }
            else if (entradas.Count() == 0 && entradas2.Count() != 0)
            {
                foreach (var item in entradas2)
                {
                    SE = SE + item.Cantidad;
                }
            }
            else if (entradas.Count() != 0 && entradas2.Count() == 0)
            {
                foreach (var item in entradas)
                {
                    SE = SE + item.CantidadEntregada;
                }
            }
            else
            {
                foreach (var item in entradas)
                {
                    SE = SE + item.CantidadEntregada;
                }
                foreach (var item in entradas2)
                {
                    SE = SE + item.Cantidad;
                }
            }

            return SE;
        }

        //Salidas
        public int SSalidas(Producto prod, DateTime FI, DateTime FF, List<Salidas_Producto> salidasWS)
        {
            int SS = 0;

            var salidas = salidasWS.Where(b => b.Salida.Fecha.Date <= FF.Date && b.Salida.Fecha.Date >= FI.Date && b.ProductoId == prod.Id).ToList();
            if (salidas.Count() == 0)
            {
                SS = 0;
            }
            else
            {
                foreach (var item in salidas)
                {
                    SS = SS + item.Cantidad;
                }
            }

            return SS;
        }

        //Cantidad Faltante
        public int CantidadFaltante (int CantidadDisponible, Producto prod)
        {
            int CF = 0;
            CF = prod.CantidadIdeal - CantidadDisponible;
            if (CF<0)
            {
                CF = 0;
            }
            else
            {
                CF = prod.CantidadIdeal - CantidadDisponible;
            }
            return CF;
        }

        //Porcentaje de Faltante
        public int PorcentajeFaltante(int CantidadFaltante, Producto prod)
        {
            
            var PF = (CantidadFaltante * 100 / prod.CantidadIdeal) ;
            return PF;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
