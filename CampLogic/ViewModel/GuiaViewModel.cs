﻿using CampLogic.Model;
using CampLogic.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.ViewModel
{
    public class GuiaViewModel : INotifyPropertyChanged
    {
        CampLogicService camplogicService = new CampLogicService();

        private List<Empleado> _personal { get; set; }
        public List<Empleado> Personal
        {
            get { return _personal; }
            set
            {
                _personal = value;
                OnPropertyChanged();
            }
        }

        private List<string> _cargoList { get; set; }
        public List<string> CargosList
        {
            get { return _cargoList; }
            set
            {
                _cargoList = value;
                OnPropertyChanged();
            }
        }

        public GuiaViewModel()
        {
            InitializeDataAsync();
        }

        public async void InitializeDataAsync()
        {
            var empleados = await camplogicService.GetEmpleadosAsync();

            Personal = empleados.OrderBy(empleado => empleado.Disponibilidad == false).ToList();

            CargosList = new List<string>
            {
                "Todos",
                "Director",
                "Coordinador",
                "Guía",
                "Inventario"
            };
        }

        public async Task<List<Empleado>> FiltrarPersonal(string keyword)
        {
            var personal = await camplogicService.GetEmpleadosAsync();
            if (keyword == "Todos")
            {
                return Personal = personal;
            }
            else
            {
                return Personal = personal.Where(x => x.Cargo == keyword).ToList();
            }
        }

        public async Task<List<Empleado>> SearchPersonal(string keyword)
        {
            var personal = await camplogicService.GetEmpleadosAsync();
            return Personal = personal.Where(x => x.Nombre.ToLower().Contains(keyword.ToLower()) || x.Apellido.ToLower().Contains(keyword.ToLower())).ToList();
        }

        public async Task<List<Empleado>> FilterCargoPersonal(string keyword, string cargo)
        {
            var personal = await camplogicService.GetEmpleadosAsync();
            return Personal = personal.Where(x => (x.Nombre.ToLower().Contains(keyword.ToLower()) || x.Apellido.ToLower().Contains(keyword.ToLower())) && x.Cargo == cargo).ToList();
        }

        public async Task<List<Empleado>> PersonalList()
        {
            var empleados = await camplogicService.GetEmpleadosAsync();

            return Personal = empleados.OrderBy(empleado => empleado.Disponibilidad == false).ToList();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
