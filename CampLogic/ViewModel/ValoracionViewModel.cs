﻿using CampLogic.HelpClasses;
using CampLogic.Model;
using CampLogic.Services;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.ViewModel
{
    public class ValoracionViewModel : INotifyPropertyChanged
    {

        CampLogicService camplogicServices = new CampLogicService();

        

        private List<Grouping<string, ValoracionProducto>> _valproductos { get; set; }
        public List<Grouping<string, ValoracionProducto>> ValProductos
        {
            get { return _valproductos; }
            set
            {
                _valproductos = value;
                OnPropertyChanged();
            }
        }

        //Inicio
        private int _invInicial;
        public int InvInicial
        {
            get { return _invInicial; }
            set
            {
                _invInicial = value;
                OnPropertyChanged();
            }
        }

        private float _costoUInicial;
        public float CostoUInicial
        {
            get { return _costoUInicial; }
            set
            {
                _costoUInicial = value;
                OnPropertyChanged();
            }
        }

        private float _valorTInicial;
        public float ValorTotalInicial
        {
            get { return _valorTInicial; }
            set
            {
                _valorTInicial = value;
                OnPropertyChanged();
            }
        }

        //Durante El Periodo Seleccionado
        private int _entradasDurante;
        public int EntradasDurante
        {
            get { return _entradasDurante; }
            set
            {
                _entradasDurante = value;
                OnPropertyChanged();
            }
        }

        private float _costoUDurante;
        public float CostoUDurante
        {
            get { return _costoUDurante; }
            set
            {
                _costoUDurante = value;
                OnPropertyChanged();
            }
        }

        private float _valorTotalDurante;
        public float ValorTotalDurante
        {
            get { return _valorTotalDurante; }
            set
            {
                _valorTotalDurante = value;
                OnPropertyChanged();
            }
        }

        //Total
        private int _cantidadTotal;
        public int Cantidad_Total
        {
            get { return _cantidadTotal; }
            set
            {
                _cantidadTotal = value;
                OnPropertyChanged();
            }
        }

        //Valor Total
        private float _valorTotal;
        public float Valor_Total
        {
            get { return _valorTotal; }
            set
            {
                _valorTotal = value;
                OnPropertyChanged();
            }
        }

        //Salidas
        private int _salidas;
        public int Salidas
        {
            get { return _salidas; }
            set
            {
                _salidas = value;
                OnPropertyChanged();
            }
        }

        //Inventario Final
        private int _invFinal;
        public int Inv_Final
        {
            get { return _invFinal; }
            set
            {
                _invFinal = value;
                OnPropertyChanged();
            }
        }

        //Valor Promedio
        private float _valorpro;
        public float Valor_Pro
        {
            get { return _valorpro; }
            set
            {
                _valorpro = value;
                OnPropertyChanged();
            }
        }

        //Valor de Inventario Final
        private float _vif;
        public float VIF
        {
            get { return _vif; }
            set
            {
                _vif = value;
                OnPropertyChanged();
            }
        }

        //Suma de Valor Promedio
        private float _sumavp;
        public float SumaVP
        {
            get { return _sumavp; }
            set
            {
                _sumavp = value;
                OnPropertyChanged();
            }
        }

        //Suma de VIF
        private float _sumavif;
        public float SumaVIF
        {
            get { return _sumavif; }
            set
            {
                _sumavif = value;
                OnPropertyChanged();
            }
        }

        public ValoracionViewModel()
        {
            InitializeDataAsync();
        }

        public void InitializeDataAsync()
        {
           
        }

        public ValoracionViewModel(ValoracionProducto p, DateTime FechaInicial, DateTime FechaFinal)
        {
            InitializeDataAsync(p, FechaInicial, FechaFinal);
        }

        public async void InitializeDataAsync(ValoracionProducto p, DateTime FechaInicial, DateTime FechaFinal)
        {
            var productos = await camplogicServices.GetProductosAsync();
            var entradas = await camplogicServices.GetEntradasAsync();
            var salidas = await camplogicServices.GetSalidasAsync();
            var cotizaciones = await camplogicServices.GetCotizacionProdAsync();
            
            int entradasRealizadas = 0;
            int salidasRealizadas = 0;
            

            //Inicial 

            //if (entradas.Where(b => b.Fecha.Date < FechaInicial.Date && b.ProductoId == p.Producto.Id).ToList() != null)
            //{
            //    var entradasPretemp = entradas.Where(b => b.Fecha.Date < FechaInicial.Date && b.ProductoId == p.Producto.Id).ToList();

            //    foreach (var entrada in entradasPretemp)
            //    {
            //        entradasRealizadas = entradasRealizadas + entrada.Cantidad;
            //    }
            //}

            

            //Inventario Inicial
            InvInicial = p.Producto.Existencia_Inicial + entradasRealizadas - salidasRealizadas;

            //Valor Total Inicial
            var cotizacionLast = cotizaciones.LastOrDefault(b => b.Cotizacion.Fecha_Cotizacion.Date < FechaInicial.Date && b.ProductoId == p.Producto.Id);
            if (cotizacionLast != null)
            {
                CostoUInicial = cotizacionLast.Precio / cotizacionLast.Cantidad;
                ValorTotalInicial = InvInicial * (CostoUInicial);
            }
            else
            {
                var cotiLast = cotizaciones.LastOrDefault(b => b.Cotizacion.Fecha_Cotizacion.Date <= FechaFinal.Date && b.Cotizacion.Fecha_Cotizacion.Date >= FechaInicial.Date && b.ProductoId == p.Producto.Id);
                CostoUInicial = cotiLast.Precio / cotiLast.Cantidad;
                ValorTotalInicial = InvInicial * (CostoUInicial);
            }

            //En el periodo

            //Entradas
            //if (entradas.Where(b => b.Fecha.Date <= FechaFinal.Date && b.Fecha.Date >= FechaInicial.Date && b.ProductoId == p.Producto.Id).ToList() != null)
            //{
            //    var entradasTemp = entradas.Where(b => b.Fecha.Date <= FechaFinal.Date && b.Fecha.Date >= FechaInicial.Date && b.ProductoId == p.Producto.Id).ToList();

            //    foreach (var entrada in entradasTemp)
            //    {
            //        EntradasDurante = EntradasDurante + entrada.Cantidad;
            //    }
            //}

            
            //Salidas
            
            //Valor Total Durante

            var cotiLastTemp = cotizaciones.LastOrDefault(b => b.Cotizacion.Fecha_Cotizacion.Date <= FechaFinal.Date && b.Cotizacion.Fecha_Cotizacion.Date >= FechaInicial.Date && b.ProductoId == p.Producto.Id);
            
            //Costo Unitario Durante
            if (cotiLastTemp != null)
            {
                CostoUDurante = cotiLastTemp.Precio / cotiLastTemp.Cantidad;
            }
            
            

            //Valor Total Durante
            ValorTotalDurante = EntradasDurante * (CostoUDurante);

            //Cantidad Total
            Cantidad_Total = InvInicial + EntradasDurante;

            //Valor Total
            Valor_Total = ValorTotalInicial + ValorTotalDurante;

            //Inventario Final
            Inv_Final = Cantidad_Total - Salidas;

            //Valor Promedio
            Valor_Pro = Valor_Total / Cantidad_Total;

            //Valor Inventario Final
            VIF = Inv_Final * Valor_Pro;
        }

        public ValoracionViewModel(DateTime comTemp, DateTime finTemp)
        {
            InitializeDataAsync(comTemp, finTemp);
        }

        public async void InitializeDataAsync(DateTime FechaInicial, DateTime FechaFinal)
        {
            var productos = await camplogicServices.GetProductosAsync();
            var entradas = await camplogicServices.GetEntradasAsync();
            var salidas = await camplogicServices.GetSalidasAsync();
            var cotizaciones = await camplogicServices.GetCotizacionProdAsync();

            List<ValoracionProducto> listaVal = new List<ValoracionProducto>();
            
            int entradasRealizadas = 0;
            int salidasRealizadas = 0;

            
            foreach (var p in productos)
            {

                entradasRealizadas = 0;
                salidasRealizadas = 0;
                EntradasDurante = 0;
                Salidas = 0;

                //Inicial 

                //if (entradas.Where(b => b.Fecha.Date < FechaInicial.Date && b.ProductoId == p.Id).ToList() != null)
                //{
                //    var entradasPretemp = entradas.Where(b => b.Fecha.Date < FechaInicial.Date && b.ProductoId == p.Id).ToList();

                //    foreach (var entrada in entradasPretemp)
                //    {
                //        entradasRealizadas = entradasRealizadas + entrada.Cantidad;
                //    }
                //}

                

                //Inventario Inicial
                InvInicial = p.Existencia_Inicial + entradasRealizadas - salidasRealizadas;

                //Valor Total Inicial
                var cotizacionLast = cotizaciones.LastOrDefault(b => b.Cotizacion.Fecha_Cotizacion.Date < FechaInicial.Date && b.ProductoId == p.Id);
                if (cotizacionLast != null)
                {
                    CostoUInicial = cotizacionLast.Precio / cotizacionLast.Cantidad;
                    ValorTotalInicial = InvInicial * (CostoUInicial);
                }
                else
                {
                    var cotiLast = cotizaciones.LastOrDefault(b => b.Cotizacion.Fecha_Cotizacion.Date <= FechaFinal.Date && b.Cotizacion.Fecha_Cotizacion.Date >= FechaInicial.Date && b.ProductoId == p.Id);
                    CostoUInicial = cotiLast.Precio / cotiLast.Cantidad;
                    ValorTotalInicial = InvInicial * (CostoUInicial);
                }

                //En el periodo

                //Entradas
                //if (entradas.Where(b => b.Fecha.Date <= FechaFinal.Date && b.Fecha.Date >= FechaInicial.Date && b.ProductoId == p.Id).ToList() != null)
                //{
                //    var entradasTemp = entradas.Where(b => b.Fecha.Date <= FechaFinal.Date && b.Fecha.Date >= FechaInicial.Date && b.ProductoId == p.Id).ToList();

                //    foreach (var entrada in entradasTemp)
                //    {
                //        EntradasDurante = EntradasDurante + entrada.Cantidad;
                //    }
                //}


                //Salidas
                
                

                //Valor Total Durante

                var cotiLastTemp = cotizaciones.LastOrDefault(b => b.Cotizacion.Fecha_Cotizacion.Date <= FechaFinal.Date && b.Cotizacion.Fecha_Cotizacion.Date >= FechaInicial.Date && b.ProductoId == p.Id);

                //Costo Unitario Durante
                if (cotiLastTemp != null)
                {
                    CostoUDurante = cotiLastTemp.Precio / cotiLastTemp.Cantidad;
                }



                //Valor Total Durante
                ValorTotalDurante = EntradasDurante * (CostoUDurante);

                //Cantidad Total
                Cantidad_Total = InvInicial + EntradasDurante;

                //Valor Total
                Valor_Total = ValorTotalInicial + ValorTotalDurante;

                //Inventario Final
                Inv_Final = Cantidad_Total - Salidas;

                //Valor Promedio
                Valor_Pro = Valor_Total / Cantidad_Total;

                //Valor Inventario Final
                VIF = Inv_Final * Valor_Pro;

                //Agregar a Listado

                listaVal.Add(new ValoracionProducto() { ValorPromedio = Valor_Pro, ValorIF = VIF, Producto = p });
                
                //Suma Los Valores Promedio
                SumaVP = SumaVP + Valor_Pro;

                //Suma los Valores de Inventario Final
                SumaVIF = SumaVIF + VIF;
            }

            //var prod = from produ in listaVal
            //           orderby produ.Producto.Tipo
            //           group produ by produ.Producto.Tipo.ToString() into produGroup
            //           select new Grouping<string, ValoracionProducto>(produGroup.Key, produGroup);

            //ValProductos = prod.ToList();


        }
        

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
