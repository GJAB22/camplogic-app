﻿using CampLogic.Helpers;
using CampLogic.Model;
using CampLogic.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CampLogic.ViewModel
{
    public class PerfilViewModel : INotifyPropertyChanged 
    {
        CampLogicService camplogicService = new CampLogicService();

        
        private Empleado _perfil;
        public Empleado  Perfil
        {
            get { return _perfil; }
            set
            {
                _perfil = value;
                OnPropertyChanged();
            }
        }

        private List<string> _cargos { get; set; }
        public List<string> Cargos
        {
            get { return _cargos; }
            set
            {
                _cargos = value;
                OnPropertyChanged();
            }
        }

        private int _su { get; set; }
        public int SU
        {
            get
            {
                return _su;
            }
            set
            {
                _su = value;
                OnPropertyChanged();
            }
        }


        public ICommand CerrarSesion
        {
            get
            {
                return new Command(() =>
                {
                    Settings.AccessToken = "";
                    Settings.Username = "";
                    Settings.Password = "";
                    Settings.Cargo = "";
                    Settings.Cedula = "";
                    

                    Application.Current.MainPage = new NavigationPage(new LogInPage());
                });
            }
        }

        public ICommand GuardarCambios
        {
            get
            {
                return new Command<object[]>(async(EP) => 
                {
                    var Disp = (bool)EP[0];
                    var Cargo = (string)EP[1];
                    var telefono = (string)EP[2];
                    var fotoperfil = (string)EP[3];
                    string cargoEmp;
                    if (Cargo != null)
                    {
                        cargoEmp = _perfil.Cargo;
                        Settings.Cargo = _perfil.Cargo;
                    }
                    else
                    {
                        cargoEmp = Cargo;
                        Settings.Cargo = Cargo;
                    }
                    


                    var ep = new Empleado
                    {
                        Cargo = cargoEmp,
                        Apellido = _perfil.Apellido,
                        Cedula = _perfil.Cedula,
                        Correo = _perfil.Correo,
                        Disponibilidad = Disp,
                        Fecha_Nacimiento = _perfil.Fecha_Nacimiento,
                        FotoPerfil = fotoperfil,
                        Id = _perfil.Id,
                        Nombre = _perfil.Nombre,
                        NombrePatrulla = _perfil.NombrePatrulla,
                        PreferenciaCampistas = _perfil.PreferenciaCampistas,
                        Telefono = telefono,
                        UserId = _perfil.UserId
                    };
                    await camplogicService.PutEmpleadoAsync(ep);
                    
                });
            }
        }
       

        public PerfilViewModel()
        {
           
            InitializeDataAsync();
        }

        public async void InitializeDataAsync()
        {
            var cedula = Settings.Cedula;
            Perfil = await camplogicService.GetEmpleadoCedulaAsync(cedula);

            Cargos = new List<string>()
            {
                
                "Coordinador",
                "Guía",
                "Inventario"
            };

            int i = 0;
            foreach (var item in Cargos)
            {
                if (item == Perfil.Cargo)
                {
                    SU = i;
                }
                else
                {
                    i++;
                }
            }
        }

       
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
