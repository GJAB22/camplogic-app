﻿using CampLogic.HelpClasses;
using CampLogic.Model;
using CampLogic.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CampLogic.ViewModel
{
    public class TemporadaViewModel : INotifyPropertyChanged
    {
        CampLogicService camplogicServices = new CampLogicService();

        //PRETEMPORADA o ARMADO DE TEMPORADA

        //PRIMERA PARTE
        private int _cantidadCampistas { get; set; }
        public int CantidadCampistas
        {
            get { return _cantidadCampistas; }
            set
            {
                _cantidadCampistas = value;
                OnPropertyChanged();
            }
        }

        private int _cantidad56 { get; set; }
        public int Cantidad56
        {
            get { return _cantidad56; }
            set
            {
                _cantidad56 = value;
                OnPropertyChanged();
            }
        }

        private int _cantidad78 { get; set; }
        public int Cantidad78
        {
            get { return _cantidad78; }
            set
            {
                _cantidad78 = value;
                OnPropertyChanged();
            }
        }

        private int _cantidad910 { get; set; }
        public int Cantidad910
        {
            get { return _cantidad910; }
            set
            {
                _cantidad910 = value;
                OnPropertyChanged();
            }
        }

        private int _cantidad1112 { get; set; }
        public int Cantidad1112
        {
            get { return _cantidad1112; }
            set
            {
                _cantidad1112 = value;
                OnPropertyChanged();
            }
        }

        private int _cantidad1314 { get; set; }
        public int Cantidad1314
        {
            get { return _cantidad1314; }
            set
            {
                _cantidad1314 = value;
                OnPropertyChanged();
            }
        }

        private int _cantidad1516 { get; set; }
        public int Cantidad1516
        {
            get { return _cantidad1516; }
            set
            {
                _cantidad1516 = value;
                OnPropertyChanged();
            }
        }

        private List<Campistas> _campistaList { get; set; }
        public List<Campistas> CampistaList
        {
            get { return _campistaList; }
            set
            {
                _campistaList = value;
                OnPropertyChanged();
            }
        }

        public async Task<List<Campistas>> GetCampistasIns()
        {
            var campistas = await camplogicServices.GetCampistasAsync();
            CampistaList = campistas.Where(x=>x.Inscrito == true).OrderByDescending(x => x.Fecha_Nacimiento).ToList();
            return CampistaList;
        }

        public async Task<int> TotalCampistas()
        {
            var campistas = await camplogicServices.GetCampistasAsync();
            CampistaList = campistas.Where(x => x.Inscrito == true).ToList();
            CantidadCampistas = 0;
            foreach (var item in CampistaList)
            {
                CantidadCampistas = CantidadCampistas + 1;
            }

            return CantidadCampistas;
        }

        public async Task<int> Campistas56()
        {
            var campistas = await camplogicServices.GetCampistasAsync();
            CampistaList = campistas.Where(x => x.Inscrito == true).ToList();
            Cantidad56 = 0;
            DateTime today = DateTime.Today;
            
            foreach (var item in CampistaList)
            {
                var Birthday = item.Fecha_Nacimiento;

                int age = today.Year - Birthday.Year;

                if (Birthday.Month > today.Month)
                {
                    age--;
                }

                if (age>=5 && age<=6)
                {
                    Cantidad56 = Cantidad56 + 1;
                }
                
            }

            return Cantidad56;
        }

        public async Task<int> Campistas78()
        {
            var campistas = await camplogicServices.GetCampistasAsync();
            CampistaList = campistas.Where(x => x.Inscrito == true).ToList();
            Cantidad78 = 0;
            DateTime today = DateTime.Today;

            foreach (var item in CampistaList)
            {
                var Birthday = item.Fecha_Nacimiento;

                int age = today.Year - Birthday.Year;

                if (Birthday.Month > today.Month)
                {
                    age--;
                }

                if (age >= 7 && age <= 8)
                {
                    Cantidad78 = Cantidad78 + 1;
                }

            }

            return Cantidad78;
        }

        public async Task<int> Campistas910()
        {
            var campistas = await camplogicServices.GetCampistasAsync();
            CampistaList = campistas.Where(x => x.Inscrito == true).ToList();
            Cantidad910 = 0;
            DateTime today = DateTime.Today;

            foreach (var item in CampistaList)
            {
                var Birthday = item.Fecha_Nacimiento;

                int age = today.Year - Birthday.Year;

                if (Birthday.Month > today.Month)
                {
                    age--;
                }

                if (age >= 9 && age <= 10)
                {
                    Cantidad910 = Cantidad910 + 1;
                }

            }

            return Cantidad910;
        }

        public async Task<int> Campistas1112()
        {
            var campistas = await camplogicServices.GetCampistasAsync();
            CampistaList = campistas.Where(x => x.Inscrito == true).ToList();
            Cantidad1112 = 0;
            DateTime today = DateTime.Today;

            foreach (var item in CampistaList)
            {
                var Birthday = item.Fecha_Nacimiento;

                int age = today.Year - Birthday.Year;

                if (Birthday.Month > today.Month)
                {
                    age--;
                }

                if (age >= 11 && age <= 12)
                {
                    Cantidad1112 = Cantidad1112 + 1;
                }

            }

            return Cantidad1112;
        }

        public async Task<int> Campistas1314()
        {
            var campistas = await camplogicServices.GetCampistasAsync();
            CampistaList = campistas.Where(x => x.Inscrito == true).ToList();
            Cantidad1314 = 0;
            DateTime today = DateTime.Today;

            foreach (var item in CampistaList)
            {
                var Birthday = item.Fecha_Nacimiento;

                int age = today.Year - Birthday.Year;

                if (Birthday.Month > today.Month)
                {
                    age--;
                }

                if (age >= 13 && age <= 14)
                {
                    Cantidad1314 = Cantidad1314 + 1;
                }

            }

            return Cantidad1314;
        }

        public async Task<int> Campistas1516()
        {
            var campistas = await camplogicServices.GetCampistasAsync();
            CampistaList = campistas.Where(x => x.Inscrito == true).ToList();
            Cantidad1516 = 0;
            DateTime today = DateTime.Today;

            foreach (var item in CampistaList)
            {
                var Birthday = item.Fecha_Nacimiento;

                int age = today.Year - Birthday.Year;

                if (Birthday.Month > today.Month)
                {
                    age--;
                }

                if (age >= 15 && age <= 16)
                {
                    Cantidad1516 = Cantidad1516 + 1;
                }

            }

            return Cantidad1516;
        }



        //SEGUNDA PARTE

        private ObservableCollection<Patrulla> _patrullasList { get; set; }
        public ObservableCollection<Patrulla> PatrullasList
        {
            get { return _patrullasList; }
            set
            {
                _patrullasList = value;
                OnPropertyChanged();
            }
        }
        
       
        //MultiSeleccion
        private ObservableCollection<SelectableItemWrapper<Campistas>> _totalCampistasList;
        public ObservableCollection<SelectableItemWrapper<Campistas>> TotalCampistasList
        {
            get { return _totalCampistasList; }
            set
            {
                _totalCampistasList = value;
                RaisePropertyChanged();
            }
        }

        //Lista Seleccionada de Campistas
        private ObservableCollection<Campistas> _selectedCampistas;
        public ObservableCollection<Campistas> SelectedCampistas
        {
            get { return _selectedCampistas; }
            set
            {
                _selectedCampistas = value;
                RaisePropertyChanged();
            }
        }

        private ObservableCollection<SelectableItemWrapper<Empleado>> _totalGuiasList;
        public ObservableCollection<SelectableItemWrapper<Empleado>> TotalGuiasList
        {
            get { return _totalGuiasList; }
            set
            {
                _totalGuiasList = value;
                RaisePropertyChanged();
            }
        }

        //Lista Seleccionada de Guia
        private ObservableCollection<Empleado> _selectedGuias;
        public ObservableCollection<Empleado> SelectedGuias
        {
            get { return _selectedGuias; }
            set
            {
                _selectedGuias = value;
                RaisePropertyChanged();
            }
        }

        private ObservableCollection<PatrullaCampista> _campistaList2 { get; set; }
        public ObservableCollection<PatrullaCampista> CampistaList2
        {
            get { return _campistaList2; }
            set
            {
                _campistaList2 = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<PatrullaEmpleado> _guiaList { get; set; }
        public ObservableCollection<PatrullaEmpleado> GuiasList
        {
            get { return _guiaList; }
            set
            {
                _guiaList = value;
                OnPropertyChanged();
            }
        }

        public async Task<ObservableCollection<Patrulla>> GetPatrullas()
        {
            return PatrullasList = await camplogicServices.GetPatrullasAsync();
        }

        public ICommand RemovePatrulla
        {
            get
            {
                return new Command<Patrulla>((PA) =>
                {
                    _patrullasList.Remove(PA);
                });
            }
        }


        public ICommand AgregarCampistas
        {
            get
            {
                return new Command<object[]>(async(DP) =>
                {
                    SelectedCampistas = GetSelectedCampistas();
                    
                    var patrulla = (Patrulla)DP[0];
                    var fa = (DateTime)DP[1];
                    foreach (var item in SelectedCampistas)
                    {
                        var ncampista = new PatrullaCampista()
                        {
                            FechaAsignada = (DateTime)DP[1],
                            CampistaId = item.Id,
                            PatrullaId = patrulla.Id

                        };

                        var ecampista = new Campistas()
                        {
                            Id = item.Id,
                            Apellido = item.Apellido,
                            Asistencia = item.Asistencia,
                            Cedula = item.Cedula,
                            Descripcion = item.Descripcion,
                            Fecha_Nacimiento = item.Fecha_Nacimiento,
                            Inscrito = item.Inscrito,
                            Nombre = item.Nombre,
                            NombrePatrulla = patrulla.Nombre,
                            Transporte = item.Transporte
                        };

                        await camplogicServices.PostPatrullaCampistaAsync(ncampista);

                        await camplogicServices.PutCampistaAsync(ecampista);
                        
                    }

                    var pc = await camplogicServices.GetPatrullasCampistasAsync();

                    CampistaList2 = new ObservableCollection<PatrullaCampista>(pc.Where(x => x.PatrullaId == patrulla.Id && x.FechaAsignada.Date == fa.Date));

                   
                });
            }
        }

        public ICommand ELCampistas
        {
            get
            {
                return new Command<int>((DP) =>
                {
                    var sCamp = GetSelectedCampistas2();

                    foreach (var item in sCamp)
                    {
                        _totalCampistasList.Remove(item);
                    }
                });
            }
        }

        public ICommand RemoveCampistas
        {
            get
            {
                return new Command<PatrullaCampista>(async (PA) =>
                {
                    
                    await camplogicServices.DeletePatrullaCampistaAsync(PA.Id,PA);
                    var ecampista = new Campistas()
                    {
                        Id = PA.Campista.Id,
                        Apellido = PA.Campista.Apellido,
                        Asistencia = PA.Campista.Asistencia,
                        Cedula = PA.Campista.Cedula,
                        Descripcion = PA.Campista.Descripcion,
                        Fecha_Nacimiento = PA.Campista.Fecha_Nacimiento,
                        Inscrito = PA.Campista.Inscrito,
                        Nombre = PA.Campista.Nombre,
                        NombrePatrulla = "",
                        Transporte = PA.Campista.Transporte
                    };
                    await camplogicServices.PutCampistaAsync(ecampista);
                    _campistaList2.Remove(PA);
                });
            }
        }

        public ICommand AgDenuevoCampistas
        {
            get
            {
                return new Command<PatrullaCampista>(async (PA) =>
                {

                    await camplogicServices.DeletePatrullaCampistaAsync(PA.Id, PA);
                    var ecampista = new Campistas()
                    {
                        Id = PA.Campista.Id,
                        Apellido = PA.Campista.Apellido,
                        Asistencia = PA.Campista.Asistencia,
                        Cedula = PA.Campista.Cedula,
                        Descripcion = PA.Campista.Descripcion,
                        Fecha_Nacimiento = PA.Campista.Fecha_Nacimiento,
                        Inscrito = PA.Campista.Inscrito,
                        Nombre = PA.Campista.Nombre,
                        NombrePatrulla = "",
                        Transporte = PA.Campista.Transporte
                    };
                    var campE = new SelectableItemWrapper<Campistas> { Item = ecampista};
                    _totalCampistasList.Add(campE);
                });
            }
        }


        public ICommand AgregarGuias
        {
            get
            {
                return new Command<object[]>(async (DP) =>
                {
                    SelectedGuias = GetSelectedGuias();
                    
                    var patrulla = (Patrulla)DP[0];
                    var fa = (DateTime)DP[1];
                    foreach (var item in SelectedGuias)
                    {
                        var nguia = new PatrullaEmpleado()
                        {
                            FechaAsignacion = (DateTime)DP[1],
                            EmpleadoId = item.Id,
                            PatrullaId = patrulla.Id

                        };

                        var eguia = new Empleado()
                        {
                            Id = item.Id,
                            Apellido = item.Apellido,
                            Cargo = item.Cargo,
                            Cedula = item.Cedula,
                            Correo = item.Correo,
                            Disponibilidad = item.Disponibilidad,
                            Fecha_Nacimiento = item.Fecha_Nacimiento,
                            FotoPerfil = item.FotoPerfil,
                            Nombre = item.Nombre,
                            NombrePatrulla = patrulla.Nombre,
                            PreferenciaCampistas = item.PreferenciaCampistas,
                            Telefono = item.Telefono,
                            UserId = item.UserId
                        };

                        await camplogicServices.PutEmpleadoAsync(eguia);
                        await camplogicServices.PostPatrullaEmpleadoAsync(nguia);
                        
                    }
                    var pg = (await camplogicServices.GetPatrullasEmpleadoAsync()).Where(x => x.PatrullaId == patrulla.Id && x.FechaAsignacion.Date == fa.Date);
                    _guiaList = new ObservableCollection<PatrullaEmpleado>();
                    foreach (var item in pg)
                    {
                        _guiaList.Add(item);
                    }
                    
                });
            }
        }

        public ICommand ELGuias
        {
            get
            {
                return new Command<int>((DP) =>
                {
                    var sGuias = GetSelectedGuias2();

                    foreach (var item in sGuias)
                    {
                        _totalGuiasList.Remove(item);
                    }
                });
            }
        }

        public ICommand RemoveGuias
        {
            get
            {
                return new Command<PatrullaEmpleado>(async(PA) =>
                {
                    await camplogicServices.DeletePatrullaEmpleadoAsync(PA.Id, PA);
                    
                    var eguia = new Empleado()
                    {
                        Id = PA.Empleado.Id,
                        Apellido = PA.Empleado.Apellido,
                        Cargo = PA.Empleado.Cargo,
                        Cedula = PA.Empleado.Cedula,
                        Correo = PA.Empleado.Correo,
                        Disponibilidad = PA.Empleado.Disponibilidad,
                        Fecha_Nacimiento = PA.Empleado.Fecha_Nacimiento,
                        FotoPerfil = PA.Empleado.FotoPerfil,
                        Nombre = PA.Empleado.Nombre,
                        NombrePatrulla = "",
                        PreferenciaCampistas = PA.Empleado.PreferenciaCampistas,
                        Telefono = PA.Empleado.Telefono,
                        UserId = PA.Empleado.UserId
                    };

                    await camplogicServices.PutEmpleadoAsync(eguia);
                    _guiaList.Remove(PA);
                });
            }
        }

        public ICommand AgDenuevoGuias
        {
            get
            {
                return new Command<PatrullaEmpleado>(async (PA) =>
                {
                    await camplogicServices.DeletePatrullaEmpleadoAsync(PA.Id, PA);

                    var eguia = new Empleado()
                    {
                        Id = PA.Empleado.Id,
                        Apellido = PA.Empleado.Apellido,
                        Cargo = PA.Empleado.Cargo,
                        Cedula = PA.Empleado.Cedula,
                        Correo = PA.Empleado.Correo,
                        Disponibilidad = PA.Empleado.Disponibilidad,
                        Fecha_Nacimiento = PA.Empleado.Fecha_Nacimiento,
                        FotoPerfil = PA.Empleado.FotoPerfil,
                        Nombre = PA.Empleado.Nombre,
                        NombrePatrulla = "",
                        PreferenciaCampistas = PA.Empleado.PreferenciaCampistas,
                        Telefono = PA.Empleado.Telefono,
                        UserId = PA.Empleado.UserId
                    };
                    
                    var eGuia = new SelectableItemWrapper<Empleado> { Item = eguia };
                    TotalGuiasList.Add(eGuia);
                });
            }
        }


        public ObservableCollection<Empleado> GetSelectedGuias()
        {
            var selected = TotalGuiasList
                .Where(p => p.IsSelected)
                .Select(p => p.Item)
                .ToList();
            return new ObservableCollection<Empleado>(selected);
        }

        public ObservableCollection<Campistas> GetSelectedCampistas()
        {
            var selected = TotalCampistasList
                .Where(p => p.IsSelected)
                .Select(p => p.Item)
                .ToList();
            return new ObservableCollection<Campistas>(selected);
        }

        public ObservableCollection<SelectableItemWrapper<Empleado>> GetSelectedGuias2()
        {
            var selected = TotalGuiasList
                .Where(p => p.IsSelected);

            return new ObservableCollection<SelectableItemWrapper<Empleado>>(selected);
        }

        public ObservableCollection<SelectableItemWrapper<Campistas>> GetSelectedCampistas2()
        {
            var selected = TotalCampistasList
                .Where(p => p.IsSelected)
                .ToList();
            return new ObservableCollection<SelectableItemWrapper<Campistas>>(selected);
        }

        public TemporadaViewModel()
        {
            InitializeDataAsync();
        }

        public async void InitializeDataAsync()
        {
            PatrullasList = await camplogicServices.GetPatrullasAsync();
            var temporadas = await camplogicServices.GetTemporadasAsync();
            if (temporadas.Count()!=0)
            {
                Temporadas tempAux = temporadas.LastOrDefault();
                
                if (temporadas.Where(x => x.FechaInicial.Date <= DateTime.Today.Date && x.FechaFinal.Date >= DateTime.Today.Date).Count() != 0)
                {
                    TemporadasList = new ObservableCollection<Temporadas>(temporadas.Reverse().Skip(1));
                    UltimaTemporadasList = new ObservableCollection<Temporadas>(temporadas.Where(x => x.FechaInicial.Date <= DateTime.Today.Date && x.FechaFinal.Date >= DateTime.Today.Date));
                }
                else
                {
                    TemporadasList = new ObservableCollection<Temporadas>(temporadas.Reverse());
                }
            }

            //Para Parte Dos
            var totalCampistasList = await camplogicServices.GetCampistasAsync();
            var TtCampistasList = totalCampistasList.Where(x => x.Inscrito == true).OrderByDescending(x => x.Fecha_Nacimiento);
            TotalCampistasList = new ObservableCollection<SelectableItemWrapper<Campistas>>(TtCampistasList
                .Select(pk => new SelectableItemWrapper<Campistas> { Item = pk }));

            var totalGuiasList = await camplogicServices.GetEmpleadosAsync();
            var TtGuiasList = totalGuiasList.Where(x => x.Cargo == "Guía" && x.Disponibilidad == true);
            TotalGuiasList = new ObservableCollection<SelectableItemWrapper<Empleado>>(TtGuiasList
                .Select(pk => new SelectableItemWrapper<Empleado> { Item = pk }));

        }

        public TemporadaViewModel(Patrulla patrulla, DateTime FA)
        {
            InitializeDataAsync(patrulla,FA);
        }

        public async void InitializeDataAsync(Patrulla patrulla, DateTime FA)
        {
            var patrullascampistas = await camplogicServices.GetPatrullasCampistasAsync();
            CampistaList2 = new ObservableCollection<PatrullaCampista>(patrullascampistas.Where(x => x.PatrullaId == patrulla.Id && x.FechaAsignada.Date == FA.Date).OrderByDescending(x => x.Campista.Fecha_Nacimiento));

            var patrullaguias = await camplogicServices.GetPatrullasEmpleadoAsync();
            GuiasList = new ObservableCollection<PatrullaEmpleado>(patrullaguias.Where(x => x.PatrullaId == patrulla.Id && x.FechaAsignacion.Date == FA.Date));

           

        }


        //TERCERA PARTE
        private ObservableCollection<Actividades> _actividadesList { get; set; }
        public ObservableCollection<Actividades> ActividadesList
        {
            get { return _actividadesList; }
            set
            {
                _actividadesList = value;
                OnPropertyChanged();
            }
        }
        
        public ObservableCollection<Patrulla> GetSelectedPatrullas()
        {
            return _patrullasList;
        }

        public async Task<ObservableCollection<Actividades>> GetActividades()
        {
            return ActividadesList = await camplogicServices.GetActividadesAsync();
        }

        public ICommand AgregarTemporada
        {
            get
            {
                return new Command<object[]>(async (NT) =>
                {
                    var patrullas = GetSelectedPatrullas();
                    var ActPrimer = (Actividades[,])NT[0];
                    var ActSegundo = (Actividades[,])NT[1];
                    var ActTercero = (Actividades[,])NT[2];
                    var FI = (DateTime)NT[3];
                    var FF = (DateTime)NT[4];
                    var Epoca = (string)NT[5];
                    var CantCampistas = (int)NT[6];
                    var NumeroTemp = (int)NT[7];
                    int aux = 0;
                    int aux2 = 0;
                    var camppatrulla = await camplogicServices.GetPatrullasCampistasAsync();

                    //Agregar Temporada
                    var NuevaTemp = new Temporadas
                    {
                        FechaInicial = FI.Date,
                        FechaFinal = FF.Date,
                        CantidadCampistas = CantCampistas,
                        NumeroTemporada = NumeroTemp,
                        Epoca = Epoca
                    };

                    await camplogicServices.PostTemporadaAsync(NuevaTemp);
                    var temporadas = await camplogicServices.GetTemporadasAsync();
                    var LastTemp = temporadas.LastOrDefault();

                    //Agregar Patrulla-Temporadas
                    foreach (var patrulla in patrullas)
                    {
                        var numerocamp = (camppatrulla.Where(x=>x.PatrullaId == patrulla.Id && x.FechaAsignada.Date == FI.Date)).Count();
                        var patrullaTemp = new Temporada_Patrulla
                        {
                            PatrullaId = patrulla.Id,
                            TemporadaId = LastTemp.Id,
                            CantidadCampistas = numerocamp

                        };

                        await camplogicServices.PostTemporadaPatrullaAsync(patrullaTemp);
                    }

                    //Agregar Actividades
                    foreach (var patrulla in patrullas)
                    {
                        aux2 = 0;
                        for (int i = 0; i < 5; i++)
                        {
                            var nuevaActividad = new Patrulla_Actividad
                            {
                                FechaAsignacion = FI,
                                ActividadId = (ActPrimer[aux, i]).Id,
                                PatrullaId = patrulla.Id,
                                Horario = new DateTime(FI.Year, FI.Month, FI.Day + aux2, 09, 00, 00)
                            };

                            await camplogicServices.PostPatrullaActividadAsync(nuevaActividad);

                            var nuevaActividad2 = new Patrulla_Actividad
                            {
                                FechaAsignacion = FI,
                                ActividadId = (ActSegundo[aux, i]).Id,
                                PatrullaId = patrulla.Id,
                                Horario = new DateTime(FI.Year, FI.Month, FI.Day + aux2, 10, 50, 00)
                            };

                            await camplogicServices.PostPatrullaActividadAsync(nuevaActividad2);

                            var nuevaActividad3 = new Patrulla_Actividad
                            {
                                FechaAsignacion = FI,
                                ActividadId = (ActTercero[aux, i]).Id,
                                PatrullaId = patrulla.Id,
                                Horario = new DateTime(FI.Year, FI.Month, FI.Day + aux2, 13, 00, 00)
                            };

                            await camplogicServices.PostPatrullaActividadAsync(nuevaActividad3);

                            aux2 = aux2 + 1;
                        }

                        aux = aux + 1;
                    }

                    TemporadasList = new ObservableCollection<Temporadas>(temporadas.Reverse().Skip(1));
                    UltimaTemporadasList = new ObservableCollection<Temporadas>(temporadas.Where(x=>x.FechaInicial.Date == FI.Date && x.FechaFinal.Date == FF.Date));
                });
            }
        }


        //DURANTE TEMPORADA

        private ObservableCollection<Temporadas> _temporadaList { get; set; }
        public ObservableCollection<Temporadas> TemporadasList
        {
            get { return _temporadaList; }
            set
            {
                _temporadaList = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<Temporadas> _ultimaTemporadaList { get; set; }
        public ObservableCollection<Temporadas> UltimaTemporadasList
        {
            get { return _ultimaTemporadaList; }
            set
            {
                _ultimaTemporadaList = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<PatrullaCampista> _duranteCampistas;
        public ObservableCollection<PatrullaCampista> DuranteCampistas
        {
            get { return _duranteCampistas; }
            set
            {
                _duranteCampistas = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<string> _porPatrulla;
        public ObservableCollection<string> PorPatrulla
        {
            get { return _porPatrulla; }
            set
            {
                _porPatrulla = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<string> _porTransporte;
        public ObservableCollection<string> PorTransporte
        {
            get { return _porTransporte; }
            set
            {
                _porTransporte = value;
                OnPropertyChanged();
            }
        }

        private int _CampistasPresentes;
        public int CampistasPresentes
        {
            get { return _CampistasPresentes; }
            set
            {
                _CampistasPresentes = value;
                OnPropertyChanged();
            }
        }

        public TemporadaViewModel(Temporadas temp)
        {
            InitializeDataAsync2(temp);
        }

        public async void InitializeDataAsync2(Temporadas temp)
        {
            var patrullastemps = await camplogicServices.GetTemporadasPatrullasAsync();
            var campistas = await camplogicServices.GetPatrullasCampistasAsync();
            ObservableCollection<PatrullaCampista> DuranteCampistas2 = new ObservableCollection<PatrullaCampista>();
            var pt = patrullastemps.Where(x => x.TemporadaId == temp.Id);
            var porPat = new ObservableCollection<string>();
            PorTransporte = new ObservableCollection<string>();
            int cantp = 0;
            porPat.Add("Todas");
            foreach (var patrulla in pt)
            {
                var cp = campistas.Where(x => x.PatrullaId == patrulla.PatrullaId && x.FechaAsignada.Date == temp.FechaInicial.Date);
                foreach (var campista in cp)
                {
                    DuranteCampistas2.Add(campista);
                    if (campista.Campista.Asistencia == true)
                    {
                        cantp = cantp + 1;
                    }
                    
                }
                porPat.Add(patrulla.Patrulla.Nombre);

            }

            DuranteCampistas = new ObservableCollection<PatrullaCampista>(DuranteCampistas2.OrderByDescending(x => x.Campista.Fecha_Nacimiento));
            PorPatrulla = porPat;
            PorTransporte.Add("Todos");
            PorTransporte.Add("En Transporte");
            CampistasPresentes = cantp;
        }

        //Horario
        public async Task<object[]> InitializeHorario(Temporadas temp)
        {
            var patrullastemps = await camplogicServices.GetTemporadasPatrullasAsync();
            var actTemp = await camplogicServices.GetActividadesPatrullaAsync();
            var pt = patrullastemps.Where(x => x.TemporadaId == temp.Id).ToList();
            var H1B = new TimeSpan(09,00,00);
            var H2B = new TimeSpan(10,50,00);
            var H3B = new TimeSpan(13,00,00);
            List<List<Patrulla_Actividad>> Act1B = new List<List<Patrulla_Actividad>>();
            List<List<Patrulla_Actividad>> Act2B = new List<List<Patrulla_Actividad>>();
            List<List<Patrulla_Actividad>> Act3B = new List<List<Patrulla_Actividad>>();

            foreach (var pat in pt)
            {
                Act1B.Add(actTemp.Where(x => x.PatrullaId == pat.PatrullaId && x.FechaAsignacion.Date == temp.FechaInicial && x.Horario.TimeOfDay == H1B).ToList());

                Act2B.Add(actTemp.Where(x => x.PatrullaId == pat.PatrullaId && x.FechaAsignacion.Date == temp.FechaInicial && x.Horario.TimeOfDay == H2B).ToList());

                Act3B.Add(actTemp.Where(x => x.PatrullaId == pat.PatrullaId && x.FechaAsignacion.Date == temp.FechaInicial && x.Horario.TimeOfDay == H3B).ToList());
            }

            object[] IG = new object[]
            {
                pt,
                Act1B,
                Act2B,
                Act3B
            };

            return IG;
        }

        //Horario para Almacen
        public async Task<object[]> InitializeHorarioAlmacen()
        {
            var patrullastemps = await camplogicServices.GetTemporadasPatrullasAsync();
            var actTemp = await camplogicServices.GetActividadesPatrullaAsync();
            var temporadas = await camplogicServices.GetTemporadasAsync();
            object[] IG = new object[4];
            if (temporadas.Count() != 0)
            {
                Temporadas tempAux = temporadas.LastOrDefault();
                Temporadas tempo;
                if (tempAux.FechaInicial.Date <= DateTime.Today.Date && tempAux.FechaFinal.Date >= DateTime.Today.Date)
                {
                    tempo = temporadas.Last(x => x.FechaInicial.Date <= DateTime.Today.Date && x.FechaFinal.Date >= DateTime.Today.Date);
                }
                else
                {
                    tempo = new Temporadas();
                }

                var pt = patrullastemps.Where(x => x.TemporadaId == tempo.Id).ToList();
                var H1B = new TimeSpan(09, 00, 00);
                var H2B = new TimeSpan(10, 50, 00);
                var H3B = new TimeSpan(13, 00, 00);
                List<List<Patrulla_Actividad>> Act1B = new List<List<Patrulla_Actividad>>();
                List<List<Patrulla_Actividad>> Act2B = new List<List<Patrulla_Actividad>>();
                List<List<Patrulla_Actividad>> Act3B = new List<List<Patrulla_Actividad>>();
                if (pt.Count() != 0)
                {
                    foreach (var pat in pt)
                    {
                        Act1B.Add(actTemp.Where(x => x.PatrullaId == pat.PatrullaId && x.FechaAsignacion.Date == tempo.FechaInicial && x.Horario.TimeOfDay == H1B).ToList());

                        Act2B.Add(actTemp.Where(x => x.PatrullaId == pat.PatrullaId && x.FechaAsignacion.Date == tempo.FechaInicial && x.Horario.TimeOfDay == H2B).ToList());

                        Act3B.Add(actTemp.Where(x => x.PatrullaId == pat.PatrullaId && x.FechaAsignacion.Date == tempo.FechaInicial && x.Horario.TimeOfDay == H3B).ToList());
                    }

                    IG[0] = pt;
                    IG[1] = Act1B;
                    IG[2] = Act2B;
                    IG[3] = Act3B;
                }
                
                
                
            }

            return IG;
        }

        //Lista Capistas
        public ObservableCollection<PatrullaCampista> CampistasList()
        {
            return _duranteCampistas;
        }

        //Search Campistas
        public ObservableCollection<PatrullaCampista> SearchNombreCampistas(string keyword)
        {
            return new ObservableCollection<PatrullaCampista>(_duranteCampistas.Where(x => x.Campista.Nombre.ToLower().Contains(keyword.ToLower()) || x.Campista.Apellido.ToLower().Contains(keyword.ToLower())));
        }

        //Search CampistasPat
        public ObservableCollection<PatrullaCampista> SearchCampistasPat(string keyword, string patrulla)
        {
            return new ObservableCollection<PatrullaCampista>(_duranteCampistas.Where(x => (x.Campista.Nombre.ToLower().Contains(keyword.ToLower()) || x.Campista.Apellido.ToLower().Contains(keyword.ToLower())) && x.Campista.NombrePatrulla == patrulla));
        }

        //Search CampistasTrans
        public ObservableCollection<PatrullaCampista> SearchCampistasTrans(string keyword)
        {
            return new ObservableCollection<PatrullaCampista>(_duranteCampistas.Where(x => (x.Campista.Nombre.ToLower().Contains(keyword.ToLower()) || x.Campista.Apellido.ToLower().Contains(keyword.ToLower())) && x.Campista.Transporte == true));
        }

        //Search CampistasPatTrans
        public ObservableCollection<PatrullaCampista> SearchCampistasPatTrans(string keyword, string patrulla)
        {
            return new ObservableCollection<PatrullaCampista>(_duranteCampistas.Where(x => (x.Campista.Nombre.ToLower().Contains(keyword.ToLower()) || x.Campista.Apellido.ToLower().Contains(keyword.ToLower())) && x.Campista.NombrePatrulla == patrulla && x.Campista.Transporte == true));
        }

        //Search Transporte
        public ObservableCollection<PatrullaCampista> FiltrarPorTransporte(string keyword)
        {
            
            return new ObservableCollection<PatrullaCampista>(_duranteCampistas.Where(x => x.Campista.Transporte == true));
            
        }

        //Search Patrulla
        public ObservableCollection<PatrullaCampista> FiltrarPorPatrulla(string keyword)
        {
            
            return new ObservableCollection<PatrullaCampista>(_duranteCampistas.Where(x => x.Patrulla.Nombre == keyword));
            
        }

        //Search Patrulla Trasnporte
        public ObservableCollection<PatrullaCampista> FiltrarPorPatTrans(string patrulla)
        {

            return new ObservableCollection<PatrullaCampista>(_duranteCampistas.Where(x => x.Patrulla.Nombre == patrulla && x.Campista.Transporte == true));

        }



        //Entrada - Salida 

        private Actividades _actividad { get; set; }
        public Actividades Actividad
        {
            get { return _actividad; }
            set
            {
                _actividad = value;
                OnPropertyChanged();
            }
        }

        private List<ActividadProducto> _actividadProd { get; set; }
        public List<ActividadProducto> ActividadProdList
        {
            get { return _actividadProd; }
            set
            {
                _actividadProd = value;
                OnPropertyChanged();
            }
        }

        private Temporada_Patrulla _patrulla { get; set; }
        public Temporada_Patrulla Patrulla
        {
            get { return _patrulla; }
            set
            {
                _patrulla = value;
                OnPropertyChanged();
            }
        }

        private List<PatrullaEmpleado> _guiaPatrulla { get; set; }
        public List<PatrullaEmpleado> GuiasPatrulla
        {
            get { return _guiaPatrulla; }
            set
            {
                _guiaPatrulla = value;
                OnPropertyChanged();
            }
        }

        

        public TemporadaViewModel(string act, int row)
        {
            InitializeDataAsync3(act, row);
        }

        public async void InitializeDataAsync3(string act, int row)
        {
            var actividades = await camplogicServices.GetActividadesAsync();
            var actprods = await camplogicServices.GetActividadesProductosAsync();
            var temporadas = await camplogicServices.GetTemporadasAsync();
            var pattemp = await camplogicServices.GetTemporadasPatrullasAsync();
            
            Actividad = actividades.FirstOrDefault(x => x.Nombre == act);
            ActividadProdList = actprods.Where(x => x.ActividadId == Actividad.Id).ToList();
            var tempActual = temporadas.LastOrDefault(x => x.FechaInicial.Date <= DateTime.Today.Date && x.FechaFinal.Date >= DateTime.Today.Date);
            

            int aux = 0;
            var patrullas = pattemp.Where(x => x.TemporadaId == tempActual.Id);

            foreach (var patrulla in patrullas)
            {
                if (aux == row)
                {
                    Patrulla = patrulla;
                }

                aux = aux + 1;
            }

            GuiasPatrulla = (await camplogicServices.GetPatrullasEmpleadoAsync()).Where(x => x.FechaAsignacion.Date == tempActual.FechaInicial.Date && x.PatrullaId == Patrulla.PatrullaId).ToList();
            

        }

        //Salida
        public ICommand SalidaKit
        {
            get
            {
                return new Command<PatrullaEmpleado>(async (EMP) =>
                {
                    int cant = 0;
                    foreach (var item in ActividadProdList)
                    {
                        if (item.EsIndividual == true)
                        {
                            cant = cant + item.Cantidad * Patrulla.CantidadCampistas;
                        }
                        else
                        {
                            cant = cant + item.Cantidad;
                        }
                    }

                    var nsalida = new Salida
                    {
                        EmpleadoId = EMP.EmpleadoId,
                        Fecha = DateTime.Now,
                        Cantidad = cant
                    };

                    await camplogicServices.PostSalidasAsync(nsalida);

                    var usalida = (await camplogicServices.GetSalidasAsync()).LastOrDefault();

                    foreach (var item in ActividadProdList)
                    {
                        if (item.EsIndividual == true)
                        {
                            cant = item.Cantidad * Patrulla.CantidadCampistas;
                        }
                        else
                        {
                            cant = item.Cantidad;
                        }

                        var nsalidap = new Salidas_Producto
                        {
                            Cantidad = cant,
                            SalidaPuntual = false,
                            EstaPendiente = false,
                            ProductoId = item.ProductoId,
                            SalidaId = usalida.Id
                        };

                        await camplogicServices.PostSalidasProdAsync(nsalidap);

                        var ProdEdit = new Producto
                        {
                            Id = item.Producto.Id,
                            Articulo = item.Producto.Articulo,
                            Descripcion = item.Producto.Descripcion,
                            CantidadIdeal = item.Producto.CantidadIdeal,
                            Disponible = item.Producto.Disponible - cant,
                            PuntoReorden = item.Producto.PuntoReorden,
                            Existencia_Inicial = item.Producto.Existencia_Inicial,
                            Valor_Inicial = item.Producto.Valor_Inicial,
                            ProveedorId = item.Producto.ProveedorId,
                            SubCategoriaId = item.Producto.SubCategoriaId,
                            UbicacionId = item.Producto.UbicacionId
                        };

                        await camplogicServices.PutProductAsync(ProdEdit);
                    }

                });
            }
        }

        //Entrada
        public ICommand EntradaKit
        {
            get
            {
                return new Command<object[]>(async (EN) =>
                {
                    var actpro = (ActividadProducto)EN[1];
                    var nreintegro = new Reintegros
                    {
                        CantidadTotal = (int)EN[0],
                        FechaEntrada = DateTime.Now
                    };

                    await camplogicServices.PostReintegroAsync(nreintegro);
                    var lastR = (await camplogicServices.GetReintegrosAsync()).LastOrDefault();
                    var nreintegroP = new ReintegroProducto
                    {
                        Cantidad = (int)EN[0],
                        ReintegroId = lastR.Id,
                        Faltante = 0,
                        ProductoId = actpro.ProductoId
                    };

                    await camplogicServices.PostReintegroProdAsync(nreintegroP);

                    var productoCambio = new Producto
                    {
                        Id = actpro.Producto.Id,
                        Articulo = actpro.Producto.Articulo,
                        Descripcion = actpro.Producto.Descripcion,
                        Disponible = actpro.Producto.Disponible + (int)EN[0],
                        PuntoReorden = actpro.Producto.PuntoReorden,
                        Existencia_Inicial = actpro.Producto.Existencia_Inicial,
                        Valor_Inicial = actpro.Producto.Valor_Inicial,
                        CantidadIdeal = actpro.Producto.CantidadIdeal,
                        SubCategoriaId = actpro.Producto.SubCategoriaId,
                        ProveedorId = actpro.Producto.ProveedorId

                    };

                    var response = await camplogicServices.PutProductAsync(productoCambio);
                });
            }
        }




        
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
