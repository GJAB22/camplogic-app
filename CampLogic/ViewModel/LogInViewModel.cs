﻿using CampLogic.Model;
using CampLogic.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using CampLogic.Helpers;

namespace CampLogic.ViewModel
{
    public class LogInViewModel 
    {
        private CampLogicService campLService = new CampLogicService();

        public string Correo { get; set; }
        public string Password { get; set; }

        public ICommand LogInCommand
        {
            get
            {
                return new Command<string[]>(async(LL) =>
                {
                    
                    Password = LL[1];

                    var emp = await campLService.GetEmpleadoCedulaAsync(LL[0]);

                    var accesstoken = await campLService.LogInAsync(emp.Correo,Password);

                    Settings.AccessToken = accesstoken;
                    Settings.Username = emp.Nombre+" "+emp.Apellido;
                    Settings.Password = Password;
                    Settings.Cedula = emp.Cedula;
                    Settings.Cargo = emp.Cargo;

                    Application.Current.MainPage = new MainPage();

                    
                });
            }
        }

        public LogInViewModel()
        {
            Correo = Settings.Username;
            Password = Settings.Password;
        }

        public async Task<bool> ValidarCedula(string cedula)
        {
            var emplado = (await campLService.GetEmpleadosAsync()).FirstOrDefault(b=>b.Cedula == cedula);
            if (emplado == null)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public async Task<bool> ValidarPassword(string password)
        {
            return true;
        }
    }
}
