﻿using CampLogic.HelpClasses;
using CampLogic.Model;
using CampLogic.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.ViewModel
{
    public class ReportesViewModel : INotifyPropertyChanged
    {
        CampLogicService camplogicService = new CampLogicService();

        // Variables para Reporte de Inventario
        private List<string> _informesInventarioList { get; set; }
        public List<string> InformesInventarioList
        {
            get { return _informesInventarioList; }
            set
            {
                _informesInventarioList = value;
                OnPropertyChanged();
            }
        }

        private List<string> _productList { get; set; }
        public List<string> ProductList
        {
            get { return _productList; }
            set
            {
                _productList = value;
                OnPropertyChanged();
            }
        }

        private List<string> _categoriaList { get; set; }
        public List<string> CateList
        {
            get { return _categoriaList; }
            set
            {
                _categoriaList = value;
                OnPropertyChanged();
            }
        }

        private List<string> _filtrarInventarioList { get; set; }
        public List<string> FiltrarInventarioList
        {
            get { return _filtrarInventarioList; }
            set
            {
                _filtrarInventarioList = value;
                OnPropertyChanged();
            }
        }

        //Variables para Reporte de Ordenes de Compra
        private List<string> _ordenesList { get; set; }
        public List<string> OrdenesList
        {
            get { return _ordenesList; }
            set
            {
                _ordenesList = value;
                OnPropertyChanged();
            }
        }

        private List<string> _opcionesList { get; set; }
        public List<string> OpcionesList
        {
            get { return _opcionesList; }
            set
            {
                _opcionesList = value;
                OnPropertyChanged();
            }
        }

        private List<string> _opciones2List { get; set; }
        public List<string> Opciones2List
        {
            get { return _opciones2List; }
            set
            {
                _opciones2List = value;
                OnPropertyChanged();
            }
        }

        private List<string> _ubicacionList { get; set; }
        public List<string> UbList
        {
            get { return _ubicacionList; }
            set
            {
                _ubicacionList = value;
                OnPropertyChanged();
            }
        }

        public ReportesViewModel()
        {
            InitializeDataAsync();
        }
        
        public async void InitializeDataAsync()
        {
           
            //Reportes de Inventario
            InformesInventarioList = new List<string>()
            {
                "Artículo",
                "Ubicación",
                "Categoría"
            };

            
            FiltrarInventarioList = new List<string>()
            {
                "Todos",
                "Por debajo del Punto Crítico"
            };

            //Reportes de Ordenes de Compra
            OrdenesList = new List<string>()
            {
                "Todas",
                "Pendientes"
            };

            OpcionesList = new List<string>()
            {
                "Todas",
                "Proveedor"
            };

            CateList = await CategoriaList();
            ProductList = await ProdsList();
            UbList = await UbicacionList();

        }

        //Reporte de Inventario
        public async Task<List<string>> ProdsList()
        {
            var productList = new List<string>();

            productList.Add("Todos");

            var prods = await ProductosList();
            var prodsOrdenados = prods.OrderBy(x => x.Articulo).ToList();

            foreach (var item in prodsOrdenados)
            {
                productList.Add(item.Articulo);
            }
            return productList;
        }
        
        //Picker Articulo
        public async Task<List<Producto>> ProductosList()
        {
            var prods = await camplogicService.GetProductosAsync();
            var Prods = prods.OrderBy(x => x.Articulo).ToList();
            return Prods;
        } 
        
        //Ordenes de Compra
        public async Task<List<string>> ProveedorList()
        {
            var provList = new List<string>();

            var provs = await camplogicService.GetProveedoresAsync();
            var PROVS = provs.OrderBy(x => x.Descripcion);
            provList.Add("Todos");
            foreach (var item in PROVS)
            {
                provList.Add(item.Descripcion);
            }

            return provList;
        }

        //Faltantes
        public async Task<List<string>> CategoriaList()
        {
            var catL = new List<string>();
            var categorias = await camplogicService.GetCategoriasAsync();
            catL.Add("Todas");
            foreach (var item in categorias)
            {
                catL.Add(item.Nombre);
            }

            return catL;
        }

        public async Task<List<string>> SubCategoriaList(string categoria)
        {
            var catL = new List<string>();
            var categorias = await camplogicService.GetSubCategoriasAsync();
            var subcatfiltrada = categorias.Where(x => x.Categoria.Nombre == categoria);
            
            foreach (var item in subcatfiltrada)
            {
                catL.Add(item.Nombre);
            }

            return catL;
        }

        public async Task<List<string>> ProdsFiltraSubList(string subcategoria)
        {
            var catL = new List<string>();
            var categorias = await camplogicService.GetProductosAsync();
            var subcatfiltrada = categorias.Where(x => x.SubCategoria.Nombre == subcategoria);

            foreach (var item in subcatfiltrada)
            {
                catL.Add(item.Articulo);
            }

            return catL;
        }

        public async Task<List<string>> ProdsFiltraCatList(string categoria)
        {
            var catL = new List<string>();
            var categorias = await camplogicService.GetProductosAsync();
            var subcatfiltrada = categorias.Where(x => x.SubCategoria.Categoria.Nombre == categoria);

            foreach (var item in subcatfiltrada)
            {
                catL.Add(item.Articulo);
            }

            return catL;
        }

        public async Task<List<string>> UbicacionList()
        {
            var catL = new List<string>();
            var categorias = await camplogicService.GetUbicacionAsync();
            catL.Add("Todas");
            foreach (var item in categorias)
            {
                catL.Add(item.Nombre);
            }

            return catL;
        }

        public async Task<List<string>> ProdsFiltraUbList(string ubicacion)
        {
            var catL = new List<string>();
            var categorias = await camplogicService.GetProductosAsync();
            var subcatfiltrada = categorias.Where(x => x.Ubicacion.Nombre == ubicacion);

            foreach (var item in subcatfiltrada)
            {
                catL.Add(item.Articulo);
            }

            return catL;
        }






        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
