﻿using CampLogic.Helpers;
using CampLogic.Model;
using CampLogic.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CampLogic.ViewModel
{
    public class NoticiasViewModel : INotifyPropertyChanged
    {
        CampLogicService camplogicService = new CampLogicService();

        private ObservableCollection<Noticias> _noticias { get; set; }
        public ObservableCollection<Noticias> Noticias
        {
            get { return _noticias; }
            set
            {
                _noticias = value;
                OnPropertyChanged();
            }
        }

        public string NoticiaNueva { get; set; }

        public DateTime Fecha_NoticiaNueva { get; set; }

        public string CedulaEmp { get; set; }

        public ICommand AgregarNoticia
        {
            get
            {
                return new Command<string>(async(NN) =>
                {

                    CedulaEmp = Settings.Cedula;
                    var emp = await camplogicService.GetEmpleadoCedulaAsync(CedulaEmp);

                    var noticiaNueva = new Noticias
                    {
                        Noticia = NN,
                        Fecha_Noticia = DateTime.Now,
                        EmpleadoId = emp.Id
                    };

                    var IsSucces = await camplogicService.PostNoticiaAsync(noticiaNueva);
                    Noticias = await camplogicService.GetNoticiaAsync();
                });
                
                
            }
        } 


        public NoticiasViewModel()
        {
            InitializeDataAsync();
        }

        public async void InitializeDataAsync()
        {
            
            Noticias = await camplogicService.GetNoticiaAsync();
        }
        

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
