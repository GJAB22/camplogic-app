﻿using CampLogic.Helpers;
using CampLogic.Model;
using CampLogic.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.ViewModel
{
    public class DetallesProductoViewModel : INotifyPropertyChanged
    {

        CampLogicService camplogicService = new CampLogicService();

        //Detalles del Producto
        private string _descripcion { get; set; }
        public string Descripcion
        {
            get { return _descripcion; }
            set
            {
                _descripcion = value;
                OnPropertyChanged();
            }
        }

        private int _cantidadActual { get; set; }
        public int CantidadActual
        {
            get { return _cantidadActual; }
            set
            {
                _cantidadActual = value;
                OnPropertyChanged();
            }
        }

        private Categoria _categoria { get; set; }
        public Categoria Categoria
        {
            get { return _categoria; }
            set
            {
                _categoria = value;
                OnPropertyChanged();
            }
        }

        private SubCategoria _subcategoria { get; set; }
        public SubCategoria SubCategoria
        {
            get { return _subcategoria; }
            set
            {
                _subcategoria = value;
                OnPropertyChanged();
            }
        }

        private Proveedor _proveedor { get; set; }
        public Proveedor Proveedor
        {
            get { return _proveedor; }
            set
            {
                _proveedor = value;
                OnPropertyChanged();
            }
        }

        private float _costoUnitario { get; set; }
        public float CostoUnitario
        {
            get { return _costoUnitario; }
            set
            {
                _costoUnitario = value;
                OnPropertyChanged();
            }
        }

        private List<Proveedor> _proveedores { get; set; }
        public List<Proveedor> Proveedores
        {
            get { return _proveedores; }
            set
            {
                _proveedores = value;
                OnPropertyChanged();
            }
        }

        private List<SubCategoria> _subcategorias { get; set; }
        public List<SubCategoria> SubCategorias
        {
            get { return _subcategorias; }
            set
            {
                _subcategorias = value;
                OnPropertyChanged();
            }
        }
        
        //public ICommand AgregarCesta
        //{
        //    get
        //    {
        //        return new Command(async () =>
        //        {
        //            var CedulaEmp = Settings.Cedula;
        //            var empleado = await camplogicService.GetEmpleadoCedulaAsync(CedulaEmp);
        //            var nuevoProdCesta = new Cesta()
        //            {
        //                Cantidad = CantidadOrden,
        //                Precio = PrecioU,
        //                ProductoId = Convert.ToInt32(ProdId),
        //                EmpleadoId = empleado.Id
        //            };

        //            var responseCesta = await camplogicService.PostProdCestaAsync(nuevoProdCesta);



        //        });
        //    }
        //}


        //Crear Nueva Cotizacion
        public float CostoU { get; set; }

        
        //public ICommand SolicitudNueva
        //{
        //    get
        //    {
        //        return new Command(async () =>
        //        {
        //            int Numb = 2;

        //            var cedula = Settings.Cedula;
        //            var emp = await camplogicService.GetEmpleadoCedulaAsync(cedula);

        //            var solicitud = new Solicitud()
        //            {
        //                Fecha_Solicitud = DateTime.Now,
        //                EmpleadoId = emp.Id,
        //                Num = Numb
        //            };

        //            await camplogicService.PostSolicitudAsync(solicitud);

        //            var solicitudList = await camplogicService.GetSolicitudListAsync();

        //            var solicitudCreada = solicitudList.LastOrDefault();

        //            var solicitudProducto = new Solicitud_Producto()
        //            {
        //                Cantidad = Cantidad,
        //                ProductoId = Convert.ToInt32(ProdId),
        //                SolicitudId = solicitudCreada.Id,
        //                Resolucion = false
        //            };

        //            await camplogicService.PostSolicitudProdAsync(solicitudProducto);


        //        });
        //    }
        //}

        //Cambiar Estado Solicitud


        //// Solicitud Entregada
        //public ICommand SolicitudEntregada
        //{
        //    get
        //    {
        //        return new Command(async () =>
        //        {
        //            //Registrar Solicitud Entregada
        //            var solLista = await camplogicService.GetSolicitudProdAsync(_solprod.Id);

        //            var solicitudLista = new Solicitud_Producto
        //            {
        //                Id = _solprod.Id,
        //                Cantidad = Convert.ToInt32(_solprod.Cantidad),
        //                Resolucion = true,
        //                SolicitudId = solLista.SolicitudId,
        //                ProductoId = Convert.ToInt32(_solprod.ProductoId),
        //            };

        //            await camplogicService.PutSolicitudAsync(solicitudLista);

        //            //Generar Salida Nueva
        //            var salidaNueva = new Salida
        //            {
        //                Fecha = DateTime.Now,
        //                Cantidad = Convert.ToInt32(_solprod.Cantidad),
        //                EmpleadoId = Convert.ToInt32(_solprod.Solicitud.EmpleadoId),
        //                ProductoId = Convert.ToInt32(_solprod.ProductoId),
        //            };

        //            await camplogicService.PostSalidasAsync(salidaNueva);

        //            //Descontar de Inventario
        //            var producto = await camplogicService.GetProductoAsync(Convert.ToInt32(_solprod.ProductoId));

        //            var productoCambio = new Producto
        //            {
        //                Id = _solprod.ProductoId,
        //                Descripcion = _solprod.Producto.Descripcion,
        //                Disponible = _solprod.Producto.Disponible - Convert.ToInt32(_solprod.Cantidad),
        //                PuntoReorden = _solprod.Producto.PuntoReorden
        //            };

        //            await camplogicService.PutProductAsync(productoCambio);
        //        });
        //    }
        //}

        
        public DetallesProductoViewModel(Producto producto)
        {
            InitializeDataAsync(producto);
        }

        public async void InitializeDataAsync(Producto producto)
        {
            var proveedores = await camplogicService.GetProveedoresAsync();
            var subcategorias = await camplogicService.GetSubCategoriasAsync();
            Descripcion = producto.Descripcion;
            Categoria = producto.SubCategoria.Categoria;
            SubCategoria = producto.SubCategoria;
            Proveedor = producto.Proveedor;
            CantidadActual = producto.Disponible;

            var cotizacion = await camplogicService.GetCotizacionProdAsync();
            var lastCot = cotizacion.LastOrDefault(b => b.ProductoId == producto.Id);

            CostoUnitario = lastCot.Precio / lastCot.Cantidad;

            SubCategorias = subcategorias.Where(x=>x.CategoriaId == producto.SubCategoria.CategoriaId).OrderBy(x => x.Nombre).ToList();
            Proveedores = proveedores.OrderBy(x => x.Descripcion).ToList();

        }

        //Cambio de Producto
        public async void CambioProducto(Producto prod, string descripcion, SubCategoria subcategoria, double costoUnitario, Proveedor proveedor)
        {
            var cambioproducto = new Producto()
            {
                Id = prod.Id,
                Articulo = prod.Articulo,
                Descripcion = descripcion,
                SubCategoriaId = subcategoria.Id,
                ProveedorId = proveedor.Id
            };

            await camplogicService.PutProductAsync(cambioproducto);
        }

        //Cambio de Costo
        public async void CambioCosto(Producto prod, double costoUnitario, DateTime date)
        {
            //Empleado
            var CedulaEmp = Settings.Cedula;
            var empleado = await camplogicService.GetEmpleadoCedulaAsync(CedulaEmp);

            //Cotizacion
            var nuevacotizacion = new Cotizacion
            {
                Fecha_Cotizacion = date,
                EmpleadoId = empleado.Id

            };

            var resp = await camplogicService.PostCotizacionAsync(nuevacotizacion);

            var cotizaciones = await camplogicService.GetCotizacionListAsync();

            var lastCot = cotizaciones.LastOrDefault();

            //Cotizacion-Producto
            var nuevacotprod = new Cotizacion_Producto
            {
                Cantidad = 1,
                Precio = (float)costoUnitario,
                ProductoId = prod.Id,
                CotizacionId = lastCot.Id
            };

            await camplogicService.PostCotizacionProdAsync(nuevacotprod);
        }
        

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
