﻿using CampLogic.HelpClasses;
using CampLogic.Model;
using CampLogic.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CampLogic.ViewModel
{
    public class ActividadesViewModel : INotifyPropertyChanged
    {
        CampLogicService camplogicService = new CampLogicService();

        private ObservableCollection<Actividades> _actividadesList { get; set; }
        public ObservableCollection<Actividades> ActividadesList
        {
            get { return _actividadesList; }
            set
            {
                _actividadesList = value;
                OnPropertyChanged();
            }
        }

        private List<string> _clasificacion { get; set; }
        public List<string> Clasificacion
        {
            get { return _clasificacion; }
            set
            {
                _clasificacion = value;
                OnPropertyChanged();
            }
        }

        private List<string> _clasificacion2 { get; set; }
        public List<string> Clasificacion2
        {
            get { return _clasificacion2; }
            set
            {
                _clasificacion2 = value;
                OnPropertyChanged();
            }
        }

        private List<Categoria> _categorias { get; set; }
        public List<Categoria> Categorias
        {
            get { return _categorias; }
            set
            {
                _categorias = value;
                OnPropertyChanged();
            }
        }

        //Agregar Producto  Actividad
        private ObservableCollection<ProductoActividad> _actividadesProdList = new ObservableCollection<ProductoActividad>();
        public ObservableCollection<ProductoActividad> ActividadesProdList
        {
            get { return _actividadesProdList; }
            set
            {
                _actividadesProdList = value;
                OnPropertyChanged();
            }
        }

        //Agregar Producto a Actividad
        public int ProductoId { get; set; }
        public int CantidadS { get; set; }
        public string Categoria { get; set; }
        public string Subcategoria { get; set; }
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        public bool EI { get; set; }

        //Agregar Actividad


        public ActividadesViewModel()
        {

            InitializeDataAsync();
           
        }

        public async void InitializeDataAsync()
        {
            ActividadesList = await camplogicService.GetActividadesAsync();
            var categoria = await camplogicService.GetCategoriasAsync();
            Categorias = categoria.OrderBy(x => x.Nombre).ToList();
            Clasificacion = new List<string>
            {
                "Todas",
                "Pequeños",
                "Medianos",
                "Grandes"
            };

            Clasificacion2 = new List<string>
            {
                "Todos",
                "Pequeños",
                "Medianos",
                "Grandes"
            };
        }

        public async Task<ObservableCollection<Actividades>> FiltrarActividades(string keyword)
        {
            var actividades = await camplogicService.GetActividadesAsync();
            if (keyword == "Todas")
            {
                return ActividadesList = await camplogicService.GetActividadesAsync();
            }
            else
            {
                return ActividadesList = new ObservableCollection<Actividades>(actividades.Where(x => x.Clasificacion == keyword));
            }
        }

        public async Task<ObservableCollection<Actividades>> SearchActividades(string keyword)
        {
            var actividades = await camplogicService.GetActividadesAsync();
            return ActividadesList = new ObservableCollection<Actividades>(actividades.Where(x => x.Nombre.ToLower().Contains(keyword.ToLower())));
        }

        public async Task<ObservableCollection<Actividades>> FilterActividadesCla(string keyword, string clasificacion)
        {
            var actividades = await camplogicService.GetActividadesAsync();
            return ActividadesList = new ObservableCollection<Actividades>(actividades.Where(x => x.Nombre.ToLower().Contains(keyword.ToLower()) && x.Clasificacion == clasificacion));
        }



        //Agregar Producto a Actividad

        //Agregar por ID
        public ICommand AgregarProdId
        {
            get
            {
                return new Command<object[]>(async (DetallesProd) =>
                {
                    ProductoId = (int)DetallesProd[0];
                    CantidadS = (int)DetallesProd[1];
                    EI = (bool)DetallesProd[2];
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.Id == ProductoId);
                    //ProductoActividad
                    var prodact = new ProductoActividad
                    {
                        Producto = prod,
                        Cantidad = CantidadS,
                        EsIndividual = EI
                    };

                    _actividadesProdList.Add(prodact);
                });
            }
        }
        //Agregar Sin SubCategoria, Con Descripcion
        public ICommand AgregarProdCategoriaDes
        {
            get
            {
                return new Command<object[]>(async (DetallesProd) =>
                {
                    Categoria = (string)DetallesProd[0];
                    Articulo = (string)DetallesProd[1];
                    Descripcion = (string)DetallesProd[2];
                    CantidadS = Convert.ToInt32(DetallesProd[3]);
                    EI = (bool)DetallesProd[4];
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.SubCategoria.Categoria.Nombre == Categoria && x.Articulo == Articulo && x.Descripcion == Descripcion);
                    //ProductoActividad
                    var prodact = new ProductoActividad
                    {
                        Producto = prod,
                        Cantidad = CantidadS,
                        EsIndividual = EI
                    };

                    _actividadesProdList.Add(prodact);
                });
            }
        }
        //Agregar Con SubCategoria, Con Descripcion
        public ICommand AgregarProdSubCategoriaDes
        {
            get
            {
                return new Command<object[]>(async (DetallesProd) =>
                {
                    Subcategoria = (string)DetallesProd[0];
                    Articulo = (string)DetallesProd[1];
                    Descripcion = (string)DetallesProd[2];
                    CantidadS = Convert.ToInt32(DetallesProd[4]);
                    EI = (bool)DetallesProd[5];
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.SubCategoria.Nombre == Subcategoria && x.Articulo == Articulo && x.Descripcion == Descripcion);
                    //ProductoActividad
                    var prodact = new ProductoActividad
                    {
                        Producto = prod,
                        Cantidad = CantidadS,
                        EsIndividual = EI
                    };

                    _actividadesProdList.Add(prodact);
                });
            }
        }
        //Agregar Con SubCategoria, Sin Descripcion
        public ICommand AgregarProdSubCategoria
        {
            get
            {
                return new Command<object[]>(async (DetallesProd) =>
                {
                    Subcategoria = (string)DetallesProd[0];
                    Articulo = (string)DetallesProd[1];
                    CantidadS = Convert.ToInt32(DetallesProd[2]);
                    EI = (bool)DetallesProd[3];
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.SubCategoria.Nombre == Subcategoria && x.Articulo == Articulo);
                    //ProductoActividad
                    var prodact = new ProductoActividad
                    {
                        Producto = prod,
                        Cantidad = CantidadS,
                        EsIndividual =EI
                    };

                    _actividadesProdList.Add(prodact);
                });
            }
        }
        //Agregar Sin SubCategoria, Sin Descripcion
        public ICommand AgregarProdCategoria
        {
            get
            {
                return new Command<object[]>(async (DetallesProd) =>
                {
                    Categoria = (string)DetallesProd[0];
                    Articulo = (string)DetallesProd[1];
                    CantidadS = Convert.ToInt32(DetallesProd[2]);
                    EI = (bool)DetallesProd[3];
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.SubCategoria.Categoria.Nombre == Categoria && x.Articulo == Articulo);
                    //ProductoActividad
                    var prodact = new ProductoActividad
                    {
                        Producto = prod,
                        Cantidad = CantidadS,
                        EsIndividual = EI
                    };

                    _actividadesProdList.Add(prodact);
                });
            }
        }


        //Filtrado de Picker
        public async Task<List<SubCategoria>> SearchSubCategoria(int id)
        {
            var subcategoria = await camplogicService.GetSubCategoriasAsync();
            var SubCategorias = subcategoria.Where(x => x.CategoriaId == id).ToList();
            return SubCategorias;
        }
        public async Task<List<Producto>> FilterCategoria(int id)
        {
            var productos = await camplogicService.GetProductosAsync();
            var Productos = productos.OrderBy(x => x.Articulo).Where(x => x.SubCategoria.Categoria.Id == id).ToList();
            return Productos;
        }
        public async Task<List<Producto>> FilterSubCategoria(int idSubCat)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prods = productos.Where(x => x.SubCategoriaId == idSubCat).ToList();
            return prods;
        }
        public async Task<List<Producto>> FilterArticulo(string articulo)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prods = productos.Where(x => x.Articulo == articulo).ToList();
            return prods;
        }

        //Validar Picker
        public async Task<Producto> ProductoF(int id)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prod = productos.FirstOrDefault(x => x.Id == id);
            return prod;
        }
        public async Task<Producto> ProductoCategoriaReintegro(string categoria, string articulo)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prod = productos.FirstOrDefault(x => x.SubCategoria.Categoria.Nombre == categoria && x.Articulo == articulo);
            return prod;
        }
        public async Task<Producto> ProductoSubCategoriaReintegro(string subcategoria, string articulo)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prod = productos.FirstOrDefault(x => x.SubCategoria.Nombre == subcategoria && x.Articulo == articulo);
            return prod;
        }
        public async Task<Producto> ProductoCategoriaDesReintegro(string categoria, string articulo, string descripcion)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prod = productos.FirstOrDefault(x => x.SubCategoria.Categoria.Nombre == categoria && x.Articulo == articulo && x.Descripcion == descripcion);
            return prod;
        }
        public async Task<Producto> ProductoSubCategoriaDesReintegro(string subcategoria, string articulo, string descripcion)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prod = productos.FirstOrDefault(x => x.SubCategoria.Nombre == subcategoria && x.Articulo == articulo && x.Descripcion == descripcion);
            return prod;
        }


        public ICommand RemoveProducto
        {
            get
            {
                return new Command<ProductoActividad>((EP) =>
                {
                    _actividadesProdList.Remove(EP);
                });
            }
        }

        //Agregar Actividad
        public ICommand AgregarActividad
        {
            get
            {
                return new Command<object[]>(async (NA) =>
                {
                    var actividad = new Actividades
                    {
                        Nombre = (string)NA[0],
                        Clasificacion = (string)NA[1],
                        Descripcion = (string)NA[2]
                    };

                    await camplogicService.PostActividadAsync(actividad);
                    var actividades = await camplogicService.GetActividadesAsync();
                    var lastAct = actividades.LastOrDefault();

                    if (_actividadesProdList.Count() != 0)
                    {
                        
                        foreach (var item in _actividadesProdList)
                        {
                            var prodact = new ActividadProducto
                            {
                                Cantidad = item.Cantidad,
                                EsIndividual = item.EsIndividual,
                                ActividadId = lastAct.Id,
                                ProductoId = item.Producto.Id
                            };

                            await camplogicService.PostActividadProductosAsync(prodact);
                        }

                        _actividadesProdList = new ObservableCollection<ProductoActividad>();
                    }

                    _actividadesList.Add(actividad);
                });
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
