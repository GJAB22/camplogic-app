﻿using CampLogic.HelpClasses;
using CampLogic.Model;
using CampLogic.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CampLogic.ViewModel
{
    public class SalidasViewModel : INotifyPropertyChanged
    {
        CampLogicService camplogicService = new CampLogicService();

        private List<SalidasReporte> _salidas { get; set; }
        public List<SalidasReporte> SalidasList
        {
            get { return _salidas; }
            set
            {
                _salidas = value;
                OnPropertyChanged();
            }
        }

        private List<Salida> _topsalidas { get; set; }
        public List<Salida> TopSalidas
        {
            get { return _topsalidas; }
            set
            {
                _topsalidas = value;
                OnPropertyChanged();
            }
        }

        private List<Salidas_Producto> _salidasPuntuales { get; set; }
        public List<Salidas_Producto> SalidasPuntuales
        {
            get { return _salidasPuntuales; }
            set
            {
                _salidasPuntuales = value;
                OnPropertyChanged();
            }
        }

        private List<Empleado> _empleados { get; set; }
        public List<Empleado> Empleados
        {
            get { return _empleados; }
            set
            {
                _empleados = value;
                OnPropertyChanged();
            }
        }

        private List<Categoria> _categorias { get; set; }
        public List<Categoria> Categorias
        {
            get { return _categorias; }
            set
            {
                _categorias = value;
                OnPropertyChanged();
            }
        }


        //SALIDAS PUNTUALES
        //Prod
        public int ProductoId { get; set; }
        public string Categoria { get; set; }
        public string Subcategoria { get; set; }
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        //Detalles
        public int Cantidad { get; set; }
        public Empleado Empleado { get; set; }
        public DateTime Fecha { get; set; }
        public TimeSpan Hora { get; set; }
        public bool SalidaPuntual { get; set; }
        public bool EstaPendiente { get; set; }

        //ENTRADAS PUNTUALES
        public int CantidadE { get; set; }
        public DateTime FechaE { get; set; }
        public TimeSpan HoraE { get; set; }
        public Salidas_Producto SalidaP { get; set; }

        public SalidasViewModel()
        {

            InitializeDataAsync();
            
        }

        public async void InitializeDataAsync()
        {
            var salidas = await camplogicService.GetSalidasProdsAsync();
            var empleados = await camplogicService.GetEmpleadosAsync();
            SalidasPuntuales = salidas.OrderByDescending(x=>x.Id).Where(x=>x.SalidaPuntual == true && x.EstaPendiente == true).ToList();
            Empleados = empleados.Where(x => x.Disponibilidad == true).ToList();
            Categorias = await camplogicService.GetCategoriasAsync();
            
            
        }

        public SalidasViewModel(DateTime fechaI, DateTime fechaF,string categoria,string subcategoria,string articulo,string ubicacion)
        {

            InitializeDataAsync(fechaI,fechaF, categoria, subcategoria, articulo, ubicacion);

        }

        public async void InitializeDataAsync(DateTime FI, DateTime FF, string categoria,string subcategoria,string articulo,string ubicacion)
        {
            var salidasS = await camplogicService.GetSalidasProdsAsync();
            var entradasS = await camplogicService.GetEntradasProdsAsync();
            var entradasN = await camplogicService.GetReintegrosProdAsync();
            var productos = await camplogicService.GetProductosAsync();
            List<Producto> prods = new List<Producto>();
            var salidasList = new List<SalidasReporte>();
            if (categoria != "NT" && subcategoria == "NT" && ubicacion == "NT" && articulo == "NT")
            {
                prods = PorCategoria(categoria, productos);
                foreach (var prod in prods)
                {
                    var EX = ExistenciaInicial(prod, FI, salidasS, entradasS, entradasN);
                    var E = SEntradas(prod, FI, FF, entradasS, entradasN);
                    var TT = Total(EX, E);
                    var salidas = SSalidas(prod, FI, FF, salidasS, entradasN);
                    var rotacion = new SalidasReporte()
                    {
                        Producto = prod,
                        CantidadInicial = EX,
                        Entradas = E,
                        CantidadTotal = TT,
                        Salidas = salidas,
                        PSalida = PS(TT,salidas)
                    };
                    salidasList.Add(rotacion);
                }

                SalidasList = salidasList.OrderBy(x => x.Producto.Articulo).ToList();
            }
            else if (categoria != "NT" && subcategoria == "NT" && ubicacion == "NT" && articulo != "NT")
            {
                prods = PorArticulo(articulo, productos);
                foreach (var prod in prods)
                {
                    var EX = ExistenciaInicial(prod, FI, salidasS, entradasS, entradasN);
                    var E = SEntradas(prod, FI, FF, entradasS, entradasN);
                    var TT = Total(EX, E);
                    var salidas = SSalidas(prod, FI, FF, salidasS, entradasN);
                    var rotacion = new SalidasReporte()
                    {
                        Producto = prod,
                        CantidadInicial = EX,
                        Entradas = E,
                        CantidadTotal = TT,
                        Salidas = salidas,
                        PSalida = PS(TT, salidas)
                    };
                    salidasList.Add(rotacion);
                }

                SalidasList = salidasList.OrderBy(x => x.Producto.Articulo).ToList();
            }
            else if (categoria != "NT" && subcategoria != "NT" && ubicacion == "NT" && articulo == "NT")
            {
                foreach (var prod in prods)
                {
                    var EX = ExistenciaInicial(prod, FI, salidasS, entradasS, entradasN);
                    var E = SEntradas(prod, FI, FF, entradasS, entradasN);
                    var TT = Total(EX, E);
                    var salidas = SSalidas(prod, FI, FF, salidasS, entradasN);
                    var rotacion = new SalidasReporte()
                    {
                        Producto = prod,
                        CantidadInicial = EX,
                        Entradas = E,
                        CantidadTotal = TT,
                        Salidas = salidas,
                        PSalida = PS(TT, salidas)
                    };
                    salidasList.Add(rotacion);
                }

                SalidasList = salidasList.OrderBy(x => x.Producto.Articulo).ToList();
            }
            else if (categoria != "NT" && subcategoria != "NT" && ubicacion == "NT" && articulo != "NT")
            {
                prods = PorArticulo(articulo, productos);
                foreach (var prod in prods)
                {
                    var EX = ExistenciaInicial(prod, FI, salidasS, entradasS, entradasN);
                    var E = SEntradas(prod, FI, FF, entradasS, entradasN);
                    var TT = Total(EX, E);
                    var salidas = SSalidas(prod, FI, FF, salidasS, entradasN);
                    var rotacion = new SalidasReporte()
                    {
                        Producto = prod,
                        CantidadInicial = EX,
                        Entradas = E,
                        CantidadTotal = TT,
                        Salidas = salidas,
                        PSalida = PS(TT, salidas)
                    };
                    salidasList.Add(rotacion);
                }

                SalidasList = salidasList.OrderBy(x => x.Producto.Articulo).ToList();
            }
            else if (categoria == "NT" && subcategoria == "NT" && ubicacion == "NT" && articulo != "NT")
            {
                prods = PorArticulo(articulo, productos);
                foreach (var prod in prods)
                {
                    var EX = ExistenciaInicial(prod, FI, salidasS, entradasS, entradasN);
                    var E = SEntradas(prod, FI, FF, entradasS, entradasN);
                    var TT = Total(EX, E);
                    var salidas = SSalidas(prod, FI, FF, salidasS, entradasN);
                    var rotacion = new SalidasReporte()
                    {
                        Producto = prod,
                        CantidadInicial = EX,
                        Entradas = E,
                        CantidadTotal = TT,
                        Salidas = salidas,
                        PSalida = PS(TT, salidas)
                    };
                    salidasList.Add(rotacion);
                }

                SalidasList = salidasList.OrderBy(x => x.Producto.Articulo).ToList();
            }
            else if (categoria == "NT" && subcategoria == "NT" && ubicacion != "NT" && articulo == "NT")
            {
                prods = PorUbicacion(ubicacion, productos);
                foreach (var prod in prods)
                {
                    var EX = ExistenciaInicial(prod, FI, salidasS, entradasS, entradasN);
                    var E = SEntradas(prod, FI, FF, entradasS, entradasN);
                    var TT = Total(EX, E);
                    var salidas = SSalidas(prod, FI, FF, salidasS, entradasN);
                    var rotacion = new SalidasReporte()
                    {
                        Producto = prod,
                        CantidadInicial = EX,
                        Entradas = E,
                        CantidadTotal = TT,
                        Salidas = salidas,
                        PSalida = PS(TT, salidas)
                    };
                    salidasList.Add(rotacion);
                }

                SalidasList = salidasList.OrderBy(x => x.Producto.Articulo).ToList();
            }
            else if (categoria != "NT" && subcategoria == "NT" && ubicacion != "NT" && articulo == "NT")
            {
                prods = PorUbicacionCat(ubicacion, categoria, productos);
                foreach (var prod in prods)
                {
                    var EX = ExistenciaInicial(prod, FI, salidasS, entradasS, entradasN);
                    var E = SEntradas(prod, FI, FF, entradasS, entradasN);
                    var TT = Total(EX, E);
                    var salidas = SSalidas(prod, FI, FF, salidasS, entradasN);
                    var rotacion = new SalidasReporte()
                    {
                        Producto = prod,
                        CantidadInicial = EX,
                        Entradas = E,
                        CantidadTotal = TT,
                        Salidas = salidas,
                        PSalida = PS(TT, salidas)
                    };
                    salidasList.Add(rotacion);
                }

                SalidasList = salidasList.OrderBy(x => x.Producto.Articulo).ToList();
            }
            else if (categoria != "NT" && subcategoria != "NT" && ubicacion != "NT" && articulo == "NT")
            {
                prods = PorUbicacionSub(ubicacion, subcategoria, productos);
                foreach (var prod in prods)
                {
                    var EX = ExistenciaInicial(prod, FI, salidasS, entradasS, entradasN);
                    var E = SEntradas(prod, FI, FF, entradasS, entradasN);
                    var TT = Total(EX, E);
                    var salidas = SSalidas(prod, FI, FF, salidasS, entradasN);
                    var rotacion = new SalidasReporte()
                    {
                        Producto = prod,
                        CantidadInicial = EX,
                        Entradas = E,
                        CantidadTotal = TT,
                        Salidas = salidas,
                        PSalida = PS(TT, salidas)
                    };
                    salidasList.Add(rotacion);
                }

                SalidasList = salidasList.OrderBy(x => x.Producto.Articulo).ToList();
            }
            else if (categoria != "NT" && subcategoria != "NT" && ubicacion != "NT" && articulo != "NT")
            {
                prods = PorArticulo(articulo, productos);
                foreach (var prod in prods)
                {
                    var EX = ExistenciaInicial(prod, FI, salidasS, entradasS, entradasN);
                    var E = SEntradas(prod, FI, FF, entradasS, entradasN);
                    var TT = Total(EX, E);
                    var salidas = SSalidas(prod, FI, FF, salidasS, entradasN);
                    var rotacion = new SalidasReporte()
                    {
                        Producto = prod,
                        CantidadInicial = EX,
                        Entradas = E,
                        CantidadTotal = TT,
                        Salidas = salidas,
                        PSalida = PS(TT, salidas)
                    };
                    salidasList.Add(rotacion);
                }

                SalidasList = salidasList.OrderBy(x => x.Producto.Articulo).ToList();
            }
            else
            {
                prods = PorArticulo(articulo, productos);
                foreach (var prod in prods)
                {
                    var EX = ExistenciaInicial(prod, FI, salidasS, entradasS, entradasN);
                    var E = SEntradas(prod, FI, FF, entradasS, entradasN);
                    var TT = Total(EX, E);
                    var salidas = SSalidas(prod, FI, FF, salidasS, entradasN);
                    var rotacion = new SalidasReporte()
                    {
                        Producto = prod,
                        CantidadInicial = EX,
                        Entradas = E,
                        CantidadTotal = TT,
                        Salidas = salidas,
                        PSalida = PS(TT, salidas)
                    };
                    salidasList.Add(rotacion);
                }

                SalidasList = salidasList.OrderBy(x => x.Producto.Articulo).ToList();
            }


        }


        public List<Producto> PorCategoria(string categoria, List<Producto> prodWS)
        {
            List<Producto> prods = new List<Producto>();
            if (categoria == "Todas")
            {
                var productos = prodWS;
                prods = productos.OrderBy(x => x.SubCategoria.Categoria.Nombre).ToList();
            }
            else
            {
                var productos = prodWS;
                prods = productos.Where(x => x.SubCategoria.Categoria.Nombre == categoria).ToList();
            }

            return prods;

        }

        public List<Producto> PorSubCategoria(string categoria, string subcategoria, List<Producto> prodWS)
        {
            var productos = prodWS;
            var prods = productos.Where(x => x.SubCategoria.Categoria.Nombre == categoria && x.SubCategoria.Nombre == subcategoria).ToList();
            return prods;
        }

        public  List<Producto> PorUbicacion(string ubicacion, List<Producto> prodWS)
        {
              List<Producto> prods = new List<Producto>();
              prods = prodWS.Where(x => x.Ubicacion.Nombre == ubicacion).ToList();
              return prods;
        }

        public List<Producto> PorUbicacionCat(string ubicacion, string categoria, List<Producto> prodWS)
        {
            List<Producto> prods = new List<Producto>();
            if (categoria == "Todas" && ubicacion == "Todas")
            {
                prods = prodWS.OrderBy(x => x.Articulo).ToList();

            }
            else if (categoria == "Todas")
            {
                prods = prodWS.Where(x => x.Ubicacion.Nombre == ubicacion).ToList();
            }
            else if (ubicacion == "Todas")
            {
                prods = prodWS.Where(x => x.SubCategoria.Categoria.Nombre == categoria).ToList();
            }
            else
            {
                prods = prodWS.Where(x => x.Ubicacion.Nombre == ubicacion && x.SubCategoria.Categoria.Nombre == categoria).ToList();
            }
            return prods;
        }

        public List<Producto> PorUbicacionSub(string ubicacion, string subcategoria, List<Producto> prodWS)
        {
            List<Producto> prods = new List<Producto>();
            if (subcategoria == "Todas")
            {
                prods = prodWS.Where(x => x.Ubicacion.Nombre == ubicacion).ToList();
            }
            else
            {
                prods = prodWS.Where(x => x.Ubicacion.Nombre == ubicacion && x.SubCategoria.Nombre == subcategoria).ToList();
            }
            return prods;
        }

        public List<Producto> PorArticulo(string articulo, List<Producto> prodWS)
        {
            List<Producto> prods = new List<Producto>();
            if (articulo == "Todos")
            {
                var productos = prodWS;
                prods = productos.OrderBy(x => x.Articulo == articulo).ToList();
            }
            else
            {
                var productos = prodWS;
                prods = productos.Where(x => x.Articulo == articulo).ToList();
            }

            return prods;
        }

        public List<Producto> Products(List<Producto> prodWS)
        {
            var productos = prodWS;
            var prods = productos.OrderBy(x => x.Articulo).ToList();
            return prods;
        }

        //Validar Disponibilidad
        public async Task<Producto> ProductoF(int id)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prod = productos.FirstOrDefault(x => x.Id == id);
            return prod;
        }
        public async Task<Producto> ProductoCategoriaSalidasP(string categoria, string articulo)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prod = productos.FirstOrDefault(x => x.SubCategoria.Categoria.Nombre == categoria && x.Articulo == articulo);
            return prod;
        }
        public async Task<Producto> ProductoSubCategoriaSalidasP(string subcategoria, string articulo)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prod = productos.FirstOrDefault(x => x.SubCategoria.Nombre == subcategoria && x.Articulo == articulo);
            return prod;
        }
        public async Task<Producto> ProductoCategoriaDesSalidasP(string categoria, string articulo, string descripcion)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prod = productos.FirstOrDefault(x => x.SubCategoria.Categoria.Nombre == categoria && x.Articulo == articulo && x.Descripcion == descripcion);
            return prod;
        }
        public async Task<Producto> ProductoSubCategoriaDesSalidasP(string subcategoria, string articulo, string descripcion)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prod = productos.FirstOrDefault(x => x.SubCategoria.Nombre == subcategoria && x.Articulo == articulo && x.Descripcion == descripcion);
            return prod;
        }


        //CALCULO DE INFORME DE SALIDAS

        //EXISTENCIA INICIAL
        public int ExistenciaInicial(Producto prod, DateTime FI, List<Salidas_Producto> salidasWS, List<EntradaProducto> entradasWS, List<ReintegroProducto> entradasN)
        {
            int EX = 0;
            int entradasRealizadas = 0;
            int salidasRealizadas = 0;
            //Salidas
            var salidas = salidasWS.Where(x => x.Salida.Fecha.Date < FI.Date && x.ProductoId == prod.Id);
            //Entradas
            var entradas = entradasWS.Where(x => x.Entrada.Fecha.Date < FI && x.ProductoId == prod.Id);
            var entradas2 = entradasN.Where(x => x.Reintegro.FechaEntrada.Date < FI && x.ProductoId == prod.Id);

            //Contar Salidas
            if (salidas.Count() == 0)
            {
                salidasRealizadas = 0;
            }
            else
            {
                foreach (var item in salidas)
                {
                    salidasRealizadas = salidasRealizadas + item.Cantidad;
                }
            }

            //Contar Entradas
            if (entradas.Count() == 0 && entradas2.Count() == 0)
            {
                entradasRealizadas = 0;
            }
            else if (entradas.Count() == 0 && entradas2.Count() != 0)
            {
                foreach (var item in entradas2)
                {
                    entradasRealizadas = entradasRealizadas + item.Cantidad;
                }
            }
            else if (entradas.Count() != 0 && entradas2.Count() == 0)
            {
                foreach (var item in entradas)
                {
                    entradasRealizadas = entradasRealizadas + item.CantidadEntregada;
                }
            }
            else
            {
                foreach (var item in entradas)
                {
                    entradasRealizadas = entradasRealizadas + item.CantidadEntregada;
                }
                foreach (var item in entradas2)
                {
                    entradasRealizadas = entradasRealizadas + item.Cantidad;
                }
            }

            EX = prod.Existencia_Inicial + entradasRealizadas - salidasRealizadas;
            return EX;
        }

        //Entradas en el Periodo
        public int SEntradas(Producto prod, DateTime FI, DateTime FF, List<EntradaProducto> entradasWS, List<ReintegroProducto> entradasN)
        {
            int SE = 0;

            var entradas = entradasWS.Where(b => b.Entrada.Fecha.Date <= FF.Date && b.Entrada.Fecha.Date >= FI.Date && b.ProductoId == prod.Id).ToList();
            var entradas2 = entradasN.Where(b => b.Reintegro.FechaEntrada.Date <= FF.Date && b.Reintegro.FechaEntrada.Date >= FI.Date && b.PorDirector == true && b.ProductoId == prod.Id).ToList();
            if (entradas.Count() == 0 && entradas2.Count() == 0)
            {
                SE = 0;
            }
            else if (entradas.Count() == 0 && entradas2.Count() != 0)
            {
                foreach (var item in entradas2)
                {
                    SE = SE + item.Cantidad;
                }
            }
            else if (entradas.Count() != 0 && entradas2.Count() == 0)
            {
                foreach (var item in entradas)
                {
                    SE = SE + item.CantidadEntregada;
                }
            }
            else
            {
                foreach (var item in entradas)
                {
                    SE = SE + item.CantidadEntregada;
                }
                foreach (var item in entradas2)
                {
                    SE = SE + item.Cantidad;
                }
            }

            return SE;
        }

        //TOTAL
        public int Total(int ex,int entradas)
        {
            int t;
            return t = ex + entradas;
        }

        //Salidas en el Periodo
        public int SSalidas(Producto prod, DateTime FI, DateTime FF, List<Salidas_Producto> salidasWS, List<ReintegroProducto> entradasN)
        {
            int SS = 0;
            int EE = 0;
            int total = 0;
            var entradas2 = entradasN.Where(b => b.Reintegro.FechaEntrada.Date <= FF.Date && b.Reintegro.FechaEntrada.Date >= FI.Date && b.PorDirector == false && b.ProductoId == prod.Id).ToList();
            var salidas = salidasWS.Where(b => b.Salida.Fecha.Date <= FF.Date && b.Salida.Fecha.Date >= FI.Date && b.ProductoId == prod.Id && b.EstaPendiente == false).ToList();
            if (salidas.Count() == 0)
            {
                return SS = 0;
            }
            else
            {
                foreach (var item in salidas)
                {
                    SS = SS + item.Cantidad;
                }

                foreach (var item in entradas2)
                {
                    EE = EE + item.Cantidad;
                }

                return total = SS - EE;
            }

             
        }

        //PORCENTAJE DE SALIDAS
        public double PS(int total, int salidas)
        {
            double ps;
            return ps = salidas * 100 / total;
        }


        //SALIDAS PUNTUALES
        
        public ICommand AgregarProdIdSalidasP
        {
            get
            {
                return new Command<object[]>(async (DetallesProd) =>
                {

                    ProductoId = (int)DetallesProd[0];
                    Cantidad = (int)DetallesProd[1];
                    Empleado = (Empleado)DetallesProd[2];
                    Fecha = (DateTime)DetallesProd[3];
                    Hora = (TimeSpan)DetallesProd[4];

                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.Id == ProductoId);

                    var NuevaSalida = new Salida()
                    {
                        Cantidad = Cantidad,
                        Fecha = new DateTime(Fecha.Year,Fecha.Month,Fecha.Day,Hora.Hours,Hora.Minutes,Hora.Seconds),
                        EmpleadoId = Empleado.Id

                    };

                    await camplogicService.PostSalidasAsync(NuevaSalida);
                    var salidas = await camplogicService.GetSalidasAsync();
                    var ultimaSalidas = salidas.LastOrDefault();

                    var NuevaSalidaP = new Salidas_Producto()
                    {
                        Cantidad = Cantidad,
                        ProductoId = prod.Id,
                        SalidaId = ultimaSalidas.Id,
                        SalidaPuntual = true,
                        EstaPendiente = true
                    };

                    await camplogicService.PostSalidasProdAsync(NuevaSalidaP);
                    

                    var ProdEdit = new Producto
                    {
                        Id = prod.Id,
                        Articulo = prod.Articulo,
                        Descripcion = prod.Descripcion,
                        CantidadIdeal = prod.CantidadIdeal,
                        Disponible = prod.Disponible - Cantidad,
                        PuntoReorden = prod.PuntoReorden,
                        Existencia_Inicial = prod.Existencia_Inicial,
                        Valor_Inicial = prod.Valor_Inicial,
                        ProveedorId = prod.ProveedorId,
                        SubCategoriaId = prod.SubCategoriaId,
                        UbicacionId = prod.UbicacionId
                    };

                    await camplogicService.PutProductAsync(ProdEdit);

                    _salidasPuntuales.Add(NuevaSalidaP);
                });
            }
        }
        //Agregar Sin SubCategoria, Con Descripcion
        public ICommand AgregarProdCategoriaDesSalidasP
        {
            get
            {
                return new Command<object[]>(async (DetallesProd) =>
                {
                    Categoria = (string)DetallesProd[0];
                    Articulo = (string)DetallesProd[1];
                    Descripcion = (string)DetallesProd[2];
                    Cantidad = (int)DetallesProd[3];
                    Empleado = (Empleado)DetallesProd[4];
                    Fecha = (DateTime)DetallesProd[5];
                    Hora = (TimeSpan)DetallesProd[6];
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.SubCategoria.Categoria.Nombre == Categoria && x.Articulo == Articulo && x.Descripcion == Descripcion);

                    var NuevaSalida = new Salida()
                    {
                        Cantidad = Cantidad,
                        Fecha = new DateTime(Fecha.Year, Fecha.Month, Fecha.Day, Hora.Hours, Hora.Minutes, Hora.Seconds),
                        EmpleadoId = Empleado.Id

                    };

                    await camplogicService.PostSalidasAsync(NuevaSalida);
                    var salidas = await camplogicService.GetSalidasAsync();
                    var ultimaSalidas = salidas.LastOrDefault();

                    var NuevaSalidaP = new Salidas_Producto()
                    {
                        Cantidad = Cantidad,
                        ProductoId = prod.Id,
                        SalidaId = ultimaSalidas.Id,
                        SalidaPuntual = true,
                        EstaPendiente = true
                    };

                    await camplogicService.PostSalidasProdAsync(NuevaSalidaP);

                    var ProdEdit = new Producto
                    {
                        Id = prod.Id,
                        Articulo = prod.Articulo,
                        Descripcion = prod.Descripcion,
                        CantidadIdeal = prod.CantidadIdeal,
                        Disponible = prod.Disponible - Cantidad,
                        PuntoReorden = prod.PuntoReorden,
                        Existencia_Inicial = prod.Existencia_Inicial,
                        Valor_Inicial = prod.Valor_Inicial,
                        ProveedorId = prod.ProveedorId,
                        SubCategoriaId = prod.SubCategoriaId,
                        UbicacionId = prod.UbicacionId
                    };

                    await camplogicService.PutProductAsync(ProdEdit);

                    _salidasPuntuales.Add(NuevaSalidaP);
                });
            }
        }
        //Agregar Con SubCategoria, Con Descripcion
        public ICommand AgregarProdSubCategoriaDesSalidasP
        {
            get
            {
                return new Command<object[]>(async (DetallesProd) =>
                {
                    Subcategoria = (string)DetallesProd[0];
                    Articulo = (string)DetallesProd[1];
                    Descripcion = (string)DetallesProd[2];
                    Cantidad = (int)DetallesProd[3];
                    Empleado = (Empleado)DetallesProd[4];
                    Fecha = (DateTime)DetallesProd[5];
                    Hora = (TimeSpan)DetallesProd[6];
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.SubCategoria.Nombre == Subcategoria && x.Articulo == Articulo && x.Descripcion == Descripcion);

                    var NuevaSalida = new Salida()
                    {
                        Cantidad = Cantidad,
                        Fecha = new DateTime(Fecha.Year, Fecha.Month, Fecha.Day, Hora.Hours, Hora.Minutes, Hora.Seconds),
                        EmpleadoId = Empleado.Id

                    };

                    await camplogicService.PostSalidasAsync(NuevaSalida);
                    var salidas = await camplogicService.GetSalidasAsync();
                    var ultimaSalidas = salidas.LastOrDefault();

                    var NuevaSalidaP = new Salidas_Producto()
                    {
                        Cantidad = Cantidad,
                        ProductoId = prod.Id,
                        SalidaId = ultimaSalidas.Id,
                        SalidaPuntual = true,
                        EstaPendiente = true
                    };

                    await camplogicService.PostSalidasProdAsync(NuevaSalidaP);

                    var ProdEdit = new Producto
                    {
                        Id = prod.Id,
                        Articulo = prod.Articulo,
                        Descripcion = prod.Descripcion,
                        CantidadIdeal = prod.CantidadIdeal,
                        Disponible = prod.Disponible - Cantidad,
                        PuntoReorden = prod.PuntoReorden,
                        Existencia_Inicial = prod.Existencia_Inicial,
                        Valor_Inicial = prod.Valor_Inicial,
                        ProveedorId = prod.ProveedorId,
                        SubCategoriaId = prod.SubCategoriaId,
                        UbicacionId = prod.UbicacionId
                    };

                    await camplogicService.PutProductAsync(ProdEdit);

                    _salidasPuntuales.Add(NuevaSalidaP);
                });
            }
        }
        //Agregar Con SubCategoria, Sin Descripcion
        public ICommand AgregarProdSubCategoriaSalidasP
        {
            get
            {
                return new Command<object[]>(async (DetallesProd) =>
                {
                    Subcategoria = (string)DetallesProd[0];
                    Articulo = (string)DetallesProd[1];
                    Cantidad = (int)DetallesProd[2];
                    Empleado = (Empleado)DetallesProd[3];
                    Fecha = (DateTime)DetallesProd[4];
                    Hora = (TimeSpan)DetallesProd[5];
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.SubCategoria.Nombre == Subcategoria && x.Articulo == Articulo);
                    var NuevaSalida = new Salida()
                    {
                        Cantidad = Cantidad,
                        Fecha = new DateTime(Fecha.Year, Fecha.Month, Fecha.Day, Hora.Hours, Hora.Minutes, Hora.Seconds),
                        EmpleadoId = Empleado.Id

                    };

                    await camplogicService.PostSalidasAsync(NuevaSalida);
                    var salidas = await camplogicService.GetSalidasAsync();
                    var ultimaSalidas = salidas.LastOrDefault();

                    var NuevaSalidaP = new Salidas_Producto()
                    {
                        Cantidad = Cantidad,
                        ProductoId = prod.Id,
                        SalidaId = ultimaSalidas.Id,
                        SalidaPuntual = true,
                        EstaPendiente = true
                    };

                    await camplogicService.PostSalidasProdAsync(NuevaSalidaP);

                    var ProdEdit = new Producto
                    {
                        Id = prod.Id,
                        Articulo = prod.Articulo,
                        Descripcion = prod.Descripcion,
                        CantidadIdeal = prod.CantidadIdeal,
                        Disponible = prod.Disponible - Cantidad,
                        PuntoReorden = prod.PuntoReorden,
                        Existencia_Inicial = prod.Existencia_Inicial,
                        Valor_Inicial = prod.Valor_Inicial,
                        ProveedorId = prod.ProveedorId,
                        SubCategoriaId = prod.SubCategoriaId,
                        UbicacionId = prod.UbicacionId
                    };

                    await camplogicService.PutProductAsync(ProdEdit);

                    _salidasPuntuales.Add(NuevaSalidaP);
                });
            }
        }
        //Agregar Sin SubCategoria, Sin Descripcion
        public ICommand AgregarProdCategoriaSalidasP
        {
            get
            {
                return new Command<object[]>(async (DetallesProd) =>
                {
                    Categoria = (string)DetallesProd[0];
                    Articulo = (string)DetallesProd[1];
                    Cantidad = (int)DetallesProd[2];
                    Empleado = (Empleado)DetallesProd[3];
                    Fecha = (DateTime)DetallesProd[4];
                    Hora = (TimeSpan)DetallesProd[5];
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.SubCategoria.Categoria.Nombre == Categoria && x.Articulo == Articulo);
                    var NuevaSalida = new Salida()
                    {
                        Cantidad = Cantidad,
                        Fecha = new DateTime(Fecha.Year, Fecha.Month, Fecha.Day, Hora.Hours, Hora.Minutes, Hora.Seconds),
                        EmpleadoId = Empleado.Id

                    };

                    await camplogicService.PostSalidasAsync(NuevaSalida);
                    var salidas = await camplogicService.GetSalidasAsync();
                    var ultimaSalidas = salidas.LastOrDefault();

                    var NuevaSalidaP = new Salidas_Producto()
                    {
                        Cantidad = Cantidad,
                        ProductoId = prod.Id,
                        SalidaId = ultimaSalidas.Id,
                        SalidaPuntual = true,
                        EstaPendiente = true
                    };

                    await camplogicService.PostSalidasProdAsync(NuevaSalidaP);

                    var ProdEdit = new Producto
                    {
                        Id = prod.Id,
                        Articulo = prod.Articulo,
                        Descripcion = prod.Descripcion,
                        CantidadIdeal = prod.CantidadIdeal,
                        Disponible = prod.Disponible - Cantidad,
                        PuntoReorden = prod.PuntoReorden,
                        Existencia_Inicial = prod.Existencia_Inicial,
                        Valor_Inicial = prod.Valor_Inicial,
                        ProveedorId = prod.ProveedorId,
                        SubCategoriaId = prod.SubCategoriaId,
                        UbicacionId = prod.UbicacionId
                    };

                    await camplogicService.PutProductAsync(ProdEdit);

                    _salidasPuntuales.Add(NuevaSalidaP);

                });
            }
        }

        //Filtrado de Pickers
        public async Task<List<SubCategoria>> SearchSubCategoria(int id)
        {
            var subcategoria = await camplogicService.GetSubCategoriasAsync();
            var SubCategorias = subcategoria.Where(x => x.CategoriaId == id).ToList();
            return SubCategorias;
        }
        public async Task<List<Producto>> FilterCategoria(int id)
        {
            var productos = await camplogicService.GetProductosAsync();
            var Productos = productos.OrderBy(x => x.Articulo).Where(x => x.SubCategoria.Categoria.Id == id).ToList();
            return Productos;
        }
        public async Task<List<Producto>> FilterSubCategoria(int idSubCat)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prods = productos.Where(x => x.SubCategoriaId == idSubCat).ToList();
            return prods;
        }
        public async Task<List<Producto>> FilterArticulo(string articulo)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prods = productos.Where(x => x.Articulo == articulo).ToList();
            return prods;
        }


        //Entradas Puntuales
        public ICommand AgregarEntradaPuntual
        {
            get
            {
                return new Command<object[]>(async (NuevaEntradaP) =>
                {
                    SalidaP = (Salidas_Producto)NuevaEntradaP[0];
                    CantidadE = (int)NuevaEntradaP[1];
                    FechaE = (DateTime)NuevaEntradaP[2];
                    HoraE = (TimeSpan)NuevaEntradaP[3];

                    //Agrego Entrada General
                    var EGP = new Reintegros()
                    {
                        CantidadTotal = CantidadE,
                        FechaEntrada = new DateTime(FechaE.Year, FechaE.Month, FechaE.Day, HoraE.Hours, HoraE.Minutes, HoraE.Seconds)
                    };

                    await camplogicService.PostReintegroAsync(EGP);
                    var entradasR = await camplogicService.GetReintegrosAsync();
                    var entradap = entradasR.LastOrDefault();

                    //Agregor EntradaProd
                    var nuevaEntradaP = new ReintegroProducto()
                    {
                        Cantidad = CantidadE,
                        Faltante = 0,
                        ProductoId = SalidaP.ProductoId,
                        ReintegroId = entradap.Id
                    };

                    await camplogicService.PostReintegroProdAsync(nuevaEntradaP);

                    var nuevaSalidaP = new Salidas_Producto
                    {
                        Id = SalidaP.Id,
                        Cantidad = SalidaP.Cantidad,
                        ProductoId = SalidaP.ProductoId,
                        SalidaId = SalidaP.SalidaId,
                        SalidaPuntual = SalidaP.SalidaPuntual,
                        EstaPendiente = false
                    };

                    await camplogicService.PutSalidaProdsAsync(nuevaSalidaP);

                    //Cambiar Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.First(x => x.Id == SalidaP.ProductoId);
                    var ProdEdit = new Producto
                    {
                        Id = prod.Id,
                        Articulo = prod.Articulo,
                        Descripcion = prod.Descripcion,
                        CantidadIdeal = prod.CantidadIdeal,
                        Disponible = prod.Disponible + CantidadE,
                        PuntoReorden = prod.PuntoReorden,
                        Existencia_Inicial = prod.Existencia_Inicial,
                        Valor_Inicial = prod.Valor_Inicial,
                        ProveedorId = prod.ProveedorId,
                        SubCategoriaId = prod.SubCategoriaId,
                        UbicacionId = prod.UbicacionId
                    };

                    await camplogicService.PutProductAsync(ProdEdit);

                    

                });
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
