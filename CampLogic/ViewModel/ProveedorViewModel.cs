﻿using CampLogic.Model;
using CampLogic.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CampLogic.ViewModel 
{
    public class ProveedorViewModel : INotifyPropertyChanged
{
        CampLogicService camplogicService = new CampLogicService();

        private ObservableCollection<Proveedor> _proveedores { get; set; }
        public ObservableCollection<Proveedor> Proveedores
        {
            get { return _proveedores; }
            set
            {
                _proveedores = value;
                OnPropertyChanged();
            }
        }

        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Cedula { get; set; }
        public string RIF { get; set; }
        public string Producto { get; set; }
        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }
        public string Correo { get; set; }

        
        public ProveedorViewModel()
        {


            InitializeDataAsync();

        }

        public async void InitializeDataAsync()
        {
            Proveedores = await camplogicService.GetProveedoresAsync();
        }

        public ICommand AgregarProveedor
        {
            get
            {
                return new Command<object[]>(async (NuevoProv) =>
                {
                    Nombre = (string)NuevoProv[0];
                    Apellido = (string)NuevoProv[1];
                    Cedula = (string)NuevoProv[2];
                    RIF = (string)NuevoProv[3];
                    Producto = (string)NuevoProv[4];
                    Telefono1 = (string)NuevoProv[5];
                    Telefono2 = (string)NuevoProv[6];
                    Correo = (string)NuevoProv[7];

                    var proveedor = new Proveedor
                    {
                        Nombre = Nombre,
                        Apellido = Apellido,
                        Cedula = Cedula,
                        RIF = RIF,
                        Descripcion = Producto,
                        Telefono_1 = Telefono1,
                        Telefono_2 = Telefono2,
                        Correo = Correo
                    };

                    await camplogicService.PostProveedorAsync(proveedor);
                    _proveedores.Add(proveedor);
                });

            }
        }

        //Filtro
        public async Task<List<Proveedor>> SearchProveedor(string keyword)
        {
            var proveedores = await camplogicService.GetProveedoresAsync();
            var prov = proveedores.Where(x => x.Descripcion.ToLower().Contains(keyword.ToLower())).ToList();
            return prov;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
