﻿using CampLogic.Helpers;
using CampLogic.Model;
using CampLogic.Services;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CampLogic.ViewModel
{
    public class ProductosViewModel : INotifyPropertyChanged
    {
        CampLogicService camplogicService = new CampLogicService();


        //private List<Grouping<string, Producto>> _productos { get; set; }
        //public List<Grouping<string,Producto>> Productos
        //{
        //    get { return _productos; }
        //    set
        //    {
        //        _productos = value;
        //        OnPropertyChanged();
        //    }
        //}

        private List<Producto> _productos { get; set; }
        public List<Producto> Productos
        {
            get { return _productos; }
            set
            {
                _productos = value;
                OnPropertyChanged();
            }
        }

        private List<Categoria> _categorias { get; set; }
        public List<Categoria> Categorias
        {
            get { return _categorias; }
            set
            {
                _categorias = value;
                OnPropertyChanged();
            }
        }

        private List<SubCategoria> _subcategorias { get; set; }
        public List<SubCategoria> SubCategorias
        {
            get { return _subcategorias; }
            set
            {
                _subcategorias = value;
                OnPropertyChanged();
            }
        }

        private List<Producto> _productosList { get; set; }
        public List<Producto> ProductosList
        {
            get { return _productosList; }
            set
            {
                _productosList = value;
                OnPropertyChanged();
            }
        }

        private List<Proveedor> _proveedores { get; set; }
        public List<Proveedor> Proveedores {
            get { return _proveedores; }
            set
            {
                _proveedores = value;
                OnPropertyChanged();
            }
        }

        public List<Ubicacion> _ubicaciones { get; set; }
        public List<Ubicacion> Ubicaciones
        {
            get { return _ubicaciones; }
            set
            {
                _ubicaciones = value;
                OnPropertyChanged();
            }
        }

        private List<string> _categoriaList { get; set; }
        public List<string> CateList
        {
            get { return _categoriaList; }
            set
            {
                _categoriaList = value;
                OnPropertyChanged();
            }
        }

        private List<string> _ubicacionList { get; set; }
        public List<string> UbList
        {
            get { return _ubicacionList; }
            set
            {
                _ubicacionList = value;
                OnPropertyChanged();
            }
        }

        private bool _isbusy { get; set; }
        public bool IsBusy
        {
            get
            {
                return _isbusy;
            }
            set
            {
                _isbusy = value;
                OnPropertyChanged();
            }
        }

        private int _su { get; set; }
        public int SU
        {
            get
            {
                return _su;
            }
            set
            {
                _su = value;
                OnPropertyChanged();
            }
        }

        private int _sp { get; set; }
        public int SP
        {
            get
            {
                return _sp;
            }
            set
            {
                _sp = value;
                OnPropertyChanged();
            }
        }

        //Agregar Categoria
        public string Categoria { get; set; }

        //Agregar SubCategoria
        public string Sub_Categoria { get; set; }
        public Categoria CaTegoria { get; set; }

        //Agregar Producto
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        public SubCategoria SubCategoria { get; set; }
        public int Disp { get; set; }
        public int CantidadIdeal { get; set; }
        public int PR { get; set; }
        public Proveedor Proveedor { get; set; }
        public Ubicacion Ubicacion { get; set; }

        //Agregar Cotizacion
        public double CostoUnitario { get; set; }
        public DateTime CostoDate { get; set; }

        //Editar Producto
        public int ProdID { get; set; }
        public int CantIdeal { get; set; }
        public int CantActual { get; set; }
        public Proveedor ProveedorE { get; set; }
        public Ubicacion UbicacionE { get; set; }
        public double Costo { get; set; }


        public ProductosViewModel()
        {
            InitializeDataAsync();
            
        }

        public async void InitializeDataAsync()
        {
            
            var productos = await camplogicService.GetProductosAsync();
            var categoria = await camplogicService.GetCategoriasAsync();
            var subcategoria = await camplogicService.GetSubCategoriasAsync();
            var proveedores = await camplogicService.GetProveedoresAsync();
            //Productos = productos.OrderBy(producto => producto.Tipo).ToList();

            //var prod = from produ in productos
            //           orderby produ.SubCategoria.Categoria
            //           group produ by produ.SubCategoria.Categoria.ToString() into produGroup
            //           select new Grouping<string, Producto>(produGroup.Key, produGroup);
            
            Productos = productos.OrderBy(x => x.Articulo).ToList();
            Categorias = categoria.OrderBy(x => x.Nombre).Reverse().ToList();
            SubCategorias = subcategoria.OrderBy(x => x.Nombre).ToList();
            Proveedores = proveedores.OrderBy(x => x.Descripcion).ToList();
            Ubicaciones = await camplogicService.GetUbicacionAsync();
            UbList = await UbicacionList();
            CateList = await CategoriaList();
            IsBusy = true;

        }

        public ProductosViewModel(int pr)
        {
            InitializeDataAsync(pr);
        }

        public async void InitializeDataAsync(int pr)
        {

            var productos = await camplogicService.GetProductosAsync();
            
            Productos = productos.OrderBy(x => x.Articulo).ToList();

            
        }

        public ProductosViewModel(string pr, string prov)
        {
            InitializeDataAsync(pr, prov);
        }

        public async void InitializeDataAsync(string pr, string prov)
        {
            var productos = await camplogicService.GetProductosAsync();
            var proveedores = await camplogicService.GetProveedoresAsync();

            Productos = productos.OrderBy(x => x.Articulo).ToList();
            Proveedores = proveedores.OrderBy(x => x.Descripcion).ToList();
            Ubicaciones = await camplogicService.GetUbicacionAsync();
            int j = 0;
            int i = 0;
            foreach (var item in Ubicaciones)
            {
                if (item.Nombre == pr)
                {
                    SU = j;
                }
                else
                {
                    j++;
                }
            }

            foreach (var item in Proveedores)
            {
                if (item.Descripcion == prov)
                {
                    SP = i;
                }
                else
                {
                    i++;
                }
            }


        }

        //Filtrados
        public async Task<List<Producto>> LProducto()
        {
            var productos = await camplogicService.GetProductosAsync();
            var prods = productos.OrderBy(x => x.Articulo).ToList();
            return prods;
        }

        public async Task<List<Producto>> SearchProducto(string keyword)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prods = productos.OrderBy(x => x.Articulo).Where(x => x.Articulo.ToLower().Contains(keyword.ToLower())).ToList();
            return prods;
        }

        public async Task<List<Producto>> FilterCategoria(string keyword)
        {
            var productos = await camplogicService.GetProductosAsync();
            var Productos = productos.OrderBy(x => x.Articulo).Where(x => x.SubCategoria.Categoria.Nombre.ToLower().Contains(keyword.ToLower())).ToList();
            return Productos;
        }
        
        public async Task<List<Producto>> SearchProductoCat(string keyword, string categoria)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prods = productos.OrderBy(x => x.Articulo).Where(x => x.Articulo.ToLower().Contains(keyword.ToLower()) && x.SubCategoria.Categoria.Nombre.ToLower().Contains(categoria.ToLower())).ToList();
            return prods;
        }

        public async Task<List<Producto>> SearchProductoSubCat(string keyword, string subcategoria)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prods = productos.OrderBy(x => x.Articulo).Where(x => x.Articulo.ToLower().Contains(keyword.ToLower()) && x.SubCategoria.Nombre.ToLower().Contains(subcategoria.ToLower())).ToList();
            return prods;
        }

        public async Task<List<Producto>> SearchUbicacion(string ubicacion, string keyword)
        {
            var productos = await camplogicService.GetProductosAsync();
            var Productos = productos.OrderBy(x => x.Articulo).Where(x => x.Articulo.ToLower().Contains(keyword.ToLower()) && x.Ubicacion.Nombre.ToLower().Contains(ubicacion.ToLower())).ToList();
            return Productos;
        }

        public async Task<List<Producto>> SearchProductoCatUb(string keyword, string categoria, string ubicacion)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prods = productos.OrderBy(x => x.Articulo).Where(x => x.Articulo.ToLower().Contains(keyword.ToLower()) && x.Ubicacion.Nombre.ToLower().Contains(ubicacion.ToLower()) && x.SubCategoria.Categoria.Nombre.ToLower().Contains(categoria.ToLower())).ToList();
            return prods;
        }

        public async Task<List<Producto>> SearchProductoSubCatUb(string keyword, string subcategoria, string ubicacion)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prods = productos.OrderBy(x => x.Articulo).Where(x => x.Articulo.ToLower().Contains(keyword.ToLower()) && x.Ubicacion.Nombre.ToLower().Contains(ubicacion.ToLower()) && x.SubCategoria.Nombre.ToLower().Contains(subcategoria.ToLower())).ToList();
            return prods;
        }

        public async Task<List<SubCategoria>> SearchSubCategoria (string id)
        {
            var subcategoria = await camplogicService.GetSubCategoriasAsync();
            SubCategorias = subcategoria.Where(x => x.Categoria.Nombre == id).ToList();
            return SubCategorias;
        }

        public async Task<List<SubCategoria>> SearchSubCategoria2(int id)
        {
            var subcategoria = await camplogicService.GetSubCategoriasAsync();
            SubCategorias = subcategoria.Where(x => x.CategoriaId == id).ToList();
            return SubCategorias;
        }

        public async Task<List<Producto>> Filter( int idSubCat )
        {
            var productos = await camplogicService.GetProductosAsync();
            var prods = productos.Where(x => x.SubCategoriaId == idSubCat).ToList();
            return prods;
        }

        public async Task<List<Producto>> FilterUbicacion(string ubicacion)
        {
            var productos = await camplogicService.GetProductosAsync();
            var Productos = productos.OrderBy(x => x.Articulo).Where(x => x.Ubicacion.Nombre == ubicacion).ToList();
            return Productos;
        }

        public async Task<List<Producto>> FilterUbCat(string ubicacion, string categoria)
        {
            var productos = await camplogicService.GetProductosAsync();
            var Productos = productos.OrderBy(x => x.Articulo).Where(x => x.Ubicacion.Nombre == ubicacion && x.SubCategoria.Categoria.Nombre == categoria).ToList();
            return Productos;
        }


        //Detalles Producto
        public async Task<List<SubCategoria>> PickerSubCategoria(string categoria)
        {
            var subcategoria = await camplogicService.GetSubCategoriasAsync();
            SubCategorias = subcategoria.Where(x => x.Categoria.Nombre == categoria).ToList();
            return SubCategorias;
        }

        public async Task<List<Proveedor>> PickerProveedor()
        {
            var proveedores = await camplogicService.GetProveedoresAsync();
            return Proveedores = proveedores.OrderBy(x => x.Descripcion).ToList();
        }

        // Agregar Categoria
        public async void NuevaCategoria( string categoria)
        {
            var nuevacategoria = new Categoria
            {
                Nombre = categoria
            };

            await camplogicService.PostCategoriaAsync(nuevacategoria);

            Categorias = await camplogicService.GetCategoriasAsync();
        }

        //Agregar Sub-Categoria
        public async void NuevaSubCategoria(string subcategoria, Categoria categoria)
        {
            var nuevaSubcategoria = new SubCategoria
            {
                Nombre = subcategoria,
                CategoriaId = categoria.Id
            };

            await camplogicService.PostSubCategoriaAsync(nuevaSubcategoria);

            SubCategorias = await camplogicService.GetSubCategoriasAsync();
        }

        //Agregar Producto
        public ICommand NProducto
        {
            get
            {
                return new Command<object[]>(async (NuevoProd) =>
                {
                    Articulo = (string)NuevoProd[0];
                    Descripcion = (string)NuevoProd[1];
                    SubCategoria = (SubCategoria)NuevoProd[2];
                    Disp = (int)NuevoProd[3];
                    CantidadIdeal = (int)NuevoProd[4];
                    PR = (int)NuevoProd[5];
                    Ubicacion = (Ubicacion)NuevoProd[6];
                    Proveedor = (Proveedor)NuevoProd[7];
                    CostoUnitario = (double)NuevoProd[8];
                    CostoDate = (DateTime)NuevoProd[9];

                    var producto = new Producto()
                    {
                        Articulo = Articulo,
                        Descripcion = Descripcion,
                        Disponible = Disp,
                        Existencia_Inicial = Disp,
                        CantidadIdeal = CantidadIdeal,
                        PuntoReorden = PR,
                        UbicacionId = Ubicacion.Id,
                        ProveedorId = Proveedor.Id,
                        SubCategoriaId = SubCategoria.Id
                        
                    };

                    await camplogicService.PostProductoAsync(producto);

                    var prods = await camplogicService.GetProductosAsync();
                    var lastprod = prods.LastOrDefault();

                    //Empleado
                    var CedulaEmp = Settings.Cedula;
                    var empleado = await camplogicService.GetEmpleadoCedulaAsync(CedulaEmp);

                    //Cotizacion
                    var nuevacotizacion = new Cotizacion
                    {
                        Fecha_Cotizacion = CostoDate,
                        EmpleadoId = empleado.Id

                    };

                    var resp = await camplogicService.PostCotizacionAsync(nuevacotizacion);

                    var cotizaciones = await camplogicService.GetCotizacionListAsync();

                    var lastCot = cotizaciones.LastOrDefault();

                    //Cotizacion-Producto
                    var nuevacotprod = new Cotizacion_Producto
                    {
                        Cantidad = 1,
                        Precio = (float)CostoUnitario,
                        ProductoId = lastprod.Id,
                        CotizacionId = lastCot.Id
                    };

                    await camplogicService.PostCotizacionProdAsync(nuevacotprod);

                    var productos = await camplogicService.GetProductosAsync();

                    _productos = productos.OrderBy(x => x.Articulo).ToList();

                });
            }
        }

        //Agregar Ubicacion
        public async Task<bool> NuevaUbicacion(Ubicacion NuevaUbicacion)
        {
           var resp = await camplogicService.PostUbicacionAsync(NuevaUbicacion);
            return resp;
        }

        public async Task<List<Ubicacion>> UbicacionesList()
        {
            var ubicaciones = await camplogicService.GetUbicacionAsync();
            return ubicaciones;
        }

        //Llenado de Filtro Categoria Pantalla Producto
        public async Task<List<string>> CategoriaList()
        {
            var catL = new List<string>();
            var categorias = await camplogicService.GetCategoriasAsync();
            catL.Add("Todas");
            foreach (var item in categorias)
            {
                catL.Add(item.Nombre);
            }

            return catL;
        }

        //Llenado de Filtro Ubicacion Pantalla Producto
        public async Task<List<string>> UbicacionList()
        {
            var catL = new List<string>();
            var categorias = await camplogicService.GetUbicacionAsync();
            catL.Add("Todas");
            foreach (var item in categorias)
            {
                catL.Add(item.Nombre);
            }

            return catL;
        }

        public ICommand EditarProducto
        {
            get
            {
                return new Command<object[]>(async (NP) =>
                {
                    ProdID = (int)NP[0];
                    var cantActual = (int)NP[1];
                    CantIdeal = (int)NP[2];
                    ProveedorE = (Proveedor)NP[3];
                    UbicacionE = (Ubicacion)NP[4];
                    var costo = (double)NP[5];
                    var CantV = (int)NP[6];
                    var CostoV = (double)NP[7];

                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.Id == ProdID);
                    if (CantV == cantActual)
                    {
                        CantActual = cantActual;
                    }
                    else
                    {
                        string emp = Settings.Cedula;
                        List<Empleado> guias = await camplogicService.GetEmpleadosAsync();
                        Empleado guia = guias.FirstOrDefault(x => x.Cedula == emp);

                        var nsalida = new Salida
                        {
                            Cantidad = CantV - cantActual,
                            EmpleadoId = guia.Id,
                            Fecha = DateTime.Now
                        };

                        await camplogicService.PostSalidasAsync(nsalida);

                        var lastSalida = (await camplogicService.GetSalidasAsync()).LastOrDefault();

                        var nsalidaProd = new Salidas_Producto
                        {
                            SalidaPuntual = false,
                            EstaPendiente = false,
                            Cantidad = CantV - cantActual,
                            ProductoId = ProdID,
                            SalidaId = lastSalida.Id
                        };

                        await camplogicService.PostSalidasProdAsync(nsalidaProd);

                        CantActual = cantActual;
                    }


                    var NuevoProd = new Producto
                    {
                        Id = prod.Id,
                        Articulo = prod.Articulo,
                        Descripcion = prod.Descripcion,
                        CantidadIdeal = CantIdeal,
                        Disponible = CantActual,
                        PuntoReorden = prod.PuntoReorden,
                        Existencia_Inicial = prod.Existencia_Inicial,
                        Valor_Inicial = prod.Valor_Inicial,
                        ProveedorId = ProveedorE.Id,
                        SubCategoriaId = prod.SubCategoriaId,
                        UbicacionId = UbicacionE.Id
                    };

                    await camplogicService.PutProductAsync(NuevoProd);

                    var CedulaEmp = Settings.Cedula;
                    var empleado = await camplogicService.GetEmpleadoCedulaAsync(CedulaEmp);
                    var nuevaCot = new Cotizacion
                    {
                        Fecha_Cotizacion = DateTime.Today,
                        EmpleadoId = empleado.Id
                    };

                    var resp = await camplogicService.PostCotizacionAsync(nuevaCot);

                    var cotizaciones = await camplogicService.GetCotizacionListAsync();

                    var lastCot = cotizaciones.LastOrDefault();

                    //Cotizacion-Producto
                    var nuevacotprod = new Cotizacion_Producto
                    {
                        Cantidad = 1,
                        Precio = (float)Costo,
                        ProductoId = prod.Id,
                        CotizacionId = lastCot.Id
                    };

                    await camplogicService.PostCotizacionProdAsync(nuevacotprod);

                    var prods = await camplogicService.GetProductosAsync();

                    Productos = prods.OrderBy(x => x.Articulo).ToList();

                });
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
