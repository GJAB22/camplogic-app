﻿using CampLogic.HelpClasses;
using CampLogic.Helpers;
using CampLogic.Model;
using CampLogic.Services;
using Plugin.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CampLogic.ViewModel
{
    public class CestaViewModel : INotifyPropertyChanged
    {
        CampLogicService camplogicService = new CampLogicService();

        
        
        private List<Pedidos> _pedidos { get; set; }
        public List<Pedidos> PedidosList
        {
            get { return _pedidos; }
            set
            {
                _pedidos = value;
                OnPropertyChanged();
            }
        }
        

        //Cesta 
        //Cantidad Total
        private int _cantidadOrden { get; set; }
        public int CantidadOrden
        {
            get { return _cantidadOrden; }
            set
            {
                _cantidadOrden = value;
                OnPropertyChanged();
            }
        }

        //Monto Total
        private float _montoTotal { get; set; }
        public float MontoTotal
        {
            get
            {
                return _montoTotal;
            }
            set
            {
                _montoTotal = value;
                OnPropertyChanged();
            }
        }

        //Multiple Seleccion
        //private ObservableCollection<SelectableItemWrapper<Cesta>> _cesta;
        //public ObservableCollection<SelectableItemWrapper<Cesta>> Cesta
        //{
        //    get { return _cesta; }
        //    set
        //    {
        //        _cesta = value;
        //        RaisePropertyChanged();
        //    }
        //}

        ////Lista Seleccionada de Cesta
        //private ObservableCollection<Cesta> _selectedProductos;
        //public ObservableCollection<Cesta> SelectedProductos
        //{
        //    get { return _selectedProductos; }
        //    set
        //    {
        //        _selectedProductos = value;
        //        RaisePropertyChanged();
        //    }
        //}

        //Lista Cesta
        private ObservableCollection<Cesta> _cestaList;
        public ObservableCollection<Cesta> Cesta
        {
            get { return _cestaList; }
            set
            {
                _cestaList = value;
                OnPropertyChanged();
            }
        }

        //Lista de Categoria
        private List<Categoria> _categorias { get; set; }
        public List<Categoria> Categorias
        {
            get { return _categorias; }
            set
            {
                _categorias = value;
                OnPropertyChanged();
            }
        }
        //Lista de SubCategoria
        private List<SubCategoria> _subcategorias { get; set; }
        public List<SubCategoria> SubCategorias
        {
            get { return _subcategorias; }
            set
            {
                _subcategorias = value;
                OnPropertyChanged();
            }
        }


        private List<string> Items { get; set; }

        private int Cantotal { get; set; }
        private float Montotal { get; set; }

        //Agregar a Cesta
        public DateTime FE { get; set; }
        public int CantidadO { get; set; }
        public int ProdId { get; set; }
        public float PrecioU { get; set; }
        public string PR { get; set; }



        public CestaViewModel()
        {

            InitializeDataAsync();
        }

        public async void InitializeDataAsync()
        {
            var cesta = await camplogicService.GetCestaAsync();
            var categorias = await camplogicService.GetCategoriasAsync();
            var subcategoria = await camplogicService.GetSubCategoriasAsync();

            Cesta = new ObservableCollection<Cesta>(cesta.Reverse());
                
            Categorias = categorias.OrderBy(x => x.Nombre).Reverse().ToList();
            SubCategorias = subcategoria.OrderBy(x => x.Nombre).ToList();

            foreach (var prod in Cesta)
            {
                MontoTotal = MontoTotal + (prod.Cantidad * prod.Precio);
                CantidadOrden = CantidadOrden + prod.Cantidad;
            }
        }

        public CestaViewModel(Orden_Compra orden)
        {

            InitializeDataAsync(orden);
        }

        public async void InitializeDataAsync(Orden_Compra orden)
        {
            var pedidos = await camplogicService.GetPedidosAsync();

            PedidosList = pedidos.Where(b => b.OrdenCompraId == orden.Id).ToList();
        }

        //Multiple Seleccion
        //public ObservableCollection<Cesta> GetSelectedProductos()
        //{
        //    var selected = Cesta
        //        .Where(p => p.IsSelected)
        //        .Select(p => p.Item)
        //        .ToList();
        //    return new ObservableCollection<Cesta>(selected);
        //}

        

        public ICommand NuevasO
        {
            get
            {
               return(new Command<int>(async (i) =>
                        {
                            //Productos Seleccionados
                            //SelectedProductos = GetSelectedProductos();
                            //Empleado
                            var CedulaEmp = Settings.Cedula;
                            var empleado = await camplogicService.GetEmpleadoCedulaAsync(CedulaEmp);

                            var proveedoresList = await camplogicService.GetProveedoresAsync();

                            var emailMessenger = CrossMessaging.Current.EmailMessenger;
                            if (emailMessenger.CanSendEmail)
                            {
                                // Send simple e-mail to single receiver without attachments, bcc, cc etc.
                                //emailMessenger.SendEmail(ProvCorreo, 
                                //    "Orden de Compra", 
                                //    "Id: " + ordenCompra.Id + "\nDescripcion del Producto: " + DesProd + "\nCantidad: " + CantidadOrden + "\nCosto: ");

                                // Alternatively use EmailBuilder fluent interface to construct more complex e-mail with multiple recipients, bcc, attachments etc. 

                                foreach (var prov in proveedoresList)
                                {
                                    var cestaList = Cesta.Where(prod => prod.Producto.ProveedorId == prov.Id).ToList();
                                    if (cestaList.Count() != 0)
                                    {
                                        Cantotal = 0;
                                        Montotal = 0;
                                        foreach (var prod in cestaList)
                                        {
                                            Cantotal = Cantotal + prod.Cantidad;
                                            Montotal = Montotal + prod.ValorTotal;
                                        }

                                        //Orden Compra
                                        var NuevaOrden = new Orden_Compra
                                        {
                                            Fecha_Orden = DateTime.Now,
                                            Costo_Total = Montotal,
                                            Cantidad_Orden = Cantotal,
                                            EmpleadoId = empleado.Id,
                                            ProveedorId = prov.Id,
                                            Aprobado = false
                                        };

                                        var respon = await camplogicService.PostOrdenCompraAsync(NuevaOrden);

                                        var ordencompraList = await camplogicService.GetOrdenesCompraAsync();

                                        var ordenCompra = ordencompraList.LastOrDefault();

                                        foreach (var prod in cestaList)
                                        {
                                            var pedido = new Pedidos()
                                            {
                                                Cantidad = prod.Cantidad,
                                                Precio = prod.Precio,
                                                EmpleadoId = prod.EmpleadoId,
                                                ProductoId = prod.ProductoId,
                                                OrdenCompraId = ordenCompra.Id,
                                                ValorTotal = prod.ValorTotal
                                            };

                                            var rPedido = await camplogicService.PostPedidoAsync(pedido);
                                        }

                                        var pedidosList = await camplogicService.GetPedidosAsync();

                                        var pedidosFiltrado = pedidosList.Where(b => b.OrdenCompraId == ordenCompra.Id).ToList();

                                        var itemsOrder = string.Join("", ItemsOrdena(pedidosFiltrado));

                                        var email = new EmailMessageBuilder()
                                          .To(prov.Correo)
                                          .Cc(empleado.Correo)
                                          //  .Bcc(new[] { "bcc1.plugins@xamarin.com", "bcc2.plugins@xamarin.com" })
                                          .Subject("Orden de Compra #" + ordenCompra.Id)
                                          .Body("Resumen de la Orden: \n\n" +"Codigo de la Orden"+ ordenCompra.Id+
                                          "Numero de la Orden: " + ordenCompra.Id +"-"+ ordenCompra.Fecha_Orden.ToString("ddMMyyyy")+
                                          "\nFecha de Emision: " + ordenCompra.Fecha_Orden.ToString("dd/MM/yyyy hh:mm tt") + "\n----------------------------------\n"+
                                          "\nInformacion de la Orden: " +
                                          "\nEmisor de Orden de Compra: " + empleado.Nombre + " " + empleado.Apellido +
                                          "\nCorreo: " + empleado.Correo + "\n----------------------------------\n"+
                                          "Items: \n" +
                                          itemsOrder
                                          + "\n\n")
                                          .Build();

                                        emailMessenger.SendEmail(email);

                                    }
                                }

                            }

                            //vaciar cesta
                            foreach (var prod in Cesta)
                            {
                                var re = await camplogicService.DeleteCestaAsync(prod.Id, prod);
                            }

                        })
               );
            }
        }

        public ICommand AgregarCesta
        {
            get
            {
                return new Command<object[]>(async (DetallesCesta) =>
                {
                    var CedulaEmp = Settings.Cedula;
                    var empleado = await camplogicService.GetEmpleadoCedulaAsync(CedulaEmp);
                    ProdId = (int)DetallesCesta[0];
                    CantidadO = (int)DetallesCesta[1];
                    PrecioU = (float)DetallesCesta[2];
                    PR = (string)DetallesCesta[3];
                    var nuevoProdCesta = new Cesta()
                    {
                        Cantidad = CantidadO,
                        Precio = PrecioU,
                        ProductoId = Convert.ToInt32(ProdId),
                        EmpleadoId = empleado.Id,
                        ValorTotal = CantidadO*PrecioU,
                        Prioridad = PR
                        
                    };

                    var responseCesta = await camplogicService.PostProdCestaAsync(nuevoProdCesta);

                });
            }
        }

        public List<string> ItemsOrdena(List<Pedidos> pedidos)
        {
            
            Items = new List<string>();

            foreach (var p in pedidos)
            {
                
                string prod = "| Artículo: " + p.Producto.Articulo + " | Descripción: " + p.Producto.Descripcion + " | Cantidad: " + p.Cantidad.ToString() + " | Monto Total: " + "Bs." + p.ValorTotal.ToString() + " |\n\n";
                
                Items.Add(prod);

            }

            return Items;
        }

        public ICommand CalcularValores
        {
            get
            {
                return (new Command(() =>
                {
                    MontoTotal = 0;
                    CantidadOrden = 0;
                    //SelectedProductos = GetSelectedProductos();
                    foreach (var prod in Cesta)
                    {
                        MontoTotal = MontoTotal + prod.ValorTotal;
                        CantidadOrden = CantidadOrden + prod.Cantidad;
                    }
                    // Navigation is not 
                    //await App.Current.MainPage.Navigation.PushAsync(new SelectedPokemonPage());
                    
                }));
            }
        }
        
        //Multiple Seleccion
        //void SelectAll(bool select)
        //{
        //    foreach (var p in Cesta)
        //    {
        //        p.IsSelected = select;
        //    }
        //}

        // Traer Listado de Cesta
        public async Task<ObservableCollection<Cesta>> CestaList()
        {
            var cesta = await camplogicService.GetCestaAsync();
            Cesta = new ObservableCollection<Cesta>(cesta.Reverse());
            return Cesta;
        }

        public void Cs(string keyword, ObservableCollection<Cesta> cesta)
        {
            Cesta = (ObservableCollection<Cesta>)cesta.Where(x => x.Producto.Articulo.ToLower().Contains(keyword.ToLower()));
        }

        //Buscar Producto
        public async Task<ObservableCollection<Cesta>> SearchProducto(string keyword)
        {
            var cesta = await camplogicService.GetCestaAsync();

            
            Cesta = (ObservableCollection<Cesta>)cesta.Where(x => x.Producto.Articulo.ToLower().Contains(keyword.ToLower()));

            return Cesta;
        }

        // Filtrar Por Categoria
        public async Task<ObservableCollection<Cesta>> FilterCategoria(string keyword)
        {
            var cesta = await camplogicService.GetCestaAsync();
            Cesta = (ObservableCollection<Cesta>)cesta.Where(x => x.Producto.SubCategoria.Categoria.Nombre.ToLower().Contains(keyword.ToLower()));
            return Cesta;
            
        }

        // Filtrar SubCategoria
        public async Task<List<SubCategoria>> SearchSubCategoria(int id)
        {
            var subcategoria = await camplogicService.GetSubCategoriasAsync();
            SubCategorias = subcategoria.Where(x => x.CategoriaId == id).ToList();
            return SubCategorias;
        }

        // Buscar por Categoria
        public async Task<ObservableCollection<Cesta>> SearchProductoCat(string keyword, string categoria)
        {
            var cesta = await camplogicService.GetCestaAsync();
            Cesta = (ObservableCollection<Cesta>)cesta.Where(x => x.Producto.Articulo.ToLower().Contains(keyword.ToLower()) && x.Producto.SubCategoria.Categoria.Nombre.ToLower().Contains(categoria.ToLower()));
            return Cesta;
        }

        //Buscar por SubCategoria
        public async Task<ObservableCollection<Cesta>> SearchProductoSubCat(string keyword, string subcategoria)
        {
            var cesta = await camplogicService.GetCestaAsync();
            Cesta = (ObservableCollection<Cesta>)cesta.Where(x => x.Producto.Articulo.ToLower().Contains(keyword.ToLower()) && x.Producto.SubCategoria.Nombre.ToLower().Contains(subcategoria.ToLower()));
            return Cesta;
        }

        //Filtrar por SubCategoria
        public async Task<ObservableCollection<Cesta>> FilterSubCategoria(int idSubCat)
        {
            var cesta = await camplogicService.GetCestaAsync();
            Cesta = (ObservableCollection<Cesta>)cesta.Where(x => x.Producto.SubCategoriaId == idSubCat);
            return Cesta;
        }

        // Filtrar Por Prioridad
        public async Task<ObservableCollection<Cesta>> FilterPrioridad(string keyword)
        {
            var cesta = await camplogicService.GetCestaAsync();
            Cesta = new ObservableCollection<Cesta>(cesta.Where(x => x.Prioridad.ToLower().Contains(keyword.ToLower())).Reverse());
            return Cesta;

        }

        public ICommand RemoverItem
        {
            get
            {
                return new Command<Cesta>(async (producto) =>
                {
                    var remove = await camplogicService.DeleteCestaAsync(producto.Id, producto);
                    _cestaList.Remove(producto);
                });
            }
        }

        

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        //Multiple Seleccion
        //void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        //{
        //    if (PropertyChanged != null)
        //    {
        //        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}
    }
}
