﻿using CampLogic.Helpers;
using CampLogic.Model;
using CampLogic.Services;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CampLogic.ViewModel
{
    public class HorarioViewModel : INotifyPropertyChanged
    {
        CampLogicService camplogicServices = new CampLogicService();

        private List<Grouping<DateTime, Patrulla_Actividad>> _patrullaActividad { get; set; }
        public List<Grouping<DateTime, Patrulla_Actividad>> PatrullaActividad
        {
            get { return _patrullaActividad; }
            set
            {
                _patrullaActividad = value;
                OnPropertyChanged();
            }
        }

        private List<Actividades> _patrullaActividad2 { get; set; }
        public List<Actividades> PatrullaActividad2
        {
            get { return _patrullaActividad2; }
            set
            {
                _patrullaActividad2 = value;
                OnPropertyChanged();
            }
        }

        //Agregar Actividad
        public string NombrePatrulla { get; set; }

        public string NombreActividad { get; set; }
        public string DescripActividad { get; set; }

        public DateTime Dia { get; set; }
        public TimeSpan Hora { get; set; }

        

        public HorarioViewModel()
        {
            //var activ = from act in actividades where act.PatrullaId == Patrulla.Id
            //           orderby act.Horario.Date
            //           group act by act.Horario.Date into actGroup
            //           select new Grouping<DateTime, Patrulla_Actividad>(actGroup.Key, actGroup);

            //PatrullaActividad2 = actividades;

        }
        
        public async Task<object[]> InitializeHorario()
        {
            var H1B = new TimeSpan(09, 00, 00);
            var H2B = new TimeSpan(10, 50, 00);
            var H3B = new TimeSpan(13, 00, 00);
            List<Patrulla_Actividad> Act1B = new List<Patrulla_Actividad>();
            List<Patrulla_Actividad> Act2B = new List<Patrulla_Actividad>();
            List<Patrulla_Actividad> Act3B = new List<Patrulla_Actividad>();

            //Guia o Usuario
            string empleado = Settings.Cedula;
            List<Empleado> guias = await camplogicServices.GetEmpleadosAsync();
            Empleado guia = guias.FirstOrDefault(x => x.Cedula == empleado);
            object[] IG = new object[3];
            //Temporada en Curso
            var temporadas = await camplogicServices.GetTemporadasAsync();
            if (temporadas.Count() != 0)
            {
                Temporadas tempAux = temporadas.LastOrDefault();
                Temporadas temp;
                if (tempAux.FechaInicial.Date <= DateTime.Today.Date && tempAux.FechaFinal.Date >= DateTime.Today.Date)
                {
                    temp = temporadas.Last(x => x.FechaInicial.Date <= DateTime.Today.Date && x.FechaFinal.Date >= DateTime.Today.Date);
                }
                else
                {
                    temp = new Temporadas();
                }

                
                //Patrulla
                var patrullaguia = await camplogicServices.GetPatrullasEmpleadoAsync();
                var patrullaG = patrullaguia.FirstOrDefault(x => x.EmpleadoId == guia.Id && x.FechaAsignacion.Date == temp.FechaInicial);

                var actTemp = await camplogicServices.GetActividadesPatrullaAsync();

                if (patrullaG != null)
                {
                    //Actividades Asignadas a la Patrulla
                    var AP = actTemp.Where(x => x.PatrullaId == patrullaG.PatrullaId && x.FechaAsignacion.Date == temp.FechaInicial.Date);
                    if (AP.Count() != 0)
                    {
                        foreach (var pat in AP)
                        {
                            if (pat.Horario.TimeOfDay == H1B)
                            {
                                Act1B.Add(pat);
                            }
                            else if (pat.Horario.TimeOfDay == H2B)
                            {
                                Act2B.Add(pat);
                            }
                            else
                            {
                                Act3B.Add(pat);
                            }
                        }

                        IG[0] = Act1B;
                        IG[1] = Act2B;
                        IG[2] = Act3B;
                    }
                }
                
                
            }
            return IG;

        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

