﻿using CampLogic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using CampLogic.Helpers;
using CampLogic.Model;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;

namespace CampLogic.ViewModel
{
    public class SignUpViewModel : INotifyPropertyChanged
    {
        CampLogicService campLogicService = new CampLogicService();

        private ObservableCollection<string> _cargoList;
        public ObservableCollection<string> CargoList
        {
            get { return _cargoList; }
            set
            {
                _cargoList = value;
                OnPropertyChanged();
            }
        }


        //Cuenta
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        
        public ICommand RegisterCommand
        {
            get
            {
                return new Command<object[]>(async(NE) =>
                {
                    Password = (string)NE[7];
                    ConfirmPassword = (string)NE[8];
                    var empleado = new Empleado
                    {
                        Nombre = (string)NE[0],
                        Apellido = (string)NE[1],
                        Cedula = (string)NE[2],
                        Fecha_Nacimiento = (DateTime)NE[3],
                        Cargo = (string)NE[4],
                        Telefono = (string)NE[5],
                        Correo= (string)NE[6],
                        FotoPerfil = " "

                    };

                    var isSuccess = await campLogicService.RegisterAsync
                    ((string)NE[6], Password, ConfirmPassword);
                    
                    if (isSuccess == true)
                    {
                        var accesstoken = await campLogicService.LogInAsync((string)NE[6], Password);

                        if (accesstoken != null)
                        {
                            await campLogicService.PostEmpleadoAsync(empleado, accesstoken);
                        }
                        

                        Settings.Username = (string)NE[0]+" "+ (string)NE[1];
                        Settings.Password = Password;
                        Settings.AccessToken = accesstoken;
                        Settings.Cedula = (string)NE[2];
                        Settings.Cargo = (string)NE[4];
                       

                        Application.Current.MainPage = new MainPage();
                    }
                    
                });
            }
        }

        public SignUpViewModel()
        {
            CargoList = new ObservableCollection<string>()
            {
                "Director",
                "Coordinador",
                "Guía",
                "Inventario"
            };
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
