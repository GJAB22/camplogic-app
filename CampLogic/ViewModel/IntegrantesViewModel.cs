﻿using CampLogic.HelpClasses;
using CampLogic.Helpers;
using CampLogic.Model;
using CampLogic.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CampLogic.ViewModel
{
    public class IntegrantesViewModel : INotifyPropertyChanged
    {
        CampLogicService camplogicServices = new CampLogicService();

        private List<PatrullaEmpleado> _guias { get; set; }
        public List<PatrullaEmpleado> GuiasList
        {
            get { return _guias; }
            set
            {
                _guias = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<SelectableItemWrapper<PatrullaCampista>> _CampistasList;
        public ObservableCollection<SelectableItemWrapper<PatrullaCampista>> CampistasList
        {
            get { return _CampistasList; }
            set
            {
                _CampistasList = value;
                RaisePropertyChanged();
            }
        }

        //Lista Seleccionada de Campistas
        private ObservableCollection<PatrullaCampista> _selectedCampistas;
        public ObservableCollection<PatrullaCampista> SelectedCampistas
        {
            get { return _selectedCampistas; }
            set
            {
                _selectedCampistas = value;
                RaisePropertyChanged();
            }
        }

        //Lista No Seleccion de Campistas
        private ObservableCollection<PatrullaCampista> _notselectedCampistas;
        public ObservableCollection<PatrullaCampista> NotSelectedCampistas
        {
            get { return _notselectedCampistas; }
            set
            {
                _notselectedCampistas = value;
                RaisePropertyChanged();
            }
        }




        public IntegrantesViewModel()
        {


            InitializeDataAsync();
        }

        public async void InitializeDataAsync()
        {
            var guiaspatrullas = await camplogicServices.GetPatrullasEmpleadoAsync();
            var campistaspatrullas = await camplogicServices.GetPatrullasCampistasAsync();

            //Guia o Usuario
            string empleado = Settings.Cedula;
            List<Empleado> guias = await camplogicServices.GetEmpleadosAsync();
            Empleado guia = guias.FirstOrDefault(x => x.Cedula == empleado);

            //Temporada Actual o En Curso
            var temporadas = await camplogicServices.GetTemporadasAsync();
            if (temporadas.Count() != 0)
            {
                Temporadas tempAux = temporadas.LastOrDefault();
                Temporadas temp;
                if (tempAux.FechaInicial.Date <= DateTime.Today.Date && tempAux.FechaFinal.Date >= DateTime.Today.Date)
                {
                    temp = temporadas.Last(x => x.FechaInicial.Date <= DateTime.Today.Date && x.FechaFinal.Date >= DateTime.Today.Date);
                }
                else
                {
                    temp = new Temporadas();
                }

                if (temp != null)
                {
                    //Patrulla
                    var patrullaguia = await camplogicServices.GetPatrullasEmpleadoAsync();
                    var patrullaG = patrullaguia.FirstOrDefault(x => x.EmpleadoId == guia.Id && x.FechaAsignacion.Date == temp.FechaInicial);


                    GuiasList = guiaspatrullas.Where(x => x.FechaAsignacion.Date == temp.FechaInicial && x.PatrullaId == patrullaG.PatrullaId).ToList();


                    var cp = campistaspatrullas.Where(x => x.FechaAsignada.Date == temp.FechaInicial && x.PatrullaId == patrullaG.PatrullaId);
                    CampistasList = new ObservableCollection<SelectableItemWrapper<PatrullaCampista>>(cp
                        .Select(pk => new SelectableItemWrapper<PatrullaCampista> { Item = pk }));
                }
            }
            
        }

        public ObservableCollection<PatrullaCampista> GetSelectedCampistas()
        {
            var selected = CampistasList
                .Where(p => p.IsSelected)
                .Select(p => p.Item)
                .ToList();
            return new ObservableCollection<PatrullaCampista>(selected);
        }

        public ObservableCollection<PatrullaCampista> GetNotSelectedCampistas()
        {
            var selected = CampistasList
                .Where(p => p.IsSelected == false)
                .Select(p => p.Item)
                .ToList();
            return new ObservableCollection<PatrullaCampista>(selected);
        }

        public ICommand EnviarAsistencia
        {
            get
            {
                return new Command<int>(async (i) =>
                {
                    SelectedCampistas = GetSelectedCampistas();
                    NotSelectedCampistas = GetNotSelectedCampistas();

                    foreach (var item in SelectedCampistas)
                    {
                        var ecampista = new Campistas()
                        {
                            Id = item.CampistaId,
                            Apellido = item.Campista.Apellido,
                            Asistencia = true,
                            Cedula = item.Campista.Cedula,
                            Descripcion = item.Campista.Descripcion,
                            Fecha_Nacimiento = item.Campista.Fecha_Nacimiento,
                            Inscrito = item.Campista.Inscrito,
                            Nombre = item.Campista.Nombre,
                            NombrePatrulla = item.Campista.NombrePatrulla,
                            Transporte = item.Campista.Transporte
                        };

                        await camplogicServices.PutCampistaAsync(ecampista);
                    }

                    foreach (var item in NotSelectedCampistas)
                    {
                        var ecampista = new Campistas()
                        {
                            Id = item.CampistaId,
                            Apellido = item.Campista.Apellido,
                            Asistencia = false,
                            Cedula = item.Campista.Cedula,
                            Descripcion = item.Campista.Descripcion,
                            Fecha_Nacimiento = item.Campista.Fecha_Nacimiento,
                            Inscrito = item.Campista.Inscrito,
                            Nombre = item.Campista.Nombre,
                            NombrePatrulla = item.Campista.NombrePatrulla,
                            Transporte = item.Campista.Transporte
                        };

                        await camplogicServices.PutCampistaAsync(ecampista);
                    }
                });
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
