﻿
using CampLogic.Model;
using CampLogic.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CampLogic.ViewModel
{
    public class CampistaViewModel : INotifyPropertyChanged
    {
        CampLogicService camplogicServices = new CampLogicService();

       
        private Campistas _campista { get; set; }
        public Campistas Campista
        {
            get { return _campista; }
            set
            {
                _campista = value;
                OnPropertyChanged();
            }
        }

        private List<string> _patrulla;
        public List<string> Patrullas
        {
            get { return _patrulla; }
            set
            {
                _patrulla = value;
                OnPropertyChanged();
            }
        }

        private List<string> _inscrito;
        public List<string> Inscritos
        {
            get { return _inscrito; }
            set
            {
                _inscrito = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<CampistaRepresentante> _representantesList { get; set; }
        public ObservableCollection<CampistaRepresentante> RepresentanteList
        {
            get { return _representantesList; }
            set
            {
                _representantesList = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<Campistas> _campistas { get; set; }
        public ObservableCollection<Campistas> Campistas
        {
            get { return _campistas; }
            set
            {
                _campistas = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<Representante> _repList = new ObservableCollection<Representante>();
        public ObservableCollection<Representante> RepList
        {
            get { return _repList; }
            set
            {
                _repList = value;
                OnPropertyChanged();
            }
        }

        
        public CampistaViewModel()
        {

            InitializeDataAsync();

        }

        public async void InitializeDataAsync()
        {
            var campistas = await camplogicServices.GetCampistasAsync();
            Campistas = new ObservableCollection<Model.Campistas>(campistas.OrderByDescending(x => x.Fecha_Nacimiento));

            var patrullas = await camplogicServices.GetPatrullasAsync();
            Patrullas = new List<string>()
            {
                "Todas"
            };
            foreach (var item in patrullas)
            {
                Patrullas.Add(item.Nombre);
            }

            Inscritos = new List<string>
            {
                "Todos",
                "Inscritos"
            };
        }

        //Detalles Campista
        public CampistaViewModel(Campistas camp)
        {

            InitializeDataAsync2(camp);

        }

        public async void InitializeDataAsync2(Campistas camp)
        {
            var reprecamp = await camplogicServices.GetCampistasRepresentanteAsync();
            Campista = camp;

            RepresentanteList = new ObservableCollection<CampistaRepresentante>(reprecamp.Where(x => x.CampistaId == camp.Id));
        }

        //Nuevo Campista
        public ICommand AgregarCampista
        {
            get
            {
                return new Command<object[]>(async (NC) =>
                {
                    var nombre = (string)NC[0];
                    var apellido = (string)NC[1];
                    var cedula = (int)NC[2];
                    var descripcion = (string)NC[3];
                    var fn = (DateTime)NC[4];
                    var inscrito = (bool)NC[5];
                    var transporte = (bool)NC[6];

                    //validar rep
                    List<Representante> repAux = new List<Representante>();

                    foreach (var r in _repList)
                    {
                        var representante = new Representante
                        {
                            Nombre = r.Nombre,
                            Apellido = r.Apellido,
                            Cedula = r.Cedula,
                            Telefono_1 = r.Telefono_1,
                            Correo = r.Correo
                        };
                        await camplogicServices.PostRepresentanteAsync(representante);

                        repAux.Add(await camplogicServices.GetRepresentanteAsync(r.Cedula));
                    }
                    
                    //Guardar Campista
                    var campista = new Campistas()
                    {
                        Nombre = nombre,
                        Apellido = apellido,
                        Cedula = cedula,
                        Fecha_Nacimiento = fn,
                        Descripcion = descripcion,
                        Inscrito = inscrito,
                        Transporte = transporte
                    };

                    await camplogicServices.PostCampistaAsync(campista);

                    var campLast = (await camplogicServices.GetCampistasAsync()).LastOrDefault();

                    foreach (var item in repAux)
                    {
                        var NRC = new CampistaRepresentante
                        {
                            CampistaId = campLast.Id,
                            RepresentanteId = item.Id
                        };

                        await camplogicServices.PostRepresentanteCampistaAsync(NRC);
                    }

                    _campistas.Add(campista);


                });
            }
        }

        //Editar Campista
        public ICommand EditarCampista
        {
            get
            {
                return new Command<object[]>(async (NC) =>
                {
                    var campista = (Campistas)NC[0];
                    var editarcampista = new Campistas
                    {
                        Id = campista.Id,
                        Nombre = campista.Nombre,
                        Apellido = campista.Apellido,
                        Cedula = campista.Cedula,
                        Descripcion = campista.Descripcion,
                        Fecha_Nacimiento = campista.Fecha_Nacimiento,
                        NombrePatrulla = campista.NombrePatrulla,
                        Asistencia = campista.Asistencia,
                        Transporte = (bool)NC[2],
                        Inscrito = (bool)NC[1]

                    };

                    await camplogicServices.PutCampistaAsync(editarcampista);

                });
            }
        }

        //Agregar Representante
        public ICommand AgregarRepresentanteN
        {
            get
            {
                return new Command<object[]>((AR) =>
                {
                    var nombre = (string)AR[0];
                    var apellido = (string)AR[1];
                    var cedula = (string)AR[2];
                    var telefono = (string)AR[3];
                    var correo = (string)AR[4];

                    var nr = new Representante
                    {
                        Nombre = nombre,
                        Apellido = apellido,
                        Cedula = cedula,
                        Correo = correo,
                        Telefono_1 = telefono
                    };

                    _repList.Add(nr);


                });
            }
        }

        public ICommand AgregarRepresentanteE
        {
            get
            {
                return new Command<object[]>(async (AR) =>
                {
                    var C1 = (string)AR[0];
                    var C2 = (string)AR[1];
                    var C3 = (string)AR[2];

                    
                    if (C1 != null)
                    {
                        var Uno = await camplogicServices.GetRepresentanteAsync(C1);
                        _repList.Add(Uno);
                    }
                    
                    if (C2 != null)
                    {
                        var Dos = await camplogicServices.GetRepresentanteAsync(C2);
                        _repList.Add(Dos);
                    }
                    
                    if (C3 != null)
                    {
                        var Tres = await camplogicServices.GetRepresentanteAsync(C3);
                        _repList.Add(Tres);
                    }

                });
            }
        }


        public async Task<ObservableCollection<Campistas>> SearchCampista(string keyword)
        {
            var campistas = await camplogicServices.GetCampistasAsync();
            return Campistas = new ObservableCollection<Campistas>(campistas.Where(x => x.Nombre.ToLower().Contains(keyword.ToLower()) || x.Apellido.ToLower().Contains(keyword.ToLower())));
        }

        public async Task<ObservableCollection<Campistas>> FiltrarPorPatrulla(string keyword)
        {
            var personal = await camplogicServices.GetCampistasAsync();
            if (keyword == "Todos")
            {
                return Campistas = personal;
            }
            else
            {
                return Campistas = new  ObservableCollection<Model.Campistas>(personal.Where(x => x.NombrePatrulla == keyword));
            }
        }

        public async Task<ObservableCollection<Campistas>> FiltrarPorInscrito(string keyword)
        {
            var personal = await camplogicServices.GetCampistasAsync();
            if (keyword == "Todos")
            {
                return Campistas = personal;
            }
            else
            {
                return Campistas = new ObservableCollection<Model.Campistas>(personal.Where(x => x.Inscrito == true));
            }
        }

        public async Task<ObservableCollection<Campistas>> CampistaList()
        {
            var campistas = await camplogicServices.GetCampistasAsync();
            return Campistas = new ObservableCollection<Model.Campistas>(campistas.OrderByDescending(x => x.Fecha_Nacimiento));
        }



        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
