﻿using CampLogic.HelpClasses;
using CampLogic.Model;
using CampLogic.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.ViewModel
{
    public class InventarioViewModel : INotifyPropertyChanged
    {
        CampLogicService camplogicService = new CampLogicService();

        private List<Inventario> _inventariolist { get; set; }
        public List<Inventario> InventarioList
        {
            get { return _inventariolist; }
            set
            {
                _inventariolist = value;
                OnPropertyChanged();
            }
        }

        private int _cantidadtotal { get; set; }
        public int CantidadTotal
        {
            get { return _cantidadtotal; }
            set
            {
                _cantidadtotal = value;
                OnPropertyChanged();
            }
        }

        private float _valortotal { get; set; }
        public float ValorTotal
        {
            get { return _valortotal; }
            set
            {
                _valortotal = value;
                OnPropertyChanged();
            }
        }

       

        public InventarioViewModel(string categoria, string subcategoria, string ubcacion, string articulo, string filtro)
        {
            InitializeDataAsync( categoria, subcategoria, ubcacion, articulo, filtro);
        }

        public async void InitializeDataAsync(string categoria, string subcategoria, string ubcacion, string articulo, string filtro)
        {
            var productos = await camplogicService.GetProductosAsync();
            List<Producto> prods = new List<Producto>();
            List<Inventario> inventarioList = new List<Inventario>();
            if (categoria != "NT" && subcategoria == "NT" && ubcacion == "NT" && articulo == "NT")
            {
                prods = PorCategoria(categoria, productos);
                if (filtro == "Todos" || filtro == "NT")
                {
                    foreach (var prod in prods)
                    {
                        var costo = await Costo(prod.Id);
                        inventarioList.Add(new Inventario()
                        {
                            Producto = prod,
                            Costo = costo,
                            ValorTotal = prod.Disponible * costo

                        });
                        CantidadTotal = CantidadTotal + prod.Disponible;
                        ValorTotal = ValorTotal + prod.Disponible * costo;
                    }
                    InventarioList = inventarioList;
                }
                else
                {
                    var pa = prods.Where(x => x.Disponible <= x.PuntoReorden);
                    if (pa.Count() != 0)
                    {
                        foreach (var prod in pa)
                        {
                            var costo = await Costo(prod.Id);
                            inventarioList.Add(new Inventario()
                            {
                                Producto = prod,
                                Costo = costo,
                                ValorTotal = prod.Disponible * costo

                            });
                            CantidadTotal = CantidadTotal + prod.Disponible;
                            ValorTotal = ValorTotal + prod.Disponible * costo;
                        }
                    }

                    InventarioList = inventarioList;
                }
            }

            else if (categoria != "NT" && subcategoria == "NT" && ubcacion == "NT" && articulo != "NT")
            {
                prods = PorArticulo(articulo, productos);
                if (filtro == "Todos" || filtro == "NT")
                {
                    foreach (var prod in prods)
                    {
                        var costo = await Costo(prod.Id);
                        inventarioList.Add(new Inventario()
                        {
                            Producto = prod,
                            Costo = costo,
                            ValorTotal = prod.Disponible * costo

                        });
                        CantidadTotal = CantidadTotal + prod.Disponible;
                        ValorTotal = ValorTotal + prod.Disponible * costo;
                    }
                    InventarioList = inventarioList;
                }
                else
                {
                    var pa = prods.Where(x => x.Disponible <= x.PuntoReorden);
                    if (pa.Count() != 0)
                    {
                        foreach (var prod in pa)
                        {
                            var costo = await Costo(prod.Id);
                            inventarioList.Add(new Inventario()
                            {
                                Producto = prod,
                                Costo = costo,
                                ValorTotal = prod.Disponible * costo

                            });
                            CantidadTotal = CantidadTotal + prod.Disponible;
                            ValorTotal = ValorTotal + prod.Disponible * costo;
                        }
                    }

                    InventarioList = inventarioList;
                }
            }
            else if (categoria != "NT" && subcategoria != "NT" && ubcacion == "NT" && articulo == "NT")
            {
                prods = PorSubCategoria(categoria, subcategoria, productos);
                if (filtro == "Todos" || filtro == "NT")
                {
                    foreach (var prod in prods)
                    {
                        var costo = await Costo(prod.Id);
                        inventarioList.Add(new Inventario()
                        {
                            Producto = prod,
                            Costo = costo,
                            ValorTotal = prod.Disponible * costo

                        });
                        CantidadTotal = CantidadTotal + prod.Disponible;
                        ValorTotal = ValorTotal + prod.Disponible * costo;
                    }
                    InventarioList = inventarioList;
                }
                else
                {
                    var pa = prods.Where(x => x.Disponible <= x.PuntoReorden);
                    if (pa.Count() != 0)
                    {
                        foreach (var prod in pa)
                        {
                            var costo = await Costo(prod.Id);
                            inventarioList.Add(new Inventario()
                            {
                                Producto = prod,
                                Costo = costo,
                                ValorTotal = prod.Disponible * costo

                            });
                            CantidadTotal = CantidadTotal + prod.Disponible;
                            ValorTotal = ValorTotal + prod.Disponible * costo;
                        }
                    }

                    InventarioList = inventarioList;
                }
            }
            else if (categoria != "NT" && subcategoria != "NT" && ubcacion == "NT" && articulo != "NT")
            {
                prods = PorArticulo(articulo, productos);
                if (filtro == "Todos" || filtro == "NT")
                {
                    foreach (var prod in prods)
                    {
                        var costo = await Costo(prod.Id);
                        inventarioList.Add(new Inventario()
                        {
                            Producto = prod,
                            Costo = costo,
                            ValorTotal = prod.Disponible * costo

                        });
                        CantidadTotal = CantidadTotal + prod.Disponible;
                        ValorTotal = ValorTotal + prod.Disponible * costo;
                    }
                    InventarioList = inventarioList;
                }
                else
                {
                    var pa = prods.Where(x => x.Disponible <= x.PuntoReorden);
                    if (pa.Count() != 0)
                    {
                        foreach (var prod in pa)
                        {
                            var costo = await Costo(prod.Id);
                            inventarioList.Add(new Inventario()
                            {
                                Producto = prod,
                                Costo = costo,
                                ValorTotal = prod.Disponible * costo

                            });
                            CantidadTotal = CantidadTotal + prod.Disponible;
                            ValorTotal = ValorTotal + prod.Disponible * costo;
                        }
                    }

                    InventarioList = inventarioList;
                }
            }
            else if (categoria == "NT" && subcategoria == "NT" && ubcacion == "NT" && articulo != "NT")
            {
                prods = PorArticulo(articulo, productos);
                if (filtro == "Todos" || filtro == "NT")
                {
                    foreach (var prod in prods)
                    {
                        var costo = await Costo(prod.Id);
                        inventarioList.Add(new Inventario()
                        {
                            Producto = prod,
                            Costo = costo,
                            ValorTotal = prod.Disponible * costo

                        });
                        CantidadTotal = CantidadTotal + prod.Disponible;
                        ValorTotal = ValorTotal + prod.Disponible * costo;
                    }
                    InventarioList = inventarioList;
                }
                else
                {
                    var pa = prods.Where(x => x.Disponible <= x.PuntoReorden);
                    if (pa.Count() != 0)
                    {
                        foreach (var prod in pa)
                        {
                            var costo = await Costo(prod.Id);
                            inventarioList.Add(new Inventario()
                            {
                                Producto = prod,
                                Costo = costo,
                                ValorTotal = prod.Disponible * costo

                            });
                            CantidadTotal = CantidadTotal + prod.Disponible;
                            ValorTotal = ValorTotal + prod.Disponible * costo;
                        }
                    }

                    InventarioList = inventarioList;
                }
            }
            else if (categoria != "NT" && subcategoria != "NT" && ubcacion != "NT" && articulo != "NT")
            {
                prods = PorArticulo(articulo, productos);
                if (filtro == "Todos" || filtro == "NT")
                {
                    foreach (var prod in prods)
                    {
                        var costo = await Costo(prod.Id);
                        inventarioList.Add(new Inventario()
                        {
                            Producto = prod,
                            Costo = costo,
                            ValorTotal = prod.Disponible * costo

                        });
                        CantidadTotal = CantidadTotal + prod.Disponible;
                        ValorTotal = ValorTotal + prod.Disponible * costo;
                    }
                    InventarioList = inventarioList;
                }
                else
                {
                    var pa = prods.Where(x => x.Disponible <= x.PuntoReorden);
                    if (pa.Count() != 0)
                    {
                        foreach (var prod in pa)
                        {
                            var costo = await Costo(prod.Id);
                            inventarioList.Add(new Inventario()
                            {
                                Producto = prod,
                                Costo = costo,
                                ValorTotal = prod.Disponible * costo

                            });
                            CantidadTotal = CantidadTotal + prod.Disponible;
                            ValorTotal = ValorTotal + prod.Disponible * costo;
                        }
                    }

                    InventarioList = inventarioList;
                }
            }
            else if (categoria == "NT" && subcategoria == "NT" && ubcacion != "NT" && articulo == "NT")
            {
                prods = PorUbicacion(ubcacion, productos);
                if (filtro == "Todos" || filtro == "NT")
                {
                    foreach (var prod in prods)
                    {
                        var costo = await Costo(prod.Id);
                        inventarioList.Add(new Inventario()
                        {
                            Producto = prod,
                            Costo = costo,
                            ValorTotal = prod.Disponible * costo

                        });
                        CantidadTotal = CantidadTotal + prod.Disponible;
                        ValorTotal = ValorTotal + prod.Disponible * costo;
                    }
                    InventarioList = inventarioList;
                }
                else
                {
                    var pa = prods.Where(x => x.Disponible <= x.PuntoReorden);
                    if (pa.Count() != 0)
                    {
                        foreach (var prod in pa)
                        {
                            var costo = await Costo(prod.Id);
                            inventarioList.Add(new Inventario()
                            {
                                Producto = prod,
                                Costo = costo,
                                ValorTotal = prod.Disponible * costo

                            });
                            CantidadTotal = CantidadTotal + prod.Disponible;
                            ValorTotal = ValorTotal + prod.Disponible * costo;
                        }
                    }

                    InventarioList = inventarioList;
                }
            }
            else if (categoria != "NT" && subcategoria == "NT" && ubcacion != "NT" && articulo == "NT")
            {
                prods = PorUbicacionCat(ubcacion, categoria, productos);
                if (filtro == "Todos" || filtro == "NT")
                {
                    foreach (var prod in prods)
                    {
                        var costo = await Costo(prod.Id);
                        inventarioList.Add(new Inventario()
                        {
                            Producto = prod,
                            Costo = costo,
                            ValorTotal = prod.Disponible * costo

                        });
                        CantidadTotal = CantidadTotal + prod.Disponible;
                        ValorTotal = ValorTotal + prod.Disponible * costo;
                    }
                    InventarioList = inventarioList;
                }
                else
                {
                    var pa = prods.Where(x => x.Disponible <= x.PuntoReorden);
                    if (pa.Count() != 0)
                    {
                        foreach (var prod in pa)
                        {
                            var costo = await Costo(prod.Id);
                            inventarioList.Add(new Inventario()
                            {
                                Producto = prod,
                                Costo = costo,
                                ValorTotal = prod.Disponible * costo

                            });
                            CantidadTotal = CantidadTotal + prod.Disponible;
                            ValorTotal = ValorTotal + prod.Disponible * costo;
                        }
                    }

                    InventarioList = inventarioList;
                }
            }
            else if (categoria != "NT" && subcategoria != "NT" && ubcacion != "NT" && articulo == "NT")
            {
                prods = PorUbicacionSub(ubcacion, subcategoria, productos);
                if (filtro == "Todos" || filtro == "NT")
                {
                    foreach (var prod in prods)
                    {
                        var costo = await Costo(prod.Id);
                        inventarioList.Add(new Inventario()
                        {
                            Producto = prod,
                            Costo = costo,
                            ValorTotal = prod.Disponible * costo

                        });
                        CantidadTotal = CantidadTotal + prod.Disponible;
                        ValorTotal = ValorTotal + prod.Disponible * costo;
                    }
                    InventarioList = inventarioList;
                }
                else
                {
                    var pa = prods.Where(x => x.Disponible <= x.PuntoReorden);
                    if (pa.Count() != 0)
                    {
                        foreach (var prod in pa)
                        {
                            var costo = await Costo(prod.Id);
                            inventarioList.Add(new Inventario()
                            {
                                Producto = prod,
                                Costo = costo,
                                ValorTotal = prod.Disponible * costo

                            });
                            CantidadTotal = CantidadTotal + prod.Disponible;
                            ValorTotal = ValorTotal + prod.Disponible * costo;
                        }
                    }

                    InventarioList = inventarioList;
                }
            }
            else
            {
                prods = PorArticulo(articulo, productos);
                if (filtro == "Todos" || filtro == "NT")
                {
                    foreach (var prod in prods)
                    {
                        var costo = await Costo(prod.Id);
                        inventarioList.Add(new Inventario()
                        {
                            Producto = prod,
                            Costo = costo,
                            ValorTotal = prod.Disponible * costo

                        });
                        CantidadTotal = CantidadTotal + prod.Disponible;
                        ValorTotal = ValorTotal + prod.Disponible * costo;
                    }
                    InventarioList = inventarioList;
                }
                else
                {
                    var pa = prods.Where(x => x.Disponible <= x.PuntoReorden);
                    if (pa.Count() != 0)
                    {
                        foreach (var prod in pa)
                        {
                            var costo = await Costo(prod.Id);
                            inventarioList.Add(new Inventario()
                            {
                                Producto = prod,
                                Costo = costo,
                                ValorTotal = prod.Disponible * costo

                            });
                            CantidadTotal = CantidadTotal + prod.Disponible;
                            ValorTotal = ValorTotal + prod.Disponible * costo;
                        }
                    }

                    InventarioList = inventarioList;
                }
            }
            
            
        }

        public async Task<float> Costo(int prodId)
        {
            var cotizacion = await camplogicService.GetCotizacionProdAsync();

            var lastCot = cotizacion.LastOrDefault(b => b.ProductoId == prodId);

            var Costo = lastCot.Precio / lastCot.Cantidad;

            return Costo;
        }

        public List<Producto> PorCategoria(string categoria, List<Producto> prodWS)
        {
            List<Producto> prods = new List<Producto>();
            if (categoria == "Todas")
            {
                prods = prodWS.OrderBy(x => x.SubCategoria.Categoria.Nombre).ToList();
            }
            else
            {
                prods = prodWS.Where(x => x.SubCategoria.Categoria.Nombre == categoria).ToList();
            }

            return prods;

        }

        public List<Producto> PorSubCategoria(string categoria, string subcategoria, List<Producto> prodWS)
        {
            var prods = prodWS.Where(x => x.SubCategoria.Categoria.Nombre == categoria && x.SubCategoria.Nombre == subcategoria).ToList();
            return prods;
        }

        public List<Producto> PorUbicacion(string ubicacion, List<Producto> prodWS)
        {
            List<Producto> prods = new List<Producto>();
            prods = prodWS.Where(x => x.Ubicacion.Nombre == ubicacion).ToList();
            return prods;
        }

        public List<Producto> PorUbicacionCat(string ubicacion, string categoria, List<Producto> prodWS)
        {
            List<Producto> prods = new List<Producto>();
            if (categoria == "Todas" && ubicacion == "Todas")
            {
                prods = prodWS.OrderBy(x => x.Articulo).ToList();

            }
            else if (categoria == "Todas")
            {
                prods = prodWS.Where(x => x.Ubicacion.Nombre == ubicacion).ToList();
            }
            else if (ubicacion == "Todas")
            {
                prods = prodWS.Where(x => x.SubCategoria.Categoria.Nombre == categoria).ToList();
            }
            else
            {
                prods = prodWS.Where(x => x.Ubicacion.Nombre == ubicacion && x.SubCategoria.Categoria.Nombre == categoria).ToList();
            }
            return prods;
        }

        public List<Producto> PorUbicacionSub(string ubicacion, string subcategoria, List<Producto> prodWS)
        {
            List<Producto> prods = new List<Producto>();
            if (subcategoria == "Todas")
            {
                prods = prodWS.Where(x => x.Ubicacion.Nombre == ubicacion).ToList();
            }
            else
            {
                prods = prodWS.Where(x => x.Ubicacion.Nombre == ubicacion && x.SubCategoria.Nombre == subcategoria).ToList();
            }
            return prods;
        }

        public List<Producto> PorArticulo(string articulo, List<Producto> prodWS)
        {
            List<Producto> prods = new List<Producto>();
            if (articulo == "Todos")
            {
                prods = prodWS.OrderBy(x => x.Articulo == articulo).ToList();
            }
            else
            {
                prods = prodWS.Where(x => x.Articulo == articulo).ToList();
            }

            return prods;
        }

        public List<Producto> Products(List<Producto> prodWS)
        {
            var prods = prodWS.OrderBy(x => x.Articulo).ToList();
            return prods;
        }



        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
