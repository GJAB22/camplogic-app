﻿using CampLogic.Model;
using CampLogic.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CampLogic.ViewModel
{
     

    public class EntradasViewModel : INotifyPropertyChanged
    {
        CampLogicService camplogicService = new CampLogicService();
        

        private List<Entrada> _entradas { get; set; }
        public List<Entrada> Entradas
        {
            get { return _entradas; }
            set
            {
                _entradas = value;
                OnPropertyChanged();
            }
        }

        private List<Entrada> _topentradas { get; set; }
        public List<Entrada> TopEntradas
        {
            get { return _topentradas; }
            set
            {
                _topentradas = value;
                OnPropertyChanged();
            }
        }

        //Entradas de Proveedores
        private ObservableCollection<EntradaProducto> _entradaList = new ObservableCollection<EntradaProducto>();
        public ObservableCollection<EntradaProducto> EntradaList
        {
            get { return _entradaList; }
            set
            {
                _entradaList = value;
                OnPropertyChanged();
            }
        }

        //Reintegros
        private ObservableCollection<ReintegroProducto> _reintegrosList = new ObservableCollection<ReintegroProducto>();
        public ObservableCollection<ReintegroProducto> ReintegrosList
        {
            get { return _reintegrosList; }
            set
            {
                _reintegrosList = value;
                OnPropertyChanged();
            }
        }

        

        private DateTime _diaEntrega { get; set; }
        public DateTime DiaEntrega
        {
            get { return _diaEntrega; }
            set
            {
                _diaEntrega = value;
                OnPropertyChanged();
            }
        }

        private TimeSpan _horaEntrega { get; set; }
        public TimeSpan HoraEntrega
        {
            get { return _horaEntrega; }
            set
            {
                _horaEntrega = value;
                OnPropertyChanged();
            }
        }

        private List<Categoria> _categorias { get; set; }
        public List<Categoria> Categorias
        {
            get { return _categorias; }
            set
            {
                _categorias = value;
                OnPropertyChanged();
            }
        }
        
        //Agregar Producto al Listado Para Agregar
        public int IdOrdenC { get; set; }
        public int ProductoId { get; set; }
        public int CantidadEntregada { get; set; }
        public string Categoria { get; set; }
        public string Subcategoria { get; set; }
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        public string IdOrden { get; set; }

        //
        public string ProdId { get; set; }
        //public string ProdDes { get; set; }
        //public string ProdTip { get; set; }
        public int CantidadR { get; set; }

        //Orden de Compra
        public int OrdenId { get; set; }

        private int Cant { get; set; }

        private int CantE { get; set; }
        private float VT { get; set; }

        private int BOT { get; set; }

        private bool Esta { get; set; }



        public EntradasViewModel()
        {

            InitializeDataAsync();
            
        }

        public async void InitializeDataAsync()
        {
            var entradas = await camplogicService.GetEntradasAsync();
            var categoria = await camplogicService.GetCategoriasAsync();

            TopEntradas = entradas.Skip(Math.Max(0, entradas.Count() - 5)).Take(5).Reverse().ToList();
            if (TopEntradas == null)
            {
                TopEntradas = entradas.Skip(Math.Max(0, entradas.Count() - 4)).Take(4).Reverse().ToList();
                if (TopEntradas == null)
                {
                    TopEntradas = entradas.Skip(Math.Max(0, entradas.Count() - 3)).Take(3).Reverse().ToList();
                    if (TopEntradas == null)
                    {
                        TopEntradas = entradas.Skip(Math.Max(0, entradas.Count() - 2)).Take(2).Reverse().ToList();
                        if (TopEntradas == null)
                        {
                            TopEntradas = entradas.Skip(Math.Max(0, entradas.Count() - 1)).Take(1).Reverse().ToList();
                        }
                    }
                }

            }

            
            Categorias = categoria.OrderBy(x => x.Nombre).ToList();
            DiaEntrega = DateTime.Today;
        }

        public EntradasViewModel(DateTime fechaI, DateTime fechaF)
        {

            InitializeDataAsync(fechaI,fechaF);

        }

        public async void InitializeDataAsync(DateTime fechaI, DateTime fechaF)
        {
            var entradas = await camplogicService.GetEntradasAsync();
            Entradas = entradas.Where(b => b.Fecha.Date >= fechaI.Date && b.Fecha.Date <= fechaF.Date).Reverse().ToList();
        }

        //Agregar Entrada General
        public ICommand RegistrarEntrada
        {
            get
            {
                return new Command<int>(async (IdOrden) =>
                {
                    
                    //Cambio Estado de Orden
                    var orden = await camplogicService.GetOrdenCompraAsync(IdOrden);
                    
                    //Entrada Nueva
                    foreach (var item in _entradaList)
                    {
                        CantE = CantE + item.CantidadEntregada;
                        VT = VT + item.ValorTotal;
                        BOT = BOT + item.BackOrder;
                    }

                    var entradaNueva = new Entrada
                    {
                        Fecha = new DateTime(_diaEntrega.Year, _diaEntrega.Month, _diaEntrega.Day, _horaEntrega.Hours, _horaEntrega.Minutes, _horaEntrega.Seconds),
                        Cantidad = CantE,
                        Valor_Total = VT,
                        OrdenCompraId = orden.Id,
                        BackOrderTotal = BOT,
                    };

                    var resp = await camplogicService.PostEntradaAsync(entradaNueva);

                    //Agregar Entradas a Entradas-Productos
                    var entradas = await camplogicService.GetEntradasAsync();
                    var entradaU = entradas.LastOrDefault(x => x.OrdenCompraId == orden.Id);

                    foreach (var item in _entradaList)
                    {
                        var entradaItem = new EntradaProducto()
                        {
                            CantidadEntregada = item.CantidadEntregada,
                            BackOrder = item.BackOrder,
                            ValorTotal = item.ValorTotal,
                            CostoUnitario = item.CostoUnitario,
                            ProductoId = item.Producto.Id,
                            EntradaId = entradaU.Id
                        };

                        var responde = await camplogicService.PostEntradaProdAsync(entradaItem);

                        //Agregar la Cantidad Entrante
                        var productos = await camplogicService.GetProductosAsync();
                        var producto = productos.FirstOrDefault(x => x.Id == item.Producto.Id);
                        var productoCambio = new Producto
                        {
                            Id = producto.Id,
                            Articulo = producto.Articulo,
                            Descripcion = producto.Descripcion,
                            Disponible = producto.Disponible + item.CantidadEntregada,
                            PuntoReorden = producto.PuntoReorden,
                            Existencia_Inicial = producto.Existencia_Inicial,
                            Valor_Inicial = producto.Valor_Inicial,
                            CantidadIdeal = producto.CantidadIdeal,
                            SubCategoriaId = producto.SubCategoriaId,
                            ProveedorId = producto.ProveedorId

                        };

                        var response = await camplogicService.PutProductAsync(productoCambio);


                    }
                    
                    //Cambio en Orden Compra
                    
                    var entradasOrden = entradas.Where(b => b.OrdenCompraId == orden.Id).ToList();

                    foreach (var entrada in entradasOrden)
                    {
                        Cant = Cant + entrada.Cantidad;
                    }

                    if (Cant == orden.Cantidad_Orden)
                    {
                        var cambioOrden = new Orden_Compra
                        {
                            Id = orden.Id,
                            Fecha_Orden = orden.Fecha_Orden,
                            Cantidad_Orden = orden.Cantidad_Orden,
                            Costo_Total = orden.Costo_Total,
                            EmpleadoId = orden.EmpleadoId,
                            ProveedorId = orden.ProveedorId,
                            Aprobado = true
                        };

                        var r = await camplogicService.PutOrdenCompraAsync(cambioOrden);
                    }

                });
            }
        }


        //Agregar por ID
        public ICommand AgregarProdId
        {
            get
            {
                return new Command<int[]>(async (DetallesProd) =>
                {

                    int BO;
                    IdOrdenC = DetallesProd[0];
                    ProductoId = DetallesProd[1];
                    CantidadEntregada = DetallesProd[2];
                    //Pedido
                    var pedidos = await camplogicService.GetPedidosAsync();
                    var ped = pedidos.FirstOrDefault(x => x.OrdenCompraId == IdOrdenC && x.ProductoId == ProductoId);
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.Id == ProductoId);
                    //Entrada Anterior
                    var entradas = await camplogicService.GetEntradasAsync();
                    if (entradas.Count() != 0)
                    {
                        var entrada = entradas.LastOrDefault(x => x.OrdenCompraId == IdOrdenC);
                        if (entrada != null)
                        {
                            var entradasprod = await camplogicService.GetEntradasProdsAsync();
                            var entraprod = entradasprod.FirstOrDefault(x => x.EntradaId == entrada.Id && x.ProductoId == ProductoId);
                            BO = entraprod.BackOrder - CantidadEntregada;
                        }
                        else
                        {
                            BO = ped.Cantidad - CantidadEntregada;
                        }
                        
                    }
                    else
                    {
                        BO = ped.Cantidad - CantidadEntregada;
                    }
                    


                    var ProdEntrada = new EntradaProducto()
                    {
                        CantidadEntregada = CantidadEntregada,
                        BackOrder = BO,
                        Producto = prod,
                        CostoUnitario = ped.Precio,
                        ValorTotal = ped.Precio * CantidadEntregada,
                         
                    };

                    _entradaList.Add(ProdEntrada);
                });
            }
        }
        //Agregar Sin SubCategoria, Con Descripcion
        public ICommand AgregarProdCategoriaDes
        {
            get
            {
                return new Command<string[]>(async (DetallesProd) =>
                {
                    Categoria = DetallesProd[0];
                    Articulo = DetallesProd[1];
                    Descripcion = DetallesProd[2];
                    IdOrdenC = Convert.ToInt32(DetallesProd[3]);
                    CantidadEntregada = Convert.ToInt32(DetallesProd[4]);
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.SubCategoria.Categoria.Nombre == Categoria && x.Articulo == Articulo && x.Descripcion == Descripcion);
                    //Pedido
                    var pedidos = await camplogicService.GetPedidosAsync();
                    var ped = pedidos.FirstOrDefault(x => x.OrdenCompraId == IdOrdenC && x.ProductoId == prod.Id);
                    
                    var ProdEntrada = new EntradaProducto()
                    {
                        CantidadEntregada = Convert.ToInt32(CantidadEntregada),
                        BackOrder = ped.Cantidad - Convert.ToInt32(CantidadEntregada),
                        Producto = prod,
                        CostoUnitario = ped.Precio,
                        ValorTotal = ped.Precio * Convert.ToInt32(CantidadEntregada),
                    };

                    _entradaList.Add(ProdEntrada);

                    int j = 1;
                });
            }
        }
        //Agregar Con SubCategoria, Con Descripcion
        public ICommand AgregarProdSubCategoriaDes
        {
            get
            {
                return new Command<string[]>(async (DetallesProd) =>
                {
                    Subcategoria = DetallesProd[0];
                    Articulo = DetallesProd[1];
                    Descripcion = DetallesProd[2];
                    IdOrdenC = Convert.ToInt32(DetallesProd[3]);
                    CantidadEntregada = Convert.ToInt32(DetallesProd[4]);
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.SubCategoria.Nombre == Subcategoria && x.Articulo == Articulo && x.Descripcion == Descripcion);
                    //Pedido
                    var pedidos = await camplogicService.GetPedidosAsync();
                    var ped = pedidos.FirstOrDefault(x => x.OrdenCompraId == IdOrdenC && x.ProductoId == prod.Id);

                    var ProdEntrada = new EntradaProducto()
                    {
                        CantidadEntregada = Convert.ToInt32(CantidadEntregada),
                        BackOrder = ped.Cantidad - Convert.ToInt32(CantidadEntregada),
                        Producto = prod,
                        CostoUnitario = ped.Precio,
                        ValorTotal = ped.Precio * Convert.ToInt32(CantidadEntregada),
                    };

                    _entradaList.Add(ProdEntrada);

                    int j = 1;
                });
            }
        }
        //Agregar Con SubCategoria, Sin Descripcion
        public ICommand AgregarProdSubCategoria
        {
            get
            {
                return new Command<string[]>(async (DetallesProd) =>
                {
                    Subcategoria = DetallesProd[0];
                    Articulo = DetallesProd[1];
                    IdOrdenC = Convert.ToInt32(DetallesProd[2]);
                    CantidadEntregada = Convert.ToInt32(DetallesProd[3]);
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.SubCategoria.Nombre == Subcategoria && x.Articulo == Articulo);
                    //Pedido
                    var pedidos = await camplogicService.GetPedidosAsync();
                    var ped = pedidos.FirstOrDefault(x => x.OrdenCompraId == IdOrdenC && x.ProductoId == prod.Id);

                    var ProdEntrada = new EntradaProducto()
                    {
                        CantidadEntregada = Convert.ToInt32(CantidadEntregada),
                        BackOrder = ped.Cantidad - Convert.ToInt32(CantidadEntregada),
                        Producto = prod,
                        CostoUnitario = ped.Precio,
                        ValorTotal = ped.Precio * Convert.ToInt32(CantidadEntregada),
                    };

                    _entradaList.Add(ProdEntrada);

                    int j = 1;
                });
            }
        }
        //Agregar Sin SubCategoria, Sin Descripcion
        public ICommand AgregarProdCategoria
        {
            get
            {
                return new Command<string[]>(async (DetallesProd) =>
                {
                    Categoria = DetallesProd[0];
                    Articulo = DetallesProd[1];
                    IdOrdenC = Convert.ToInt32(DetallesProd[2]);
                    CantidadEntregada = Convert.ToInt32(DetallesProd[3]);
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.SubCategoria.Categoria.Nombre == Categoria && x.Articulo == Articulo);
                    //Pedido
                    var pedidos = await camplogicService.GetPedidosAsync();
                    var ped = pedidos.FirstOrDefault(x => x.OrdenCompraId == IdOrdenC && x.ProductoId == prod.Id);

                    var ProdEntrada = new EntradaProducto()
                    {
                        CantidadEntregada = Convert.ToInt32(CantidadEntregada),
                        BackOrder = ped.Cantidad - Convert.ToInt32(CantidadEntregada),
                        Producto = prod,
                        CostoUnitario = ped.Precio,
                        ValorTotal = ped.Precio * Convert.ToInt32(CantidadEntregada),
                    };

                    _entradaList.Add(ProdEntrada);

                    int j = 1;
                });
            }
        }

        //Reintegros
        public ICommand RegistrarReintegro
        {
            get
            {
                return new Command<int>(async(num) =>
                {
                    //Cambio Estado de Orden
                    
                    //Entrada Nueva
                    foreach (var item in _reintegrosList)
                    {
                        CantE = CantE + item.Cantidad;
                    }

                    var entradaNueva = new Reintegros
                    {
                        FechaEntrada = new DateTime(_diaEntrega.Year, _diaEntrega.Month, _diaEntrega.Day, _horaEntrega.Hours, _horaEntrega.Minutes, _horaEntrega.Seconds),
                        CantidadTotal = CantE
                    };

                    var resp = await camplogicService.PostReintegroAsync(entradaNueva);

                    //Agregar Entradas a Entradas-Productos
                    var entradas = await camplogicService.GetReintegrosAsync();
                    var entradaU = entradas.LastOrDefault();

                    foreach (var item in _reintegrosList)
                    {
                        var entradaItem = new ReintegroProducto()
                        {
                            Cantidad = item.Cantidad,
                            Faltante = 0,
                            ProductoId = item.Producto.Id,
                            ReintegroId = entradaU.Id,
                            PorDirector = true
                        };

                        var responde = await camplogicService.PostReintegroProdAsync(entradaItem);

                        //Agregar la Cantidad Entrante
                        var productos = await camplogicService.GetProductosAsync();
                        var producto = productos.FirstOrDefault(x => x.Id == item.Producto.Id);
                        var productoCambio = new Producto
                        {
                            Id = producto.Id,
                            Articulo = producto.Articulo,
                            Descripcion = producto.Descripcion,
                            Disponible = producto.Disponible + item.Cantidad,
                            PuntoReorden = producto.PuntoReorden,
                            Existencia_Inicial = producto.Existencia_Inicial,
                            Valor_Inicial = producto.Valor_Inicial,
                            CantidadIdeal = producto.CantidadIdeal,
                            SubCategoriaId = producto.SubCategoriaId,
                            ProveedorId = producto.ProveedorId

                        };

                        var response = await camplogicService.PutProductAsync(productoCambio);


                    }
                });
            }
        }
        public ICommand AgregarProdIdReintegro
        {
            get
            {
                return new Command<int[]>(async (DetallesProd) =>
                {
                    
                    ProductoId = DetallesProd[0];
                    CantidadEntregada = DetallesProd[1];
                    
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.Id == ProductoId);
                    
                    var ProdEntrada = new ReintegroProducto()
                    {
                        Cantidad = CantidadEntregada,
                        Faltante = 0,
                        Producto = prod

                    };

                    _reintegrosList.Add(ProdEntrada);
                });
            }
        }
        //Agregar Sin SubCategoria, Con Descripcion
        public ICommand AgregarProdCategoriaDesReintegro
        {
            get
            {
                return new Command<string[]>(async (DetallesProd) =>
                {
                    Categoria = DetallesProd[0];
                    Articulo = DetallesProd[1];
                    Descripcion = DetallesProd[2];
                    CantidadEntregada = Convert.ToInt32(DetallesProd[3]);
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.SubCategoria.Categoria.Nombre == Categoria && x.Articulo == Articulo && x.Descripcion == Descripcion);
                    

                    var ProdEntrada = new ReintegroProducto()
                    {
                        Cantidad = Convert.ToInt32(CantidadEntregada),
                        Faltante = 0,
                        Producto = prod
                        
                    };

                    _reintegrosList.Add(ProdEntrada);
                });
            }
        }
        //Agregar Con SubCategoria, Con Descripcion
        public ICommand AgregarProdSubCategoriaDesReintegro
        {
            get
            {
                return new Command<string[]>(async (DetallesProd) =>
                {
                    Subcategoria = DetallesProd[0];
                    Articulo = DetallesProd[1];
                    Descripcion = DetallesProd[2];
                    CantidadEntregada = Convert.ToInt32(DetallesProd[3]);
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.SubCategoria.Nombre == Subcategoria && x.Articulo == Articulo && x.Descripcion == Descripcion);
                   
                    var ProdEntrada = new ReintegroProducto()
                    {
                        Cantidad = Convert.ToInt32(CantidadEntregada),
                        Faltante = 0,
                        Producto = prod
                        
                    };

                    _reintegrosList.Add(ProdEntrada);
                });
            }
        }
        //Agregar Con SubCategoria, Sin Descripcion
        public ICommand AgregarProdSubCategoriaReintegro
        {
            get
            {
                return new Command<string[]>(async (DetallesProd) =>
                {
                    Subcategoria = DetallesProd[0];
                    Articulo = DetallesProd[1];
                    CantidadEntregada = Convert.ToInt32(DetallesProd[2]);
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.SubCategoria.Nombre == Subcategoria && x.Articulo == Articulo);
                    

                    var ProdEntrada = new ReintegroProducto()
                    {
                        Cantidad = Convert.ToInt32(CantidadEntregada),
                        Faltante = 0,
                        Producto = prod
                       
                    };

                    _reintegrosList.Add(ProdEntrada);
                });
            }
        }
        //Agregar Sin SubCategoria, Sin Descripcion
        public ICommand AgregarProdCategoriaReintegro
        {
            get
            {
                return new Command<string[]>(async (DetallesProd) =>
                {
                    Categoria = DetallesProd[0];
                    Articulo = DetallesProd[1];
                    CantidadEntregada = Convert.ToInt32(DetallesProd[2]);
                    //Producto
                    var productos = await camplogicService.GetProductosAsync();
                    var prod = productos.FirstOrDefault(x => x.SubCategoria.Categoria.Nombre == Categoria && x.Articulo == Articulo);

                    var ProdEntrada = new ReintegroProducto()
                    {
                        Cantidad = Convert.ToInt32(CantidadEntregada),
                        Faltante = 0,
                        Producto = prod
                    };

                    _reintegrosList.Add(ProdEntrada);
                    
                });
            }
        }

        //Filtrado de Picker
        public async Task<List<SubCategoria>> SearchSubCategoria(int id)
        {
            var subcategoria = await camplogicService.GetSubCategoriasAsync();
            var SubCategorias = subcategoria.Where(x => x.CategoriaId == id).ToList();
            return SubCategorias;
        }
        public async Task<List<Producto>> FilterCategoria(int id)
        {
            var productos = await camplogicService.GetProductosAsync();
            var Productos = productos.OrderBy(x => x.Articulo).Where(x => x.SubCategoria.Categoria.Id == id).ToList();
            return Productos;
        }
        public async Task<List<Producto>> FilterSubCategoria(int idSubCat)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prods = productos.Where(x => x.SubCategoriaId == idSubCat).ToList();
            return prods;
        }
        public async Task<List<Producto>> FilterArticulo(string articulo)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prods = productos.Where(x => x.Articulo == articulo).ToList();
            return prods;
        }

        //Validacion de Entradas
        public async Task<Orden_Compra> OrdenU(int id)
        {
            var productos = await camplogicService.GetOrdenesCompraAsync();
            var prod = productos.FirstOrDefault(x => x.Id == id && x.Aprobado == false);
            return prod;
        }

        public async Task<bool> ValidarContenidoOrden(int IdOrden, int ProdId)
        {   
            var pedidos = await camplogicService.GetPedidosAsync();
            var ped = pedidos.FirstOrDefault(x => x.OrdenCompraId == IdOrden && x.ProductoId == ProdId);
            if (ped ==  null)
            {
                Esta = false;
            }
            else
            {
                Esta = true;
            }

            return Esta;
        }
        
        // Validaciones Reintegro
        public async Task<Producto> ProductoF(int id)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prod = productos.FirstOrDefault(x => x.Id == id);
            return prod;
        }
        public async Task<Producto> ProductoCategoriaReintegro(string categoria, string articulo)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prod = productos.FirstOrDefault(x => x.SubCategoria.Categoria.Nombre == categoria && x.Articulo == articulo);
            return prod;
        }
        public async Task<Producto> ProductoSubCategoriaReintegro(string subcategoria, string articulo)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prod = productos.FirstOrDefault(x => x.SubCategoria.Nombre == subcategoria && x.Articulo == articulo);
            return prod;
        }
        public async Task<Producto> ProductoCategoriaDesReintegro(string categoria, string articulo, string descripcion)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prod = productos.FirstOrDefault(x => x.SubCategoria.Categoria.Nombre == categoria && x.Articulo == articulo && x.Descripcion == descripcion);
            return prod;
        }
        public async Task<Producto> ProductoSubCategoriaDesReintegro(string subcategoria, string articulo, string descripcion)
        {
            var productos = await camplogicService.GetProductosAsync();
            var prod = productos.FirstOrDefault(x => x.SubCategoria.Nombre == subcategoria && x.Articulo == articulo && x.Descripcion == descripcion);
            return prod;
        }

        

        public Command RemoveReintegro
        {
            get
            {
                return new Command<ReintegroProducto>((RI) =>
                {
                    _reintegrosList.Remove(RI);
                });
            }
        }

        public Command RemoveEntrada
        {
            get
            {
                return new Command<EntradaProducto>((EP) =>
                {
                    _entradaList.Remove(EP);
                });
            }
        }

        



        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
