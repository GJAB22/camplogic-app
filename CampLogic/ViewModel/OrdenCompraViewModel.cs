﻿using CampLogic.Helpers;
using CampLogic.Model;
using CampLogic.Services;
using Plugin.Messaging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CampLogic.ViewModel
{
    public class OrdenCompraViewModel : INotifyPropertyChanged
    {
        CampLogicService camplogicService = new CampLogicService();

        private List<Orden_Compra> _ordencompra { get; set; }
        public List<Orden_Compra> OrdenCompras
        {
            get { return _ordencompra; }
            set
            {
                _ordencompra = value;
                OnPropertyChanged();
            }
        }

        private List<Orden_Compra> _toporden { get; set; }
        public List<Orden_Compra> TopOrdenes
        {
            get { return _toporden; }
            set
            {
                _toporden = value;
                OnPropertyChanged();
            }
        }

        private List<Pedidos> _pedidos { get; set; }
        public List<Pedidos> PedidosList
        {
            get { return _pedidos; }
            set
            {
                _pedidos = value;
                OnPropertyChanged();
            }
        }



        private List<Cesta> _cesta { get; set; }
        public List<Cesta> Cesta
        {
            get { return _cesta; }
            set
            {
                _cesta = value;
                OnPropertyChanged();
            }
        }

        private bool _isfull { get; set; }
        public bool IsFull
        {
            get { return _isfull; }
            set
            {
                _isfull = value;
                OnPropertyChanged();
            }
        }



        //Producto
        public string ProdId { get; set; }
        public string DesProd { get; set; }
        public string TipProd { get; set; }

        //Proveedor
        public int ProvId { get; set; }
        public string ProvCorreo { get; set; }

        

        //OrdenCompra
        private int _cantidadOrden { get; set; }
        public int CantidadOrden
        {
            get { return _cantidadOrden; }
            set
            {
                _cantidadOrden = value;
                OnPropertyChanged();
            }
        }

        private float _montoTotal { get; set; }
        public float MontoTotal
        {
            get
            {
                return _montoTotal;
            }
            set
            {
                _montoTotal = value;
                OnPropertyChanged();
            }
        }

        //Remover Item de La Cesta
        public Command<Cesta> RemoverItem
        {
            get
            {
                return new Command<Cesta>((producto) =>
                {
                    var remove = camplogicService.DeleteCestaAsync(producto.Id, producto);
                });
            }
        }

        private List<String> Items { get; set; }
        
        private int Cantotal { get; set; }
        private float Montotal { get; set; }

        public ICommand RegistrarOrden
        {
            get
            {
                return new Command(async() =>
                {
                    //Empleado
                    var CedulaEmp = Settings.Cedula;
                    var empleado = await camplogicService.GetEmpleadoCedulaAsync(CedulaEmp);

                    var proveedoresList = await camplogicService.GetProveedoresAsync();
                    
                    var emailMessenger = CrossMessaging.Current.EmailMessenger;
                    if (emailMessenger.CanSendEmail)
                    {
                        // Send simple e-mail to single receiver without attachments, bcc, cc etc.
                        //emailMessenger.SendEmail(ProvCorreo, 
                        //    "Orden de Compra", 
                        //    "Id: " + ordenCompra.Id + "\nDescripcion del Producto: " + DesProd + "\nCantidad: " + CantidadOrden + "\nCosto: ");

                        // Alternatively use EmailBuilder fluent interface to construct more complex e-mail with multiple recipients, bcc, attachments etc. 

                        foreach (var prov in proveedoresList)
                        {
                            var cestaList = Cesta.Where(prod => prod.Producto.ProveedorId == prov.Id).ToList();
                            if (cestaList != null)
                            {
                                Cantotal = 0;
                                Montotal = 0;
                                foreach (var prod in cestaList)
                                {
                                    Cantotal = Cantotal + prod.Cantidad;
                                    Montotal = Montotal + (prod.Cantidad * prod.Precio);
                                }

                                //Orden Compra
                                var NuevaOrden = new Orden_Compra
                                {
                                    Fecha_Orden = DateTime.Now,
                                    Costo_Total = Montotal,
                                    Cantidad_Orden = Cantotal,
                                    EmpleadoId = empleado.Id,
                                    ProveedorId = prov.Id,
                                    Aprobado = false
                                };

                                var respon = await camplogicService.PostOrdenCompraAsync(NuevaOrden);

                                var ordencompraList = await camplogicService.GetOrdenesCompraAsync();

                                var ordenCompra = ordencompraList.LastOrDefault();

                                foreach (var prod in cestaList)
                                {
                                    var pedido = new Pedidos()
                                    {
                                        Cantidad = prod.Cantidad,
                                        Precio = prod.Precio,
                                        EmpleadoId = prod.EmpleadoId,
                                        ProductoId = prod.ProductoId,
                                        OrdenCompraId = ordenCompra.Id
                                    };

                                    var rPedido = await camplogicService.PostPedidoAsync(pedido);
                                }

                                var pedidosList = await camplogicService.GetPedidosAsync();

                                var pedidosFiltrado = pedidosList.Where(b => b.OrdenCompraId == ordenCompra.Id).ToList();

                                var itemsOrder = string.Join("", ItemsOrdenar(pedidosFiltrado));

                                var email = new EmailMessageBuilder()
                                  .To(prov.Correo)
                                  .Cc(empleado.Correo)
                                  //  .Bcc(new[] { "bcc1.plugins@xamarin.com", "bcc2.plugins@xamarin.com" })
                                  .Subject("Orden de Compra #" + ordenCompra.Id)
                                  .Body("Resumen de la Orden: \n\n" +
                                  "Numero de la Orden: " + ordenCompra.Id +"-"+ordenCompra.Fecha_Orden.ToString("ddMMyyyy")+
                                  "\nFecha de Emision: " + ordenCompra.Fecha_Orden +
                                  "\n\nInformacion de la Orden: \n" +
                                  "\nNombre: " + empleado.Nombre + " " + empleado.Apellido +
                                  "\nCorreo: " + empleado.Correo +
                                  "\n\nItems \n" +
                                  itemsOrder
                                  + "\n\n")
                                  .Build();

                                emailMessenger.SendEmail(email);

                            }
                        }
                        
                    }

                    //vaciar cesta
                    foreach (var prod in _cesta)
                    {
                        var re = await camplogicService.DeleteCestaAsync(prod.Id, prod); 
                    }

                });
            }
        }

        public OrdenCompraViewModel()
        {

            InitializeDataAsync();
        }

        public async void InitializeDataAsync()
        {
            var ordenC = await camplogicService.GetOrdenesCompraAsync();

            OrdenCompras = ordenC.Where(x => x.Aprobado == false).Reverse().ToList();
        }

        public OrdenCompraViewModel(DateTime fechaI, DateTime fechaF, string TOC , string filtrodos)
        {

            InitializeDataAsync(fechaI, fechaF, TOC, filtrodos);
        }

        public async void InitializeDataAsync(DateTime fechaI, DateTime fechaF, string TOC, string filtrodos)
        {
            var ordenC = await camplogicService.GetOrdenesCompraAsync();
            if (TOC == "Todas" || TOC == "NT")
            {
                if (filtrodos == "Todos" || filtrodos == "NT")
                {
                    OrdenCompras = ordenC.Where(b => b.Fecha_Orden.Date >= fechaI.Date && b.Fecha_Orden.Date <= fechaF.Date).Reverse().ToList();
                }
                else
                {
                    OrdenCompras = ordenC.Where(b => b.Fecha_Orden.Date >= fechaI.Date && b.Fecha_Orden.Date <= fechaF.Date && b.Proveedor.Descripcion == filtrodos).Reverse().ToList();
                }

            }
            else
            {
                if (filtrodos == "Todos" || filtrodos == "NT")
                {
                    OrdenCompras = ordenC.Where(b => b.Fecha_Orden.Date >= fechaI.Date && b.Fecha_Orden.Date <= fechaF.Date && b.Aprobado == false).Reverse().ToList();
                }
                else
                {
                    OrdenCompras = ordenC.Where(b => b.Fecha_Orden.Date >= fechaI.Date && b.Fecha_Orden.Date <= fechaF.Date && b.Aprobado == false && b.Proveedor.Descripcion == filtrodos).Reverse().ToList();
                }

            }

            if (OrdenCompras.Count() == 0)
            {
                IsFull = true;
            }
            else
            {
                IsFull = false;
            }

            
        }

        public OrdenCompraViewModel(string Id)
        {

            InitializeDataAsync(Id);
        }

        public async void InitializeDataAsync(string Id)
        {
            var cesta = await camplogicService.GetCestaAsync();

            Cesta = cesta.ToList();

            foreach (var prod in Cesta)
            {
                MontoTotal = MontoTotal + (prod.Cantidad * prod.Precio);
                CantidadOrden = CantidadOrden + prod.Cantidad;
            }
        }

        public OrdenCompraViewModel(Orden_Compra orden)
        {

            InitializeDataAsync(orden);
        }

        public async void InitializeDataAsync(Orden_Compra orden)
        {
            var pedidos = await camplogicService.GetPedidosAsync();

            PedidosList = pedidos.Where(b => b.OrdenCompraId == orden.Id).ToList();
        }



        public List<String> ItemsOrdenar(List<Pedidos> pedidos)
        {
            var peList = pedidos;

            Items = new List<string>();

            foreach (var p in peList)
            {
                var peprod = "\n "+p.Producto.Descripcion + "           Cant: " + p.Cantidad + "           Precio: " + p.Precio + "(c/u)    \n";
                Items.Add(peprod);
                
            }

            return Items;
        }



        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
