﻿using CampLogic.Model;
using CampLogic.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CampLogic.ViewModel
{
    class PatrullaViewModel : INotifyPropertyChanged
    {
        CampLogicService camplogicService = new CampLogicService();

        private ObservableCollection<Patrulla> _patrulla;
        public ObservableCollection<Patrulla> Patrullas
        {
            get { return _patrulla; }
            set
            {
                _patrulla = value;
                OnPropertyChanged();
            }
        }

        public string Nombre { get; set; }
        public int EMin { get; set; }
        public int EMax { get; set; }

        public ICommand AgregarPatrulla
        {
            get
            {
                return new Command<object[]>(async(NP) =>
                {
                    Nombre = (string)NP[0];
                    EMin = (int)NP[1];
                    EMax = (int)NP[2];


                    var patrulla = new Patrulla
                    {
                        Nombre = Nombre,
                        EdadMinima = EMin,
                        EdadMaxima = EMax
                    };

                    _patrulla.Add(patrulla);
                    await camplogicService.PostPatrullaAsync(patrulla);
                });
            }
        }

        bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        Command _refreshCommand;
        public Command RefreshCommand
        {
            get { return _refreshCommand; }
        }
        

        

        public PatrullaViewModel()
        {
            InitializeDataAsync();
            _refreshCommand = new Command(RefreshPatrullas);
        }

        private async void RefreshPatrullas()
        {
            IsBusy = true;
            Patrullas = await camplogicService.GetPatrullasAsync();
            IsBusy = false;
        }

        public async void InitializeDataAsync()
        {
            Patrullas = await camplogicService.GetPatrullasAsync();
        }

        public async Task<ObservableCollection<Patrulla>> PatrullasList()
        {
            Patrullas = await camplogicService.GetPatrullasAsync();
            return Patrullas;
        }

        public async Task<ObservableCollection<Patrulla>> SearchPatrulla(string keyword)
        {
            var patrullas = await camplogicService.GetPatrullasAsync();
            return Patrullas = new ObservableCollection<Patrulla>(patrullas.Where(x => x.Nombre.ToLower().Contains(keyword.ToLower())));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
