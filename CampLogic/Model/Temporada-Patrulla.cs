﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class Temporada_Patrulla
    {
        public int Id { get; set; }
        public int CantidadCampistas { get; set; }

        //Temporada
        public int TemporadaId { get; set; }
        public Temporadas Temporada { get; set; }
        //Patrulla
        public int PatrullaId { get; set; }
        public Patrulla Patrulla { get; set; }
    }
}
