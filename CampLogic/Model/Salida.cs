﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class Salida
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public int Cantidad { get; set; }

        //Empleado
        //Foreign Key
        public int EmpleadoId { get; set; }
        //Navigation Property
        public Empleado Empleado { get; set; }

        //Producto
        //Foreign Key
        //public int ProductoId { get; set; }
        //Navigation Property
        //public Producto Producto { get; set; }

        //Entrada
        //Foreign Key
        //public int EntradaId { get; set; }
        //Navigation Property
        //public Entrada Entrada { get; set; }

    }
}
