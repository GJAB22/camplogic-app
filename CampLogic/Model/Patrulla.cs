﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class Patrulla
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int EdadMinima { get; set; }
        public int EdadMaxima { get; set; }
    }
}
