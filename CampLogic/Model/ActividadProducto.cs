﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class ActividadProducto
    {
        public int Id { get; set; }
        public int Cantidad { get; set; }
        public bool EsIndividual { get; set; }

        //Producto
        public int ProductoId { get; set; }
        public Producto Producto { get; set; }

        //Actividad
        public int ActividadId { get; set; }
        public Actividades Actividad { get; set; }
    }
}
