﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class Noticias
    {
        public int Id { get; set; }
        public string Noticia { get; set; }
        public DateTime Fecha_Noticia { get; set; }

        //Empleado
        //Foreign Key
        public int EmpleadoId { get; set; }
        //Navigation Property
        public Empleado Empleado { get; set; }
    }
}
