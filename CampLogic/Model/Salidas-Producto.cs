﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class Salidas_Producto
    {
        public int Id { get; set; }
        public int Cantidad { get; set; }
        public bool SalidaPuntual { get; set; }
        public bool EstaPendiente { get; set; }

        public int ProductoId { get; set; }
        public Producto Producto { get; set; }

        public int SalidaId { get; set; }
        public Salida Salida { get; set; }
    }
}
