﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class Orden_Compra
    {
        public int Id { get; set; }
        public DateTime Fecha_Orden { get; set; }
        public float Costo_Total { get; set; }
        public bool Aprobado { get; set; }
        public int Cantidad_Orden { get; set; }

        //Empleado
        //Foreign Key
        public int EmpleadoId { get; set; }
        //Navigation Property
        public Empleado Empleado { get; set; }

        //Proveedor
        public int ProveedorId { get; set; }
        public Proveedor Proveedor { get; set; }




    }
}
