﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class Temporadas
    {
        public int Id { get; set; }
        public DateTime FechaInicial { get; set; }
        public DateTime FechaFinal { get; set; }
        public int CantidadCampistas { get; set; }
        public string Epoca { get; set; }
        public int NumeroTemporada { get; set; }
    }
}
