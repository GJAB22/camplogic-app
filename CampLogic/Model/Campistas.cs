﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
     public class Campistas
     {
        public int Id { get; set; }
        public int Cedula { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public DateTime Fecha_Nacimiento { get; set; }
        public string Descripcion { get; set; }
        public bool Inscrito { get; set; }
        public bool Asistencia { get; set; }
        public bool Transporte { get; set; }
        public string NombrePatrulla { get; set; }


    }
}
