﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.Model
{
    public class ReintegroProducto
    {
        public int Id { get; set; }
        public int Cantidad { get; set; }
        public int Faltante { get; set; }
        public bool PorDirector { get; set; }

        //Producto
        public int ProductoId { get; set; }
        public Producto Producto { get; set; }

        //Reintegro
        public int ReintegroId { get; set; }
        public Reintegros Reintegro { get; set; }

        
    }
}
