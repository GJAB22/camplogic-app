﻿using CampLogic.Helpers;
using CampLogic.View;
using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CampLogic
{
    public partial class MasterHS : ContentPage
    {
        public ListView ListView { get { return listView; } }

        public MasterHS()
        {
            InitializeComponent();
            BindingContext = new PerfilViewModel();
            NombreUsuario.Text = Settings.Username;
            CargoU.Text = Settings.Cargo;
            MDC.BackgroundColor = Color.FromRgba(225,255,255,0.7);
            var masterPageItems = new List<MasterPageItem>();

            masterPageItems.Add(new MasterPageItem()
            {
                Title = "Inicio",
                IconSource = "camplogic.png",
                TargetType = new Detail()
                
                
            });
            masterPageItems.Add(new MasterPageItem()
            {
                Title = "Mi Perfil",
                IconSource = "perfil.png",
                TargetType = new PerfilPage()
            });
            masterPageItems.Add(new MasterPageItem()
            {
                Title = "Personal",
                IconSource = "guia.png",
                TargetType = new GuiaPage()
            });
            masterPageItems.Add(new MasterPageItem()
            {
                Title = "Campistas",
                IconSource = "participante.png",
                TargetType = new CampistaPage()
            });
            if (Settings.Cargo != "Inventario")
            {
                masterPageItems.Add(new MasterPageItem()
                {
                    Title = "Actividades",
                    IconSource = "actividades.png",
                    TargetType = new ActividadesPage()
                });
            }
            if (Settings.Cargo != "Inventario" && Settings.Cargo != "Guía")
            {
                masterPageItems.Add(new MasterPageItem()
                {
                    Title = "Patrullas",
                    IconSource = "patrulla.png",
                    TargetType = new PatrullaPage()
                });
            }

            if (Settings.Cargo != "Guía")
            {
                masterPageItems.Add(new MasterPageItem()
                {
                    Title = "Almacén",
                    IconSource = "inventario.png",
                    TargetType = new InventarioPage()
                });
            }
            if (Settings.Cargo != "Guía" && Settings.Cargo != "Inventario")
            {
                masterPageItems.Add(new MasterPageItem()
                {
                    Title = "Temporadas",
                    IconSource = "temporada.png",
                    TargetType = new TemporadasPage()
                });
            }
            if (Settings.Cargo == "Guía")
            {
                masterPageItems.Add(new MasterPageItem()
                {
                    Title = "Mi Patrulla",
                    IconSource = "patrulla.png",
                    TargetType = new MiPatrullaPage() { BarTextColor = Color.White }
                });
            }
            

            listView.ItemsSource = masterPageItems;

        }
    }
}
