﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CampLogic
{
    public partial class MainPage : MasterDetailPage
    {
        MasterHS masterPage;
        
        public MainPage()
        {
            InitializeComponent();
            masterPage = new MasterHS();
            Master = masterPage;
            Detail = new NavigationPage(new Detail()) {
                BarBackgroundColor = Color.FromHex("#ff5722"),
                BarTextColor = Color.White
            };
             
            masterPage.ListView.ItemSelected += OnItemSelected;

            


        }

        public void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = (MasterPageItem)e.SelectedItem;
            if (item != null)
            {
                Detail = new NavigationPage(item.TargetType) {
                    BarBackgroundColor = Color.FromHex("#ff5722"),
                    BarTextColor = Color.White
                };
                
                masterPage.ListView.SelectedItem = null;
                IsPresented = false;
            }
            
            

        }
    }
}