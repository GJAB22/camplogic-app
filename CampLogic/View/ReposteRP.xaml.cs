﻿using CampLogic.Model;
using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ReposteRP : ContentPage
	{
		public ReposteRP ()
		{
			InitializeComponent ();
            BindingContext = new ProductosViewModel(1);
            FechaReporte.Text = DateTime.Today.ToString("dd/MM/yyyy");
		}

        private async void CategoriaPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private async void SubCategoriaPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var subcategoria = (SubCategoria)SubCategoriaPicker.SelectedItem;
            var vm = new ProductosViewModel();
            if (subcategoria != null)
            {
                ProductosListView.ItemsSource = await vm.Filter(subcategoria.Id);
            }
        }

        private async void SearchProductos_TextChanged(object sender, TextChangedEventArgs e)
        {
            var keyword = SearchProductos.Text;
            var vm = new ProductosViewModel();

            ProductosListView.ItemsSource = await vm.SearchProducto(keyword);
        }
    }
}