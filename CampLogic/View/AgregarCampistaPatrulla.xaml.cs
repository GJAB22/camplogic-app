﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AgregarCampistaPatrulla : PopupPage
    {
        public Patrulla PatAsig { get; set; }
        public DateTime FAsig { get; set; }
        public object BCont2 { get; set; }
        public AgregarCampistaPatrulla(object BC, Patrulla patrulla, DateTime FA, object BC2)
        {
            InitializeComponent();
            BindingContext = BC2;
            PatAsig = patrulla;
            FAsig = FA;
            BCont2 = BC;
            
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // Method for animation child in PopupPage
        // Invoced after custom animation end
        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        protected override bool OnBackButtonPressed()
        {
            // Prevent hide popup
            //return base.OnBackButtonPressed();
            return true;
        }

        // Invoced when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return default value - CloseWhenBackgroundIsClicked
            return base.OnBackgroundClicked();
        }


        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }

        private async void AgregarCampistas_Clicked(object sender, EventArgs e)
        {
            var vm = BindingContext as TemporadaViewModel;
            object[] m = new object[]
            {
                PatAsig,
                FAsig
            };
            vm.AgregarCampistas.Execute(m);
            vm.ELCampistas.Execute(1);
            await DisplayAlert("Exito", "El o Los Campistas se han agregado Exitosamente", "OK");
            await Navigation.PopAllPopupAsync();
        }
    }
}