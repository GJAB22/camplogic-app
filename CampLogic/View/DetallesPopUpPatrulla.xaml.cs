﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetallesPopUpPatrulla : PopupPage
    {
        public Patrulla PatAsign { get; set; }
        public DateTime Fasig { get; set; }
        public object BCont { get; set; }
        public DetallesPopUpPatrulla(Patrulla patrulla , DateTime FA, object BC)
        {
            InitializeComponent();
            PatrullaName.Text = patrulla.Nombre;
            PatAsign = patrulla;
            Fasig = FA;
            BCont = BC;
            BindingContext = new TemporadaViewModel(patrulla,FA);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // Method for animation child in PopupPage
        // Invoced after custom animation end
        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        protected override bool OnBackButtonPressed()
        {
            // Prevent hide popup
            //return base.OnBackButtonPressed();
            return true;
        }

        // Invoced when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return default value - CloseWhenBackgroundIsClicked
            return base.OnBackgroundClicked();
        }


        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }

        private void CampistasListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            CampistasListView.SelectedItem = null;
        }

        private void GuiaListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            GuiaListView.SelectedItem = null;
        }

        private async void AgregarGuias_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushPopupAsync(new AgregarGuiaPatrulla(BindingContext, PatAsign, Fasig, BCont));
        }

        private async void AgregarCampistas_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushPopupAsync(new AgregarCampistaPatrulla(BindingContext, PatAsign, Fasig, BCont));
        }

        private void EliminarGuia_Clicked(object sender, EventArgs e)
        {
            var button = sender as Button;
            var guia = button?.BindingContext as PatrullaEmpleado;
            var vm = BindingContext as TemporadaViewModel;
            var vm2 = BCont as TemporadaViewModel;
            vm?.RemoveGuias.Execute(guia);
            vm2?.AgDenuevoGuias.Execute(guia);
        }

        private void EliminarCampista_Clicked(object sender, EventArgs e)
        {
            var button = sender as Button;
            var campista = button?.BindingContext as PatrullaCampista;
            var vm = BindingContext as TemporadaViewModel;
            var vm2 = BCont as TemporadaViewModel;
            vm?.RemoveCampistas.Execute(campista);
            vm2?.AgDenuevoCampistas.Execute(campista);
        }
    }
}