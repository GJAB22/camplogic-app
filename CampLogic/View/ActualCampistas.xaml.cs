﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActualCampistas : ContentPage
    {
        public Temporadas UT { get; set; }
        public ActualCampistas(Temporadas ut)
        {
            InitializeComponent();
            CampistaC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            CInscritos.Text = ut.CantidadCampistas.ToString();
            UT = ut;
            BindingContext = new TemporadaViewModel(ut);
        }

        private async void CampistasListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var campista = (PatrullaCampista)CampistasListView.SelectedItem;
            if ( campista != null)
            {
                await Navigation.PushPopupAsync(new PerfilCampistaPage(campista.Campista));
            }
            CampistasListView.SelectedItem = null;
        }

        private void SearchCampista_TextChanged(object sender, TextChangedEventArgs e)
        {
            var keyword = SearchCampista.Text;
            var patrulla = (string)PorPatrulla.SelectedItem;
            var vm = BindingContext as TemporadaViewModel;
            if (((string)PorPatrulla.SelectedItem == null || (string)PorPatrulla.SelectedItem == "Todas") && ((string)PorTrasporte.SelectedItem == null || (string)PorTrasporte.SelectedItem == "Todos"))
            {
                CampistasListView.ItemsSource = vm.SearchNombreCampistas(keyword);
            }
            else if ((string)PorPatrulla.SelectedItem == null || (string)PorPatrulla.SelectedItem == "Todas")
            {
                CampistasListView.ItemsSource = vm.SearchCampistasTrans(keyword);
            }
            else if ((string)PorTrasporte.SelectedItem == null || (string)PorTrasporte.SelectedItem == "Todos")
            {
                CampistasListView.ItemsSource = vm.SearchCampistasPat(keyword,patrulla);
            }
            else
            {
                CampistasListView.ItemsSource = vm.SearchCampistasPatTrans(keyword, patrulla);
            }
        }

        private void CampRefresh_Activated(object sender, EventArgs e)
        {
            BindingContext = new TemporadaViewModel(UT); ;
        }

        private void PorPatrulla_SelectedIndexChanged(object sender, EventArgs e)
        {
            var vm = BindingContext as TemporadaViewModel;
            if ((string)PorPatrulla.SelectedItem == "Todas")
            {
                CampistasListView.ItemsSource = vm.CampistasList();
            }
            else
            {
                CampistasListView.ItemsSource = vm.FiltrarPorPatrulla((string)PorPatrulla.SelectedItem);
            }

            PorTrasporte.SelectedItem = null;
        }

        private void PorTrasporte_SelectedIndexChanged(object sender, EventArgs e)
        {
            var vm = BindingContext as TemporadaViewModel;
            if (((string)PorTrasporte.SelectedItem == "Todos" || (string)PorTrasporte.SelectedItem == null) && ((string)PorPatrulla.SelectedItem == "Todas" || (string)PorPatrulla.SelectedItem == null))
            {
                CampistasListView.ItemsSource = vm.CampistasList();
            }
            else if ((string)PorTrasporte.SelectedItem == "Todos" || (string)PorTrasporte.SelectedItem == null)
            {
                CampistasListView.ItemsSource = vm.FiltrarPorPatrulla((string)PorPatrulla.SelectedItem);
            }
            else if ((string)PorPatrulla.SelectedItem == "Todas" || (string)PorPatrulla.SelectedItem == null)
            {
                CampistasListView.ItemsSource = vm.FiltrarPorTransporte((string)PorTrasporte.SelectedItem);
            }
            else
            {
                CampistasListView.ItemsSource = vm.FiltrarPorPatTrans((string)PorPatrulla.SelectedItem);
            }

           
        }
    }
}