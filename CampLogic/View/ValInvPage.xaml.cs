﻿using CampLogic.HelpClasses;
using CampLogic.Model;
using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ValInvPage : ContentPage
	{
        public DateTime FechaInicial { get { return ComTemp.Date; } }
        public DateTime FechaFinal { get { return FinTemp.Date; } }

        public ValInvPage ()
		{
			InitializeComponent ();

            BindingContext = new ValoracionViewModel();

            ResultadosValoracion.IsVisible = false;
		}

        private void MostrarValoracion_Clicked(object sender, EventArgs e)
        {
            BindingContext = new ValoracionViewModel(ComTemp.Date, FinTemp.Date);
            fInicio.Text = ComTemp.Date.ToString("dd/MM/yyyy");
            fFin.Text = FinTemp.Date.ToString("dd/MM/yyyy");
            FiltroValoracion.IsVisible = false;
            ResultadosValoracion.IsVisible = true;
        }

        private async void ProdListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var producto = (ValoracionProducto)e.SelectedItem;
            if (producto != null)
            {
                await Navigation.PushAsync(new DetallesValoracionPage(producto, FechaInicial, FechaFinal));
            }

            ProdListView.SelectedItem = null;
        }
    }
}