﻿using CampLogic.Model;
using CampLogic.View;
using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HorarioPage : ContentPage
    {
        public Patrulla PatrullaAsignada { get; set; }

        public HorarioPage()
        {
            InitializeComponent();
            HorarioSL.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            InitializeGrid(GridActividades1er, GridActividades2do, GridActividades3er);
        }

        public async void InitializeGrid(Grid GridAct1er, Grid GridAct2do, Grid GridAct3er)
        {
            var vm = new HorarioViewModel();

            var HG = await vm.InitializeHorario();

            
            var A1B = (List<Patrulla_Actividad>)HG[0];
            var A2B = (List<Patrulla_Actividad>)HG[1];
            var A3B = (List<Patrulla_Actividad>)HG[2];

            
            int aux1BC = 0;
            
            int aux2BC = 0;
            
            int aux3BC = 0;


            //Inicializar El Grid de Actividades
            GridAct1er.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            GridAct2do.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            GridAct3er.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

            // Bloque 1 de Actividades
            if (A1B != null)
            {
                foreach (var act in A1B)
                {
                    GridAct1er.Children.Add(new Label() { Text = act.Actividad.Nombre, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold }, aux1BC, 0);
                    aux1BC = aux1BC + 1;
                }
            }

            // Bloque 2 de Actividades
            if (A2B != null)
            {
                foreach (var act in A2B)
                {
                    GridAct2do.Children.Add(new Label() { Text = act.Actividad.Nombre, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold }, aux2BC, 0);
                    aux2BC = aux2BC + 1;
                }
            }

            // Bloque 3 de Actividades
            if (A3B != null)
            {
                foreach (var act in A3B)
                {
                    GridAct3er.Children.Add(new Label() { Text = act.Actividad.Nombre, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold }, aux3BC, 0);
                    aux3BC = aux3BC + 1;
                }
            }

            
            

        }


    }
}
