﻿using CampLogic.Model;
using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActualHorario : ContentPage
    {
        public ActualHorario(Temporadas temp)
        {
            InitializeComponent();
            BindingContext = new TemporadaViewModel();
            HorarioSL.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            InitializeGrid(Grid1er, Grid2do, Grid3er, GridActividades1er, GridActividades2do, GridActividades3er, temp);

        }

        public async void InitializeGrid(Grid grid1, Grid grid2, Grid grid3, Grid GridAct1er, Grid GridAct2do, Grid GridAct3er, Temporadas temp)
        {
            var vm = new TemporadaViewModel();

            var HG = await vm.InitializeHorario(temp);

            var patrullasList = (List<Temporada_Patrulla>)HG[0];
            var A1B = (List<List<Patrulla_Actividad>>)HG[1];
            var A2B = (List<List<Patrulla_Actividad>>)HG[2];
            var A3B = (List<List<Patrulla_Actividad>>)HG[3];

            int aux = 0;
            int aux2 = 0;
            int aux1BR = 0;
            int aux1BC = 0;
            int aux2BR = 0;
            int aux2BC = 0;
            int aux3BR = 0;
            int aux3BC = 0;

            //Inicializar El Grid de Patrullas
            foreach (var patrulla in patrullasList)
            {
                grid1.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

                grid2.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

                grid3.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

                aux2 = aux2 + 1;
               
            }

            grid1.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            grid2.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            grid3.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });

            foreach (var patrulla in patrullasList)
            {

                grid1.Children.Add(new Label() { Text = patrulla.Patrulla.Nombre, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold }, 0, aux);

                grid2.Children.Add(new Label() { Text = patrulla.Patrulla.Nombre, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold }, 0, aux);

                grid3.Children.Add(new Label() { Text = patrulla.Patrulla.Nombre, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold }, 0, aux);

                aux = aux + 1;

            }

            //Inicializar El Grid de Actividades
            for (int i = 0; i < aux2; i++)
            {
                GridAct1er.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
                GridAct2do.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
                GridAct3er.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            }

            // Bloque 1 de Actividades
            foreach (var actList in A1B)
            {
                aux1BC = 0;
                foreach (var act in actList)
                {
                    GridAct1er.Children.Add(new Label() { Text = act.Actividad.Nombre, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold }, aux1BC, aux1BR);
                    aux1BC = aux1BC + 1;
                }

                aux1BR = aux1BR +1;
            }

            // Bloque 2 de Actividades
            foreach (var actList in A2B)
            {
                aux2BC = 0;
                foreach (var act in actList)
                {
                    GridAct2do.Children.Add(new Label() { Text = act.Actividad.Nombre, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold }, aux2BC, aux2BR);
                    aux2BC = aux2BC + 1;
                }

                aux2BR = aux2BR + 1;
            }

            // Bloque 3 de Actividades
            foreach (var actList in A3B)
            {
                aux3BC = 0;
                foreach (var act in actList)
                {
                    GridAct3er.Children.Add(new Label() { Text = act.Actividad.Nombre, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold }, aux3BC, aux3BR);
                    aux3BC = aux3BC + 1;
                }

                aux3BR = aux3BR + 1;
            }

        }
    }
}