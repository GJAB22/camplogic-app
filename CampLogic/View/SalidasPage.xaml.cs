﻿using CampLogic.Model;
using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SalidasPage : ContentPage
    {
        public SalidasPage(DateTime FI,DateTime FF,string categoria,string subcategoria,string articulo,string ubicacion)
        {
            InitializeComponent();

            BindingContext = new SalidasViewModel(FI, FF, categoria, subcategoria, articulo, ubicacion);
            FInicial.Text = FI.Date.ToString("dd/MM/yyyy");
            FFinal.Text = FF.Date.ToString("dd/MM/yyyy");
            RepSalidasC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
        }

        private void RepSalidasList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            RepSalidasList.SelectedItem = null;
        }
    }
}
