﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopUpReIntegros : PopupPage
    {
        private object BContext { get { return BindingContext; } }
        public PopUpReIntegros(object BC)
        {
            InitializeComponent();
            BindingContext = BC;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // Method for animation child in PopupPage
        // Invoced after custom animation end
        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        protected override bool OnBackButtonPressed()
        {
            // Prevent hide popup
            //return base.OnBackButtonPressed();
            return true;
        }

        // Invoced when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return default value - CloseWhenBackgroundIsClicked
            return base.OnBackgroundClicked();
        }

        private async void AgregarProducto_Clicked(object sender, EventArgs e)
        {
            var vm = (EntradasViewModel)BContext;
            
            if (ProdID.Text == null || ProdID.Text == "")
            {
                string[] DP;
                var categoria = (Categoria)Categoria.SelectedItem;
                var subcategoria = (SubCategoria)SubCategoria.SelectedItem;
                var articulo = (Producto)Articulo.SelectedItem;
                var descripcion = (Producto)Descripcion.SelectedItem;
                if ((subcategoria == null || subcategoria.Nombre == "Ninguna") && (descripcion == null || descripcion.Descripcion == "Ninguna"))
                {
                    var prod = await vm.ProductoCategoriaReintegro(categoria.Nombre, articulo.Articulo);
                    if (prod.CantidadIdeal < Convert.ToInt32(CantEntregada.Text))
                    {
                        await DisplayAlert("Error", "Introduzca una cantidad menor o igual a la ideal de " + prod.Articulo, "Ok");
                    }
                    else
                    {
                        DP = new string[3]{
                            categoria.Nombre,
                            articulo.Articulo,
                            CantEntregada.Text
                        };
                        vm.AgregarProdCategoriaReintegro.Execute(DP);
                        await DisplayAlert("Exito!", "El Producto ya se añadio", "Ok");
                        await Navigation.PopPopupAsync();
                    }
                   
                }
                else if (descripcion == null || descripcion.Descripcion == "Ninguna")
                {
                    var prod = await vm.ProductoSubCategoriaReintegro(subcategoria.Nombre, articulo.Articulo);
                    if (prod.CantidadIdeal < Convert.ToInt32(CantEntregada.Text))
                    {
                        await DisplayAlert("Error", "Introduzca una cantidad menor o igual a la ideal de " + prod.Articulo, "Ok");
                    }
                    else
                    {
                        DP = new string[3]{
                            subcategoria.Nombre,
                            articulo.Articulo,
                            CantEntregada.Text
                        };
                        vm.AgregarProdSubCategoriaReintegro.Execute(DP);
                        await DisplayAlert("Exito!", "El Producto ya se añadio", "Ok");
                        await Navigation.PopPopupAsync();
                    }
                    
                }
                else if (subcategoria == null || subcategoria.Nombre == "Ninguna")
                {
                    var prod = await vm.ProductoCategoriaDesReintegro(categoria.Nombre, articulo.Articulo, descripcion.Descripcion);
                    if (prod.CantidadIdeal < Convert.ToInt32(CantEntregada.Text))
                    {
                        await DisplayAlert("Error", "Introduzca una cantidad menor o igual a la ideal de " + prod.Articulo, "Ok");
                    }
                    else
                    {
                        DP = new string[4]{
                            categoria.Nombre,
                            articulo.Articulo,
                            descripcion.Descripcion,
                            CantEntregada.Text
                        };
                        vm.AgregarProdCategoriaDesReintegro.Execute(DP);
                        await DisplayAlert("Exito!", "El Producto ya se añadio", "Ok");
                        await Navigation.PopPopupAsync();
                    }
                }
                else
                {
                    DP = new string[4]{
                            subcategoria.Nombre,
                            articulo.Articulo,
                            descripcion.Descripcion,
                            CantEntregada.Text
                        };
                    vm.AgregarProdSubCategoriaDesReintegro.Execute(DP);
                    await DisplayAlert("Exito!", "El Producto ya se añadio", "Ok");
                    await Navigation.PopPopupAsync();
                    var prod = await vm.ProductoSubCategoriaDesReintegro(subcategoria.Nombre, articulo.Articulo, descripcion.Descripcion);
                }

                
            }
            else
            {
                var DP = new int[2]
                    {

                    Convert.ToInt32(ProdID.Text),
                    Convert.ToInt32(CantEntregada.Text)
                    };
                vm.AgregarProdIdReintegro.Execute(DP);
                await DisplayAlert("Exito!", "El Producto ya se añadio", "Ok");
                await Navigation.PopPopupAsync();

            }
        }

        private async void Categoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            var categoria = (Categoria)Categoria.SelectedItem;
            var vm = new EntradasViewModel();

            if (categoria != null)
            {
                SubCategoria.ItemsSource = await vm.SearchSubCategoria(categoria.Id);
                SubCategoria.IsEnabled = true;
                Articulo.ItemsSource = await vm.FilterCategoria(categoria.Id);
                Articulo.IsEnabled = true;
            }
        }

        private async void SubCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            var subcategoria = (SubCategoria)SubCategoria.SelectedItem;
            var vm = new EntradasViewModel();

            if (subcategoria != null)
            {
                Articulo.ItemsSource = await vm.FilterSubCategoria(subcategoria.Id);
            }
        }

        private async void Articulo_SelectedIndexChanged(object sender, EventArgs e)
        {
            var articulo = (Producto)Articulo.SelectedItem;
            var vm = new EntradasViewModel();
            if (articulo != null)
            {
                Descripcion.ItemsSource = await vm.FilterArticulo(articulo.Articulo);
                Descripcion.IsEnabled = true;
            }
        }

        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }

        private void CantEntregada_TextChanged(object sender, TextChangedEventArgs e)
        {
            
            if (CantEntregada.Text == null || CantEntregada.Text == "" )
            {
                AgregarProducto.IsEnabled = false;
            }
            else
            {
                AgregarProducto.IsEnabled = true;
            }
        }
    }
}