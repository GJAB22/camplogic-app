﻿using CampLogic.Model;
using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReportePage : ContentPage
    {
        public ReportePage(string categoria, string subcategoria, string ubicacion, string articulo, string filtrar)
        {
            InitializeComponent();
            BindingContext = new InventarioViewModel(categoria, subcategoria,  ubicacion,  articulo, filtrar);
            
            if (filtrar == "Por debajo del Punto Crítico")
            {
                SNB.IsVisible = true;
            }
            RepInvC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            FechaReporte.Text = DateTime.Today.ToString("dd/MM/yyyy");
            
        }

        private void InvRepList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            InvRepList.SelectedItem = null;
        }
    }
}
