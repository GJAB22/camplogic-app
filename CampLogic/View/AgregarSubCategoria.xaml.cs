﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AgregarSubCategoria : PopupPage
    {
        public AgregarSubCategoria()
        {
            InitializeComponent();
            BindingContext = new ProductosViewModel();
        }

        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }


        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }

        private void Categoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            var categoria = (Categoria)Categoria.SelectedItem;
            if (categoria.Nombre == "Todo")
            {
                NombreSubCategoria.IsEnabled = false;
            }
            else
            {
                NombreSubCategoria.IsEnabled = true;
                NuevaSubCategoria.IsEnabled = true;
            }
            
        }

        private async void NuevaSubCategoria_Clicked(object sender, EventArgs e)
        {
            var vm = new ProductosViewModel();

            if (NombreSubCategoria.Text == null || NombreSubCategoria.Text == "")
            {
                await DisplayAlert("Alerta", "Porfavor ingrese un Nombre", "Ok");
            }
            else
            {
                var categoria = (Categoria)Categoria.SelectedItem;
                vm.NuevaSubCategoria(NombreSubCategoria.Text, categoria);
                await DisplayAlert("Exito", "La Nueva Sub-Categoria se ha guardado exitosamente", "Ok");
            }
        }
    }
}