﻿using CampLogic.Model;
using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetallesSalidaPage : ContentPage
    {
        public DetallesSalidaPage(Salida salida)
        {
            InitializeComponent();
            BindingContext = new SalidasViewModel();

            IdSalida.Text = salida.Id.ToString();
            SalidaEmpNombre.Text = salida.Empleado.Nombre;
            SalidaEmpApellido.Text = salida.Empleado.Apellido;
            FSalida.Text = salida.Fecha.ToString("dd/MM/yyyy HH:mm tt");
            Cantidad.Text = salida.Cantidad.ToString();
        }
    }
}
