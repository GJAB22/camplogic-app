﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AgregarTemporada : ContentPage
    {
        public int CantPatrullas { get; set; }
        public Actividades[,] ActPrimer = new Actividades[50,5];
        public Actividades[,] ActSegundo = new Actividades[50,5];
        public Actividades[,] ActTercero = new Actividades[50,5];

        public AgregarTemporada(object BC)
        {
            InitializeComponent();
            PasoNumber.Text = "1";
            PrimeroNTC.BackgroundColor = Color.FromRgba(225,225,225,0.7);
            SegundoNTC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            TercerNTC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            SegundoNT.IsVisible = false;
            TercerNT.IsVisible = false;
            BindingContext = BC;
        }

        // Primer Paso

        private async void CampistasInscritos_Clicked(object sender, EventArgs e)
        {
            var vm = BindingContext as TemporadaViewModel;
            CampistasListView.ItemsSource = await vm.GetCampistasIns();
            CampistaPrimero.Text = (await vm.TotalCampistas()).ToString();
            CSPrimer.Text = (await vm.Campistas56()).ToString();
            SOPrimer.Text = (await vm.Campistas78()).ToString();
            NDPrimer.Text = (await vm.Campistas910()).ToString();
            ODPrimer.Text = (await vm.Campistas1112()).ToString();
            TCPrimer.Text = (await vm.Campistas1314()).ToString();
            QDPrimer.Text = (await vm.Campistas1516()).ToString();
            var campistasIns = await vm.GetCampistasIns();
            if (campistasIns.Count()!=0)
            {
                PasarSegunda.IsEnabled = true;
            }
            else
            {
                await DisplayAlert("Alerta", "Porfavor vaya a la seccion de campistas y coloque cuales estan inscritos para esta Temporada", "OK");
            }
        }

        private async void PasarSegunda_Clicked(object sender, EventArgs e)
        {
            var Comienzo = FI.Date;
            var Fin = FF.Date;

            if (Fin.Day - Comienzo.Day == 4)
            {
                var r = await DisplayAlert("Alerta", "Porfavor Verifique que ingreso las fechas correcta que desea para esta Nueva Temporada, asi como los Campistas Inscritos, de lo contrario seleccione la opcion 'Continuar'", "Continuar","Cancelar");
                if (r == true)
                {
                    PrimeroNT.IsVisible = false;

                    if ((Comienzo.Month == 07 || Comienzo.Month == 08 || Comienzo.Month == 09) && (Fin.Month == 07 || Fin.Month == 08 || Fin.Month == 09))
                    {
                        EpocaSegundo.Text = "Verano";
                    }
                    else
                    {
                        EpocaSegundo.Text = "Invierno";
                    }
                    FISegundo.Text = Comienzo.Date.ToString("dd/MM/yyyy");
                    FFSegundo.Text = Fin.Date.ToString("dd/MM/yyyy");
                    CampistasSegundo.Text = CampistaPrimero.Text;
                    CSSegundo.Text = CSPrimer.Text;
                    SOSegundo.Text = SOPrimer.Text;
                    NDSegundo.Text = NDPrimer.Text;
                    ODSegundo.Text = ODPrimer.Text;
                    TCSegundo.Text = TCPrimer.Text;
                    QDSegundo.Text = QDPrimer.Text;
                    PasoNumber.Text = "2";
                    SegundoNT.IsVisible = true;
                }
            }
            else
            {
                await DisplayAlert("Error", "Porfavor Seleccione dos fechas que tenga 5 dias de Diferencia","OK");
            }
            
        }

        // Segundo Paso

        private void EliminarPatrullaSegundo_Clicked(object sender, EventArgs e)
        {
            var button = sender as Button;
            var patrulla = button?.BindingContext as Patrulla;
            var vm = BindingContext as TemporadaViewModel;
            vm?.RemovePatrulla.Execute(patrulla);
        }

        private async void PatrullaListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var patrulla = (Patrulla)PatrullaListView.SelectedItem;
            if (patrulla != null)
            {
                await Navigation.PushPopupAsync(new DetallesPopUpPatrulla(patrulla, FI.Date,BindingContext));
            }

            PatrullaListView.SelectedItem = null;
        }

        private async void CargarPatrullasSegundo_Clicked(object sender, EventArgs e)
        {
            var vm = BindingContext as TemporadaViewModel;
            PatrullaListView.ItemsSource = await vm.GetPatrullas();

        }

        private async void PasarTercero_Clicked(object sender, EventArgs e)
        {
            var r = await DisplayAlert("Alerta", "Porfavor Verifique que todas las Patrullas contengan sus respectivos Guias y Campistas, si esta seguro porfavor seleccione la opcion 'Continuar'", "Continuar", "Cancelar");
            if (r==true)
            {
                SegundoNT.IsVisible = false;
                EpocaTercera.Text = EpocaSegundo.Text;
                FITercera.Text = FISegundo.Text;
                FFTercera.Text = FFSegundo.Text;
                CampistasTercera.Text = CampistaPrimero.Text;
                PasoNumber.Text = "3";
                InitializeGrid(Grid1er, Grid2do, Grid3er, GridActividades1er, GridActividades2do, GridActividades3er);
                TercerNT.IsVisible = true;
            }
        }

        //Tercer Paso
        private async void FinProceso_Clicked(object sender, EventArgs e)
        {
            var r = await DisplayAlert("Alerta", "Porfavor Verifique que todas las Patrullas tienen Actividades Asignadas en sus respectivos Bloques, si esta seguro porfavor seleccione la opcion 'Continuar'", "Continuar", "Cancelar");
            if (r==true)
            {
                var vm = BindingContext as TemporadaViewModel;
                object[] m = new object[]
                {
                ActPrimer,
                ActSegundo,
                ActTercero,
                FI.Date,
                FF.Date,
                EpocaSegundo.Text,
                Convert.ToInt32(CampistaPrimero.Text),
                Convert.ToInt32(NumeroTemp.Text)
                };
                vm?.AgregarTemporada.Execute(m);
                await Navigation.PopAsync();
            }
            
        }

        public async void InitializeGrid(Grid grid1, Grid grid2, Grid grid3, Grid GridAct1er, Grid GridAct2do, Grid GridAct3er)
        {
            var vm = BindingContext as TemporadaViewModel;
            var patrullasList = vm.GetSelectedPatrullas();
            CantPatrullas = patrullasList.Count;
            var actividades = await vm.GetActividades();
            int aux = 0;
            int aux2 = 0;

            

            foreach (var patrulla in patrullasList)
            {
                grid1.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

                grid2.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

                grid3.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

                aux2 = aux2 + 1;
            }

            grid1.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            grid2.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            grid3.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });

            foreach (var patrulla in patrullasList)
            {

                grid1.Children.Add(new Label() { Text = patrulla.Nombre, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold }, 0, aux);

                grid2.Children.Add(new Label() { Text = patrulla.Nombre, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold }, 0, aux);

                grid3.Children.Add(new Label() { Text = patrulla.Nombre, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold }, 0, aux);

                

                aux = aux + 1;

            }

            for (int i = 0; i < aux2; i++)
            {
                GridAct1er.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
                GridAct2do.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
                GridAct3er.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            }

            // Bloque 1 de Actividades
            for (int i = 0; i < aux2; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    var picker = new Picker() { ItemsSource = actividades, ItemDisplayBinding = new Binding("Nombre") };
                    picker.SelectedIndexChanged += this.PrimerBSelectedIndexChanged;
                    GridAct1er.Children.Add(picker, j, i);
                    
                }
            }

            // Bloque 2 de Actividades
            for (int i = 0; i < aux2; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    var picker = new Picker() { ItemsSource = actividades, ItemDisplayBinding = new Binding("Nombre") };
                    picker.SelectedIndexChanged += this.SegundoBSelectedIndexChanged;
                    GridAct2do.Children.Add(picker, j, i);
                }
            }

            // Bloque 3 de Actividades
            for (int i = 0; i < aux2; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    var picker = new Picker() { ItemsSource = actividades, ItemDisplayBinding = new Binding("Nombre") };
                    picker.SelectedIndexChanged += this.TercerBSelectedIndexChanged;
                    GridAct3er.Children.Add(picker, j, i);
                }
            }

        }

       
        private async void PrimerBSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            var act = (Actividades)picker.SelectedItem;
            if (act == null)
            {
                await DisplayAlert("Alerta", "Porfavor Seleccione las Actividades que desea asignarle a las Patrullas", "OK");
            }
            else
            {
                var row = Grid.GetRow(picker);
                var column = Grid.GetColumn(picker);
                ActPrimer[row, column] = act;
            }
            
        }

        private async void SegundoBSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            var act = (Actividades)picker.SelectedItem;
            if (act == null)
            {
                await DisplayAlert("Alerta", "Porfavor Seleccione las Actividades que desea asignarle a las Patrullas", "OK");
            }
            else
            {
                var row = Grid.GetRow(picker);
                var column = Grid.GetColumn(picker);
                ActSegundo[row, column] = act;
            }
        }

        private async void TercerBSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            var act = (Actividades)picker.SelectedItem;
            if (act == null)
            {
                await DisplayAlert("Alerta", "Porfavor Seleccione las Actividades que desea asignarle a las Patrullas", "OK");
            }
            else
            {
                var row = Grid.GetRow(picker);
                var column = Grid.GetColumn(picker);
                ActTercero[row, column] = act;
            }
        }
    }
}