﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopUpEntradaIngreso : PopupPage
    {
        private string IdOrden { get { return Orden.Text; } }

        private object BContext { get { return BindingContext; } }
        public PopUpEntradaIngreso (string IdOrden, object BC)
		{
			InitializeComponent ();
            BindingContext = BC;
            Orden.Text = IdOrden;
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // Method for animation child in PopupPage
        // Invoced after custom animation end
        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        protected override bool OnBackButtonPressed()
        {
            // Prevent hide popup
            //return base.OnBackButtonPressed();
            return true;
        }

        // Invoced when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return default value - CloseWhenBackgroundIsClicked
            return base.OnBackgroundClicked();
        }

        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }

        private async void Categoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            var categoria = (Categoria)Categoria.SelectedItem;
            var vm = new EntradasViewModel();

            if(categoria != null)
            {
                SubCategoria.ItemsSource = await vm.SearchSubCategoria(categoria.Id);
                SubCategoria.IsEnabled = true;
                Articulo.ItemsSource = await vm.FilterCategoria(categoria.Id);
                Articulo.IsEnabled = true;
            }
        }

        private async void SubCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            var subcategoria = (SubCategoria)SubCategoria.SelectedItem;
            var vm = new EntradasViewModel();

            if (subcategoria != null)
            {
                Articulo.ItemsSource = await vm.FilterSubCategoria(subcategoria.Id);
            }
        }

        private async void Articulo_SelectedIndexChanged(object sender, EventArgs e)
        {
            var articulo = (Producto)Articulo.SelectedItem;
            var vm = new EntradasViewModel();
            if (articulo != null)
            {
                Descripcion.ItemsSource = await vm.FilterArticulo(articulo.Articulo);
                Descripcion.IsEnabled = true;
            }

        }

        
        private void CantEntregada_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (CantEntregada.Text == null || CantEntregada.Text == "")
            {
                AgregarProducto.IsEnabled = false;
            }
            else
            {
                AgregarProducto.IsEnabled = true;
            }
        }

        private async void AgregarProducto_Clicked(object sender, EventArgs e)
        {
            var vm = (EntradasViewModel)BContext;
            if (ProdID.Text == null || ProdID.Text == "")
            {
                string[] DP;
                var categoria = (Categoria)Categoria.SelectedItem;
                var subcategoria = (SubCategoria)SubCategoria.SelectedItem;
                var articulo = (Producto)Articulo.SelectedItem;
                var descripcion = (Producto)Descripcion.SelectedItem;
                if ((subcategoria == null || subcategoria.Nombre == "Ninguna") && (descripcion == null || descripcion.Descripcion == "Ninguna"))
                {
                    DP = new string[4]{
                        categoria.Nombre,
                        articulo.Articulo,
                        IdOrden,
                        CantEntregada.Text
                    };
                    vm.AgregarProdCategoria.Execute(DP);
                }
                else if (descripcion == null || descripcion.Descripcion == "Ninguna")
                {
                    DP = new string[4]{
                        subcategoria.Nombre,
                        articulo.Articulo,
                        IdOrden,
                        CantEntregada.Text
                    };
                    vm.AgregarProdSubCategoria.Execute(DP);
                }
                else if (subcategoria == null || subcategoria.Nombre == "Ninguna")
                {
                   
                    DP = new string[5]{
                        categoria.Nombre,
                        articulo.Articulo,
                        descripcion.Descripcion,
                        IdOrden,
                        CantEntregada.Text
                    };
                    vm.AgregarProdCategoriaDes.Execute(DP);
                }
                else
                {
                    DP = new string[5]{
                        subcategoria.Nombre,
                        articulo.Articulo,
                        descripcion.Descripcion,
                        IdOrden,
                        CantEntregada.Text
                    };
                    vm.AgregarProdSubCategoriaDes.Execute(DP);
                }

                await DisplayAlert("Exito!", "El Producto ya se añadio", "Ok");
                await Navigation.PopPopupAsync();
            }
            else
            {
                var r = await vm.ValidarContenidoOrden(Convert.ToInt32(IdOrden), Convert.ToInt32(ProdID.Text));
                if (r == false)
                {
                    await DisplayAlert("Error!", "Porfavor ingrese o seleccione un producto que este contenido en la Orden de Compra #"+ IdOrden, "Ok");
                }
                else
                {
                    var DP = new int[3]
                    {
                        Convert.ToInt32(IdOrden),
                        Convert.ToInt32(ProdID.Text),
                        Convert.ToInt32(CantEntregada.Text)
                    };
                    vm.AgregarProdId.Execute(DP);
                    await DisplayAlert("Exito!", "El Producto ya se añadio", "Ok");
                    await Navigation.PopPopupAsync();
                }
            }
        }

        
    }
}