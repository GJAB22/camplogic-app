﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetallesProductoPage : PopupPage 
    {

        public string ProductoId { get { return ProdID.Text; } }
        public DetallesProductoPage(Producto prod)
        {
            InitializeComponent();
            ProdID.Text = prod.Id.ToString();
            Titulo.Text = prod.Articulo;
            UB.Text = prod.Ubicacion.Nombre;
            CI.Text = prod.CantidadIdeal.ToString();
            BindingContext = new DetallesProductoViewModel(prod);
            
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // Method for animation child in PopupPage
        // Invoced after custom animation end
        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        protected override bool OnBackButtonPressed()
        {
            // Prevent hide popup
            //return base.OnBackButtonPressed();
            return true;
        }

        // Invoced when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return default value - CloseWhenBackgroundIsClicked
            return base.OnBackgroundClicked();
        }

        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }

        
        private async void EditarProducto_Clicked(object sender, EventArgs e)
        {

            await Navigation.PushPopupAsync(new EditarProducto(ProductoId, Titulo.Text, DescripcionField.Text, CU.Text, CantidadActField.Text,UB.Text,ProveedorNombre.Text, CI.Text));
            
        }

        private async void AnadirCesta_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushPopupAsync(new PopUpAgregarCesta(ProductoId, CU.Text));
        }
        
        private void GC_Clicked(object sender, EventArgs e)
        {

        }
    }
}
