﻿using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AgregarPatrullaPage : PopupPage
    {
        public AgregarPatrullaPage( object BC)
        {
            InitializeComponent();
            BindingContext = BC;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // Method for animation child in PopupPage
        // Invoced after custom animation end
        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        protected override bool OnBackButtonPressed()
        {
            // Prevent hide popup
            //return base.OnBackButtonPressed();
            return true;
        }

        // Invoced when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return default value - CloseWhenBackgroundIsClicked
            return base.OnBackgroundClicked();
        }


        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }
        
        

        private async void AgregarPatrulla_Clicked(object sender, EventArgs e)
        {
            var vm = BindingContext as PatrullaViewModel;

            if (PatrullaN.Text == null || PatrullaN.Text == "")
            {
                await DisplayAlert("Alerta", "Porfavor ingrese un Nombre", "Ok");
            }
            else
            {
                object[] NP = new object[]
                {
                    PatrullaN.Text,
                    Convert.ToInt32(EMin.Text),
                    Convert.ToInt32(EMax.Text)
                };
                vm.AgregarPatrulla.Execute(NP);
                await DisplayAlert("Exito", "La Nueva Categoria se ha guardado exitosamente", "Ok");
                await Navigation.PopPopupAsync();
            }
        }
    }
}
