﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EntradaPage : ContentPage
    {
        public object BC { get { return BindingContext; } }
        public EntradaPage()
        {
            InitializeComponent();

            BindingContext = new EntradasViewModel();
            EntradaC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            
        }

        
        private async void AgregarEntrada_Clicked(object sender, EventArgs e)
        {
            var vm = (EntradasViewModel)BC;
            var orden = await vm.OrdenU(Convert.ToInt32(IdOrden.Text));
            if (orden == null)
            {
                await DisplayAlert("Error", "Introduzca un Código de Orden Válido o que este Pendiente", "Ok");

            }
            else
            {
                await Navigation.PushPopupAsync(new PopUpEntradaIngreso(IdOrden.Text, BC));
            }
            
        }

        private void IdOrden_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IdOrden.Text == null || IdOrden.Text == "")
            {
                AgregarEntrada.IsEnabled = false;
            }
            else
            {
                AgregarEntrada.IsEnabled = true;
            }
        }

        private async void GenerarEntrada_Clicked(object sender, EventArgs e)
        {
            var vm = (EntradasViewModel)BC;
            if (vm.EntradaList.Count() == 0)
            {
                await DisplayAlert("Error", "Porfavor agregue un producto al listado.", "OK");
            }
            else
            {
                vm.RegistrarEntrada.Execute(Convert.ToInt32(IdOrden.Text));
                await DisplayAlert("Exito", "La nueva entrada del almacén a sido registrada", "OK");
                await Navigation.PopAsync();
            }
        }

        private  async void CancelarEntrega_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        private void EliminarProducto_Clicked(object sender, EventArgs e)
        {
            var button = sender as Button;
            var producto = button?.BindingContext as EntradaProducto;
            var vm = BindingContext as EntradasViewModel;
            vm?.RemoveEntrada.Execute(producto);
        }
    }
}
