﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class IngresosAlmacen : TabbedPage
    {
		public IngresosAlmacen ()
		{
			InitializeComponent ();
            Children.Add(new EntradaPage());
            Children.Add(new ReIntegros());
            BarBackgroundColor = Color.FromHex("#ff8a50");
        }
	}
}