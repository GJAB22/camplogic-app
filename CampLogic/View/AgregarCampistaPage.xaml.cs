﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AgregarCampistaPage : PopupPage
    {
        public AgregarCampistaPage()
        {
            InitializeComponent();
            BindingContext = new CampistaViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // Method for animation child in PopupPage
        // Invoced after custom animation end
        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        protected override bool OnBackButtonPressed()
        {
            // Prevent hide popup
            //return base.OnBackButtonPressed();
            return true;
        }

        // Invoced when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return default value - CloseWhenBackgroundIsClicked
            return base.OnBackgroundClicked();
        }


        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }

        private async void GuardarC_Clicked(object sender, EventArgs e)
        {
            var vm = BindingContext as CampistaViewModel;

            object[] NC = new object[]
            {
                NombreNC.Text,
                ApellidoNC.Text,
                Convert.ToInt32(CedulaNC.Text),
                DesC.Text,
                FNacimientoNC.Date,
                Ins.IsToggled,
                Trans.IsToggled
            };

            
            vm.AgregarCampista.Execute(NC);
            await DisplayAlert("Exito", "El Nuevo Campista se ha Registrado", "OK");
            await Navigation.PopPopupAsync();


        }

        private async void AgregarRepN_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushPopupAsync(new AgregarRepresentanteN(BindingContext));
        }

        private async void AgregarRepEx_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushPopupAsync(new AgregarRepresentanteE(BindingContext));
        }

        private void NombreNC_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (NombreNC.Text == "" || NombreNC.Text == null)
            {
                GuardarC.IsEnabled = false;
            }
            else
            {
                GuardarC.IsEnabled = true;
            }
        }
    }
}
