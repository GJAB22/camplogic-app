﻿using CampLogic.HelpClasses;
using CampLogic.Model;
using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetallesValoracionPage : ContentPage
    {
        public DetallesValoracionPage(ValoracionProducto p, DateTime FechaInicio, DateTime FechaFinal)
        {
            InitializeComponent();
            Title = p.Producto.Descripcion;
            BindingContext = new ValoracionViewModel(p, FechaInicio, FechaFinal);
        }
    }
}