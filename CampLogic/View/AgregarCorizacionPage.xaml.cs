﻿using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AgregarCorizacionPage : ContentPage
    {
        public AgregarCorizacionPage(string idprod, string desprod, string tipoprod)
        {
            InitializeComponent();
            

            IdProducto.Text = idprod;
            DescripcionProducto.Text = desprod;
            TipoProducto.Text = tipoprod;
        }

        private async void AgregarCotizacion_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}