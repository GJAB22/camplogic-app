﻿using CampLogic.Model;
using CampLogic.View;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProveedorPage : ContentPage
    {

        public ListView ListView { get { return ProveedoresListView; } }
        public object BC { get { return BindingContext; } }
        public ProveedorPage()
        {
            InitializeComponent();
            ProveedorC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            BindingContext = new ProveedorViewModel();
            
        }

        private async void ProveedoresListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var proveedor = (Proveedor)e.SelectedItem;
            if (proveedor!= null)
            {
                await Navigation.PushPopupAsync(new PerfilProveedorPage(proveedor));
            }

            ProveedoresListView.SelectedItem = null;
        }

        private async void SearchProveedor_TextChanged(object sender, TextChangedEventArgs e)
        {
            var keyword = SearchProveedor.Text;
            var vm = BindingContext as ProveedorViewModel;
            ProveedoresListView.ItemsSource = await vm.SearchProveedor(keyword);
        }

        private async void AgregarProveedor_Activated(object sender, EventArgs e)
        {
            await Navigation.PushPopupAsync(new AgregarProveedorPage(BC));
        }

        private void RefreshProveedor_Activated(object sender, EventArgs e)
        {
            BindingContext = new ProveedorViewModel();
        }
        

        private void ProveedorPicker_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
