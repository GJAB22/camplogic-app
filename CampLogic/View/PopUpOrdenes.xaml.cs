﻿using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopUpOrdenes : PopupPage
    {
		public PopUpOrdenes ( string informe)
		{
			InitializeComponent ();

            if (informe == "Ordenes Pendientes")
            {
                titulo.Text = "Ordenes Pendientes";
            }
            else if (informe == "Rotación del Inventario")
            {
                titulo.Text = "Rotación del Inventario";
            }
            else if (informe == "Rentabilidad")
            {
                titulo.Text = "Rentabilidad";
                ganacias.IsVisible = true;
            }
            else if (informe == "Valoración del Inventario")
            {
                titulo.Text = "Valoración del Inventario";
            }

            
            
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // Method for animation child in PopupPage
        // Invoced after custom animation end
        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        protected override bool OnBackButtonPressed()
        {
            // Prevent hide popup
            //return base.OnBackButtonPressed();
            return true;
        }

        // Invoced when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return default value - CloseWhenBackgroundIsClicked
            return base.OnBackgroundClicked();
        }


        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }

        private void GenerarInforme_Clicked(object sender, EventArgs e)
        {
            
            
        }
    }
}