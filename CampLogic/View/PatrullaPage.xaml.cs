﻿using CampLogic.Model;
using CampLogic.View;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CampLogic
{
    public partial class PatrullaPage : ContentPage
    {
        public PatrullaPage()
        {
            
            InitializeComponent();
            PatrullaC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            BindingContext = new PatrullaViewModel();
            
        }

       

        private  void PatrullasListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            
            PatrullasListView.SelectedItem = null;
            
        }

        private async void AgregarPatrulla_Activated(object sender, EventArgs e)
        {
            await Navigation.PushPopupAsync(new AgregarPatrullaPage(BindingContext));
        }

        private async void BuscarPatrulla_TextChanged(object sender, TextChangedEventArgs e)
        {
            var keyword = BuscarPatrulla.Text;
            var vm = BindingContext as PatrullaViewModel;
            PatrullasListView.ItemsSource = await vm.SearchPatrulla(keyword);
        }

        private void PatrullasListView_Refreshing(object sender, EventArgs e)
        {
            InitializeComponent();

            PatrullasListView.IsPullToRefreshEnabled = false;

        }

        private void PatRefresh_Activated(object sender, EventArgs e)
        {
            BindingContext = new PatrullaViewModel();
        }
    }
}
