﻿using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopUpAgregarCesta : PopupPage
    {
        private string ProductoID { get { return ProdID.Text; } }
        private string CostoU { get { return PU.Text; } }
        public PopUpAgregarCesta( string ProdId, string CU)
        {
            InitializeComponent();
            ProdID.Text = ProdId;
            PU.Text = CU;
            NuevaProdCesta.IsEnabled = false;

            var monkeyList = new List<string>();
            monkeyList.Add("Baja");
            monkeyList.Add("Media");
            monkeyList.Add("Alta");

            PR.ItemsSource = monkeyList;
        }

        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }

        private void CantidadOrdenada_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (CantidadO.Text==null || CantidadO.Text == "")
            {
                NuevaProdCesta.IsEnabled = false;
            }
            else
            {
                NuevaProdCesta.IsEnabled = true;
            }
            
        }

        private async void NuevaProdCesta_Clicked(object sender, EventArgs e)
        {
            var vm = new CestaViewModel();
            var prioridad = (string)PR.SelectedItem;
            object[] DC = new object[]
            {
                Convert.ToInt32(ProductoID),
                Convert.ToInt32(CantidadO.Text),
                float.Parse(CostoU),
                prioridad
            };

            vm.AgregarCesta.Execute(DC);
            await DisplayAlert("Exito", "Se ha agregado a la Cesta para Ordenar", "Ok");
            await Navigation.PopAllPopupAsync();
        }
    }
}