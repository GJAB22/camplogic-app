﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EntradaPrestamo : PopupPage
    {
        public Salidas_Producto SalidaP { get; set; }
        public EntradaPrestamo ( Salidas_Producto salidap)
		{
			InitializeComponent ();
            EntregadoA.Text = salidap.Salida.Empleado.Nombre +" "+ salidap.Salida.Empleado.Apellido;
            Articulo.Text = salidap.Producto.Articulo;
            Descripcion.Text = salidap.Producto.Descripcion;
            FE.Text = salidap.Salida.Fecha.ToString("dd/MM/yyyy hh:mm tt");
            Cantidad.Text = salidap.Cantidad.ToString();
            BindingContext = new SalidasViewModel();
            SalidaP = salidap;
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // Method for animation child in PopupPage
        // Invoced after custom animation end
        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        protected override bool OnBackButtonPressed()
        {
            // Prevent hide popup
            //return base.OnBackButtonPressed();
            return true;
        }

        // Invoced when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return default value - CloseWhenBackgroundIsClicked
            return base.OnBackgroundClicked();
        }

        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }

        private async void AgregarEntrada_Clicked(object sender, EventArgs e)
        {
            var vm = new SalidasViewModel();

            if (CantidadEntrada.Text == null || CantidadEntrada.Text == "")
            {
                await DisplayAlert("Error!", "Porfavor introduzca la cantidad recibida", "OK");
            }
            else
            {
                object[] EP = new object[]
                {
                    SalidaP,
                    Convert.ToInt32(CantidadEntrada.Text),
                    FechaEntrada.Date,
                    HoraEntrada.Time
                };
                vm.AgregarEntradaPuntual.Execute(EP);
                await DisplayAlert("Exito!", "La entrada a sido registrada, porfavor seleccionar la opcion de 'Actualizar' en 'Listado de Prestamos' y en el listado principal de Productos ", "OK");
                await Navigation.PopPopupAsync();
            }

           
        }
    }
}