﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrdenesPendientesPage : ContentPage
    {
        public OrdenesPendientesPage()
        {
            InitializeComponent();
            RepOrdenC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            BindingContext = new OrdenCompraViewModel();
        }

        private async void OrdenesListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var ordenCompra = (Orden_Compra)e.SelectedItem;
            if (ordenCompra != null)
            {
                await Navigation.PushPopupAsync(new DetallesOrdenPage(ordenCompra));
            }

            OrdenesListView.SelectedItem = null;
        }
    }
}