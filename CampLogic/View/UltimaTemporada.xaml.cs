﻿using CampLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UltimaTemporada : TabbedPage
    {
        public UltimaTemporada(Temporadas ut)
        {
            
            
            Children.Add(new ActualHorario(ut));
            Children.Add(new ActualCampistas(ut));
            InitializeComponent();
            BarBackgroundColor = Color.FromHex("#ff8a50");
        }
    }
}