﻿using CampLogic.HelpClasses;
using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AgregarActividad : PopupPage
    {
        public object BC { get { return BindingContext; } }
        public AgregarActividad(object BC)
        {
            InitializeComponent();
            
            BindingContext = BC;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // Method for animation child in PopupPage
        // Invoced after custom animation end
        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        protected override bool OnBackButtonPressed()
        {
            // Prevent hide popup
            //return base.OnBackButtonPressed();
            return true;
        }

        // Invoced when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return default value - CloseWhenBackgroundIsClicked
            return base.OnBackgroundClicked();
        }

        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }
        
        private void Clasificacion_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void EliminarProducto_Clicked(object sender, EventArgs e)
        {
            var button = sender as Button;
            var producto = button?.BindingContext as ProductoActividad;
            var vm = BindingContext as ActividadesViewModel;
            vm?.RemoveProducto.Execute(producto);
        }

        
        private async void AgregarProducto_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushPopupAsync(new AgregarProductoActividadPage(BC));
        }
        
        private void ActivdadList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ActivdadList.SelectedItem = null;
        }

        private void Entry_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (NomAct.Text != null || NomAct.Text != "")
            {
                AgregarAct.IsEnabled = true;
            }
        }

        private async void AgregarActividad_Clicked(object sender, EventArgs e)
        {
            var vm = BindingContext as ActividadesViewModel;
            if (Clasificacion.SelectedItem == null)
            {
                await DisplayAlert("Alerta", "Porfavor seleccione a quien va dirigido esta actividad", "OK");
            }
            else
            {
                object[] NA = new object[]
                {
                    NomAct.Text,
                    Clasificacion.SelectedItem,
                    Descripcion.Text
                };
                vm.AgregarActividad.Execute(NA);
                await DisplayAlert("Exito", "La Actividad a sido registrada", "OK");
                await Navigation.PopPopupAsync();
            }
        }
    }
}
