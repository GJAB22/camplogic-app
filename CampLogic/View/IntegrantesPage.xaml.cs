﻿using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IntegrantesPage : ContentPage
    {
        public IntegrantesPage()
        {
            InitializeComponent();
            IntegrantesC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            BindingContext = new IntegrantesViewModel();

        }

        private async void EnviarAsistencia_Clicked(object sender, EventArgs e)
        {
            var vm = BindingContext as IntegrantesViewModel;

            vm.EnviarAsistencia.Execute(1);
            await DisplayAlert("Exito", "La Asistencia a sido Enviada", "OK");
            
        }
    }
}
