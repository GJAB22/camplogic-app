﻿using CampLogic.Helpers;
using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PerfilCampistaPage : PopupPage
    {
        public Campistas Camp { get; set; }
        public PerfilCampistaPage(Campistas campista)
        {
            InitializeComponent();
            CampistaN.Text = campista.Nombre + " " + campista.Apellido;
            Camp = campista;
            if (Settings.Cargo == "Director" || Settings.Cargo == "Coordinador")
            {
                EnableIns.IsVisible = true;
            }
            BindingContext = new CampistaViewModel(campista);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // Method for animation child in PopupPage
        // Invoced after custom animation end
        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        protected override bool OnBackButtonPressed()
        {
            // Prevent hide popup
            //return base.OnBackButtonPressed();
            return true;
        }

        // Invoced when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return default value - CloseWhenBackgroundIsClicked
            return base.OnBackgroundClicked();
        }


        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }


        private void EditarCampistaPerfil_Activated(object sender, EventArgs e)
        {
           
        }

        private async  void GuardarCambios_Clicked(object sender, EventArgs e)
        {

            //Agregar Patrulla a los Datos
            var vm = new CampistaViewModel();
            object[] NC = new object[]
            {
                Camp,
                Ins.IsToggled,
                Trans.IsToggled
            };
            vm.EditarCampista.Execute(NC);
            await DisplayAlert("", "Porfavor seleccione 'Actualizar' en el listado de campista para ver los cambios.", "OK");
            Ins.IsEnabled = false;
            Trans.IsEnabled = false;
            GuardarC.IsVisible = false;
            EnableIns.IsVisible = true;
            await Navigation.PopPopupAsync();




        }

        private void CancelarCambios_Clicked(object sender, EventArgs e)
        {
            
        }

        private void EnableIns_Clicked(object sender, EventArgs e)
        {
            EnableIns.IsVisible = false;
            Ins.IsEnabled = true;
            Trans.IsEnabled = true;
            GuardarC.IsVisible = true;
        }
    }
}
