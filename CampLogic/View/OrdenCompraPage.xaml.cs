﻿using CampLogic.Helpers;
using CampLogic.Model;
using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrdenCompraPage : ContentPage
    {
        public OrdenCompraPage()
        {
            InitializeComponent();
            BindingContext = new CestaViewModel();
            CestaC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);

            var monkeyList = new List<string>();
            monkeyList.Add("Todas");
            monkeyList.Add("Baja");
            monkeyList.Add("Media");
            monkeyList.Add("Alta");

            PreferenciaPicker.ItemsSource = monkeyList;

        }
        
        private async void BorrarProd_Clicked(object sender, EventArgs e)
        {
            var button = sender as Button;

            var producto = button?.BindingContext as Cesta;

            var vm = BindingContext as CestaViewModel;

            var resp = await DisplayAlert("¿Desea Eliminar este producto de la Cesta?", "", "Aceptar", "Cancelar");

            if (resp == true)
            {
                vm?.RemoverItem.Execute(producto);
                await DisplayAlert("El producto se ha eliminado de la cesta exitosamente, recuerde seleccionar 'Actualizar' para el Monto Total"," ","OK");
            }

            
        }

        private void refreshCesta_Activated(object sender, EventArgs e)
        {
            BindingContext = new CestaViewModel();
        }

        

        private void CestaList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            CestaList.SelectedItem = null;
        }
        
        private async void NuevaOrdenes_Clicked(object sender, EventArgs e)
        {
            var vm = BindingContext as CestaViewModel;
            var cestaL = await vm.CestaList();
            if (cestaL.Count()==0)
            {
                await DisplayAlert("Error", "Porfavor agregue algun producto con sus respectiva cantidad a Ordenar", "Ok");
            }
            else
            {
                var r = await DisplayAlert("¿Desea Generar las Ordenes de Compra?", "", "Aceptar", "Cancelar");
                if (r == false)
                {
                    
                }
                else
                {
                    int i = 0;
                    vm.NuevasO.Execute(i);
                    await DisplayAlert("Exito, sus ordenes han sido enviadas", "", "OK");
                }
            }
        }

        private async void PreferenciaPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var prioridad = (string)PreferenciaPicker.SelectedItem;
            var vm = BindingContext as CestaViewModel;
            if (prioridad != null)
            {
                if (prioridad != "Todas")
                {
                    CestaList.ItemsSource = await vm.FilterPrioridad(prioridad);
                }
                else
                {
                    CestaList.ItemsSource = await vm.CestaList();
                }

            }
        }
    }
}
