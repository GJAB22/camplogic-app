﻿using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AgregarProveedorPage : PopupPage
    {
        public AgregarProveedorPage(object BC)
        {
            InitializeComponent();
            BindingContext = BC;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // Method for animation child in PopupPage
        // Invoced after custom animation end
        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        protected override bool OnBackButtonPressed()
        {
            // Prevent hide popup
            //return base.OnBackButtonPressed();
            return true;
        }

        // Invoced when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return default value - CloseWhenBackgroundIsClicked
            return base.OnBackgroundClicked();
        }

        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }

        private async void AgregarProveedor_Clicked(object sender, EventArgs e)
        {
            if (DescripcionField.Text == null || DescripcionField.Text == "")
            {
                await DisplayAlert("Error!", "Porfavor introduzca el tipo de producto que proporciona el proveedor","Ok");
            }
            else if (NombreProv.Text == null || NombreProv.Text == "")
            {
                await DisplayAlert("Error!", "Porfavor introduzca el Nombre del proveedor", "Ok");
            }
            else if (CedulaProv.Text == null || CedulaProv.Text == "" || RIFProv.Text == null || RIFProv.Text == "")
            {
                await DisplayAlert("Error!", "Porfavor introduzca la cedula o RIF del proveedor", "Ok");
            }
            else if (Telf1Prov.Text == null || Telf1Prov.Text == "" || Telf2Prov.Text == null || Telf2Prov.Text == "")
            {
                await DisplayAlert("Error!", "Porfavor introduzca un Numero telefonico del proveedor", "Ok");
            }
            else if (CorreoProv.Text == null || CorreoProv.Text == "")
            {
                await DisplayAlert("Error!", "Porfavor introduzca el Correo del proveedor", "Ok");
            }
            else
            {
                var vm = BindingContext as ProveedorViewModel;
                object[] np = new object[]
                {
                    NombreProv.Text,
                    ApellidoProv.Text,
                    CedulaProv.Text,
                    RIFProv.Text,
                    DescripcionField.Text,
                    Telf1Prov.Text,
                    Telf2Prov.Text,
                    CorreoProv.Text
                };
                vm.AgregarProveedor.Execute(np);
                await DisplayAlert("Exito!", "El proveedor se a agregado exitosamente", "Ok");
                await Navigation.PopPopupAsync();
            }
            
        }
    }
}
