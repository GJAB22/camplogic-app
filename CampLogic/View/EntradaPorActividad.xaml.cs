﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EntradaPorActividad : PopupPage
    {
        public ActividadProducto ActProd { get; set; }
        public EntradaPorActividad(ActividadProducto actprod, object BC)
        {
            InitializeComponent();
            ProdArt.Text = actprod.Producto.Articulo;
            ProdDes.Text = actprod.Producto.Descripcion;
            ProdUb.Text = actprod.Producto.Ubicacion.Nombre;
            ActCant.Text = actprod.Cantidad.ToString();
            ActProd = actprod;
            BindingContext = BC;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // Method for animation child in PopupPage
        // Invoced after custom animation end
        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        protected override bool OnBackButtonPressed()
        {
            // Prevent hide popup
            //return base.OnBackButtonPressed();
            return true;
        }

        // Invoced when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return default value - CloseWhenBackgroundIsClicked
            return base.OnBackgroundClicked();
        }

        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }

        private async void ReEntrada_Clicked(object sender, EventArgs e)
        {
            var vm = BindingContext as TemporadaViewModel;

            object[] EN = new object[]
            {
                Convert.ToInt32(CantEntrada.Text),
                ActProd
            };

            vm?.EntradaKit.Execute(EN);
            await DisplayAlert("Exito", "Se ha ingresado la entrada del producto", "OK");
            await Navigation.PopAllPopupAsync();
        }
    }
}