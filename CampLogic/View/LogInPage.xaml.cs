﻿using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CampLogic
{
    public partial class LogInPage : ContentPage
    {
        public LogInPage()
        {
            InitializeComponent();
            BindingContext = new LogInViewModel();
            LSC.BackgroundColor = new Color(225, 225, 225, 0.7);
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private async void SignUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SignUpPage(), true);
        }

        private async void LogIn_Clicked(object sender, EventArgs e)
        {
            var vm = BindingContext as LogInViewModel;

            if ((Cedula.Text == "" || Cedula.Text == null) && (PasswordEmpleado.Text == "" || PasswordEmpleado.Text == null))
            {
                await DisplayAlert("Alerta", "Porfavor Ingrese la cedula y contraseña para Iniciar", "OK");
            }
            else if (Cedula.Text == "" || Cedula.Text == null)
            {
                await DisplayAlert("Alerta", "Porfavor Ingrese la Cedula  para Iniciar", "OK");
            }
            else if (PasswordEmpleado.Text == "" || PasswordEmpleado.Text == null)
            {
                await DisplayAlert("Alerta", "Porfavor Ingrese la Contraseña para Iniciar", "OK");
            }
            else
            {
                var b = await vm.ValidarCedula(Cedula.Text);
                if (b == false)
                {
                    await DisplayAlert("Alerta", "Porfavor ingrese una Cedula Valida","OK");
                }
                else
                {
                    string[] LL = new string[]
                    {
                        Cedula.Text,
                        PasswordEmpleado.Text
                    };
                    vm.LogInCommand.Execute(LL);
                    await DisplayAlert("Exito", "A Ingresado con Exito", "OK");
                }
                
            }
        }
    }
}
