﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MiPatrullaPage : TabbedPage
    {
        public MiPatrullaPage()
        {
            InitializeComponent();
            BarBackgroundColor = Color.FromHex("#ff8a50");
            Children.Add(new HorarioPage());
            Children.Add(new IntegrantesPage());
        }
    }
}