﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReIntegros : ContentPage
    {
        private object BC { get { return BindingContext; } }
        public ReIntegros()
        {
            InitializeComponent();
            ReintegroC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            BindingContext = new EntradasViewModel();
        }

        private async void AgregarEntrada_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushPopupAsync(new PopUpReIntegros(BC));
        }

        private async void GenerarEntrada_Clicked(object sender, EventArgs e)
        {
            var vm = (EntradasViewModel)BC;
            if (vm.ReintegrosList.Count() == 0)
            {
                await DisplayAlert("Error", "Porfavor agregue un producto al listado.", "OK");
            }
            else
            {
                vm.RegistrarReintegro.Execute(1);
                await DisplayAlert("Exito", "La nueva entrada del almacén a sido registrada", "OK");
                await Navigation.PopAsync();
            }
        }

        private async void CancelarEntrega_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
        
        
        private void EliminarProducto_Clicked(object sender, EventArgs e)
        {
            var button = sender as Button;
            var producto = button?.BindingContext as ReintegroProducto;
            var vm = BindingContext as EntradasViewModel;
            vm?.RemoveReintegro.Execute(producto);
        }

        private void RIList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            RIList.SelectedItem = null;
        }
    }
}