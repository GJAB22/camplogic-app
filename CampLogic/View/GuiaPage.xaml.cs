﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CampLogic
{
    public partial class GuiaPage : ContentPage
    {

        public GuiaPage()
        {
            InitializeComponent();
            PersonalC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            BindingContext = new GuiaViewModel();
        }

        

        private async void GuiaListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var guia = (Empleado)e.SelectedItem;
            if (guia!=null)
            {
                await Navigation.PushPopupAsync(new PerfilCorGuia(guia));
            }

            GuiaListView.SelectedItem = null;
            
        }

        private async void SearchGuia_TextChanged(object sender, TextChangedEventArgs e)
        {
            var keyword = SearchGuia.Text;
            var vm = BindingContext as GuiaViewModel;
            if (Cargos.SelectedItem == null ||  (string)Cargos.SelectedItem == "Todos")
            {
                GuiaListView.ItemsSource = await vm.SearchPersonal(keyword);
            }
            else
            {
                var cargo = (string)Cargos.SelectedItem;
                GuiaListView.ItemsSource = await vm.FilterCargoPersonal(keyword, cargo);
            }
            

        }

        private async void GuiaRefresh_Activated(object sender, EventArgs e)
        {
            var vm = BindingContext as GuiaViewModel;
            GuiaListView.ItemsSource = await vm.PersonalList();
        }

        private async void Cargos_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cargo = (string)Cargos.SelectedItem;
            var vm = BindingContext as GuiaViewModel;
            GuiaListView.ItemsSource = await vm.FiltrarPersonal(cargo);
        }
    }
}
