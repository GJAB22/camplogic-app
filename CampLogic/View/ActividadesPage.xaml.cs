﻿using CampLogic.Helpers;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActividadesPage : ContentPage
    {
        public object BC { get { return BindingContext; } }
        public ActividadesPage()
        {
            InitializeComponent();
            ActividadC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            BindingContext = new ActividadesViewModel();
            if (Settings.Cargo == "Guía" || Settings.Cargo == "Inventario")
            {
                ToolbarItems.Remove(Add);
            }
        }

        private void Refresh_Activated(object sender, EventArgs e)
        {
            BindingContext = new ActividadesViewModel();
        }

        private async void Add_Activated(object sender, EventArgs e)
        {
            await Navigation.PushPopupAsync(new AgregarActividad(BC));
        }

        private async void SearchActividad_TextChanged(object sender, TextChangedEventArgs e)
        {
            var keyword = SearchActividad.Text;
            var vm = BindingContext as ActividadesViewModel;
            if (Clasificacion.SelectedItem == null || (string)Clasificacion.SelectedItem == "Todas")
            {
                ActividadesListView.ItemsSource = await vm.SearchActividades(keyword);
            }
            else
            {
                ActividadesListView.ItemsSource = await vm.FilterActividadesCla(keyword, (string)Clasificacion.SelectedItem);
            }

            
        }

        private void ActividadesListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ActividadesListView.SelectedItem = null;
        }

        private async void Clasificacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            var clasificacion = (string)Clasificacion.SelectedItem;
            var vm = BindingContext as ActividadesViewModel;
            if (clasificacion != null)
            {
                ActividadesListView.ItemsSource = await vm.FiltrarActividades(clasificacion);
            }
        }
    }
}