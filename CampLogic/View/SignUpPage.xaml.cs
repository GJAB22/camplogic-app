﻿using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CampLogic
{
    public partial class SignUpPage : ContentPage
    {
        public SignUpPage()
        {
            InitializeComponent();
            
            BindingContext = new SignUpViewModel();

        }

        private async void Registrar_Clicked(object sender, EventArgs e)
        {
            var vm = BindingContext as SignUpViewModel;

            if (NombreEmp.Text == null || NombreEmp.Text == "")
            {
                await DisplayAlert("Alerta", "Porfavor coloque su Nombre", "OK");
            }
            else if (ApellidoEmp.Text == null || ApellidoEmp.Text == "")
            {
                await DisplayAlert("Alerta", "Porfavor coloque su Apellido", "OK");
            }
            else if (CedulaEmp.Text == null || CedulaEmp.Text == "")
            {
                await DisplayAlert("Alerta", "Porfavor coloque su Número de Cedula", "OK");
            }
            else if (CorreoEmp.Text == null || CorreoEmp.Text == "")
            {
                await DisplayAlert("Alerta", "Porfavor coloque su Correo", "OK");
            }
            else if (TelefonoEmp.Text == null || TelefonoEmp.Text == "")
            {
                await DisplayAlert("Alerta", "Porfavor coloque su Telefono", "OK");
            }
            else if (ClaveEmp.Text=="" || ClaveEmp.Text==null)
            {
                await DisplayAlert("Alerta", "Porfavor coloque una Clave", "OK");
            }
            else if (ConClaveEmp.Text == "" || ConClaveEmp.Text == null)
            {
                await DisplayAlert("Alerta", "Porfavor confirme su Clave", "OK");
            }
            else if (CargoEmp.SelectedItem == null)
            {
                await DisplayAlert("Alerta", "Porfavor seleccione un Cargo", "OK");
            }
            else
            {
                object[] m = new object[]
                {
                    NombreEmp.Text,
                    ApellidoEmp.Text,
                    CedulaEmp.Text,
                    FNEmp.Date,
                    (string)CargoEmp.SelectedItem,
                    TelefonoEmp.Text,
                    CorreoEmp.Text,
                    ClaveEmp.Text,
                    ConClaveEmp.Text
                };

                vm.RegisterCommand.Execute(m);
                await DisplayAlert("Exito", "Se ha Registrado Exitosamente","OK");
            }

        }
    }
}
