﻿using CampLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetallesEntradaPage : ContentPage
    {
        public DetallesEntradaPage(Entrada entrada)
        {
            InitializeComponent();

            IdEntrada.Text = entrada.Id.ToString();
            //EntradaProducto.Text = entrada.Producto.Descripcion;
            Cantidad.Text = entrada.Cantidad.ToString();
            FEntrada.Text = entrada.Fecha.ToString("dd/MM/yyyy hh:mm tt");
            ValorTotal.Text = entrada.Valor_Total.ToString();
            //BackOrder.Text = entrada.BackOrder.ToString();
            IdOrden.Text = entrada.OrdenCompraId.ToString();
            NombreProv.Text = entrada.OrdenCompra.Proveedor.Nombre;
            ApellidoProv.Text = entrada.OrdenCompra.Proveedor.Apellido;
            TlfProv_1.Text = entrada.OrdenCompra.Proveedor.Telefono_1;
            TlfProv_2.Text = entrada.OrdenCompra.Proveedor.Telefono_2;
            CorreoProv.Text = entrada.OrdenCompra.Proveedor.Correo;
        }
    }
}
