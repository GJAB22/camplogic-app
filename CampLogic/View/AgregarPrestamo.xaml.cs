﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AgregarPrestamo : PopupPage
    {
		public AgregarPrestamo ()
		{
			InitializeComponent ();
            BindingContext = new SalidasViewModel();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // Method for animation child in PopupPage
        // Invoced after custom animation end
        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        protected override bool OnBackButtonPressed()
        {
            // Prevent hide popup
            //return base.OnBackButtonPressed();
            return true;
        }

        // Invoced when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return default value - CloseWhenBackgroundIsClicked
            return base.OnBackgroundClicked();
        }

        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }

        private async void Categoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            var categoria = (Categoria)Categoria.SelectedItem;
            var vm = new EntradasViewModel();

            if (categoria != null)
            {
                SubCategoria.ItemsSource = await vm.SearchSubCategoria(categoria.Id);
                SubCategoria.IsEnabled = true;
                Articulo.ItemsSource = await vm.FilterCategoria(categoria.Id);
                Articulo.IsEnabled = true;
            }
        }

        private async void SubCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            var subcategoria = (SubCategoria)SubCategoria.SelectedItem;
            var vm = new EntradasViewModel();

            if (subcategoria != null)
            {
                Articulo.ItemsSource = await vm.FilterSubCategoria(subcategoria.Id);
            }
        }

        private async void Articulo_SelectedIndexChanged(object sender, EventArgs e)
        {
            var articulo = (Producto)Articulo.SelectedItem;
            var vm = new EntradasViewModel();
            if (articulo != null)
            {
                Descripcion.ItemsSource = await vm.FilterArticulo(articulo.Articulo);
                Descripcion.IsEnabled = true;
            }

        }


        private void CantEntregada_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (CantEntregada.Text == null || CantEntregada.Text == "")
            {
                AgregarSalida.IsEnabled = false;
            }
            else
            {
                AgregarSalida.IsEnabled = true;
            }
        }

        private async void AgregarSalida_Clicked(object sender, EventArgs e)
        {
            var vm = new SalidasViewModel();
            var categoria = (Categoria)Categoria.SelectedItem;
            var subcategoria = (SubCategoria)SubCategoria.SelectedItem;
            var articulo = (Producto)Articulo.SelectedItem;
            var descripcion = (Producto)Descripcion.SelectedItem;
            var empleado = (Empleado)EmpleadoS.SelectedItem;
            var fecha = Fecha.Date;
            var hora = Hora.Time;
            if (ProdID.Text == null || ProdID.Text == "")
            {
                object[] DP;
                if ((subcategoria == null || subcategoria.Nombre == "Ninguna") && (descripcion == null || descripcion.Descripcion == "Ninguna"))
                {
                    var prod = await vm.ProductoCategoriaSalidasP(categoria.Nombre,articulo.Articulo);
                    if (prod.Disponible < Convert.ToInt32(CantEntregada.Text))
                    {
                        await DisplayAlert("Error!", "Las unidades a entregar superan las disponibles.", "Ok");
                    }
                    else
                    {
                        DP = new object[]{
                            categoria.Nombre,
                            articulo.Articulo,
                            Convert.ToInt32(CantEntregada.Text),
                            empleado,
                            fecha,
                            hora

                        };
                        vm.AgregarProdCategoriaSalidasP.Execute(DP);
                        await DisplayAlert("Exito!", "Se a generado una nueva Salida", "Ok");
                        await Navigation.PopPopupAsync();
                    }
                    
                }
                else if (descripcion == null || descripcion.Descripcion == "Ninguna")
                {
                    var prod = await vm.ProductoSubCategoriaSalidasP(subcategoria.Nombre,articulo.Articulo);
                    if (prod.Disponible < Convert.ToInt32(CantEntregada.Text))
                    {
                        await DisplayAlert("Error!", "Las unidades a entregar superan las disponibles.", "Ok");
                    }
                    else
                    {
                        DP = new object[]{
                            subcategoria.Nombre,
                            articulo.Articulo,
                            Convert.ToInt32(CantEntregada.Text),
                            empleado,
                            fecha,
                            hora
                        };
                        vm.AgregarProdSubCategoriaSalidasP.Execute(DP);
                        await DisplayAlert("Exito!", "Se a generado una nueva Salida", "Ok");
                        await Navigation.PopPopupAsync();
                    }
                   
                }
                else if (subcategoria == null || subcategoria.Nombre == "Ninguna")
                {
                    var prod = await vm.ProductoCategoriaDesSalidasP(categoria.Nombre,articulo.Articulo, descripcion.Descripcion);
                    if (prod.Disponible < Convert.ToInt32(CantEntregada.Text))
                    {
                        await DisplayAlert("Error!", "Las unidades a entregar superan las disponibles.", "Ok");
                    }
                    else
                    {
                        DP = new object[]{
                            categoria.Nombre,
                            articulo.Articulo,
                            descripcion.Descripcion,
                            Convert.ToInt32(CantEntregada.Text),
                            empleado,
                            fecha,
                            hora
                        };
                        vm.AgregarProdCategoriaDesSalidasP.Execute(DP);
                        await DisplayAlert("Exito!", "Se a generado una nueva Salida", "Ok");
                        await Navigation.PopPopupAsync();
                    }
                    
                    
                }
                else
                {
                    var prod = await vm.ProductoSubCategoriaDesSalidasP(subcategoria.Nombre,articulo.Articulo,descripcion.Descripcion);
                    if (prod.Disponible < Convert.ToInt32(CantEntregada.Text))
                    {
                        await DisplayAlert("Error!", "Las unidades a entregar superan las disponibles.", "Ok");
                    }
                    else
                    {
                        DP = new object[]{
                            subcategoria.Nombre,
                            articulo.Articulo,
                            descripcion.Descripcion,
                            Convert.ToInt32(CantEntregada.Text),
                            empleado,
                            fecha,
                            hora
                        };
                        vm.AgregarProdSubCategoriaDesSalidasP.Execute(DP);
                        await DisplayAlert("Exito!", "Se a generado una nueva Salida", "Ok");
                        await Navigation.PopPopupAsync();
                    }
                   
                }

                
            }
            else
            {
                var prod = await vm.ProductoF(Convert.ToInt32(ProdID.Text));
                if (prod.Disponible < Convert.ToInt32(CantEntregada.Text))
                {
                    await DisplayAlert("Error!", "Las unidades a entregar superan las disponibles.", "Ok");
                }
                else
                {
                    object[] DP = new object[]
                    {
                        Convert.ToInt32(ProdID.Text),
                        Convert.ToInt32(CantEntregada.Text),
                        empleado,
                        fecha,
                        hora
                    };
                    vm.AgregarProdIdSalidasP.Execute(DP);
                    await DisplayAlert("Exito!", "Se a generado una nueva Salida", "Ok");
                    await Navigation.PopPopupAsync();
                }
                
            }
        }
        

        
    }

}