﻿using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FaltanteReport : ContentPage
	{
		public FaltanteReport (DateTime FI, DateTime FF, string categoria, string subcategoria, string ubcacion, string articulo)
		{
			InitializeComponent ();
            RepFaltanteC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            FInicial.Text = FI.Date.ToString("dd/MM/yyy");
            FFinal.Text = FF.Date.ToString("dd/MM/yyyy");
            BindingContext = new FaltanteViewModel(FI,FF,categoria,subcategoria,ubcacion,articulo);
		}

        private void RepFList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            RepFList.SelectedItem = null;
        }
    }
}