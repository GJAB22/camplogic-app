﻿using CampLogic.Model;
using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SolicitudesPage : ContentPage
    {
        public SolicitudesPage()
        {
            InitializeComponent();
            

            
        }

        private async void SolicitudesListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            

            SolicitudesListView.SelectedItem = null;
        }

        

        private async void Entradas_Activated(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new EntradaListaPage());
        }

        private async void Salidas_Activated(object sender, EventArgs e)
        {
            
        }

        private void SearchSolicitudes_TextChanged(object sender, TextChangedEventArgs e)
        {
            //var keyword = SearchSolicitudes.Text;
            //var solicitudes = new SolicitudesViewModel();
            //SolicitudesListView.ItemsSource = solicitudes.Solicitudes.Where(solicitud => solicitud.Producto.Descripcion.ToLower().Contains(keyword.ToLower()));
        }

        private void Refresh_Activated(object sender, EventArgs e)
        {
            
        }

        private async void Reporte_Activated(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ListSolicitudesPage());
            
        }
    }
}
