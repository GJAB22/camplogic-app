﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Prestamos : ContentPage
	{
		public Prestamos ()
		{
			InitializeComponent ();
            PrestamosC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            BindingContext = new SalidasViewModel();
		}
        
        private async void AgregarSalida_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushPopupAsync(new AgregarPrestamo());
        }

        private async void PrestamosList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var prestamo = (Salidas_Producto)PrestamosList.SelectedItem;
            if (prestamo != null)
            {
                await Navigation.PushPopupAsync(new EntradaPrestamo(prestamo));
            }
            
            PrestamosList.SelectedItem = null;
        }

        private void refreshSalidas_Activated(object sender, EventArgs e)
        {
            BindingContext = new SalidasViewModel();
        }
    }
}