﻿using CampLogic.Helpers;
using CampLogic.ViewModel;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PerfilPage : ContentPage
    {

        private MediaFile _mediaFile { get; set; }

        public PerfilPage()
        {
            InitializeComponent();
            PerfilC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            BindingContext = new PerfilViewModel();
            
            
        }

        private void EditarPerfil_Activated(object sender, EventArgs e)
        {
            if (Settings.Cargo == "Director")
            {
                TelefonoP.IsVisible = false;
                Disponible.IsEnabled = true;
                EditarTelefonoP.IsVisible = true;
            }
            else
            {
                TelefonoP.IsVisible = false;
                CargoP.IsVisible = false;
                Disponible.IsEnabled = true;
                EditarTelefonoP.IsVisible = true;
                SelectCargo.IsVisible = true;
            }
            


            EditarBotones.IsVisible = true;
            botonesImagenes.IsVisible = true;
            

        }

        private void CancelarEditar_Clicked(object sender, EventArgs e)
        {
            //Desabilitar los Entrys
            EditarTelefonoP.IsVisible = false;
            Disponible.IsEnabled = false;
            SelectCargo.IsVisible = false;
            EditarBotones.IsVisible = false;
            botonesImagenes.IsVisible = false;

            //Habilitar los Labels
            CargoP.IsVisible = true;
            PatrullaP.IsVisible = true;
            TelefonoP.IsVisible = true;
            ApellidoP.IsVisible = true;

        }

        private async void GuadraEditar_Clicked(object sender, EventArgs e)
        {
            

            //Guardar Cambios en Perfil
            var vm = BindingContext as PerfilViewModel;
            
            var r = await DisplayAlert("Alerta", "¿Desea Guardar los Cambios Realizados?", "Aceptar", "Cancelar");
            if (r == true)
            {
                
                //Agregar Cambios Por Pantalla
                EditarTelefonoP.IsVisible = false;
                Disponible.IsEnabled = false;
                SelectCargo.IsVisible = false;
                EditarBotones.IsVisible = false;
                botonesImagenes.IsVisible = false;

                //Habilitar los Labels
                CargoP.Text = (string)SelectCargo.SelectedItem;
                CargoP.IsVisible = true;
                PatrullaP.IsVisible = true;
                TelefonoP.Text = EditarTelefonoP.Text;
                TelefonoP.IsVisible = true;

                //Guardar Foto Perfil
                var content = new MultipartFormDataContent();
                string url2 ="";
                string url3 = "";
                if (_mediaFile != null)
                {
                    content.Add(new StreamContent(_mediaFile.GetStream()), "\"file\"", $"{_mediaFile.Path}");
                    var httpClient = new HttpClient();
                    var uploadServiceBaseAddress = "http://localhost:62431/api/Files/Upload";
                    var httpResponseMessage = await httpClient.PostAsync(uploadServiceBaseAddress, content);
                    var url = await httpResponseMessage.Content.ReadAsStringAsync();
                    url2 = RemoveSpecialChars(url);
                    url3 = "http://localhost:62431" + url2;
                }
                
                

                object[] EP = new object[]
                {
                Disponible.IsToggled,
                CargoP.Text,
                TelefonoP.Text,
                url3
                };

                vm.GuardarCambios.Execute(EP);
                await DisplayAlert("Alerta", "Si realizo un cambio en su Cargo, porfavor Cierre la Sesión y vuelva a ingresar", "OK");
            }
            

        }

        public static string RemoveSpecialChars(string str)
        {
            // Create  a string array and add the special characters you want to remove
            string[] chars = new string[] { ",","!", "@", "#", "$", "%", "^", "&", "*", "'", "\"", ";", ":", "|", "[", "]" };
            //Iterate the number of times based on the String array length.
            for (int i = 0; i < chars.Length; i++)
            {
                if (str.Contains(chars[i]))
                {
                    str = str.Replace(chars[i], "");
                }
            }
            return str;
        }



        private async void TakePic_Clicked(object sender, EventArgs e)
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DisplayAlert("Disponibilidad de Camera:", "No se pudo acceder a la Camara", "OK");
                return;
            }

            _mediaFile = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                    Directory="CampLogic",
                    AllowCropping = true,
                    SaveToAlbum = true 
            });


            if (_mediaFile == null)
                return;

            //PathLabel.Text = file.AlbumPath; Sirve referencia de obtener el path donde se encuentra la foto


            ImagenPerfil.Source = ImageSource.FromStream(() =>
            {
                return _mediaFile.GetStream();
            });
            //_mediaFile.Dispose();
        }

        private async void PickPic_Clicked(object sender, EventArgs e)
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await DisplayAlert("Error:", "No se puede seleccionar imagenes", "OK");
                return;
            }

            _mediaFile = await CrossMedia.Current.PickPhotoAsync();

            if (_mediaFile == null)
            {
                return;
            }

            var text = _mediaFile.Path;
            
            ImagenPerfil.Source = ImageSource.FromStream(()=> 
            {
                return _mediaFile.GetStream();
            });
                
            //_mediaFile.Dispose();
        }
    }
}
