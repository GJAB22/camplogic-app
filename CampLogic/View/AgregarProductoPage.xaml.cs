﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AgregarProductoPage : PopupPage
    {
        public AgregarProductoPage()
        {
            InitializeComponent();
            BindingContext = new ProductosViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // Method for animation child in PopupPage
        // Invoced after custom animation end
        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        protected override bool OnBackButtonPressed()
        {
            // Prevent hide popup
            //return base.OnBackButtonPressed();
            return true;
        }

        // Invoced when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return default value - CloseWhenBackgroundIsClicked
            return base.OnBackgroundClicked();
        }


        private async void AgregarProducto_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        private async void CategoriaPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var categoria = (Categoria)CategoriaPicker.SelectedItem;
            var vm = new ProductosViewModel();
            SubCategoriaPicker.ItemsSource = await vm.SearchSubCategoria2(categoria.Id);
            SubCategoriaPicker.IsEnabled = true;
            NuevoProducto.IsEnabled = true;
        }

        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }

        

        private async void NuevaProducto_Clicked(object sender, EventArgs e)
        {
            var vm = new ProductosViewModel();

            if (CategoriaPicker.SelectedItem == null)
            {
                await DisplayAlert("Error", "Por favor Seleccione una Categoria", "Ok");
            }
            else if(SubCategoriaPicker.SelectedItem == null)
            {
                await DisplayAlert("Error", "Por favor Seleccione una Sub-Categoria", "Ok");
            }
            else if (ArticuloField.Text == null || ArticuloField.Text == "")
            {
                await DisplayAlert("Error", "Por favor Ingrese el Nombre del Articulo", "Ok");
            }
            else if (CantidadActField.Text == null || CantidadActField.Text == "")
            {
                await DisplayAlert("Error", "Por favor Ingrese La Cantidad Actual", "Ok");
            }
            else if (CantidadIdField.Text == null || CantidadIdField.Text == "")
            {
                await DisplayAlert("Error", "Por favor Ingrese La Cantidad Ideal", "Ok");
            }
            else if (PRField.Text == null || PRField.Text == "")
            {
                await DisplayAlert("Error", "Por favor Ingrese El Punto Crítico", "Ok");
            }
            else if (Ubicacion.SelectedItem == null)
            {
                await DisplayAlert("Error", "Por favor Seleccione una Ubicación", "Ok");
            }
            else if (Proveedor.SelectedItem == null)
            {
                await DisplayAlert("Error", "Por favor Seleccione un Proveedor", "Ok");
            }
            else if (CostoUnitario.Text == null || CostoUnitario.Text == "")
            {
                await DisplayAlert("Error", "Por favor Ingrese El Costo Unitario", "Ok");
            }
            else
            {
                var subcategoria = (SubCategoria)SubCategoriaPicker.SelectedItem;
                var proveedor = (Proveedor)Proveedor.SelectedItem;
                var ubicacion = (Ubicacion)Ubicacion.SelectedItem;
                object[] nuevoprod = new object[] 
                {
                    
                    ArticuloField.Text,
                    DescripcionField.Text,
                    subcategoria,
                    Convert.ToInt32(CantidadActField.Text),
                    Convert.ToInt32(CantidadIdField.Text),
                    Convert.ToInt32(PRField.Text),
                    ubicacion,
                    proveedor,
                    Convert.ToDouble(CostoUnitario.Text),
                    CostoDate.Date
                };

                vm.NProducto.Execute(nuevoprod);
                await DisplayAlert("Exito", "El Nuevo Producto se a registrado exitosamente", "Ok");
            }

        }
    }
}
