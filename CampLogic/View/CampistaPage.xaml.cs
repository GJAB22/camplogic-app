﻿using CampLogic.Helpers;
using CampLogic.Model;
using CampLogic.View;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CampistaPage : ContentPage
    {
        

        public CampistaPage()
        {
            InitializeComponent();
            CampistaC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            BindingContext = new CampistaViewModel();
            if (Settings.Cargo == "Guía" || Settings.Cargo == "Inventario")
            {
                ToolbarItems.Remove(AgregarCampista);
            }
        }

        private async void SearchCampista_TextChanged(object sender, TextChangedEventArgs e)
        {
            var keyword = SearchCampista.Text;
            var vm = BindingContext as CampistaViewModel;
            
            CampistasListView.ItemsSource = await vm.SearchCampista(keyword);
           
        }

        private async void CampistasListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var campista = (Campistas)e.SelectedItem;
            if (campista!=null)
            {
                await Navigation.PushPopupAsync(new PerfilCampistaPage(campista));
            }

            CampistasListView.SelectedItem = null;

        }

        private async void AgregarCampista_Activated(object sender, EventArgs e)
        {
            await Navigation.PushPopupAsync(new AgregarCampistaPage());
        }

        private async void CampRefresh_Activated(object sender, EventArgs e)
        {
            var vm = BindingContext as CampistaViewModel;
            CampistasListView.ItemsSource = await vm.CampistaList();
        }

        private async void FP_SelectedIndexChanged(object sender, EventArgs e)
        {
            var keyword = (string)FP.SelectedItem;
            var vm = BindingContext as CampistaViewModel;
            if (keyword == "Todas")
            {
                CampistasListView.ItemsSource = await vm.CampistaList();
            }
            else
            {
                CampistasListView.ItemsSource = await vm.FiltrarPorPatrulla(keyword);
            }
            
        }

        private async void FI_SelectedIndexChanged(object sender, EventArgs e)
        {
            var keyword = (string)FI.SelectedItem;
            var vm = BindingContext as CampistaViewModel;
            if (keyword == "Todos")
            {
                CampistasListView.ItemsSource = await vm.CampistaList();
            }
            else
            {
                CampistasListView.ItemsSource = await vm.FiltrarPorInscrito(keyword);
            }
            
        }
    }
}
