﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EditarProducto : PopupPage
    {
        public string CantDV { get; set; }
        public string CostoV { get; set; }
        public EditarProducto (string PordId, string articulo, string descripcion, string costo, string ca, string ub, string prov ,string CI)
		{
			InitializeComponent ();
            ProdID.Text = PordId;
            Titulo.Text = articulo;
            DescripcionField.Text = descripcion;
            NuevoCosto.Text = costo;
            NuevaCantidad.Text = ca;
            NuevaCantidadIdeal.Text = CI;
            CantDV = ca;
            CostoV = costo;
            BindingContext = new ProductosViewModel(ub, prov);

		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // Method for animation child in PopupPage
        // Invoced after custom animation end
        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        protected override bool OnBackButtonPressed()
        {
            // Prevent hide popup
            //return base.OnBackButtonPressed();
            return true;
        }

        // Invoced when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return default value - CloseWhenBackgroundIsClicked
            return base.OnBackgroundClicked();
        }

        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }

        private async void GC_Clicked(object sender, EventArgs e)
        {
            var resp = await DisplayAlert("Confirmar", "Desea Guardar estos Cambios?", "Aceptar", "Cancelar");
            var vm = new ProductosViewModel();
            var proveedor = (Proveedor)NuevoProveedor.SelectedItem;
            var ubicacion = (Ubicacion)NuevaUbicacion.SelectedItem;
            if (resp == false)
            {
                await Navigation.PopPopupAsync();
            }
            else
            {
                object[] NP = new object[]
                {
                    Convert.ToInt32(ProdID.Text),
                    Convert.ToInt32(NuevaCantidad.Text),
                    Convert.ToInt32(NuevaCantidadIdeal.Text),
                    proveedor,
                    ubicacion,
                    Convert.ToDouble(NuevoCosto.Text),
                    Convert.ToInt32(CantDV),
                    Convert.ToDouble(CostoV)

                };

                vm.EditarProducto.Execute(NP);
                await DisplayAlert("Exito", "Se han Guardado Los Cambios realizados", "Aceptar", "Cancelar");
                await Navigation.PopAllPopupAsync();
            }
        }

        private async void CancelarB_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }
    }
}