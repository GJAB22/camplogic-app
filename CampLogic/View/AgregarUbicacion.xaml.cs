﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AgregarUbicacion : PopupPage
    {
		public AgregarUbicacion ()
		{
			InitializeComponent ();
		}
        // Method for animation child in PopupPage
        // Invoced after custom animation end
        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        protected override bool OnBackButtonPressed()
        {
            // Prevent hide popup
            //return base.OnBackButtonPressed();
            return true;
        }

        // Invoced when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return default value - CloseWhenBackgroundIsClicked
            return base.OnBackgroundClicked();
        }


        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }

        private async void NuevaUbicacion_Clicked(object sender, EventArgs e)
        {
            var vm = new ProductosViewModel();
            var ublist = await vm.UbicacionesList();
            var nuevaubicacion = new Ubicacion()
            {
                Nombre = NombreUbicacion.Text
            };
            if (NombreUbicacion.Text == null || NombreUbicacion.Text == "")
            {
                await DisplayAlert("Alerta", "Porfavor ingrese un Nombre", "Ok");
            }
            else if (ublist.Exists(j=>j.Nombre == nuevaubicacion.Nombre) == true)
            {
                await DisplayAlert("Alerta", "Esta Ubicación ya esta registrada, porfavor ingrese una nueva.", "Ok");
            }
            else
            {
                var resp = await vm.NuevaUbicacion(nuevaubicacion);
                if (resp == true)
                {
                    await DisplayAlert("Exito", "La Nueva Ubicación se ha guardado exitosamente", "Ok");
                }
                else
                {
                    await DisplayAlert("Error!", "La Nueva Ubicación no se ha registardo", "Ok");
                }
                
            }
        }

        private void NombreUbicacion_TextChanged(object sender, TextChangedEventArgs e)
        {
            NuevaUbicacion.IsEnabled = true;
        }
    }
}