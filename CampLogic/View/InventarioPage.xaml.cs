﻿using CampLogic.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InventarioPage : TabbedPage
    {
        public InventarioPage()
        {
            

            Children.Add(new ProductoPage());
            Children.Add(new HorarioGeneralPage());
            Children.Add(new Prestamos());
            Children.Add(new OtrosInventarioPage());
            
            InitializeComponent();
            BarBackgroundColor = Color.FromHex("#ff8a50");
            

        }
    }
}
