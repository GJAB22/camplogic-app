﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductoPage : ContentPage
    {
        public ProductoPage()
        {
            InitializeComponent();
            ProductosC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            BindingContext = new ProductosViewModel();
        }

        private async void AgregarProducto_Activated(object sender, EventArgs e)
        {
            await Navigation.PushPopupAsync(new AgregarProductoPage());
        }

        private async void SearchProductos_TextChanged(object sender, TextChangedEventArgs e)
        {
            var keyword = SearchProductos.Text;
            var categoria = (string)CategoriaPicker.SelectedItem;
            var subcategoria = (SubCategoria)SubCategoriaPicker.SelectedItem;
            var ubicacion = (string)UbicacionPicker.SelectedItem;
            var vm = new ProductosViewModel();

            if ((categoria == null || categoria == "Todas") && subcategoria == null && (ubicacion == null || ubicacion == "Todas"))
            {
                ProductosListView.ItemsSource = await vm.SearchProducto(keyword);
                
            }
            else if(subcategoria == null && (ubicacion == null || ubicacion == "Todas"))
            {
                ProductosListView.ItemsSource = await vm.SearchProductoCat(keyword, categoria);
            }
            else if (ubicacion == null || ubicacion == "Todas")
            {
                ProductosListView.ItemsSource = await vm.SearchProductoSubCat(keyword, subcategoria.Nombre);
            }
            else if ((categoria == null || categoria == "Todas") && subcategoria == null)
            {
                ProductosListView.ItemsSource = await vm.SearchUbicacion(ubicacion,keyword);
            }
            else if (subcategoria == null)
            {
                ProductosListView.ItemsSource = await vm.SearchProductoCatUb(keyword, categoria, ubicacion);
            }
            else
            {
                ProductosListView.ItemsSource = await vm.SearchProductoSubCatUb(keyword, subcategoria.Nombre, ubicacion);
            }
            
            

        }

        private async void ProductosListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var producto = (Producto)e.SelectedItem;
            if (producto != null)
            {
                await Navigation.PushPopupAsync(new DetallesProductoPage(producto));
            }

            ProductosListView.SelectedItem = null;
        }
        
        private void Refresh_Activated(object sender, EventArgs e)
        {
            BindingContext = new ProductosViewModel();
        }
        

        private async void Orden_Activated(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CestaOrdenes() {
                
                BarTextColor = Color.White
            });
        }

        private async void AgregarCategoria_Activated(object sender, EventArgs e)
        {
            await Navigation.PushPopupAsync(new AgregarCategoria());
        }

        private async void AgregarSubCategoria_Activated(object sender, EventArgs e)
        {
            await Navigation.PushPopupAsync(new AgregarSubCategoria());
        }

        private async void Proveedores_Activated(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ProveedorPage());
        }

        private async void CategoriaPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var categoria = (string)CategoriaPicker.SelectedItem;
            var vm = new ProductosViewModel();
            if (categoria != null)
            {
                if (categoria == "Todas")
                {
                    ProductosListView.ItemsSource = await vm.LProducto();
                    SubCategoriaPicker.IsEnabled = false;
                }
                else
                {
                    ProductosListView.ItemsSource = await vm.FilterCategoria(categoria);
                    SubCategoriaPicker.ItemsSource = await vm.SearchSubCategoria(categoria);
                    SubCategoriaPicker.IsEnabled = true;
                }
                
            }

            SubCategoriaPicker.SelectedItem = null;
            UbicacionPicker.SelectedItem = null;
            
        }

        private async void SubCategoriaPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var subcategoria = (SubCategoria)SubCategoriaPicker.SelectedItem;
            var vm = new ProductosViewModel();
            if(subcategoria != null)
            {
                ProductosListView.ItemsSource = await vm.Filter(subcategoria.Id);
            }
            
        }

        private async  void NuevaEntrada_Activated(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new IngresosAlmacen());
        }

        private async void NuevaUbicacion_Activated(object sender, EventArgs e)
        {
            await Navigation.PushPopupAsync(new AgregarUbicacion());
        }

        private async void UbicacionPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var ubicacion = (string)UbicacionPicker.SelectedItem;
            var categoria = (string)CategoriaPicker.SelectedItem;
            var vm = new ProductosViewModel();
            if (ubicacion != null)
            {
                if (ubicacion == "Todas" && (categoria == "Todas" || categoria == null))
                {
                    ProductosListView.ItemsSource = await vm.LProducto();
                }
                else if(categoria == "Todas" || categoria == null)
                {
                    ProductosListView.ItemsSource = await vm.FilterUbicacion(ubicacion);

                }
                else if(ubicacion == "Todas" || ubicacion == null)
                {
                    ProductosListView.ItemsSource = await vm.FilterCategoria(categoria);
                }
                else
                {
                    ProductosListView.ItemsSource = await vm.FilterUbCat(ubicacion, categoria);
                }

            }
        }
    }
}
