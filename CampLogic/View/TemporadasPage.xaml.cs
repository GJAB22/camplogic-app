﻿using CampLogic.Model;
using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TemporadasPage : ContentPage
    {
        public object BC { get { return BindingContext; } }
        public TemporadasPage()
        {
            InitializeComponent();
            TemporadaC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            BindingContext = new TemporadaViewModel();
        }

        private void Refresh_Activated(object sender, EventArgs e)
        {
            BindingContext = new TemporadaViewModel();
        }

        private void SearchTemporada_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TemporadasListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            TemporadasListView.SelectedItem = null;
        }

        private async void Add_Activated(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AgregarTemporada(BC));
        }

        private async void TemporadasActual_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var ut = (Temporadas)TemporadasActual.SelectedItem;
            if (ut != null)
            {
                await Navigation.PushAsync(new UltimaTemporada(ut) { BarTextColor = Color.White});
            }
            TemporadasActual.SelectedItem = null;
        }
    }
}