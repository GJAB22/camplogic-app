﻿using CampLogic.Model;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetallesOrdenPage : PopupPage
    {
        
        public DetallesOrdenPage(Orden_Compra orden)
        {
            InitializeComponent();

            BindingContext = new OrdenCompraViewModel(orden);

            TituloOC.Text = "Orden de Compra #" + orden.Id;

            
            OrdenCantidad.Text = orden.Cantidad_Orden.ToString();
            CostoTotal.Text = orden.Costo_Total.ToString("0,0.00");
            FechaOrden.Text = orden.Fecha_Orden.Date.ToString("dd/MM/yyyy");

            ProvNombre.Text = orden.Proveedor.Nombre;
            //ProvApellido.Text = orden.Proveedor.Apellido;
            //ProvDes.Text = orden.Proveedor.Descripcion;
            ProvCorreo.Text = orden.Proveedor.Correo;
            ProvT1.Text = orden.Proveedor.Telefono_1;
            ProvT2.Text = orden.Proveedor.Telefono_2;

            OrdenEmpNombre.Text = orden.Empleado.Nombre;
            OrdenEmpApellido.Text = orden.Empleado.Apellido;
            CorreoOrdenEmp.Text = orden.Empleado.Correo;
            TelefonoOrdenEmp.Text = orden.Empleado.Telefono;
            
        }

        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.9);
        }

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        protected override bool OnBackgroundClicked()
        {
            // Return default value - CloseWhenBackgroundIsClicked
            return base.OnBackgroundClicked();
        }


        private async void EliminarPopUp_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }
    }
}
