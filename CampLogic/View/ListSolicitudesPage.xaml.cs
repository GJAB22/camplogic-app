﻿using CampLogic.Model;
using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListSolicitudesPage : ContentPage
	{
		public ListSolicitudesPage ()
		{
			InitializeComponent ();

            

            SolicitudesListas.IsVisible = false;
		}

        private void filtrosol_Clicked(object sender, EventArgs e)
        {
            filtroSolicitud.IsVisible = false;
            UltimasSolicitudes.IsVisible = false;

            fInicio.Text = FechaInicial.Date.ToString("dd/MM/yyyy");
            fFin.Text = FechaFinal.Date.ToString("dd/MM/yyyy");
            SolicitudesListas.IsVisible = true;

            
        }

        private async void SolicitudesListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
           

            SolicitudesListView.SelectedItem = null;
        }
    }
}