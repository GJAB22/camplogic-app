﻿using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RotacionInvPage : ContentPage
    {
        public RotacionInvPage(DateTime FI, DateTime FF,string categoria, string subcategoria, string articulo, string ubicacion)
        {
            InitializeComponent();

            BindingContext = new RotacionViewModel(FI, FF, categoria, subcategoria, articulo, ubicacion);

            FInicial.Text = FI.Date.ToString("dd/MM/yyy");
            FFinal.Text = FF.Date.ToString("dd/MM/yyyy");
            RepRotacionC.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
        }

        private void RepRList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            RepRList.SelectedItem = null;
        }
    }
}