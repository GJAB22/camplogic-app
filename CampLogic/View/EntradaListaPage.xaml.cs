﻿using CampLogic.Model;
using CampLogic.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EntradaListaPage : ContentPage
    {
        public EntradaListaPage()
        {
            InitializeComponent();

            BindingContext = new EntradasViewModel();

            
            EntradasListas.IsVisible = false;
        }

        private async void EntradasListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var entrada = (Entrada)e.SelectedItem;
            if (entrada!= null)
            {
                await Navigation.PushAsync(new DetallesEntradaPage(entrada));
            }

            EntradasListView.SelectedItem = null;
        }

        private void SearchEntrada_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        

        private void filtro_Clicked(object sender, EventArgs e)
        {
            filtroEntrada.IsVisible = false;
            ultimasEntradas.IsVisible = false;
            fInicio.Text = FechaInicial.Date.ToString("dd/MM/yyyy");
            fFin.Text = FechaFinal.Date.ToString("dd/MM/yyyy");
            EntradasListas.IsVisible = true;

            BindingContext = new EntradasViewModel(FechaInicial.Date, FechaFinal.Date);
        }
    }
}
