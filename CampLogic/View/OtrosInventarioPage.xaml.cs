﻿using CampLogic.HelpClasses;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CampLogic.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class OtrosInventarioPage : ContentPage
	{
		public OtrosInventarioPage ()
		{
			InitializeComponent ();
            ReporteSL.BackgroundColor = Color.FromRgba(225, 225, 225, 0.7);
            OrdenCompra.IsVisible = false;
            RotacionC.IsVisible = false;
            FaltanteC.IsVisible = false;
            SalidasC.IsVisible = false;
            InventarioB.TextColor = Color.Black;
            BindingContext = new ReportesViewModel();

            
		}
        
        private void InformesList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            //var informe = (Informes)e.SelectedItem;

            //if (informe != null)
            //{
            //    if (informe.Nombre == "Valuación del Inventario")
            //    {
                    
            //        await Navigation.PushAsync(new ValInvPage());
            //    }
            //}

            //InformesList.SelectedItem = null;
        }

        
        private void Inventario_Clicked(object sender, EventArgs e)
        {
            OrdenesCompra.TextColor = Color.White;
            Rotacion.TextColor = Color.White;
            Faltante.TextColor = Color.White;
            Salidas.TextColor = Color.White;
            InventarioB.TextColor = Color.Black;

            OrdenCompra.IsVisible = false;
            RotacionC.IsVisible = false;
            FaltanteC.IsVisible = false;
            SalidasC.IsVisible = false;
            InventarioC.IsVisible = true;
        }

        private void OrdenesCompra_Clicked(object sender, EventArgs e)
        {
            InventarioB.TextColor = Color.White;
            Rotacion.TextColor = Color.White;
            Faltante.TextColor = Color.White;
            Salidas.TextColor = Color.White;
            OrdenesCompra.TextColor = Color.Black;

            NuevoReporte.IsEnabled = false;
            InventarioC.IsVisible = false;
            RotacionC.IsVisible = false;
            FaltanteC.IsVisible = false;
            SalidasC.IsVisible = false;
            OrdenCompra.IsVisible = true;
            
        }
        

        private void Rotacion_Clicked(object sender, EventArgs e)
        {
            InventarioB.TextColor = Color.White;
            OrdenesCompra.TextColor = Color.White;
            Faltante.TextColor = Color.White;
            Salidas.TextColor = Color.White;
            Rotacion.TextColor = Color.Black;

            NuevoReporte.IsEnabled = false;
            InventarioC.IsVisible = false;
            OrdenCompra.IsVisible = false;
            FaltanteC.IsVisible = false;
            SalidasC.IsVisible = false;
            RotacionC.IsVisible = true;
        }

        private void Faltante_Clicked(object sender, EventArgs e)
        {
            InventarioB.TextColor = Color.White;
            OrdenesCompra.TextColor = Color.White;
            Rotacion.TextColor = Color.White;
            Salidas.TextColor = Color.White;
            Faltante.TextColor = Color.Black;

            NuevoReporte.IsEnabled = false;
            InventarioC.IsVisible = false;
            OrdenCompra.IsVisible = false;
            RotacionC.IsVisible = false;
            SalidasC.IsVisible = false;
            FaltanteC.IsVisible = true;
        }

        private void Salidas_Clicked(object sender, EventArgs e)
        {
            InventarioB.TextColor = Color.White;
            OrdenesCompra.TextColor = Color.White;
            Rotacion.TextColor = Color.White;
            Faltante.TextColor = Color.White;
            Salidas.TextColor = Color.Black;

            NuevoReporte.IsEnabled = false;
            InventarioC.IsVisible = false;
            OrdenCompra.IsVisible = false;
            RotacionC.IsVisible = false;
            FaltanteC.IsVisible = false;
            SalidasC.IsVisible = true;
            
        }

        //INVENTARIO
        private async void InventarioCatPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var vm = new ReportesViewModel();
            var categoria = (string)InventarioCatPicker.SelectedItem;
            if (categoria == "Todas")
            {
                InventarioSubCatPicker.IsEnabled = false;
                InventarioSubCatPicker.SelectedItem = null;
                InventarioArticuloPicker.ItemsSource = await vm.ProdsList();

            }
            else
            {
                InventarioSubCatPicker.ItemsSource = await vm.SubCategoriaList(categoria);
                InventarioSubCatPicker.ItemDisplayBinding = new Binding(".");
                InventarioSubCatPicker.IsEnabled = true;

                InventarioArticuloPicker.ItemsSource = await vm.ProdsFiltraCatList(categoria);
            }

            NuevoReporte.IsEnabled = true;
        }

        private async void InventarioSubCatPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var vm = new ReportesViewModel();
            var categoria = (string)InventarioCatPicker.SelectedItem;
            var subcategoria = (string)InventarioSubCatPicker.SelectedItem;
            if (subcategoria == "Ninguna")
            {
                InventarioArticuloPicker.ItemsSource = await vm.ProdsFiltraCatList(categoria);
            }
            else
            {
                InventarioArticuloPicker.ItemsSource = await vm.ProdsFiltraSubList(subcategoria);
            }
        }

        private void InventarioArticuloPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            NuevoReporte.IsEnabled = true;
        }

        private async void InventarioUbicacionPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var vm = new ReportesViewModel();
            var ubicacion = (string)InventarioUbicacionPicker.SelectedItem;
            if(ubicacion == "Todas")
            {
                InventarioArticuloPicker.ItemsSource = await vm.ProdsList();
            }
            else
            {
                InventarioArticuloPicker.ItemsSource = await vm.ProdsFiltraUbList(ubicacion);
            }
            
            NuevoReporte.IsEnabled = true;
        }

        //ORDENES DE COMPRA
        private async void TOrdenes_SelectedIndexChanged(object sender, EventArgs e)
        {
            var vm = new ReportesViewModel();
            NuevoReporte.IsEnabled = true;
            TercerFOC.ItemsSource = await vm.ProveedorList();
            TercerFOC.ItemDisplayBinding = new Binding(".");
            TercerFOC.IsEnabled = true;
        }

        private void SegundoFOC_SelectedIndexChanged(object sender, EventArgs e)
        {
            //var vm = new ReportesViewModel();
            //var SI = (string)SegundoFOC.SelectedItem;
            //if (SI == "Todas")
            //{
            //    TercerFOC.IsEnabled = false;
            //}
            //else if (SI == "Artículo")
            //{
                
            //    TercerFOC.ItemsSource = await vm.ProdsList();
            //    TercerFOC.ItemDisplayBinding = new Binding(".");
            //    TercerFOC.IsEnabled = true;
            //}
            //else
            //{
            //    TercerFOC.Title = "Seleccione el " + SI;
            //    TercerFOC.ItemsSource = await vm.ProveedorList();
            //    TercerFOC.ItemDisplayBinding = new Binding(".");
            //    TercerFOC.IsEnabled = true;
            //}
        }

        //FALTANTES
        private async void FaltanteCatPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var vm = new ReportesViewModel();
            var categoria = (string)FaltanteCatPicker.SelectedItem;
            if(categoria == "Todas")
            {
                FaltanteSubCatPicker.IsEnabled = false;
                FaltanteSubCatPicker.SelectedItem = null;
                FaltanteArticuloPicker.ItemsSource = await vm.ProdsList();
                
            }
            else
            {
                FaltanteSubCatPicker.ItemsSource = await vm.SubCategoriaList(categoria);
                FaltanteSubCatPicker.ItemDisplayBinding = new Binding(".");
                FaltanteSubCatPicker.IsEnabled = true;

                FaltanteArticuloPicker.ItemsSource = await vm.ProdsFiltraCatList(categoria);
            }

            NuevoReporte.IsEnabled = true;
        }

        private async void FaltanteSubCatPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var vm = new ReportesViewModel();
            var categoria = (string)FaltanteCatPicker.SelectedItem;
            var subcategoria = (string)FaltanteSubCatPicker.SelectedItem;
            if(subcategoria == "Ninguna")
            {
                FaltanteArticuloPicker.ItemsSource = await vm.ProdsFiltraCatList(categoria);
            }
            else
            {
                FaltanteArticuloPicker.ItemsSource = await vm.ProdsFiltraSubList(subcategoria);
            }
        }

        private void FaltanteArticuloPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            NuevoReporte.IsEnabled = true;
        }

        private async void FaltanteUbicacionPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var vm = new ReportesViewModel();
            var ubicacion = (string)FaltanteUbicacionPicker.SelectedItem;
            if (ubicacion == "Todas")
            {
                FaltanteArticuloPicker.ItemsSource = await vm.ProdsList();
            }
            else
            {
                FaltanteArticuloPicker.ItemsSource = await vm.ProdsFiltraUbList(ubicacion);
            }

            NuevoReporte.IsEnabled = true;

        }

        //ROTACION
        private async void RotacionCatPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var vm = new ReportesViewModel();
            var categoria = (string)RotacionCatPicker.SelectedItem;
            if (categoria == "Todas")
            {
                RotacionSubCatPicker.IsEnabled = false;
                RotacionSubCatPicker.SelectedItem = null;
                RotacionArticuloPicker.ItemsSource = await vm.ProdsList();

            }
            else
            {
                RotacionSubCatPicker.ItemsSource = await vm.SubCategoriaList(categoria);
                RotacionSubCatPicker.ItemDisplayBinding = new Binding(".");
                RotacionSubCatPicker.IsEnabled = true;

                RotacionArticuloPicker.ItemsSource = await vm.ProdsFiltraCatList(categoria);
            }

            NuevoReporte.IsEnabled = true;
        }

        private async void RotacionSubCatPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var vm = new ReportesViewModel();
            var categoria = (string)RotacionCatPicker.SelectedItem;
            var subcategoria = (string)RotacionSubCatPicker.SelectedItem;
            if (subcategoria == "Ninguna")
            {
                RotacionArticuloPicker.ItemsSource = await vm.ProdsFiltraCatList(categoria);
            }
            else
            {
                RotacionArticuloPicker.ItemsSource = await vm.ProdsFiltraSubList(subcategoria);
            }
        }

        private void RotacionArticuloPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            NuevoReporte.IsEnabled = true;
        }

        private async void RotacionUbicacionPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var vm = new ReportesViewModel();
            var ubicacion = (string)FaltanteUbicacionPicker.SelectedItem;
            if (ubicacion == "Todas")
            {
                FaltanteArticuloPicker.ItemsSource = await vm.ProdsList();
            }
            else
            {
                FaltanteArticuloPicker.ItemsSource = await vm.ProdsFiltraUbList(ubicacion);
            }
            NuevoReporte.IsEnabled = true;
        }

        //SALIDAS
        private async void SalidasCatPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var vm = new ReportesViewModel();
            var categoria = (string)SalidasCatPicker.SelectedItem;
            if (categoria == "Todas")
            {
                SalidasSubCatPicker.IsEnabled = false;
                SalidasSubCatPicker.SelectedItem = null;
                SalidasArticuloPicker.ItemsSource = await vm.ProdsList();

            }
            else
            {
                SalidasSubCatPicker.ItemsSource = await vm.SubCategoriaList(categoria);
                SalidasSubCatPicker.ItemDisplayBinding = new Binding(".");
                SalidasSubCatPicker.IsEnabled = true;

                SalidasArticuloPicker.ItemsSource = await vm.ProdsFiltraCatList(categoria);
            }

            NuevoReporte.IsEnabled = true;
        }

        private async void SalidasSubCatPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var vm = new ReportesViewModel();
            var categoria = (string)SalidasCatPicker.SelectedItem;
            var subcategoria = (string)SalidasSubCatPicker.SelectedItem;
            if (subcategoria == "Ninguna")
            {
                SalidasArticuloPicker.ItemsSource = await vm.ProdsFiltraCatList(categoria);
            }
            else
            {
                SalidasArticuloPicker.ItemsSource = await vm.ProdsFiltraSubList(subcategoria);
            }
        }

        private void SalidasArticuloPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            NuevoReporte.IsEnabled = true;
        }

        private async void SalidasUbicacionPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var vm = new ReportesViewModel();
            var ubicacion = (string)FaltanteUbicacionPicker.SelectedItem;
            if (ubicacion == "Todas")
            {
                FaltanteArticuloPicker.ItemsSource = await vm.ProdsList();
            }
            else
            {
                FaltanteArticuloPicker.ItemsSource = await vm.ProdsFiltraUbList(ubicacion);
            }
            NuevoReporte.IsEnabled = true;
        }

        //GENERAR REPORTE
        private async void NuevoReporte_Clicked(object sender, EventArgs e)
        {
            if (InventarioC.IsVisible ==  true)
            {
                string categoria = (string)InventarioCatPicker.SelectedItem;
                if (categoria == null)
                {
                    categoria = "NT";
                }
                string subcategoria = (string)InventarioSubCatPicker.SelectedItem;
                if (subcategoria == null)
                {
                    subcategoria = "NT";
                }
                string articulo = (string)InventarioArticuloPicker.SelectedItem;
                if (articulo == null)
                {
                    articulo = "NT";
                }
                string ubicacion = (string)InventarioUbicacionPicker.SelectedItem;
                if (ubicacion == null)
                {
                    ubicacion = "NT";
                }
                string filtro = (string)TercerFiltroInv.SelectedItem;
                if (filtro == null)
                {
                    filtro = "NT";
                }
                await Navigation.PushAsync(new ReportePage(categoria,subcategoria,ubicacion,articulo,filtro));
            }
            else if (OrdenCompra.IsVisible == true)
            {
                var TOC = (string)TOrdenes.SelectedItem;
                if (TOC == null)
                {
                    TOC = "NT";
                }
                var filtrosegundo = (string)TercerFOC.SelectedItem;
                if (filtrosegundo == null)
                {
                    filtrosegundo = "NT";
                }
                await Navigation.PushAsync(new OrdenesPage(FI.Date,FF.Date,TOC,filtrosegundo));
            }
            else if (FaltanteC.IsVisible == true)
            {
                var FI = FaltanteFI.Date;
                var FF = FaltanteFF.Date;
                string categoria = (string)FaltanteCatPicker.SelectedItem;
                if (categoria == null)
                {
                    categoria = "NT";
                }
                string subcategoria = (string)FaltanteSubCatPicker.SelectedItem;
                if (subcategoria == null)
                {
                    subcategoria = "NT";
                }
                string articulo = (string)FaltanteArticuloPicker.SelectedItem;
                if(articulo == null)
                {
                    articulo = "NT";
                }
                string ubicacion = (string)FaltanteUbicacionPicker.SelectedItem;
                if (ubicacion == null)
                {
                    ubicacion = "NT";
                }

                await Navigation.PushAsync(new FaltanteReport(FI,FF,categoria,subcategoria,ubicacion,articulo)); 


            }
            else if(RotacionC.IsVisible == true)
            {
                var FI = RotacionFI.Date;
                var FF = RotacionFF.Date;
                string categoria = (string)RotacionCatPicker.SelectedItem;
                if (categoria == null)
                {
                    categoria = "NT";
                }
                string subcategoria = (string)RotacionSubCatPicker.SelectedItem;
                if (subcategoria == null)
                {
                    subcategoria = "NT";
                }
                string articulo = (string)RotacionArticuloPicker.SelectedItem;
                if (articulo == null)
                {
                    articulo = "NT";
                }
                string ubicacion = (string)RotacionUbicacionPicker.SelectedItem;
                if (ubicacion == null)
                {
                    ubicacion = "NT";
                }

                await Navigation.PushAsync(new RotacionInvPage(FI,FF, categoria, subcategoria,articulo,ubicacion));
            }
            else
            {
                var FI = SalidasFI.Date;
                var FF = SalidasFF.Date;
                string categoria = (string)SalidasCatPicker.SelectedItem;
                if (categoria == null)
                {
                    categoria = "NT";
                }
                string subcategoria = (string)SalidasSubCatPicker.SelectedItem;
                if (subcategoria == null)
                {
                    subcategoria = "NT";
                }
                string articulo = (string)SalidasArticuloPicker.SelectedItem;
                if (articulo == null)
                {
                    articulo = "NT";
                }
                string ubicacion = (string)SalidasUbicacionPicker.SelectedItem;
                if (ubicacion == null)
                {
                    ubicacion = "NT";
                }

                await Navigation.PushAsync(new SalidasPage(FI,FF,categoria,subcategoria,articulo,ubicacion));
            }
        }

        
    }
}