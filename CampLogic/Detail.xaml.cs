﻿using CampLogic.Model;
using CampLogic.View;
using CampLogic.ViewModel;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CampLogic
{
    public partial class Detail : ContentPage
    {
        

        public Detail()
        {
            InitializeComponent();

            BindingContext = new NoticiasViewModel();
            
        }

        

        private void NoticiasListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }

        private async void AgregarNoticia_Activated(object sender, EventArgs e)
        {

            await Navigation.PushPopupAsync(new AgregarNoticia(BindingContext));
        }
        
        
    }
}
