﻿using CampLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.HelpClasses
{
    public class ProductoActividad
    {
        public Producto Producto { get; set; }
        public int  Cantidad { get; set; }
        public bool EsIndividual { get; set; }
    }
}
