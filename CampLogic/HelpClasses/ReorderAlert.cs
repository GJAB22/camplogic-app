﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CampLogic.HelpClasses
{
    class ReorderAlert : IValueConverter
    {
        public object PR { get; set; }
        public Color Mayor { get; set; }
        public Color Igual { get; set; }
        public Color Menor { get; set; }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var disp = (int)value;
            var PReorden = (Label)PR;
            var PRD = int.Parse(PReorden.Text);
            int i = 1; 
            

            if (disp> PRD)
            {
                return Mayor;
            }
            else if (disp == PRD)
            {
                return Igual;
            }
            else
            {
                return Menor;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }


}
