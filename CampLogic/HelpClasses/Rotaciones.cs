﻿using CampLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.HelpClasses
{
    public class Rotaciones
    {
        public Producto Producto { get; set; }
        public int InvInicial { get; set; }
        public int InvFinal { get; set; }
        public int InvPromedio { get; set; }
        public int Salidas { get; set; }
        public double Rotacion { get; set; }
    }
}
