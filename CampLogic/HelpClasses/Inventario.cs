﻿using CampLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.HelpClasses
{
    public class Inventario
    {
        public Producto Producto { get; set; }

        public float Costo { get; set; }

        public float ValorTotal { get; set; }
        
    }
}
