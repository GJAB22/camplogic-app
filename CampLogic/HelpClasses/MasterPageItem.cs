﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CampLogic
{
    class MasterPageItem
    {
        public string Title { get; set; }
        public string IconSource {get; set;}
        public Page TargetType { get; set; }
    }
}
