﻿using CampLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.HelpClasses
{
    public class ValoracionProducto
    {
        public float ValorPromedio { get; set; }

        public float ValorIF { get; set; }

        public Producto Producto { get; set; }
    }
}
