﻿using CampLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.HelpClasses
{
    public class Faltante
    {
        public Producto Producto { get; set; }
        public int CantidadDisponible { get; set; }
        public int CantidadFaltante { get; set; }
        public double PF { get; set; }

    }
}
