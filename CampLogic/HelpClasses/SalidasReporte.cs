﻿using CampLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CampLogic.HelpClasses
{
    public class SalidasReporte
    {
        public Producto Producto { get; set; }
        public int CantidadInicial { get; set; }
        public int Entradas { get; set; }
        public int CantidadTotal { get; set; }
        public int Salidas { get; set; }
        public double PSalida { get; set; }
    }
}
