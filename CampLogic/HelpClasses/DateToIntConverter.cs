﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CampLogic.HelpClasses
{
    public class DateToIntConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime today = DateTime.Today;

            var Birthday = (DateTime)value;

            int age = today.Year - Birthday.Year;

            if(Birthday.Month > today.Month)
            {
                age--;
            }

            return age;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return false;
        }
    }
}
